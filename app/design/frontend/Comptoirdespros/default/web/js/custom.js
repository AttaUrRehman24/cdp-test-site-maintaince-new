require([
    'jquery',
    'slick',
    'bootstrap',
    'custom-plugins',
    'tabjs',
    'attrchange',
    'ellipsis'
], function($){

    $(document).ready(function() {
        $('.one-line').ellipsis({ lines: 1 });
        $('.two-lines').ellipsis({ lines: 2 });
        $('.three-lines').ellipsis({ lines: 3 });
        $('.four-lines').ellipsis({ lines: 4 });
        $('.five-lines').ellipsis({ lines: 5 });
        $('.six-lines').ellipsis({ lines: 6 });
        $('.seven-lines').ellipsis({ lines: 7 });

        window.setInterval(change_complete_height, 200);
        function change_complete_height() {
            var left_height = $("#search_autocomplete .left").height();
            $("#search_autocomplete .right").height(left_height);
        }
        window.setInterval(show_hid_block, 200);
        function show_hid_block() {
            if($('#search_autocomplete .block-categories.no-result').length){
                $('#search_autocomplete .block-suggests').show();
            }
        }

        /*===== Homepage Main sildeShow =====*/
        $('#home_main_slider').slick({
            slidesToScroll: 1,
            arrows: false
        });

        /*===== Homepage Main sildeShow =====*/
        $('.config-slider').slick({
            dots: false,
            infinite: false,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 4,
            draggable: false,
            responsive: [
                {
                    breakpoint: 1199,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        draggable: true
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        draggable: true
                    }
                },
                {
                    breakpoint: 601,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        vertical: true,
                        draggable: true
                    }
                }
            ]
        });

        /*===== Maga menu get Title =====*/
        $('.col-level').on('mouseover', 'a', function(){
            var val = $(this).attr('title');
            $('.dynamic-content .dynamic-title').html(val);
            var parentId = $(this).prev().prop('title');
            var rightFeatured = $('.ms-featured .form-group');
            var bottomFeatured = $('.ms-footer .form-group');
            if(parentId) {
                rightFeatured.removeClass('active');
                bottomFeatured.removeClass('active');
                $('.featured-'+parentId).addClass('active');
            }
        });

        /*=====Smooth scroll short description =====*/
        $('#service_maintenance').click(function(){
            $('html, body').animate({
                scrollTop: $( '#tab-label-garantie_et_sav' ).offset().top
            }, 500);
            $('#shortDesc .items').find('.item').each(function(){
                $(this).removeClass('active');
            });
            // $('[id^="tab-label-product.info.description"]').addClass('active');
            $('#tab-label-garantie_et_sav').trigger( "click" );

            return false;
        });

        if($('body').hasClass('catalog-product-view')) {
            var fotoramaThumbs = $('.fotorama__nav--thumbs').length;
            if(fotoramaThumbs){
                $('.left-sku.overview').css('max-height','230px');
            }
        }

        $('#product-full-desc').click(function(){
            $('html, body').animate({
                scrollTop: $( '#tab-label-product_info_description' ).offset().top
            }, 500);
            $('.product-tabs .items').find('.item').each(function(){
                $(this).removeClass('active');
            });
            $("#tab-label-product_info_description").addClass('active');
            $('#tab-label-product_info_description-title').trigger( "click" );

            return false;
        });
        $('#compare_models').click(function(){
            $('html, body').animate({
                scrollTop: $( '#tab-label-compare_models' ).offset().top
            }, 500);
            $('.product-tabs .items').find('.item').each(function(){
                $(this).removeClass('active');
            });
            $("#tab-label-compare_models").addClass('active');
            $('#tab-label-compare_models-title').trigger( "click" );

            return false;
        });

        $('.add_reviews').click(function(){
            $('html, body').animate({
                scrollTop: $( '#tab-label-reviews' ).offset().top
            }, 500);
            $('#shortDesc .items').find('.item').each(function(){
                $(this).removeClass('active');
            });
            // $('[id^="tab-label-product.info.description"]').addClass('active');
            $('#tab-label-reviews').trigger( "click" );

            return false;
        });


        /*=====Smooth scroll tabs reviews =====*/
        $('.product-reviews-summary>.reviews-actions>a.action').click(function(){
            $('html, body').animate({
                scrollTop: $( '#tab-label-reviews' ).offset().top
            }, 500);
            $('.product-tabs .items').find('.item').each(function(){
                $(this).removeClass('active');
            });
            $("#tab-label-reviews").addClass('active');
            $('#tab-label-reviews-title').trigger( "click" );

            return false;
        });

        $('.reviews-actions>a').click(function(){
            //$('#reviews').css({ 'display': 'block' });
            //$('#tab-label-reviews').addClass('active');
            $('html, body').animate({
                scrollTop: $('#tab-label-reviews').offset().top
            }, 500);
            $('#shortDesc .items').find('.item').each(function(){
                $(this).removeClass('active');
            });
            $('#tab-label-reviews').trigger( "click" );
            return false;
        });

        /*===== Tooltip =====*/
        $('[data-toggle="tooltip"]').tooltip();



        $("#tab-label-modeles").on('click', '#tab-label-modeles-title', function() {
            setTimeout(function(){
                //$('.config-slider').slick('reinit');
                $('.config-slider').slick('resize');
            }, 500);
        });



        $(".footer-top").on('click', '.top-col', function() {
            var id = $(this).data('id').split('_')[1];
            setTimeout(function(){
                var $selectSection = $(".modal-dialog .tabs-opener .fragment"+id);
                $selectSection.click();
            }, 100);
        });
        if(typeof poupModalTarget !== 'undefined') {
            $('.second-banner').attr('data-id', 'fragment_5').attr('data-toggle', 'modal').attr('data-target', '#' + poupModalTarget);
            $('.second-banner').on('click', function () {
                //$('.product_popup_prix').click();
                setTimeout(function () {
                    var $selectSection = $(".modal-dialog .tabs-opener .fragment5");
                    $selectSection.click();
                }, 100);
            });
            $('#' + poupModalTarget).on('mouseover mouseleave mousemove pointermove mouseover mouseout', function(event) {
                event.stopPropagation(); return false;
            });
        }
        // Removed empty p tag in Magento
        setTimeout(function(){
            $('p').each(function() {
                var $this = $(this);
                if($this.html().replace(/\s|&nbsp;/g, '').length == 0)
                    $this.remove();
            });
        }, 1000);



        setTimeout(function(){
            $(".one-step-checkout .control label span:contains('Code postal')").html("Code postal");
            $("#shipping-zip-form .field label span:contains('Code postal')").html("Code postal");
        }, 3000);


        $("#main").on('click', '.payment-method-title label span,.payment-method label span.title', function() {
            setTimeout(function(){  $(".control label span:contains('Code postal')").html("Code postal"); }, 3000);
        });


        // section for header
        // here we have event for clicking on phone icon for mobile version of site
        var container = $(".tel-info-mobile .info-text");
        var icon = $(".tel-info-mobile .icon-phone");
        icon.on('click',function(){
            if(container.is(":hidden")){
                container.show();
            }else {
                container.hide();
            }
        });

        //product page, section with 3 blocks (paiment, prix, fidelite)
        var first_bannerFirstrow = $(".first-banner.mob_ile .first-row");
        var first_bannerHiddenBlock = $(".first-banner.mob_ile .second-row.hidden_block");
        var first_banner = $(".first-banner.mob_ile");

        first_bannerFirstrow.on('click',function() {
            first_bannerHiddenBlock.show();
        });

        var third_bannerFirstrow = $(".third-banner.mob_ile .first-row");
        var third_bannerHiddenBlock = $(".third-banner.mob_ile .second-row.hidden_block");
        var third_banner = $(".third-banner.mob_ile");

        third_bannerFirstrow.on('click',function() {
            third_bannerHiddenBlock.show();
        });

        $('body').on('click',function(e)
        {

            if (!$("a").is(e.target) // if the target of the click isn't a link ...
                && !container.is(e.target) // ... or the container ...
                && !icon.is(e.target) // ... or the icon ...
                && container.has(e.target).length === 0) // ... or a descendant of the container
            {
                container.hide();
            }

            if (!$("a").is(e.target) // if the target of the click isn't a link ...
                && !third_banner.is(e.target) // ... or the third_banner ...
                && !third_bannerHiddenBlock.is(e.target) // ... or the third_bannerHiddenBlock ...
                && third_banner.has(e.target).length === 0) // ... or a descendant of the container
            {
                third_bannerHiddenBlock.hide();
            }

            if (!$("a").is(e.target) // if the target of the click isn't a link ...
                && !first_banner.is(e.target) // ... or the first_banner ...
                && !first_bannerHiddenBlock.is(e.target) // ... or the first_bannerHiddenBlock ...
                && first_banner.has(e.target).length === 0) // ... or a descendant of the container
            {
                first_bannerHiddenBlock.hide();
            }
        });

        $('body').on('keydown',function(e)
        {
            if (( e.keyCode === 27 ) && (container.is(":visible"))) { // ESC
                container.hide();
            }

            if (( e.keyCode === 27 ) && (third_bannerHiddenBlock.is(":visible"))) { // ESC
                third_bannerHiddenBlock.hide();
            }

            if (( e.keyCode === 27 ) && (first_bannerHiddenBlock.is(":visible"))) { // ESC
                first_bannerHiddenBlock.hide();
            }
        });
        // section for header ends


        $('.cart.cart-items-holder').on("click", '.alo_qty_dec', function () {
            var input = $(this).parent().find('input');
            var value = parseInt(input.val());
            if (value) input.val(value - 1);
            if (value === 0) {
                input.val(1);
            }
        });
        $('.cart.cart-items-holder').on("click", '.alo_qty_inc', function () {
            var input = $(this).parent().find('input');
            var value = parseInt(input.val());
            input.val(value + 1);
        });

        replaceReview();
        replaceCaptcha();

    });

    /*===== SameHieght Function =====*/
    $(function(){
        initSameHeight();
        initTabs();
    });


    /*===== Categories page SameHieght Function =====*/
    function initSameHeight() {
        $('ol.items, .item-holder-info').sameHeight({
            elements: 'li.item-holder, .item-photo',
            flexible: true,
            multiLine: true,
            biggestHeight: true
        });

        $('.config-products').sameHeight({
            elements: '.sameheight',
            flexible: true,
            multiLine: true,
            biggestHeight: true
        });
    }

    function isTabletDevice() {
        if (
            navigator.userAgent.match(/Tablet/i) ||
            navigator.userAgent.match(/iPad/i) ||
            navigator.userAgent.match(/Kindle/i) ||
            navigator.userAgent.match(/Playbook/i) ||
            navigator.userAgent.match(/Nexus/i) ||
            navigator.userAgent.match(/Xoom/i) ||
            navigator.userAgent.match(/SM-N900T/i) || //Samsung Note 3
            navigator.userAgent.match(/GT-N7100/i) || //Samsung Note 2
            navigator.userAgent.match(/SAMSUNG-SGH-I717/i) || //Samsung Note
            navigator.userAgent.match(/SM-T330NU/i) //Samsung Tab 4

        ){
            return true;
        }
        return false;
    }

    function DeviceType()
    {
        var device;
        if(/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile|w3c|acs\-|alav|alca|amoi|audi|avan|benq|bird|blac|blaz|brew|cell|cldc|cmd\-|dang|doco|eric|hipt|inno|ipaq|java|jigs|kddi|keji|leno|lg\-c|lg\-d|lg\-g|lge\-|maui|maxo|midp|mits|mmef|mobi|mot\-|moto|mwbp|nec\-|newt|noki|palm|pana|pant|phil|play|port|prox|qwap|sage|sams|sany|sch\-|sec\-|send|seri|sgh\-|shar|sie\-|siem|smal|smar|sony|sph\-|symb|t\-mo|teli|tim\-|tosh|tsm\-|upg1|upsi|vk\-v|voda|wap\-|wapa|wapi|wapp|wapr|webc|winw|winw|xda|xda\-) /i.test(navigator.userAgent))
        {
            if(/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i.test(navigator.userAgent))
            {
                device = 'tablet';
            }
            else {
                device = 'mobile';
            }
        }
        else if(/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i.test(navigator.userAgent))
        {
            device = 'tablet';
        }
        else
        {
            device = 'desktop';
        }

        return device;
    }

    $(window).resize(function () {
        productAccessoriesTab();
        replaceReview();
        replaceCaptcha();
    });

    function productAccessoriesTab() {
        var bodyWidth = $(window).width();
        if(typeof bodyWidth === 'number') {
            if ($('.tab-links > li').length && (bodyWidth <= 767)) {
                $('.tabs .tab-links').addClass('hidden');
            } else {
                $('.tabs .tab-links').removeClass('hidden');
                $('.product-tabs .tab-links').addClass('hidden');
            }
        }
    }

    function replaceReview() {
        var bodyWidth = $(window).width();
        if(typeof bodyWidth === 'number') {
            if ($('.review-brand.clearfix').length && (bodyWidth <= 650)) {
                $('.review-brand.clearfix').insertAfter($('.product-image-holder .top-sku.sku'));
            }
            else if($('.product-image-holder .review-brand.clearfix').length) {
                $('.review-brand.clearfix').insertBefore($('.product-info-holder .element-holder .left-sku.overview'));
            }
        }
    }

    function replaceCaptcha() {
        var bodyWidth = $(window).width();
        if (typeof bodyWidth === 'number'){
            if ($('.fieldset.fieldset-pro:visible') && (bodyWidth <= 767) && ('.fieldset-pro .fieldset.create.info .field.captcha.required').length) {
                $('.fieldset-pro .fieldset.create.info .field.captcha.required').insertAfter($('.fieldset-pro .fieldset.create.account .field.choices.newsletter'));
            }
            else if($('.fieldset.fieldset-pro:visible') && (bodyWidth >= 768) && ('.fieldset-pro .fieldset.create.account .field.captcha.required').length) {
                $('.fieldset-pro .fieldset.create.account .field.captcha.required').insertAfter($('.fieldset-pro .fieldset.create.info .field.taxvat'));
            }
        }
    }

    /*===== Product Details page tabs Function =====*/
    function initTabs() {
        $('#tab-label-related_accessoires').append($('.tab-links').clone().addClass('hidden'));
        var productTabsContainer = $('.product-tabs-container');
        $('.product-tabs > li > a').on('click', function(e) {
            var currentTab = $(this),
                currentAttrValue = currentTab.attr('href'),
                parentLiTab = currentTab.closest('li'),
                requiredTabRole = 'accessory',
                bodyWidth = $(window).width();
            $(currentAttrValue,productTabsContainer).addClass('active').siblings().removeClass('active');
            $('.product-tabs > li').removeClass('active');
            $('#tab-label-related_accessoires-title').toggleClass("symbol");
            parentLiTab.addClass('active');
            if(typeof bodyWidth === 'number') {
                if (parentLiTab.data('role') === requiredTabRole && (bodyWidth <= 650)) {
                    $('.tab-links', parentLiTab).toggleClass('hidden');
                } else {
                    $('.product-tabs .tab-links').addClass('hidden');
                    $('#tab-label-related_accessoires-title').removeClass("symbol");
                }
            } else {
                $('.product-tabs .tab-links').addClass('hidden');
                $('#tab-label-related_accessoires-title').removeClass("symbol");
            }
            e.preventDefault();
        });
        $('.tab-links > li > a').on('click', function(e) {
            var currentAttrValue = $(this).attr('href');
            $('.tabs ' + currentAttrValue).addClass('active').siblings().removeClass('active');
            $('.tab-links > li').removeClass('active');
            $(this).closest('li').addClass('active');
            $('.related-ul').slick('refresh');
            e.preventDefault();
        });

        productAccessoriesTab();
    }

    $(".loadMore a.opener").on('click',function(){
        showRemainingItems('#'+$(this).attr("data-id"));
    });

    function showRemainingItems(selectorname) {
        $(selectorname + " .category-box").removeClass("hide-product-item");
        $(selectorname + " .loadMore" ).hide();
    }

    $("div[id^='tabModal']").on('mouseover mouseleave mousemove pointermove mouseover mouseout', function(event) {
        event.stopPropagation();
        return false;
    });

});


//change url for home page if user click on items "LES BONS PLANS DE LA SEMAINE"
// or promo-offer blocks

function setLocation(url){
    window.location.href = encodeURI(url);
}

function addCommentClick() {
    var dest = jQuery('#review-form-block');
    jQuery('.review-btn').hide();
    jQuery('.review-form').fadeIn("slow");
    jQuery('.close-reviews-form-btn').show();
}

function closeReviewBlock() {
    jQuery('.close-reviews-form-btn').hide();
    jQuery('.review-form').fadeOut("slow");
    jQuery('.review-btn').show();
}

function toggleTitle() {
    var bodyWidth = jQuery(window).width();
    if (typeof bodyWidth === 'number' && (bodyWidth <= 599)) {
        jQuery(".checkout-cart-index #block-lrwuserpoints-summary").toggleClass("visible");
        jQuery(".checkout-cart-index #block-lrwuserpoints .fa.fa-angle-down").toggleClass("transform-angle");
    }
    else if(bodyWidth > 599){
        jQuery(".checkout-cart-index #block-lrwuserpoints-summary").removeClass("visible");
        jQuery(".checkout-cart-index #block-lrwuserpoints .fa.fa-angle-down").removeClass("transform-angle");
    }
}