/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * @api
 */

define([
    'Magento_Tax/js/view/checkout/summary/grand-total',
    'Magento_Checkout/js/model/totals'
], function (Component ,totals) {
    'use strict';

    return Component.extend({
        /**
         * @override
         */
        isDisplayed: function () {
            return true;
        },


        getTTHValue: function () {
            var amount;
            amount = totals.getSegment('tax').value;
            return this.getFormattedPrice(amount);

        },

    });
});
