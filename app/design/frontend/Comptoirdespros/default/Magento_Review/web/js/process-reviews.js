/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery'
], function ($) {
    'use strict';

    var currentReviewPage = 1;
    var pageUrl = null;

    /**
     * @param {String} url
     * @param {*} fromPages
     */
    function processReviews(url, fromPages) {

        if (!pageUrl) {
            pageUrl = url;
        }

        $.ajax({
            url: url,
            cache: true,
            dataType: 'html',
            showLoader: false,
            loaderContext: $('.product.data.items')
        }).done(function (data) {
            if (fromPages == true) {
                $('.review-items').append($('ol.review-items > li', data));
            } else {
                $('#product-review-container').html(data);
            }

            $(window).scroll(function() {
                if ($(window).scrollTop() >= $(document).height() - $(window).height() - 70) {

                    currentReviewPage++;

                    var pageCount = ($('.review-toolbar ul.pages-items li').length / 2) - 1;

                    var listAjaxUrl = pageUrl + '?p='+currentReviewPage;
                    if (currentReviewPage <= pageCount) {
                        processReviews(listAjaxUrl, true);
                    }
                }
            });
        });
    }

    return function (config) {
        var reviewTab = $(config.reviewsTabSelector),
            requiredReviewTabRole = 'tab';

        if (reviewTab.data('role') === requiredReviewTabRole) {
            processReviews(config.productReviewUrl);
        } else if(reviewTab.hasClass('active')){
            processReviews(config.productReviewUrl,false);
        } else {
            reviewTab.one('beforeOpen', function () {
                processReviews(config.productReviewUrl);
            });
        }

        $(function () {
            $('.product-info-main .reviews-actions a').click(function (event) {
                var acnchor;

                event.preventDefault();
                acnchor = $(this).attr('href').replace(/^.*?(#|$)/, '');
                $('.product.data.items [data-role="content"]').each(function (index) { //eslint-disable-line
                    if (this.id == 'reviews') { //eslint-disable-line eqeqeq
                        $('.product.data.items').tabs('activate', index);
                        $('html, body').animate({
                            scrollTop: $('#' + acnchor).offset().top - 50
                        }, 300);
                    }
                });
            });
        });
    };
});
