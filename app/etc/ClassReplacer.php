<?php
class ClassReplacer
{
    public function includeReplacedFiles($src)
    {
        try {
            $replacedFiles = $this->listDir($src, false, true);
            foreach ($replacedFiles as $replacedFile) {
                include_once $src . $replacedFile;
            }
        } catch (Exception $e) {
            return;
        }
    }

    protected function listDir($dir, $prependDir = false, $recursive = false, $entityRegexp = null, $currPath = '')
    {
        if (!is_dir($dir)) {
            return array();
        }
        $currPath = $prependDir ? $dir : $currPath;
        $currPath = $currPath !== '' ? rtrim($currPath, '/') . '/' : '';
        $files = array();
        foreach (scandir($dir) as $file) {
            if (in_array($file, array('.', '..'))) {
                continue;
            }
            $entity = $currPath . $file;
            if ($recursive && is_dir("$dir/$file")) {
                $files = array_merge($files, $this->listDir("$dir/$file", false, true, $entityRegexp, $entity . '/'));
                continue;
            }
            if ($entityRegexp && !preg_match($entityRegexp, $entity)) continue;
            $files[] = $entity;
        }
        return $files;
    }
}
$replace = new ClassReplacer();
$replace->includeReplacedFiles(dirname(__DIR__) . '/code/Magento/');