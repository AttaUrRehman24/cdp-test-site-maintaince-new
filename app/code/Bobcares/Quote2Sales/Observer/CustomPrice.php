<?php

/**
 * Bobcares Quote2Sales CustomPrice Observer
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */

namespace Bobcares\Quote2Sales\Observer;

use Magento\Framework\Event\ObserverInterface;

/**
 * Bobcares Quote2Sales CustomPrice Observer
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */
class CustomPrice implements ObserverInterface {

    /**
     * Function to add custom price as per quote for the products to be added in the cart.
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        $item = $observer->getEvent()->getData('quote_item');
        $item = ( $item->getParentItem() ? $item->getParentItem() : $item );

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->get('\Magento\Customer\Model\Session');
        $price = $customerSession->getPrice();
        $item->setCustomPrice($price);
        $item->setOriginalCustomPrice($price);
        $item->getProduct()->setIsSuperMode(true);
    }

}
