<?php
namespace Bobcares\Quote2Sales\Controller\Index;
 
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
 
class AddProduct extends \Magento\Customer\Controller\AbstractAccount {
    
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    
    /**
     * @var \Magento\Framework\Data\Form\FormKey
     */
    protected $formKey;
    
    /**
     * @var \Magento\Checkout\Model\Cart 
     */
    protected $cart;
        
/**
 * 
 * @param Context $context
 * @param \Magento\Framework\Data\Form\FormKey $formKey
 * @param PageFactory $resultPageFactory
 * @param \Magento\Framework\App\Request\Http $request
 * @param \Magento\Quote\Model\Quote $quoteFactory
 * @param \Magento\Checkout\Model\Cart $cart
 * @param \Magento\Catalog\Model\ProductRepository $productRepository
 */
    public function __construct(
        Context $context,
        \Magento\Framework\Data\Form\FormKey $formKey,
        PageFactory $resultPageFactory,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Quote\Model\Quote $quoteFactory,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Customer\Model\Session $session,
        \Magento\ConfigurableProduct\Helper\Data $helper
    ) {
        parent::__construct($context);
        $this->formKey = $formKey;
        $this->resultPageFactory = $resultPageFactory;
        $this->quotes = $request;
        $this->quoteFactory = $quoteFactory;
        $this->cart = $cart;
        $this->_productRepository = $productRepository;
        $this->session = $session;
        $this->helper = $helper;
    }
 
    /**
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute() {

        $resultPage = $this->resultPageFactory->create();

        $quoteId = (int) $this->quotes->getParam('quote_id');
        $quote = $this->quoteFactory->load($quoteId);

        // If this is not the same customer the quote is created for, dont do anything. he cannot checkout this quote
        $quoteCustomerEmail = $quote->getBillingAddress()->getEmail();
        $currentCustomerEmail = $this->session->getCustomer()->getEmail();

        if ($quoteCustomerEmail === $currentCustomerEmail) {

            $items = $quote->getAllItems();

            foreach ($items as $item) {
                $price = $item->getPrice();
                $this->session->setPrice($price);
                $quantity = $item['qty'];
                $productId = (int) $item->getProductId();
               
                $_product = $this->_productRepository->getById($productId);
                //$type = $_product->getTypeId();
               

                //$productTypeInstance = $product->getTypeInstance();
                //
                //$usedProducts = $productTypeInstance->getUsedProducts($_product);
                //
                //echo $_product->getId(); //Main configurable product ID
                //echo $product->getName(); //Main Configurable Name
                //
                //foreach ($usedProducts  as $child) {
                //    echo $child->getId()."</br>"; //Child Product Id  
                //    echo $child->getName()."</br>"; //Child Product Name
                //}


                $params = array(
                    'form_key' => $this->formKey->getFormKey(),
                    'product' => $productId, //product Id
                    'qty' => $quantity, //quantity of product
                );

                if ($_product) {

                    $this->cart->addProduct($_product, $params);
                }
            }

            try {
                $this->cart->save();
                $this->messageManager->addSuccess(__('Added to cart successfully.'));
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addException($e, __('%1', $e->getMessage()));
            }

            $this->_redirect("checkout/cart/");
        }
        
        /** @var \Magento\Framework\View\Result\Page $resultPage */
        return $resultPage;
    }
}
