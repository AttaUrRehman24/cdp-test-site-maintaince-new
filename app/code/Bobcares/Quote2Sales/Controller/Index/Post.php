<?php

/**
 * Created on 29th Sept 2016
 * Bobcares Quote2Sales Post Controller Action.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */

namespace Bobcares\Quote2Sales\Controller\Index;

/**
 * Bobcares Quote2Sales Post Controller Action.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */
class Post extends \Bobcares\Quote2Sales\Controller\Index {
    
    /**
     * 
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Controller\Result\Redirect $resultRedirect
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
     * @param \Magento\Quote\Model\QuoteFactory $quoteFactory
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Bobcares\Quote2Sales\Model\Quotes $requestModelQuote
     * @param \Bobcares\Quote2Sales\Model\RequestStatus $requestStatusModelQuote
     * @param \Bobcares\Quote2Sales\Model\Request $requestStatusUpdateQuote
     * @param \Magento\Customer\Model\Customer $customer
     * @param \Bobcares\Quote2Sales\Helper\Request $createRequest
     * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
    \Magento\Backend\App\Action\Context $context,             
    \Magento\Framework\View\Result\PageFactory $resultPageFactory, 
    \Magento\Framework\Controller\Result\Redirect $resultRedirect, 
    \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory, 
    \Magento\Quote\Model\QuoteFactory $quoteFactory, 
    \Magento\Customer\Model\Session $customerSession,
    \Bobcares\Quote2Sales\Model\Quotes $requestModelQuote,
    \Bobcares\Quote2Sales\Model\RequestStatus $requestStatusModelQuote,
    \Bobcares\Quote2Sales\Model\Request $requestStatusUpdateQuote,
    \Magento\Customer\Model\Customer $customer,
    \Bobcares\Quote2Sales\Helper\Request $createRequest,
    \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
    \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
    \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
    \Magento\Store\Model\StoreManagerInterface $storeManager


    ) {
        parent::__construct($context,$transportBuilder,$inlineTranslation,$scopeConfig,$storeManager);
        $this->resultPageFactory = $resultPageFactory;
        $this->resultForwardFactory = $resultForwardFactory;
        $this->resultRedirect = $resultRedirect;
        $this->quoteFactory = $quoteFactory;
        $this->customerSession = $customerSession;
        $this->requestModelQuote = $requestModelQuote;
        $this->requestStatusModelQuote = $requestStatusModelQuote;
        $this->requestStatusUpdateQuote = $requestStatusUpdateQuote;
        $this->customer = $customer;
        $this->createRequest = $createRequest;
        $this->inlineTranslation = $inlineTranslation;  
        $this->scopeConfig = $scopeConfig;  
        $this->storeManager = $storeManager;

    }
        
    /**
     * Post user action
     *
     * @return void
     * @throws \Exception
     */
    public function execute() {
        $post = $this->getRequest()->getPostValue();
        $post['name'] = $post['firstname'] . ' ' . $post['lastname'];

        // Instantiate objects
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        
        $session = $this->customerSession;

        $frstName = $post['firstname'];
        $lstName = $post['lastname'];
        $emaill = $post['email'];
        $comment = $post['comment'];
        $phone = $post['telephone'];

        // Imcase there is no data for post
        if (!$post) {
            $this->_redirect('*/*/');
            return;
        }
        
        /* If post not empty */
        if ($post) {

            //$this->inlineTranslation->suspend();
            try {
                $postObject = new \Magento\Framework\DataObject();
                $postObject->setData($post);

                $error = false;

                /* Validation for name */
                if (!\Zend_Validate::is(trim($post['name']), 'NotEmpty')) {
                    $error = true;
                }

                /* Validation for comment */
                if (!\Zend_Validate::is(trim($post['comment']), 'NotEmpty')) {
                    $error = true;
                }

                /* Validation for email */
                if (!\Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
                    $error = true;
                }

                /* Validation for null */
                if (\Zend_Validate::is(trim($post['hideit']), 'NotEmpty')) {
                    $error = true;
                }

                /* exception */
                if ($error) {
                    throw new \Exception();
                }

                // Do no execute if the customer is logged in
                if (!$session->isLoggedIn()) {
                    $storeHelper = $this->_objectManager->get('Bobcares\Quote2Sales\Helper\Data');
                    $websiteId = $storeHelper->getCurrentWebsiteId();
                    $store = $storeHelper->getStore();
                    
                    $customer = $this->customer;
                    
                    //$this->customerExists($emaill, $websiteId = null);
                    if ($websiteId) {
                        $customer->setWebsiteId($websiteId);
                    }
                    $customer->loadByEmail($emaill);

                    // Start- New code for guest user functioanlity
                    // If the form contains password field, it means a customer who doesn't have an account has requested a quote.
                    //  Create an account for the customer with the passed credentials and then proceed to save quote request.
                    if ($post['password']) {
                        
                        // If the customer already exists
                        if ($customer->getId()) {
                            $this->_redirect('quote2sales/index/index/');
                            $this->messageManager->addError
                                    (__('A customer with the same email already exists in an associated website.'));
                            return;
                        }

                        $passwd = $post['password'];
                        $repeatPasswd = $post['confirm_password'];
                        
                        $customer->setWebsiteId($websiteId)
                                ->setStore($store)
                                ->setFirstname($frstName)
                                ->setLastname($lstName)
                                ->setEmail($emaill)
                                ->setPassword($passwd);
                        try {
 
                            try {
                               $customer->save(); 
                            } catch (\Exception $e) {
                               $this->_messageManager->addError(__("Email already exists"));
                            }                       

                            //$customer->sendNewAccountEmail();
                            
                            // Programmatically make the user logged in
                            //Email functionality on form submission.
                            //Email functionality on form submission.
                            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;

                    if ($this->scopeConfig->getValue(self::XML_PATH_EMAIL_SENDER) && $this->scopeConfig->getValue(self::XML_PATH_EMAIL_TEMPLATE)) {
                        $transport = $this->_transportBuilder
                            ->setTemplateIdentifier($this->scopeConfig->getValue(self::XML_PATH_EMAIL_TEMPLATE, $storeScope))
                            ->setTemplateOptions(
                                    [
                                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                                        'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                                    ]
                            )
                            ->setTemplateVars(['data' => $postObject])
                            ->setFrom($this->scopeConfig->getValue(self::XML_PATH_EMAIL_SENDER, $storeScope))
                            ->addTo($this->scopeConfig->getValue(self::XML_PATH_EMAIL_RECIPIENT, $storeScope))
                            ->setReplyTo($post['email'])
                            ->getTransport();

                        $transport->sendMessage();
                        $this->inlineTranslation->resume();
                    } else {
                        $this->messageManager->addError(__('Sorry could\'nt send the email. No email sender/template defined'));
                        $this->_redirect('customer/account');
                    }

                            try {
                                $customer->loadByEmail($emaill);
                                //$session->loginById($emaill);
                               
                                $session->setCustomerAsLoggedIn($customer);
                              
                            } catch (\Exception $e) {
                                $this->messageManager->addError
                                        (__('You are not logged in.' + $e));
                                return;
                            }
                        } catch (\Exception $e) {
                            $this->messageManager->addError(__('You are not logged in.'));
                            return;
                            //Zend_Debug::dump($e->getMessage());
                        }
                    }
                }
            
                // End- New code for guest user functioanlity.
                // If the product_name paramater is present, adding the product name to the comment.
                if ($this->getRequest()->getPost('product_name')) {
                    $fullComment = $postObject->getComment() . '<br><br>  Product Name: ' . $this->getRequest()->getPost('product_name');
                }

                // End- New code for guest user functioanlity
                //If the user is logged in then save request and display success message.
                //Else display an error message
                if ($session->isLoggedIn()) {
                    
                    $customerSession = $this->customerSession;
                    $customerId = $customerSession->getCustomerId();
                    $name = $post['name'];

                    // Saving the data in the custom table.               
                    //$createRequest = $objectManager->create('Bobcares\Quote2Sales\Helper\Request');
                    $createRequest = $this->createRequest;
                    
                    $createRequest->customer_id = $customerId;
                    $createRequest->name = $name;
                    $createRequest->comment = $comment;
                    $createRequest->email = $emaill;
                    $createRequest->phone = $phone;
                    $savedRequestId = $createRequest->create(false);

                    //Email functionality on form submission.
                    $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;

                    if ($this->scopeConfig->getValue(self::XML_PATH_EMAIL_SENDER) && $this->scopeConfig->getValue(self::XML_PATH_EMAIL_TEMPLATE)) {
                        $transport = $this->_transportBuilder
                            ->setTemplateIdentifier($this->scopeConfig->getValue(self::XML_PATH_EMAIL_TEMPLATE, $storeScope))
                            ->setTemplateOptions(
                                    [
                                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                                        'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                                    ]
                            )
                            ->setTemplateVars(['data' => $postObject])
                            ->setFrom($this->scopeConfig->getValue(self::XML_PATH_EMAIL_SENDER, $storeScope))
                            ->addTo($this->scopeConfig->getValue(self::XML_PATH_EMAIL_RECIPIENT, $storeScope))
                            ->setReplyTo($post['email'])
                            ->getTransport();

                        $transport->sendMessage();
                        $this->inlineTranslation->resume();
                    } else {
                        $this->messageManager->addError(__('Sorry could\'nt send the email. No email sender/template defined'));
                        $this->_redirect('customer/account');
                    }
                    
     
                    //$viewRequestsURL = $this->_redirect('quote2sales/index/requests/');
                    $url = $objectManager->create('\Magento\Framework\UrlInterface');
                    $viewRequestsURL = $this->_url->getUrl('quote2sales/index/requests/');
                    $this->messageManager->addSuccess(__('Your request was submitted and will be responded to as soon as possible. Thank you for contacting us.'));
                    $this->messageManager->addSuccess(__('Request for Quote was successfully saved. <a href="' . $viewRequestsURL . '">View all saved requests</a>'));
                    $this->_redirect('customer/account');
                } else {

                    //$translate->setTranslateInline(true);
                    $session->setData('contact_form_data', $post);
                    $session->addError(__('You are not logged in.'));
                }

                $this->_redirect('*/*/');

                return;
            } catch (\Exception $e) {
                $session->setData('contact_form_data', $post);
                // $translate->setTranslateInline(true);
                $this->messageManager->addError(__('Unable to submit your request. Please, try again later'));
                $this->_redirect('*/*/');
                return;
            }
        } else {
            $this->_redirect('*/*/');
        }
    }
    
    /**
     * @desc Rejects current quote and redirects to RFQ page
     * @param $quoteId quote Id to reject
     */
    public function rejectQuote($quoteId) {
        
        $requestId =  $this->requestStatusUpdateQuote->getCollection()
                        ->addFieldToSelect('request_id')
                        ->addFieldToFilter('quote_id', ((int) $quoteId))->getFirstItem()->getData('request_id');              

        /* If request Id exists */
        if ($requestId) {
            $statusTable = $this->requestStatusUpdateQuote->getCollection()
                            ->addFieldToFilter('quote_id', ((int) $quoteId))->getFirstItem();
            $statusTable->setData('status', 'Rejected');
            $statusTable->save();

            $requestTable = $this->requestStatusUpdateQuote->getCollection()
                            ->addFieldToFilter('request_id', ((int) $requestId))->getFirstItem();
            $requestTable->setData('status', 'Rejected');
            $requestTable->save();
        }


        // Send a mail to the store admin to let him know that a customer has rejected his quote
//        $sender = Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER);
//        $sender = Mage::getStoreConfig('quotes/email/sender_email_identity');
//
//
//        $res = Mage::getStoreConfig(self::XML_PATH_EMAIL_RECIPIENT);
//
//        $mailBody = '<p>The quote number' . $quoteId . ' has been rejected by the customer </p>';
//
//        $mail = Mage::getModel('core/email');
//        $mail->setToEmail($res);
//        $mail->setBody($mailBody);
//        $mail->setSubject('Quote Rejected');
//        $mail->setFromEmail($sender);
//        $mail->setType('html');
//        $mail->send();
    }
}
