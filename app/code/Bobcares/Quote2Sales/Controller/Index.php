<?php

/**
 * Created on 8th April 2016
 * Bobcares Quote2Sales Index Controller Action
 * for email constants.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */

namespace Bobcares\Quote2Sales\Controller;

use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\App\RequestInterface;
use Magento\Store\Model\ScopeInterface;

/**
 * Created on 29th Sept 2016
 * Bobcares Quote2Sales Index Controller Action
 * for email constants.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */
abstract class Index extends \Magento\Framework\App\Action\Action {

    /**
     * Recipient email config path
     */
    const XML_PATH_EMAIL_RECIPIENT = 'quotes/email/send_email';

    /**
     * Sender email config path
     */
    const XML_PATH_EMAIL_SENDER = 'quotes/email/email_sender';

    /**
     * Email template config path
     */
    const XML_PATH_EMAIL_TEMPLATE = 'quotes/email/email_template';

    /**
     * Enabled config path
     */
    const XML_PATH_ENABLED = 'quotes/quotes/enabled';

    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $_transportBuilder;

    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    protected $inlineTranslation;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
    \Magento\Framework\App\Action\Context $context, \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder, \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);
        $this->_transportBuilder = $transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
    }

    /**
     * Dispatch request
     *
     * @param RequestInterface $request
     * @return \Magento\Framework\App\ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function dispatch(RequestInterface $request) {

        //Incase the RFQ fom is enabled in config.
        if (!$this->scopeConfig->isSetFlag(self::XML_PATH_ENABLED, ScopeInterface::SCOPE_STORE)) {
            throw new NotFoundException(__('Page not found.'));
        }

        return parent::dispatch($request);
    }

}
