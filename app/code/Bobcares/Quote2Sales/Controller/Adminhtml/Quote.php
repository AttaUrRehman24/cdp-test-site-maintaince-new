<?php

/**
 * Bobcares Quote2Sales quote controller 
 * called for cancelling the quotes..
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */
namespace Bobcares\Quote2Sales\Controller\Adminhtml;

use Magento\Backend\App\Action;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Psr\Log\LoggerInterface;
use Magento\Quote\Api\CartManagementInterface;
use Magento\Quote\Api\CartRepositoryInterface;

/**
 * Adminhtml quote2sales quotes controller
 *
 * @author      Bobcares Developer Team
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
abstract class Quote extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Bobcares_Quote2Sales::quotes';

    /**
     * Array of actions which can be processed without secret key validation
     *
     * @var string[]
     */
    protected $_publicActions = ['view', 'index'];

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var \Magento\Framework\App\Response\Http\FileFactory
     */
    protected $_fileFactory;

    /**
     * @var \Magento\Framework\Translate\InlineInterface
     */
    protected $_translateInline;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \Magento\Framework\View\Result\LayoutFactory
     */
    protected $resultLayoutFactory;

    /**
     * @var \Magento\Framework\Controller\Result\RawFactory
     */
    protected $resultRawFactory;

    /**
     * @var OrderManagementInterface
     */
    protected $orderManagement;
   
    /**
     *
     * @var CartManagementInterface 
     */
    protected $quoteManagement;
      
    /**
     * @var CartRepositoryInterface
     */
    protected $quoteRepository;

    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     * @param \Magento\Framework\Translate\InlineInterface $translateInline
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory
     * @param \Magento\Framework\Controller\Result\RawFactory $resultRawFactory
     * @param OrderManagementInterface $orderManagement
     * @param OrderRepositoryInterface $orderRepository
     * @param LoggerInterface $logger
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
     */
    public function __construct(
       Action\Context $context,
       \Magento\Framework\Registry $coreRegistry,
       \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
       \Magento\Framework\Translate\InlineInterface $translateInline,
       \Magento\Framework\View\Result\PageFactory $resultPageFactory,
       \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
       \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory,
       \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
       \Magento\Quote\Model\Quote $quoteFactory,
       \Magento\Framework\App\Request\Http $request, 
       \Bobcares\Quote2Sales\Model\Quotes $requestModelQuote,
       \Magento\Customer\Model\Session $customerSession,
       \Bobcares\Quote2Sales\Model\Quotes $quote,
       \Bobcares\Quote2Sales\Model\Email $quoteEmail,
       \Bobcares\Quote2Sales\Model\RequestStatus $requestStatusModelQuote,
       \Bobcares\Quote2Sales\Model\Request $requestStatusUpdateQuote,

        LoggerInterface $logger
    ) {
        $this->_coreRegistry = $coreRegistry;
        $this->_fileFactory = $fileFactory;
        $this->_translateInline = $translateInline;
        $this->resultPageFactory = $resultPageFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->resultLayoutFactory = $resultLayoutFactory;
        $this->resultRawFactory = $resultRawFactory;
        $this->quoteFactory = $quoteFactory;
        $this->logger = $logger;
        $this->quotes = $request;
        $this->requestModelQuote = $requestModelQuote;
        $this->customerSession = $customerSession;
        $this->quoteModel = $quote;
        $this->quoteEmail = $quoteEmail;
        $this->requestStatusModelQuote = $requestStatusModelQuote;
        $this->requestStatusUpdateQuote = $requestStatusUpdateQuote;

        parent::__construct($context);
    }

    /**
     * Init layout, menu and breadcrumb
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function _initAction() {
        
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Bobcares_Quote2Sales::quotes');
        $resultPage->addBreadcrumb(__('Quote2Sales'), __('Quote2Sales'));
        $resultPage->addBreadcrumb(__('Quotes'), __('Quotes'));
        return $resultPage;
    }
    
}
