<?php

/**
 * Bobcares Quote2Sales Helper.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */
namespace Bobcares\Quote2Sales\Helper;

use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Helper\View as CustomerViewHelper;

/**
 * Bobcares Quote2Sales Post Controller Action.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper {

    const XML_PATH_ENABLED = 'quotes/quotes/enabled';

    /**
     * Customer session
     *
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var \Magento\Customer\Helper\View
     */
    protected $_customerViewHelper;
    
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param CustomerViewHelper $customerViewHelper
     */
    public function __construct(
    \Magento\Framework\App\Helper\Context $context, 
    \Magento\Customer\Model\Session $customerSession, 
    CustomerViewHelper $customerViewHelper,
    \Magento\Store\Model\StoreManagerInterface $storeManager,
    \Magento\Framework\ObjectManagerInterface $objectManager

    ) {
        $this->_customerSession = $customerSession;
        $this->_customerViewHelper = $customerViewHelper;
        parent::__construct($context);
        $this->_storeManager = $storeManager;
        $this->_objectManager = $objectManager;
    }

    /**
     * Check if enabled
     *
     * @return string|null
     */
    public function isEnabled() {
        return $this->scopeConfig->getValue(
                        self::XML_PATH_ENABLED, \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get user name
     *
     * @return string
     */
    public function getUserName() {
        if (!$this->_customerSession->isLoggedIn()) {
            return "";
        }

        /**
         * @var \Magento\Customer\Api\Data\CustomerInterface $customer
         */
        $customer = $this->_customerSession->getCustomerDataObject();
        return trim($this->_customerViewHelper->getCustomerName($customer));
    }

    /**
     * Get user email
     *
     * @return string
     */
    public function getUserEmail() {
        if (!$this->_customerSession->isLoggedIn()) {
            return "";
        }

        /**
         * @var CustomerInterface $customer
         */
        $customer = $this->_customerSession->getCustomerDataObject();
        return $customer->getEmail();
    }
    
    /**
     * Get the website ID
     *
     * @return string
     */
    public function getCurrentWebsiteId() {
        return $this->_storeManager->getStore()->getWebsiteId();
    }

    /**
     * Get the current store
     *
     * @return string
     */
    public function getStore() {
        return $this->_storeManager->getStore();
    }
}