<?php

/**
 * Bobcares Quote2Sales Model class.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */

namespace Bobcares\Quote2Sales\Model;

/**
 * Bobcares Quote2Sales Model.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */
class Quotes extends \Magento\Framework\Model\AbstractModel {

    /**
     * Define context
     *
     * @return void
     */
    public function __construct(
    \Magento\Framework\Model\Context $context, 
    \Magento\Framework\Registry $registry, 
    \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null, 
    \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null, array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Define Model Class
     *
     * @return void
     */
    public function _construct() {
        $this->_init('Bobcares\Quote2Sales\Model\ResourceModel\Quotes');
    }

    /**
     * Model class to get all the requests for the requetsed customer id
     *
     * @param int $customerId
     * @return array $result
     */
    function getQuotes($customerId) {
        if (!$customerId) {
            return;
        }
        $allQuotes = $this->getResource()->getAllQuotes($customerId);
        return $allQuotes;
    }

    /**
     * Model function to get the quotes for the requetsed ID
     *
     * @param int $id
     * @return array $details
     */
    public function loadByQuoteId($id) {
        if (!$id) {
            return;
        }

        $details = $this->getResource()->loadByQuoteId($id);
        return $details;
    }
    
    /**
     * Model function to get the quotes for the requetsed ID
     *
     * @param int $id
     * @return array $details
     */
    public function insertQuotes($requestId,$quoteId, $quoteDate, $quoteItem, $subTotal, $total, $id, $customer, $customerEmail, $sellerComment, $customShippingSet, $customShipPriceSet) {
        $details = $this->getResource()->insertQuotes($requestId,$quoteId, $quoteDate, $quoteItem, $subTotal, $total, $id, $customer, $customerEmail, $sellerComment, $customShippingSet, $customShipPriceSet);
        return $details;
    }
    
    /**
     * This is the function to cancel quotes.
     * @param type $quoteId
     * @return type
     */
    public function cancelQuotes($quoteId) {
        $details = $this->getResource()->cancelQuotes($quoteId);
        return $details;
    }
    
    /**
     * Function to get seller comment for each quote.
     * @param type $quoteId
     * @return type
     */
    public function getSellerComment($quoteId) {
        $details = $this->getResource()->getSellerComment($quoteId);
        return $details;
    }

}
