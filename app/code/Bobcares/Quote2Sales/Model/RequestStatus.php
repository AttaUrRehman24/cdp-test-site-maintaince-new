<?php

/**
 * Bobcares Quote2Sales Model class.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */

namespace Bobcares\Quote2Sales\Model;

/**
 * Bobcares Quote2Sales Model.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */
class RequestStatus extends \Magento\Framework\Model\AbstractModel {

    /**
     * Define context
     *
     * @return void
     */
    public function __construct(
        \Magento\Framework\Model\Context $context, 
        \Magento\Framework\Registry $registry, 
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null, 
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null, 
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Define Model Class
     *
     * @return void
     */
    public function _construct() {
        $this->_init('Bobcares\Quote2Sales\Model\ResourceModel\RequestStatus');
    }
    
        
    /**
     * @desc This function is used for inserting the quote and status of the request
     *
     * @param int $requestId
     * @param int $quoteId
     * @return array $details
     */
    public function insertStatus($requestId, $quoteId) {
        $details = $this->getResource()->insertStatus($requestId, $quoteId);
        return $details;
    }
    
    /**
     * Function to update the data in 
     * status table when a quote is cancelled.
     * @param type $quoteId
     * @param type $requestId
     * @return type
     */
    public function updateStatus($status, $quoteId = NULL) {
        $details = $this->getResource()->updateStatus($status, $quoteId);
        return $details;
    }

    /**
     * @desc This function is used for calling the resource model function
     * for fetching the quote,status and order details of the request
     * @param type $requestId : request id
     * @return type : array of quote id, order id and status
     * @throws Exception
     */
    public function getRequestData($requestId) {
        $details = $this->getResource()->getRequestData($requestId);
        return $details;
    }
    
    /**
     * To get the request id.
     * @param type $quoteId
     * @return type
     */
    public function getRequestId($quoteId) {
        $details = $this->getResource()->getRequestId($quoteId);
        return $details;
    }

}
