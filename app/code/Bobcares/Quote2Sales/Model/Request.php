<?php

/**
 * Bobcares Quote2Sales Model class.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */

namespace Bobcares\Quote2Sales\Model;

/**
 * Bobcares Quote2Sales Model.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */
class Request extends \Magento\Framework\Model\AbstractModel {

    public function __construct(
    \Magento\Framework\Model\Context $context, 
    \Magento\Framework\Registry $registry, 
    \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null, 
    \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null, array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    public function _construct() {
        $this->_init('Bobcares\Quote2Sales\Model\ResourceModel\Request');
    }

    /**
     * Model class to get all the requests for the requetsed customer id
     *
     * @param int $customerId
     * @return array $result
     */
    function getRequests($customerId) {
        if(!$customerId){
            return;
        }
        $allRequests = $this->getResource()->getAllRequests($customerId);
        return $allRequests;
    }
    
    /**
     * Model function to get the requests for the requetsed request ID
     *
     * @param int $requestId
     * @return array $details
     */
    public function loadByRequestId($requestId){
        if(!$requestId){
            return;
        }
        $details = $this->getResource()->loadByRequestId($requestId);
        return $details;
    }
    
    /**
     * Model function to get the customer details for the requetsed customer ID
     *
     * @param int $requestId
     * @return array $details
     */
    public function loadCustomerDetails($customerId){
        if(!$customerId){
            return;
        }
        $details = $this->getResource()->loadCustomerDetails($customerId);
        return $details;
    }
    
    /**
     * @desc This function is used for inserting the quote and status of the request
     *
     * @param int $requestId
     * @param int $quoteId
     * @param text $sellerComment
     * @return array $details
     */
    public function updateRequestStatus($status, $requestId, $sellerComment) {
        $details = $this->getResource()->updateRequestStatus($status, $requestId, $sellerComment);
        return $details;
    }
    
}
