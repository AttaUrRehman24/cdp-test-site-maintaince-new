<?php

/**
 * Created on 19th Oct 2016 
 * @category   System Configuration 
 *             dropdown fields in admin
 * @package    Bobcares_Quote2Sales
 * @author     BDT 
 *
 */

namespace Bobcares\Quote2Sales\Model\Config\Source;

/**
 * @category   System Configuration 
 *             dropdown fields in admin
 * @package    Bobcares_Quote2Sales
 * @author     BDT 
 *
 */
class Quote2Sales implements \Magento\Framework\Option\ArrayInterface {

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray() {
        return [['value' => 1, 'label' => __('Yes')], ['value' => 0, 'label' => __('No')]];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray() {
        return [0 => __('No'), 1 => __('Yes')];
    }
}
