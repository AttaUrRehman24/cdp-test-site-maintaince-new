<?php

/**
 * Bobcares Quote2Sales Model class.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */

namespace Bobcares\Quote2Sales\Model;

use Magento\Quote\Model\Quote;
use Magento\Framework\Mail\Template\TransportBuilder;


/**
 * Bobcares Quote2Sales Model.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */
class Email extends \Magento\Framework\Model\AbstractModel {
    
    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $_localeDate;
    
    //construct params
    protected $_transportBuilder;
    
    /**
     * @var \Magento\Quote\Model\QuoteFactory
     */
    protected $quoteFactory;
    
    /**
     * Sender email config path
     */
    const XML_PATH_EMAIL_SENDER = 'quotes/email/email_sender';
    
    /**
     * Email template config path
     */
    const XML_PATH_EMAIL_TEMPLATE = 'quote2sales/email/template';
    
/**
 * 
 * @param \Magento\Framework\Model\Context $context
 * @param \Magento\Framework\Registry $registry
 * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfigObject
 * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
 * @param \Magento\Framework\Pricing\Helper\Data $priceCurrency
 * @param \Magento\Store\Model\StoreManagerInterface $storeManager
 * @param TransportBuilder $transportBuilder
 * @param \Magento\Quote\Model\QuoteFactory $quoteFactory
 * @param \Magento\Framework\Message\ManagerInterface $messageManager
 */
    public function __construct(
    \Magento\Framework\Model\Context $context,
    \Magento\Framework\Registry $registry, 
    \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfigObject,
    \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
    \Magento\Framework\Pricing\Helper\Data $priceCurrency,
    \Magento\Store\Model\StoreManagerInterface $storeManager,
    TransportBuilder $transportBuilder,
    \Magento\Quote\Model\QuoteFactory $quoteFactory,
    \Magento\Framework\Message\ManagerInterface $messageManager,
    \Magento\Framework\Escaper $escaper,
    \Magento\Customer\Model\Customer $customerData,
    \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
    \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation


    ) {
         $this->scopeConfigObject = $scopeConfigObject;
         $this->_localeDate = $localeDate;
         $this->priceCurrency = $priceCurrency;
         $this->storeManager = $storeManager;
         $this->_transportBuilder = $transportBuilder;
         $this->quoteFactory = $quoteFactory;
         $this->messageManager = $messageManager;
         $this->escaper = $escaper;
         $this->customerData = $customerData;
         $this->scopeConfig = $scopeConfig;
         $this->inlineTranslation = $inlineTranslation;


         parent::__construct($context,$registry);
    }
    
    /**
     * 
     * @author bobcares bobcares.com
     * @copyright (c) 2017, bobcares 
     * @param Bobcares_Quote2Sales_Model_Adminhtml_Quote_Create $quote $quote object of type quote which contains details about the quote generated.
     * @param $sellerComment The seller comment object.
     * @todo Use of the template files to generate the quote table and Using the object directly in the email template to allow cutomisation.
     * 
     * Description Sends email to cutomer on quote creation.
     * 
     */
    public function sendEmail($quoteId, $sellerComment,$customerId) {

        $quote = $this->quoteFactory->create()->load($quoteId);
        $customerInstance = $this->customerData->load($customerId);
                
        $customer = $customerInstance->getName();
        $customerEmail = $customerInstance->getEmail();
        //$createdAt = $this->_localeDate->date(new \DateTime($quote->getCreatedAt()));
        $createdAt = $quote->getCreatedAt();
        $currency_code = $quote->getQuoteCurrencyCode();
        $currency_symbol = $this->storeManager->getStore()->getCurrentCurrency($currency_code)->getCurrencySymbol(); //toget the currency symbol from the Currency code.
        $subTotal = $quote->getSubtotal();
        $grandTotal = $quote->getBaseGrandTotal();
        $discount = $quote->getShippingAddress()->getBaseDiscountAmount();
        $subtotalDiscount = $quote->getSubtotalWithDiscount();
        $shippingTitle = $quote->getShippingAddress()->getShippingDescription();
        $shippingAmount = $quote->getShippingAddress()->getBaseShippingAmount();
        $websiteName = $quote->getStore()->getWebsite()->getName();
        
        //Get all Quote Items
        $items = $quote->getAllItems();
        $quoteDetails = null;
        
        foreach ($items as $item) {
        
        $formatedPrice = $this->priceCurrency->currency($item['price'],true,false);
        $rowTotal = $this->priceCurrency->currency($item['row_total'],true,false);
        
            //Quote table rows as string
            $quoteDetails .= '<tr><td align="left" width="325"  style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;"><strong>' . $item['name'] . $this->getCustomOptionText($item) . '<br>sku: </strong>' . $item['sku']
                    . '</td><td align="right" width="325"  style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;">' . $formatedPrice
                    . '</td><td align="right" width="325"  style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;">' . $item['qty']
                    . '</td><td align="right" width="325"  style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;">' . $rowTotal . '</td></tr>';
        }
        
        $formattedsubTotal = $this->priceCurrency->currency($subTotal,true,false);
        $formattedDiscount = $this->priceCurrency->currency($discount,true,false);
        $formattedShippingAmount = $this->priceCurrency->currency($shippingAmount,true,false);
        $formattedGrandTotal = $this->priceCurrency->currency($grandTotal,true,false);
        
        //Quote totals as a table in string
        $quote_footer = '<table>';

        $quote_footer .= '<tr><td align="right" width="975" style="font-size:13px;padding:5px 9px 6px 9px; line-height:1em;" >SubTotal : </td><td>&nbsp;</td><td align="right" width="325" style="font-size:13px;padding:5px 9px 6px 9px; line-height:1em;" >' . $formattedsubTotal . '</td></tr>';

        if ($discount) {
            $quote_footer .= '<tr><td align="right" width="975" style="font-size:13px;padding:5px 9px 6px 9px; line-height:1em;" >Discount : </td><td>&nbsp;</td><td align="right" width="325" style="font-size:13px;padding:5px 9px 6px 9px; line-height:1em;" >' . $formattedDiscount . '</td></tr>';
        }

        if (!$quote->isVirtual()) { // if the product is not virtual
            $quote_footer .= '<tr><td align="right" width="975" style="font-size:13px;padding:5px 9px 6px 9px; line-height:1em;" >Shipping & Delivery ( ' . $shippingTitle . ' ) : </td><td>&nbsp;</td><td align="right" width="325" style="font-size:13px;padding:5px 9px 6px 9px; line-height:1em;" >' . $formattedShippingAmount . '</td></tr>';
        }

        $quote_footer .= '<tr><td align="right" width="975" style="font-size:13px;padding:5px 9px 6px 9px; line-height:1em;" ><strong>GrandTotal : </strong></td><td>&nbsp;</td><td align="right" width="325" style="font-size:13px;padding:5px 9px 6px 9px; line-height:1em;" ><strong>' . $formattedGrandTotal . '</strong></td></tr>';

        $quote_footer .= '</table>';
             
        //Parametes the template email variable to send
        $params = array(
            'customer' => $customer,
            'quoteid' => $quoteId,
            'createdAt' => $createdAt,
            'currency_code' => $currency_code,
            'currency_symbol' => $currency_symbol,
            'subTotal' => $subTotal,
            'subTotalDiscount' => $subtotalDiscount,
            'discount' => $discount,
            'shippingTitle' => $shippingTitle,
            'shippingAmount' => $shippingAmount,
            'grandTotal' => $grandTotal,
            'website' => $websiteName,
            'sellerComment' => $sellerComment,
            'quoteDetails' => $quoteDetails,
            'quote_footer'=>$quote_footer
        );
        
            $postObject = new \Magento\Framework\DataObject();
            $postObject->setData($params);
                                
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        
        try {
            $transport = $this->_transportBuilder
                    ->setTemplateIdentifier('quotes_email_email_quote')
                    ->setTemplateOptions(
                            [
                                'area' => \Magento\Backend\App\Area\FrontNameResolver::AREA_CODE,
                                'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                            ]
                    )
                    ->setTemplateVars(['data' => $postObject])
                    ->setFrom($this->scopeConfig->getValue(self::XML_PATH_EMAIL_SENDER, $storeScope))
                    ->addTo($customerEmail, $customer)
                    ->getTransport();

            $transport->sendMessage();
            $this->inlineTranslation->resume();
        } catch (\Exception $e) {
            $this->messageManager->addError(__('Sorry, we can\'t send the email right now.' . $e->getMessage()));
        }
    }
    
    /**
     * Creates text that shows the configurable options and custom options of the quote items.
     * @param type $item
     * @return string
     */
    public function getCustomOptionText($item) {

        $finalResult = array();

        // Loop through all items in the cart
        // Array to hold the item's options
        $result = array();

        // Load the configured product options
        $options = $item->getProduct()->getTypeInstance(true)->getOrderOptions($item->getProduct());

        // Check for options
        if ($options) {
            if (isset($options['options'])) {
                $result = array_merge($result, $options['options']);
            }
            if (isset($options['additional_options'])) {
                $result = array_merge($result, $options['additional_options']);
            }
            if (!empty($options['attributes_info'])) {
                $result = array_merge($options['attributes_info'], $result);
            }
        }
        $finalResult = array_merge($finalResult, $result);

        $returnValue = "<br><br";

        //Fetching custom options associated with this quote item, if any, and displaying them under the product name
        $returnValue = $returnValue . "<hr>";

        // Looping through and adding options names and values into the whole text
        foreach ($finalResult as $_option) {

            $returnValue = $returnValue . "<br><br>";

            $label = $_option['label'];
            $value = $_option['value'];

            $returnValue = $returnValue . "<b>";

            $returnValue = $returnValue . $label . ' :';

            $returnValue = $returnValue . "</b>";

            $returnValue = $returnValue . "<i>";

            $returnValue = $returnValue . $value;

            $returnValue = $returnValue . "</i>";
        }
        
        return $returnValue;
    }

}
