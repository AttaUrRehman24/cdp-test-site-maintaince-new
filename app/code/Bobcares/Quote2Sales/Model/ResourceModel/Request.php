<?php

/**
 * Quote2Sales Resource Model.
 * @category    Bobcares
 * @author      BDT
 */

namespace Bobcares\Quote2Sales\Model\ResourceModel;

/**
 * Quote2Sales Resource model.
 * @category    Bobcares
 * @author      BDT
 */
class Request extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {
    
        public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    ) {
         parent::__construct($context);
    }
        
    /* Function defining
     * the database table
     */
    protected function _construct() {
        $this->_init('quote2sales_requests', 'request_id');
    }
    
    /**
     * Function to get all the requests
     *
     * @param $requestId
     * @return request array
     */
    public function getAllRequests($customerId){
        $table = $this->getMainTable();
        $sql = "SELECT * FROM " . $table . " WHERE customer_id = " . $customerId;
        return $this->getConnection()->fetchAll($sql);
    }
    
    /**
     * Function to get the request details
     *
     * @param $requestId
     * @return request array
     */
    public function loadByRequestId($requestId){
        $table = $this->getMainTable();
        $sql = "SELECT * FROM " . $table . " WHERE request_id = " . $requestId;
        return $this->getConnection()->fetchAll($sql);
    }
    
    /**
     * Function to get the request details
     *
     * @param $requestId
     * @return request array
     */
    public function loadCustomerDetails($customerId){
        $table = $this->getMainTable();
        $sql = "SELECT * FROM customer_address_entity WHERE parent_id = " . $customerId;
        return $this->getConnection()->fetchAll($sql);
    }
    
    /**
     * @desc This function is used for updating the status of the request
     * @param type $customerId : user id
     * @param type $status : status of the request
     * @return \Bobcares_Quote2Sales_Model_Request : query status
     * @throws Exception
     */
    public function updateRequestStatus($status,$requestId,$sellerComment) {

        $requestsTable = $this->getMainTable();

        try {
            $connection = $this->_resources->getConnection();

            //$this->_beforeSave();
            $sql = "update $requestsTable set status ='$status', seller_comment ='$sellerComment' where request_id=$requestId";
            $connection->query($sql);
            //$this->_afterSave();
        } catch (\Exception $e) {
            throw $e;
        }
        
        return $this;
    }
    
}