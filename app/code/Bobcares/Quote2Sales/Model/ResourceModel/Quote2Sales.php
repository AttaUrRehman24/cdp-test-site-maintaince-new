<?php

/**
 * Added on 13th April, 2017
 * Quote2Sales Resource Model.
 * @category    Bobcares
 * @author      BDT
 */

namespace Bobcares\Quote2Sales\Model\ResourceModel;

/**
 * Quote2Sales Resource model.
 * @category    Bobcares
 * @author      BDT
 */
class Quote2Sales extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {
    
/* Function defining
 * the database table
 */
    protected function _construct() {
        $this->_init('quote2sales_quotes', 'quote_id');
    }
}