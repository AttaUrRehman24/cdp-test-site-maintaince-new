<?php

/**
 * Quote2Sales Resource Collection.
 * Created on 12th April 2017.
 * @category    Bobcares
 * @author      BDT
 */
namespace Bobcares\Quote2Sales\Model\ResourceModel\Quote2Sales;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {
    
    /**
     * @var string
     */
    protected $_idFieldName = 'quote_id';
    
    /**
     * @param EntityFactoryInterface $entityFactory,
     * @param LoggerInterface        $logger,
     * @param FetchStrategyInterface $fetchStrategy,
     * @param ManagerInterface       $eventManager,
     * @param StoreManagerInterface  $storeManager,
     * @param AdapterInterface       $connection,
     * @param AbstractDb             $resource
     */
    public function __construct(
        \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\DB\Adapter\AdapterInterface $connection = null,
        \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource = null
    ) {
        $this->_init(
            'Bobcares\Quote2Sales\Model\Quote2Sales',
            'Bobcares\Quote2Sales\Model\ResourceModel\Quote2Sales'
        );
        parent::__construct(
            $entityFactory, $logger, $fetchStrategy, $eventManager, $connection,
            $resource
        );
        $this->storeManager = $storeManager;
    }
    protected function _initSelect()
    {
        parent::_initSelect();

       $this->getSelect()->joinLeft(
                ['secondTable' => $this->getTable('quote2sales_requests')],
                'main_table.request_id = secondTable.request_id',
                ['request_id','email','comment', 'name']
            );
    }
}