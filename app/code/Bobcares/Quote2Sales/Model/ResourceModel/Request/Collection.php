<?php

/**
 * Quote2Sales Resource Collection.
 * @category    Bobcares
 * @author      BDT
 */

namespace Bobcares\Quote2Sales\Model\ResourceModel\Request;

/**
 * Quote2Sales Resource Collection.
 * @category    Bobcares
 * @author      BDT
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {

    /**
     * @var string
     */
    protected $_idFieldName = 'request_id';

    protected function _construct() {
        $this->_init('Bobcares\Quote2Sales\Model\Request', 'Bobcares\Quote2Sales\Model\ResourceModel\Request');
    }

}
