<?php

/**
 * Updated on 30th Sept, 2017
 * Quote2Sales Resource Model.
 * @category    Bobcares
 * @author      BDT
 */

namespace Bobcares\Quote2Sales\Model\ResourceModel;

/**
 * Quote2Sales Resource model.
 * @category    Bobcares
 * @author      BDT
 */
class Quotes extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {
    
    /* Function defining
     * the database table
     */

    protected function _construct() {
        $this->_init('quote2sales_quotes', 'quote_id');
    }

    /**
     * Function to get all the requests
     *
     * @param $customerId
     * @return request array
     */
    public function getAllQuotes($customerId) {
        $table = $this->getMainTable();
        $sql = "SELECT * FROM " . $table . " WHERE user_id = " . $customerId;
        return $this->getConnection()->fetchAll($sql);
    }

    /**
     * Function to get the request details
     *
     * @param $quoteId
     * @return request array
     */
    public function loadByQuoteId($quoteId) {
        $table = $this->getMainTable();
        $sql = "SELECT * FROM " . $table . " WHERE quote_id = " . $quoteId;
        return $this->getConnection()->fetchAll($sql);
    }

    /**
     * This function is used for inserting the quote details to the quote2sales_quotes table
     * @param unknown $quoteId : quote id
     * @param unknown $quoteDate : quote created date
     * @param unknown $quoteItem : quote item
     * @param unknown $subTotal : sub total
     * @param unknown $total : total
     * @param unknown $id : user id
     * @param unknown $customer : customer
     * @param unknown $customerEmail : customerEmail
     * @param unknown $sellerComment : seller comments
     * @param unknown $customShippingSet: 0/1 to indicate whether custom shipping is set on this quote. 0 is no, 1 is yes.
     * @param unknown $customShipPriceSet: The custom shipping price set for this quote, if any
     * 
     * @throws Exception
     * @return Bobcares_Quote2Sales_Model_Quote
     */
    public function insertQuotes($requestId,$quoteId, $quoteDate, $quoteItem, $subTotal, $total, $id, $customer, $customerEmail, $sellerComment, $customShippingSet, $customShipPriceSet) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->_resources = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $item = ['request_id' => $requestId, 'quote_id' => $quoteId, 'quote_date' => $quoteDate, 'quote_item' => $quoteItem, 'subtotal' => $subTotal, 'quote_total' => $total, 'user_id' => $id, 'customer_name' => $customer, 'customer_email' => $customerEmail, 'seller_comment' => $sellerComment, 'is_custom_shipping_applied' => $customShippingSet, 'custom_shipping_amount' => $customShipPriceSet];
        $tableName = $this->getMainTable();
        $this->_resources->getConnection()->insert($tableName, $item);
    }

    /**
     * This is the function to cancel quotes.
     * @param type $quoteId
     */
    public function cancelQuotes($quoteId) {
        //$this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection = $this->_resources->getConnection();
        $tableName = $this->getMainTable();
        $sql = "DELETE FROM $tableName WHERE quote_id=$quoteId";
        $connection->query($sql);
    }
    
    /**
     * Function to get seller comment for each quote.
     * @param type $quoteId
     * @return type
     */
    public function getSellerComment($quoteId) {
        $table = $this->getMainTable();
        $sql = "SELECT seller_comment FROM " . $table . " WHERE quote_id = " . $quoteId;
        return $this->getConnection()->fetchAll($sql);
    }

}
