<?php

/**
 * Quote2Sales Resource Model.
 * @category    Bobcares
 * @author      BDT
 */

namespace Bobcares\Quote2Sales\Model\ResourceModel;

/**
 * Quote2Sales Resource model.
 * @category    Bobcares
 * @author      BDT
 */
class RequestStatus extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {
    
        public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    ) {
         
         parent::__construct($context);
    }
    
    /* Function defining
     * the database table
     */
    protected function _construct() {
        $this->_init('quote2sales_requests_status', 'status_id');
    }

    /**
     * @desc This fuction is used for inserting the quote and status of the request
     * @param type $requestId
     * @param type $quoteId
     */
    public function insertStatus($requestId, $quoteId) {

        $item = ['requests_id' => $requestId, 'quote_id' => $quoteId, 'status' => 'Converted To Quote'];
        $tableName = $this->getMainTable();
        $this->_resources->getConnection()->insert($tableName, $item);
    }
      
    /**
     * @desc This function is used for fetching the quote,status and order details of the request
     * @param type $requestId : request id
     * @return type : array of quote id, order id and status
     * @throws Exception
     */
    public function getRequestData($requestId) {

        try {
            
            //if there is no request id then throw an exception
            if (!$requestId) {
                throw new \Exception("request ID empty");
            }

            $requestsTable = $this->getMainTable();
            
            $sql = "select quote_id,order_id,status from $requestsTable where requests_id= $requestId";
                        
            return $this->getConnection()->fetchAll($sql);
        } catch (\Exception $e) {
            throw $e;
        }
        return array();
    }
    
    /**
     * @desc This function is used for updating the status of the request after quote cancellation
     * @param type $customerId : user id
     * @param type $status : status of the request
     * @return \Bobcares_Quote2Sales_Model_Request : query status
     * @throws Exception
     */
    public function updateStatus($status, $quoteId) {

        $requestsTable = $this->getMainTable();

        try {
            $connection = $this->_resources->getConnection();
            $sql = "update $requestsTable set status ='$status' where quote_id=$quoteId";
            $connection->query($sql);
        } catch (\Exception $e) {
            throw $e;
        }

        return $this;
    }

    /**
     * To get the request id.
     * @param type $quoteId
     * @return type
     */
    public function getRequestId($quoteId) {
        $connection = $this->_resources->getConnection();
        $tableName = $this->getMainTable();
        $sql = "SELECT requests_id FROM $tableName WHERE quote_id=$quoteId";
        return $this->getConnection()->fetchAll($sql);
    }

}
