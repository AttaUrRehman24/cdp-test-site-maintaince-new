<?php

/**
 * Created on 29th May 2017.
 * Bobcares Quote2Sales View Block.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */

namespace Bobcares\Quote2Sales\Block;

use Magento\Customer\Api\Data\AddressInterface;
use Magento\Customer\Model\Address\Mapper;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Bobcares Quote2Sales View Block.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */
class View extends \Magento\Framework\View\Element\Template {
    
    /**
     * @var string
     */
    protected $_template = 'templates/view.phtml';
    
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var \Magento\Customer\Model\Address\Config
     */
    protected $_addressConfig;

    /**
     * @var \Magento\Customer\Helper\Session\CurrentCustomer
     */
    protected $currentCustomer;

    /**
     * @var \Magento\Customer\Helper\Session\CurrentCustomerAddress
     */
    protected $currentCustomerAddress;

    /**
     * @var Mapper
     */
    protected $addressMapper;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer
     * @param \Magento\Customer\Helper\Session\CurrentCustomerAddress $currentCustomerAddress
     * @param \Magento\Customer\Model\Address\Config $addressConfig
     * @param Mapper $addressMapper
     * @param array $data
     */
    public function __construct(
    \Magento\Framework\View\Element\Template\Context $context, 
    \Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer, 
    \Magento\Customer\Helper\Session\CurrentCustomerAddress $currentCustomerAddress, 
    \Magento\Customer\Model\Address\Config $addressConfig,
    \Magento\Framework\Registry $registry,
    \Magento\Customer\Model\Session $customerSession,
     Mapper $addressMapper, array $data = []
    ) {
        $this->currentCustomer = $currentCustomer;
        $this->_coreRegistry = $registry;
        $this->currentCustomerAddress = $currentCustomerAddress;
        $this->_addressConfig = $addressConfig;
        $this->addressMapper = $addressMapper;
        $this->customerSession = $customerSession;

        parent::__construct($context, $data);

    }

    /**
     * Return back title for logged in and guest users
     *
     * @return string
     */
    public function getBackTitle() {
        
         $customerSession = $this->customerSession;

        //Incase the customer is logged in, provide link to go back to quotes else view another quote.
        if ($customerSession->isLoggedIn()) {

            return __('Back to My Quotes');
        }

        return __('View Another Quote');
    }

    /**
     * Get the logged in customer
     *
     * @return \Magento\Customer\Api\Data\CustomerInterface|null
     */
    public function getCustomer() {
        try {
            return $this->currentCustomer->getCustomer();
        } catch (NoSuchEntityException $e) {
            return null;
        }
    }

    /**
     * HTML for Shipping Address
     *
     * @return \Magento\Framework\Phrase|string
     */
    public function getPrimaryShippingAddressHtml() {
        try {
            $address = $this->currentCustomerAddress->getDefaultShippingAddress();
        } catch (NoSuchEntityException $e) {
            return __('You have not set a default shipping address.');
        }

        //Show address incase there is an address associated with the customer or display a message.
        if ($address) {
            return $this->_getAddressHtml($address);
        } else {
            return __('You have not set a default shipping address.');
        }
    }

    /**
     * HTML for Billing Address
     *
     * @return \Magento\Framework\Phrase|string
     */ public function getPrimaryBillingAddressHtml() {
        try {
            $address = $this->currentCustomerAddress->getDefaultBillingAddress();
        } catch (NoSuchEntityException $e) {
            return __('You have not set a default billing address.');
        }

        //Show address incase there is an address associated with the customer or display a message.
        if ($address) {
            return $this->_getAddressHtml($address);
        } else {
            return __('You have not set a default billing address.');
        }
    }

    /**
     * @return string
     */
    public function getPrimaryShippingAddressEditUrl() {

        //If the cutomer is not there return null else get the current customer address.
        if (!$this->getCustomer()) {
            return '';
        } else {
            $address = $this->currentCustomerAddress->getDefaultShippingAddress();
            $addressId = $address ? $address->getId() : null;
            return $this->_urlBuilder->getUrl(
                            'customer/address/edit', ['id' => $addressId]
            );
        }
    }

    /**
     * @return string
     */
    public function getAddressBookUrl() {
        return $this->getUrl('customer/address/');
    }
    
    /**
     * Render an address as HTML and return the result
     *
     * @param AddressInterface $address
     * @return string
     */
    protected function _getAddressHtml($address) {

        /** @var \Magento\Customer\Block\Address\Renderer\RendererInterface $renderer */
        $renderer = $this->_addressConfig->getFormatByCode('html')->getRenderer();
        return $renderer->renderArray($this->addressMapper->toFlatArray($address));
    }

    /**
     * @return string
     */
    public function getPrimaryBillingAddressEditUrl() {

        //If the cutomer is not there return null else get the current customer address.
        if (!$this->getCustomer()) {
            return '';
        } else {
            $address = $this->currentCustomerAddress->getDefaultBillingAddress();
            $addressId = $address ? $address->getId() : null;
            return $this->_urlBuilder->getUrl(
                            'customer/address/edit', ['id' => $addressId]
            );
        }
    }
    
    /**
     * Function to get the customer session.
     * @return type
     */
    public function getCustomerSession() {
        return $this->customerSession;
    }

}
