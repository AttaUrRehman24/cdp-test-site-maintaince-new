<?php

/**
 * Created on 29th May 2017.
 * Bobcares Quote2Sales Links Block.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */

namespace Bobcares\Quote2Sales\Block;

use Magento\Framework\View\Element\Template;

/**
 * Bobcares Quote2Sales Links Block.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */
class Link extends Template {
    
     public function __construct(
    \Magento\Framework\View\Element\Template\Context $context, 
    \Magento\Framework\Registry $registry,
    \Magento\Customer\Model\Session $customerSession,
    \Bobcares\Quote2Sales\Block\Quotes $quotes,
    \Bobcares\Quote2Sales\Block\Requests $requests,
    \Bobcares\Quote2Sales\Model\Quotes $quoteComment

    ) {
        $this->_coreRegistry = $registry;
        $this->customerSession = $customerSession;
        $this->quotes = $quotes;
        $this->requests = $requests;
        $this->quoteComment = $quoteComment;

        parent::__construct($context);

    }

    public function getCheckoutUrl() {
        return $this->getUrl('checkout/');
    }

    public function getQuoteDetails() {
        $_quotes = $this->quotes->getQuotesCollection();
        return $_quotes;
    }
    
    public function getCustomerSession() {
        return $this->customerSession;
    }
    
    public function getRequestDetails() {
        $_requests = $this->requests->getRequestsCollection();
        return $_requests;
    }
}
