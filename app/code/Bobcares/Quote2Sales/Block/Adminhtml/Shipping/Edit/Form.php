<?php
namespace Bobcares\Quote2Sales\Block\Adminhtml\Shipping\Edit;
 
use \Magento\Backend\Block\Widget\Form\Generic;
 
class Form extends Generic
{
 
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;
    protected $customer; 
    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \Magento\Directory\Block\Data $directoryBlock,
        \Bobcares\Quote2Sales\Model\RequestFactory $db,  
        \Magento\Customer\Model\Customer $customer,
        \Magento\Customer\Model\Address $address,
        \Magento\Directory\Model\CountryFactory $countryFactory, 
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        $this->directoryBlock = $directoryBlock;
        $this->_requestFactory = $db;
        $this->customer = $customer;
        $this->address = $address;
        $this->_countryFactory = $countryFactory;
        parent::__construct($context, $registry, $formFactory, $data);
    }
 
    /**
     * Init form
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('shipping_form');
        $this->setTitle(__('Shipping Form'));
    }
 
    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm() {
 
        /* Getting the form */
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post']]
        ); 
        $form->setHtmlIdPrefix('shipping_');
        
        /* Get teh form details */
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $countrySourceModel = $objectManager->get('Magento\Directory\Model\Config\Source\Country'); 
        $countryModel = $objectManager->get('Magento\Directory\Model\CountryFactory');
        $countries = $countrySourceModel->toOptionArray();
        $requestId = $this->getRequest()->getParam('request_id');
        $request = $this->_requestFactory->create();
        $requestModel = $request->load($requestId, 'request_id');
        $modelData = $request->getData();
        $customerId = (int)$modelData["customer_id"];
        $customers = $this->customer->load($customerId);
        $customerdetails = $request->loadCustomerDetails($customerId);
        
        /* Get full addresss */
        if ($customerdetails) {
            $fulladdress =  $customerdetails[0]['firstname']." ".$customerdetails[0]['middlename']." ".$customerdetails[0]['lastname']." , ".$customerdetails[0]['street']. " , ".$customerdetails[0]['city']." ,".$customerdetails[0]['region']." ".$customerdetails[0]['postcode']." , ".$this->getCountryname($customerdetails[0]['country_id']);
        } else {
            $fulladdress = ''; 
        }
        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('Shipping Address'), 'class' => 'fieldset-wide']
        );
        
        /* Customer suffix */
        if ($customerdetails) {
            if ($customerdetails[0]['suffix'] != 'NULL') {
                $suffix = $customerdetails[0]['suffix'];
            } else {
                $suffix = '';
            }
        } else {
            $suffix = '';
        }
        
        $fieldset->addField(
            'address',
            'select',
            ['name1' => 'name1', 
             'label' => __('Select from existing customer addresses:'), 
             'title' => __('Select from existing customer addresses:'), 
             'required' => false,
             'value'  => '1',
             'values' => array(1 => 'Add New Address',2 => $fulladdress ? $fulladdress : '')]
        );

        $fieldset->addField(
            'prefix',
            'text',
            ['name' => 'prefix', 'label' => __('Prefix'), 'title' => __('Prefix'), 'value' => $customerdetails ? $customerdetails[0]['prefix'] : '', 'required' => true]
        );

        $fieldset->addField(
            'firstname',
            'text',
            ['name' => 'firstname', 'label' => __('First Name'), 'title' => __('First Name'), 'value' => $customerdetails ? $customerdetails[0]['firstname'] : '', 'required' => true]
        );

        $fieldset->addField(
            'middlename',
            'text',
            ['name' => 'middlename', 'label' => __('Middle Name/Initial'), 'title' => __('Middle Name/Initial'), 'value' => $customerdetails ? $customerdetails[0]['middlename'] : '', 'required' => false]
        );

        $fieldset->addField(
            'lastname',
            'text',
            ['name' => 'lastname', 'label' => __('Last Name'), 'title' => __('Last Name'), 'value' => $customerdetails ? $customerdetails[0]['lastname'] : '', 'required' => true]
        );

        $fieldset->addField(
            'suffix',
            'text',
            ['name' => 'suffix', 'label' => __('Suffix'), 'title' => __('Suffix'),  'value' => $suffix ,'required' => false]
        );

        $fieldset->addField(
            'company',
            'text',
            ['name' => 'company', 'label' => __('Company'), 'title' => __('Company'), 'value' => $customerdetails ? $customerdetails[0]['company'] : '' ,'required' => false]
        );

        $fieldset->addField(
            'address1',
            'text',
            ['name' => 'address1', 'label' => __('Street Address'), 'title' => __('Street Address'), 'value' => $customerdetails ? $customerdetails[0]['street'] : '','required' => true]
        );
        $fieldset->addField(
            'address2',
            'text',
            ['name' => 'address2', 'label' => __(''), 'title' => __(''), 'value' => $customerdetails ? $customerdetails[0]['street'] : '','required' => false]
        );

        $fieldset->addField(
            'city',
            'text',
            ['name' => 'city', 'label' => __('City'), 'title' => __('City'), 'value' => $customerdetails ? $customerdetails[0]['city'] : '','required' => true]
        );

        $fieldset->addField(
            'country',
            'select',
            ['name' => 'Country', 
             'label' => __('Country'), 
             'title' => __('Country'), 
             'required' => true,
             'value'  => $customerdetails ? $customerdetails[0]['country_id'] : '',    
             'values' => $countries ]
        );

        $fieldset->addField(
            'state',
            'text',
            ['name' => 'state', 'label' => __('State/Province'), 'title' => __('State/Province'), 'value' => $customerdetails ? $customerdetails[0]['region'] : '','required' => true]
        );

        $fieldset->addField(
            'Zip/Postal Code',
            'text',
            ['name' => 'Zip/Postal Code', 'label' => __('Zip/Postal Code'), 'title' => __('Zip/Postal Code'), 'value' => $customerdetails ? $customerdetails[0]['postcode'] : '','required' => false]
        );

        $fieldset->addField(
            'phone',
            'text',
            ['name' => 'phone', 'label' => __('Phone Number'), 'title' => __('Phone Number'), 'value' => $customerdetails ? $customerdetails[0]['telephone'] : '','required' => true]
        );

        $fieldset->addField(
            'fax',
            'text',
            ['name' => 'fax', 'label' => __('Fax'), 'title' => __('Fax'), 'value' => $customerdetails ? $customerdetails[0]['fax'] : '' ,'required' => false]
        );

        $fieldset->addField(
            'vat',
            'text',
            ['name' => 'vat', 'label' => __('VAT number'), 'title' => __('VAT number'), 'value' => $customerdetails ? $customerdetails[0]['vat_id'] : '', 'required' => false]
        );
        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }
    
    /**
     * Get country name from country code
     *
     * @return country name
     */
    public function getCountryname($countryCode){    
        $country = $this->_countryFactory->create()->loadByCode($countryCode);
        return $country->getName();
    }
}