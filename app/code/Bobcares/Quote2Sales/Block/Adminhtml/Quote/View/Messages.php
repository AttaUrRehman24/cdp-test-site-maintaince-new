<?php

/**
 * Added on 10th Oct 2017.
 * Bobcares Quote2Sales Order view messages.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */
namespace Bobcares\Quote2Sales\Block\Adminhtml\Quote\View;

use Magento\Framework\View\Element\Message\InterpretationStrategyInterface;
use Magento\Sales\Model\Order;

/**
 * Bobcares Quote2Sales Order view messages.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */
class Messages extends \Magento\Sales\Block\Adminhtml\Order\View\Messages {
    
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry = null;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Message\Factory $messageFactory
     * @param \Magento\Framework\Message\CollectionFactory $collectionFactory
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param InterpretationStrategyInterface $interpretationStrategy
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Message\Factory $messageFactory,
        \Magento\Framework\Message\CollectionFactory $collectionFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        InterpretationStrategyInterface $interpretationStrategy,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $messageFactory,
            $collectionFactory,
            $messageManager,
            $interpretationStrategy,
            $registry
        );
        $this->coreRegistry = $registry;
    }

    /**
     * Retrieve order model instance
     *
     * @return Order
     */
    protected function _getQuote()
    {
        return $this->coreRegistry->registry('sales_quote');
    }

    /**
     * Preparing global layout
     *
     * @return $this
     */
    protected function _prepareLayout()
    {
        /**
         * Check Item products existing
         */
//        $productIds = [];
//        foreach ($this->_getOrder()->getAllItems() as $item) {
//            $productIds[] = $item->getProductId();
//        }
        return parent::_prepareLayout();
    }
}
