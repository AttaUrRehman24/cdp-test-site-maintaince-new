<?php

/**
 * Added on 10th Oct 2017.
 * Bobcares Quote2Sales Adminhtml items 
 * renderer block.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */

namespace Bobcares\Quote2Sales\Block\Adminhtml\Quote\View\Items\Renderer;

use Magento\Sales\Model\Order\Item;

/**
 * Bobcares Quote2Sales Adminhtml items 
 * renderer block.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */
class DefaultRenderer extends \Bobcares\Quote2Sales\Block\Adminhtml\Items\AbstractItems {

    /**
     * Message helper
     *
     * @var \Magento\GiftMessage\Helper\Message
     */
    protected $_messageHelper;

    /**
     * Checkout helper
     *
     * @var \Magento\Checkout\Helper\Data
     */
    protected $_checkoutHelper;

    /**
     * Giftmessage object
     *
     * @var \Magento\GiftMessage\Model\Message
     */
    protected $_giftMessage = [];

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     * @param \Magento\CatalogInventory\Api\StockConfigurationInterface $stockConfiguration
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\GiftMessage\Helper\Message $messageHelper
     * @param \Magento\Checkout\Helper\Data $checkoutHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\CatalogInventory\Api\StockConfigurationInterface $stockConfiguration,
        \Magento\Framework\Registry $registry,
        \Magento\GiftMessage\Helper\Message $messageHelper,
        \Magento\Checkout\Helper\Data $checkoutHelper,
        \Magento\Framework\App\Request\Http $request, 
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \Magento\Quote\Api\CartManagementInterface $quoteManagement,
        \Magento\Framework\Pricing\Helper\Data $priceHelper,

        array $data = []
    ) {
        $this->_checkoutHelper = $checkoutHelper;
        $this->_messageHelper = $messageHelper;
        $this->quotes = $request;
        $this->quoteFactory = $quoteFactory;
        $this->_quoteManagement = $quoteManagement;
        $this->priceHelper = $priceHelper;

        parent::__construct($context, $stockRegistry, $stockConfiguration, $registry,$request,$quoteFactory,$quoteManagement);
    }

    /**
     * Get order item
     *
     * @return Item
     */
    public function getItem()
    { 
        return $this->_getData('item');
    }

    /**
     * Retrieve real html id for field
     *
     * @param string $id
     * @return string
     */
    public function getFieldId($id)
    {
        return $this->getFieldIdPrefix() . $id;
    }

    /**
     * Retrieve field html id prefix
     *
     * @return string
     */
    public function getFieldIdPrefix()
    {
        return 'quote_item_' . $this->getItem()->getId() . '_';
    }

    /**
     * Indicate that block can display container
     *
     * @return bool
     */
    public function canDisplayContainer()
    {
        return $this->getRequest()->getParam('reload') != 1;
    }

    /**
     * Retrieve default value for giftmessage sender
     *
     * @return string
     */
    public function getDefaultSender()
    {
        if (!$this->getItem()) {
            return '';
        }

        if ($this->getItem()->getOrder()) {
            return $this->getItem()->getOrder()->getBillingAddress()->getName();
        }

        return $this->getItem()->getBillingAddress()->getName();
    }
    
    /**
     * Retrieve block html id
     *
     * @return string
     */
    public function getHtmlId()
    {
        return substr($this->getFieldIdPrefix(), 0, -1);
    }


    /**
     * Display susbtotal price including tax
     *
     * @param Item $item
     * @return string
     */
    public function displaySubtotalInclTax($item)
    {
        return $this->displayPrices(
            $this->_checkoutHelper->getBaseSubtotalInclTax($item),
            $this->_checkoutHelper->getSubtotalInclTax($item)
        );
    }

    /**
     * Display item price including tax
     *
     * @param Item|\Magento\Framework\DataObject $item
     * @return string
     */
    public function displayPriceInclTax(\Magento\Framework\DataObject $item)
    {
        return $this->displayPrices(
            $this->_checkoutHelper->getBasePriceInclTax($item),
            $this->_checkoutHelper->getPriceInclTax($item)
        );
    }

    /**
     * @param \Magento\Framework\DataObject|Item $item
     * @param string $column
     * @param null $field
     * @return string
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function getColumnHtml(\Magento\Framework\DataObject $item, $column, $field = null) {  
        
        $html = '';
        switch ($column) {
            case 'product':
                if ($this->canDisplayContainer()) {
                    $html .= '<div id="' . $this->getHtmlId() . '">';
                }
                $html .= $item->getName();
                if ($this->canDisplayContainer()) {
                    $html .= '</div>';
                }
                break;
                
            case 'sku':
                $html = $item->getSku();
                break;

            case 'price':
                $price = $item->getPrice();
                $html = $this->priceHelper->currency($price, true, false);
                break;

            case 'qty':
                $html =   $item->getQty();
                //$html = (int) $quote->getitemsQty();
                break;

            case 'subtotal':
                $html = $this->priceHelper->currency($item['row_total'], true, false);
                break;

            case 'tax-amount':
                $tax = $this->getQuote()->getShippingAddress()->getData('tax_amount');
                $html = $this->priceHelper->currency($tax, true, false);
                break;

            case 'tax-percent':
                $html = $this->displayTaxPercent($item);
                break;

            case 'discont':
                $discount = $this->getQuote()->getShippingAddress()->getData('discount_amount');
                $html = $this->priceHelper->currency($discount, true, false);
                break;
            case 'total':
                $html = $this->priceHelper->currency($item['row_total'], true, false);
                break;

            default:
                $html = parent::getColumnHtml($item, $column, $field);
        }
        return $html;
    }

    /**
     * Function to get the columns from the layout arguments.
     * @return array
     */
    public function getColumns()
    {
        $columns = array_key_exists('columns', $this->_data) ? $this->_data['columns'] : [];
                
        return $columns;
    } 
}