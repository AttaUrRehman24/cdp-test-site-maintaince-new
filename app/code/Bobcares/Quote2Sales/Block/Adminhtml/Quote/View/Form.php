<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Bobcares\Quote2Sales\Block\Adminhtml\Quote\View;

/**
 * Adminhtml sales order view plane
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Form extends \Magento\Sales\Block\Adminhtml\Order\View
{
    /**
     * Template
     *
     * @var string
     */
    protected $_template = 'Bobcares_Quore2Sales::quote/view/form.phtml';
}
