<?php

/**
 * Added on 10th Oct 2017.
 * Bobcares Quote2Sales Adminhtml order
 * items grid Block.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */

namespace Bobcares\Quote2Sales\Block\Adminhtml\Quote\View;

use Magento\Sales\Model\ResourceModel\Order\Item\Collection;

/**
 * Bobcares Quote2Sales Adminhtml order
 * items grid Block.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */
class Items extends \Bobcares\Quote2Sales\Block\Adminhtml\Items\AbstractItems {
        
    /**
     * @var \Magento\CatalogInventory\Api\StockRegistryInterface
     */
    protected $stockRegistry;
    
    /**
     * @var \Magento\CatalogInventory\Api\StockConfigurationInterface
     */
    protected $stockConfiguration;
    
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;
    
        public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\CatalogInventory\Api\StockConfigurationInterface $stockConfiguration,
        \Magento\Framework\Registry $registry,
        \Bobcares\Quote2Sales\Controller\Adminhtml\Quotes\Convert $convert, 
        \Magento\Framework\App\Request\Http $request,
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \Magento\Quote\Api\CartManagementInterface $quoteManagement,
        array $data = []
    ) {
        
        $this->convert = $convert;
        $this->stockRegistry = $stockRegistry;
        $this->stockConfiguration = $stockConfiguration;
        $this->_coreRegistry = $registry;
        $this->quotes = $request;
        $this->quoteFactory = $quoteFactory;
        $this->_quoteManagement = $quoteManagement;
    
        parent::__construct($context, $stockRegistry,$stockConfiguration,$registry,$request,$quoteFactory,$quoteManagement);
    }

    /**
     * @return array
     */
    public function getColumns() {
        $columns = array_key_exists('columns', $this->_data) ? $this->_data['columns'] : [];
        return $columns;
    }

    /**
     * Retrieve quote items collection
     *
     * @return Collection
     */
    public function getItemsCollection() { 
        return $this->getQuote()->getItemsCollection();
    }
}
