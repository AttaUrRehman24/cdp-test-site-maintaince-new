<?php

/**
 * Created on 11tth April 2017.
 * Bobcares Quote2Sales Shipping 
 * Block for convert quotes page.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */

namespace Bobcares\Quote2Sales\Block\Adminhtml\Quote\Create;
/**
 * Bobcares Quote2Sales Shipping 
 * Block for convert quotes page.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */
class Customer extends \Magento\Sales\Block\Adminhtml\Order\Create\AbstractCreate {

    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct() {
        parent::_construct();
        $this->setId('quote2sales_quote_create_customer');
    }

    /**
     * Get header text
     *
     * @return \Magento\Framework\Phrase
     */
    public function getHeaderText() {
        return __('Please select a customer');
    }

    /**
     * Get buttons html
     *
     * @return string
     */
    public function getButtonsHtml() {
        if ($this->_authorization->isAllowed('Magento_Customer::manage')) {
            $addButtonData = [
                'label' => __('Create New Customer'),
                'onclick' => 'order.setCustomerId(false)',
                'class' => 'primary',
            ];
            return $this->getLayout()->createBlock('Magento\Backend\Block\Widget\Button')
                            ->setData($addButtonData)
                            ->toHtml();
        }
        return '';
    }

    /**
     * {@inheritdoc}
     *
     * @return $this
     */
    protected function _prepareLayout() {
        $pageTitle = $this->getLayout()->createBlock('Bobcares\Quote2Sales\Block\Adminhtml\Quote\Create\Header')->toHtml();
        if (is_object($this->getLayout()->getBlock('page.title'))) {
            $this->getLayout()->getBlock('page.title')->setPageTitle($pageTitle);
        }
        return parent::_prepareLayout();
    }

    /**
     * Prepare header html
     *
     * @return string
     */
    public function getHeaderHtml() {
        $out = '<div id="order-header">' . $this->getLayout()->createBlock(
                        'Bobcares\Quote2Sales\Block\Adminhtml\Quote\Create\Header'
                )->toHtml() . '</div>';

        return $out;
    }

    /**
     * Get header width
     *
     * @return string
     */
    public function getHeaderWidth() {
        return 'width: 70%;';
    }

    /**
     * Retrieve quote session object
     *
     * @return \Magento\Backend\Model\Session\Quote
     */
    protected function _getSession() {
        return $this->_sessionQuote;
    }

    /**
     * Get cancel url
     *
     * @return string
     */
    public function getCancelUrl() {
        if ($this->_sessionQuote->getOrder()->getId()) {
            $url = $this->getUrl('sales/order/view', ['order_id' => $this->_sessionQuote->getOrder()->getId()]);
        } else {
            $url = $this->getUrl('sales/*/cancel');
        }

        return $url;
    }

    /**
     * Get URL for back (reset) button
     *
     * @return string
     */
    public function getBackUrl() {
        return $this->getUrl('sales/' . $this->_controller . '/');
    }

}
