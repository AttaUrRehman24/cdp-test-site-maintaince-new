<?php

namespace Bobcares\Quote2Sales\Block\Adminhtml\Quote\Create;

class Search extends \Magento\Sales\Block\Adminhtml\Order\Create\Search {

    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct() {
        parent::_construct();
        $this->setId('sales_order_create_search');
    }

    /**
     * Get header text
     *
     * @return \Magento\Framework\Phrase
     */
    public function getHeaderText() {
        return __('Please select products');
    }

    /**
     * Get buttons html
     *
     * @return string
     */
    public function getButtonsHtml() {
        $addButtonData = [
            'label' => __('Add Selected Product(s) to Quote'),
            'onclick' => 'order.productGridAddSelected()',
            'class' => 'action-add action-secondary',
        ];
        return $this->getLayout()->createBlock(
                        'Magento\Backend\Block\Widget\Button'
                )->setData($addButtonData)->toHtml();
    }

    /**
     * Get header css class
     *
     * @return string
     */
    public function getHeaderCssClass() {
        return 'head-catalog-product';
    }

}
