<?php

/**
 * Created on 2nd Nov 2016.
 * Bobcares Quote2Sales Request 
 * Block.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */

namespace Bobcares\Quote2Sales\Block\Adminhtml;

/**
 * Bobcares Quote2Sales Request 
 * Block.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */
class Request extends \Magento\Backend\Block\Template {

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;
    
    /**
     * Group service
     *
     * @var \Magento\Customer\Api\GroupRepositoryInterface
     */
    protected $groupRepository;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context, 
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Request\Http $request,
        \Bobcares\Quote2Sales\Model\RequestFactory $db,
        \Magento\Customer\Api\GroupRepositoryInterface $groupRepository,
        \Magento\Customer\Model\Customer $customerSession
    ) {
        $this->registry = $registry;
        $this->request = $request;
        $this->_requestFactory = $db;
        $this->groupRepository = $groupRepository;
        $this->customerSession = $customerSession;

        return parent::__construct($context);
    }
    
    /**
     * Function to get the request details
     *
     * @return request array
     */
    public function getRequest() {
        $requestId = $this->request->getParam('request_id');
        $requestModel = $this->_requestFactory->create();
        return $requestModel->loadByRequestId($requestId);
    }
    
    /**
     * Function to get customer group.
     * @return type
     */
    public function groupRepository() {
        return $this->groupRepository;
    }

    /**
     * Function to get customer session.
     * @return type
     */
    public function getCustomerSession() {
        return $this->customerSession;
    }
}
