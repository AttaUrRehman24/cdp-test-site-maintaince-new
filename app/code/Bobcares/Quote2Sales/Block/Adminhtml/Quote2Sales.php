<?php

namespace Bobcares\Quote2Sales\Block\Adminhtml\Quote2Sales;

class Quote2Sales extends \Magento\Backend\Block\Widget\Grid\Container {

    protected function _construct() {
        $this->_blockGroup = 'Bobcares_Quote2Sales';
        $this->_controller = 'adminhtml_quote2sales';
        $this->_headerText = __('Quotes');
        $this->_addButtonLabel = __('Add New Quotes');
        parent::_construct();
        $this->buttonList->add(
                'affiliate_apply', [
            'label' => __('Quotes'),
            'onclick' => "location.href='" . $this->getUrl('quote2sales/*/applyQuote2Sales') . "'",
            'class' => 'apply'
                ]
        );
    }

}
