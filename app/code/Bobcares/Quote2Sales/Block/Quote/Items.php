<?php

/**
 * Created on 20th Sep 2017.
 * Bobcares Quote2Sales Items Quoted Block.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */

namespace Bobcares\Quote2Sales\Block\Quote;


/**
 * Bobcares Quote2Sales Items Quoted Block.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */
class Items extends \Magento\Sales\Block\Items\AbstractItems {
    
        /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \Magento\Framework\App\Request\Http $quotes,
        \Magento\Framework\Pricing\Helper\Data $priceHelper,

        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        $this->quoteFactory = $quoteFactory;
        $this->quotes = $quotes;
        $this->priceHelper = $priceHelper;

        parent::__construct($context, $data);
    }

    public function getCheckoutUrl() {
        return $this->getUrl('checkout/');
    }

    /**
     * Retrieve current quote.
     * @return type
     */
    public function getQuote() {
        $quoteId = $this->quotes->getParam('quote_id');
        $quote = $this->quoteFactory->create()->load($quoteId);
        return $quote;
    }
    
        public function formatPrice() {
        return $this->priceHelper;
    }

}
