<?php

namespace Bobcares\Quote2Sales\Block;

use Magento\Framework\View\Element\Template;

class Quotes extends Template {

    protected $quotesCollectionFactory;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
    \Magento\Framework\View\Element\Template\Context $context, 
    \Bobcares\Quote2Sales\Model\ResourceModel\Quote2Sales\CollectionFactory $quotesCollectionFactory,
    \Magento\Customer\Model\Session $customerSession,
    \Bobcares\Quote2Sales\Model\Quotes $quotes,
    \Magento\Framework\Pricing\Helper\Data $priceHelper,
    \Magento\Quote\Model\QuoteFactory $quoteFactory,
    \Magento\Framework\App\Request\Http $request,
    array $data = array()
    ) {
        parent::__construct($context, $data);
        $this->quotesCollectionFactory = $quotesCollectionFactory;
        $this->customerSession = $customerSession;
        $this->quotes = $quotes;
        $this->priceHelper = $priceHelper;
        $this->quoteFactory = $quoteFactory;
        $this->quoteId = $request;

        $customer_id = $this->customerSession->getCustomer()->getId();

        //get collection of data 
        $quotes = $this->quotesCollectionFactory->create()
                ->addFieldToFilter('user_id', array('eq' => $customer_id));
        $this->setCollection($quotes);
    }

    /**
     * Function to call the model call for getting all the saved requests for the logged in user.
     *
     * @return $requests
     */
    public function getQuotesCollection() {
        $customerId = $this->customerSession->getId();
        $quotes = $this->quotes->getQuotes($customerId);
        return $quotes;
    }
    
    /**
     * Function for the pager.
     * @return $this
     */
    protected function _prepareLayout() {
        parent::_prepareLayout();
        if ($this->getCollection()) {
            $pager = $this->getLayout()->createBlock(
                            'Magento\Theme\Block\Html\Pager', 'quote2sales.quotes.history.pager'
                    )->setCollection(
                    $this->getCollection()
            );
            $this->setChild('pager', $pager);
            $this->getCollection()->load();
        }
        return $this;
    }

    /**
     * @return string
     * method for get pager html.
     */
    public function getPagerHtml() {
        return $this->getChildHtml('pager');
    }

    /**
     * Function to get the 
     * pricing helper data
     * @return type
     */
    public function getPriceHelper() {
        return $this->priceHelper;
    }

    /**
     * Function to get the customer session.
     * @return type
     */
    public function getCustomerSession() {
        return $this->customerSession;
    }
}
