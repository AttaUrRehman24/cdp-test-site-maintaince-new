<?php

namespace Bobcares\Quote2Sales\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Bobcares\Quote2Sales\Block\Adminhtml\Quote2Sales\Grid\Renderer\Action\UrlBuilder;
use Magento\Framework\UrlInterface;

class RequestActions extends Column {

    /** Url path */
    const URL_PATH_VIEW = 'quote2sales/request/view';
    const URL_PATH_QUOTES = 'quote2sales/quotes/convert';

    /** @var UrlBuilder */
    protected $actionUrlBuilder;

    /** @var UrlInterface */
    protected $urlBuilder;

    /**
     * @var string
     */
    private $_editUrl;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlBuilder $actionUrlBuilder
     * @param UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
    public function __construct(
    ContextInterface $context, UiComponentFactory $uiComponentFactory, UrlBuilder $actionUrlBuilder, UrlInterface $urlBuilder, array $components = [], array $data = [], $editUrl = self::URL_PATH_VIEW
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->actionUrlBuilder = $actionUrlBuilder;
        $this->_editUrl = $editUrl;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource) {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $name = $this->getData('name');
                if (isset($item['request_id'])) {
                                        $item[$name]['view'] = [
                      'href' => $this->urlBuilder->getUrl(self::URL_PATH_VIEW, ['request_id' => $item['request_id']]),
                      'label' => __('View')
                      ]; 
                    $v = $this->urlBuilder->getUrl(self::URL_PATH_QUOTES, ['request_id' => $item['request_id']]);

                    $item[$name]['convert'] = [
                        'href' => $this->urlBuilder->getUrl(self::URL_PATH_QUOTES, ['request_id' => $item['request_id']]),
                        'label' => __('Convert to quotes'),
                    ];
                }
            }
        }
        return $dataSource;
    }

}
