<?php

namespace WebMeridian\RewardPoints\Block\Checkout\Cart;


class Message extends \Lof\RewardPoints\Block\Checkout\Cart\Message
{
    public function __construct(\Magento\Framework\View\Element\Template\Context $context, \Magento\Framework\Message\Factory $messageFactory, \Magento\Framework\Message\CollectionFactory $collectionFactory, \Magento\Framework\Message\ManagerInterface $messageManager, \Magento\Framework\View\Element\Message\InterpretationStrategyInterface $interpretationStrategy, \Lof\RewardPoints\Helper\Purchase $rewardsPurchase, \Lof\RewardPoints\Helper\Data $rewardsData, array $data = [])
    {
        parent::__construct($context, $messageFactory, $collectionFactory, $messageManager, $interpretationStrategy, $rewardsPurchase, $rewardsData, $data);
    }

    protected function _prepareLayout()
    {

    }
}