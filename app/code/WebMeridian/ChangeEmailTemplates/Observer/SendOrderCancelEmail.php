<?php

namespace WebMeridian\ChangeEmailTemplates\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Payment\Helper\Data as PaymentHelper;
use Magento\Sales\Model\Order\Address\Renderer;
use WebMeridian\ChangeEmailTemplates\Helper\Data;

class SendOrderCancelEmail implements ObserverInterface
{
    /**
     * @var StateInterface
     */
    protected $_inlineTranslation;
    /**
     * @var TransportBuilder
     */
    protected $_transportBuilder;
    /**
     * @var PaymentHelper
     */
    protected $_paymentHelper;
    /**
     * @var Data
     */
    protected $_helper;

    /**
     * @var Renderer
     */
    protected $_addressRenderer;

    /**
     * SendOrderCancelEmail constructor.
     * @param TransportBuilder $transportBuilder
     * @param StateInterface $inlineTranslation
     * @param PaymentHelper $paymentHelper
     * @param Renderer $addressRenderer
     * @param ProductRepositoryInterfaceFactory $productRepositoryFactory
     * @param Data $helper
     */
    public function __construct(
        TransportBuilder $transportBuilder,
        StateInterface $inlineTranslation,
        PaymentHelper $paymentHelper,
        Renderer $addressRenderer,
        Data $helper
    )
    {
        $this->_transportBuilder = $transportBuilder;
        $this->_inlineTranslation = $inlineTranslation;
        $this->_paymentHelper = $paymentHelper;
        $this->_addressRenderer = $addressRenderer;
        $this->_helper = $helper;
    }

    public function execute(Observer $observer)
    {
        $order = $observer->getOrder();
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $storeId = $order->getStoreId();
        $footerImages = $this->_helper->getFooterImages();

        $logoTemplate = $this->_helper->getLogoParameters();

        $templateVars = [
            'order' => $order,
            'billing' => $order->getBillingAddress(),
            'payment_html' => $this->getPaymentHtml($order, $storeId),
            'store' => $order->getStore(),
            'formattedShippingAddress' => $this->getFormattedShippingAddress($order),
            'formattedBillingAddress' => $this->getFormattedBillingAddress($order),
            'itemsData' => $this->getItemsTotalTemplate($order),
            'footer_images_first' => $footerImages['first'],
            'footer_images_second' => $footerImages['second'],
            'footer_images_third' => $footerImages['third'],
            'logoTemplate_img' => $logoTemplate['img'],
            'logoTemplate_height' => $logoTemplate['height'],
            'logoTemplate_width' => $logoTemplate['width'],
            'logoTemplate_alt' => $logoTemplate['alt'],
        ];
        $emailSender = 'sales';
        $emailRecipient = $order->getCustomerEmail();

        $this->_inlineTranslation->suspend();
        $transport = $this->_transportBuilder
            ->setTemplateIdentifier('order_canceled_email')
            ->setTemplateOptions(
                [
                    'area' => \Magento\Framework\App\Area::AREA_ADMINHTML,
                    'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                ]
            )
            ->setTemplateVars($templateVars)
            ->setFrom($emailSender)
            ->addTo($emailRecipient)
            ->getTransport();

        $transport->sendMessage();
        $this->_inlineTranslation->resume();
    }

    public function getItemsTotalTemplate($order)
    {
        $items = $order->getItems();
        $itemsData = '';
        $itemsData .= '<table style="border-collapse:collapse;width:100%;border-spacing:0;max-width:100%;border:1px solid #d1d1d1; font-size: 14px;">';
        $itemsData .= '<thead>
                            <tr>
                                <th colspan="2" style="font-family: \'Open Sans\',\'Helvetica Neue\',Helvetica,Arial,sans-serif; text-align: left;vertical-align: bottom;padding: 10px;background-color: #f2f2f2;">Articles</th>
                                <th style="text-align: right; font-family: \'Open Sans\',\'Helvetica Neue\',Helvetica,Arial,sans-serif; vertical-align: bottom;padding: 10px;background-color: #f2f2f2;">Qté</th>
                                <th style="text-align: right;padding: 10px;font-family: \'Open Sans\',\'Helvetica Neue\',Helvetica,Arial,sans-serif;vertical-align: bottom;padding: 10px;background-color: #f2f2f2;">Prix</th>
                            </tr>
                       </thead>
                       <tbody>';

        foreach ($items as $item){
            $itemImage = $this->_helper->getProductImg($item->getProductId());
            $itemsData .= '<tr><td colspan="2">';
            $itemsData .= '<img style="width: 50px;" class="email-product-image" src="' . $itemImage . '" title="' . $item->getName() . '" alt="' . $item->getName() . '" />';
            $itemsData .= '<div style="display: inline-block;margin-left: 30px;"><p>' . $item->getName() . '</p><p> Réf. :' . $item->getSku() . '</p></div></td>';
            $itemsData .= '<td style="text-align: right;padding-right: 10px;">' . $item->getQtyCanceled() . '</td>';
            $itemsData .= '<td style="text-align: right;padding: 10px;">' . rtrim($item->getPrice(), '0') . '</td></tr>';
        }

        $itemsData .= '</tbody>';
        $itemsData .= '<tfoot>';
        $itemsData .= '<tr><th colspan="3" style="font-family: \'Open Sans\',\'Helvetica Neue\',Helvetica,Arial,sans-serif;text-align: right;vertical-align: top;padding: 10px;background-color: #f2f2f2;border-top: 1px solid #d1d1d1;font-weight: 400;">Sous-total (HT)</th>
                            <td style="font-family: \'Open Sans\',\'Helvetica Neue\',Helvetica,Arial,sans-serif;vertical-align: top;padding: 10px;background-color: #f2f2f2;border-top: 1px solid #d1d1d1;text-align: right;">' . round($order->getSubtotal(), 2) . '</td></tr>';
        $itemsData .= '<tr><th colspan="3" style="font-family: \'Open Sans\',\'Helvetica Neue\',Helvetica,Arial,sans-serif;text-align: right;vertical-align: top;padding: 10px;background-color: #f2f2f2;border-top: 1px solid #d1d1d1;font-weight: 400;">Frais de port (HT)</th>
                            <td style="font-family: \'Open Sans\',\'Helvetica Neue\',Helvetica,Arial,sans-serif;vertical-align: top;padding: 10px;background-color: #f2f2f2;border-top: 1px solid #d1d1d1;text-align: right;">' . round($order->getShippingAmount(), 2) . '</td></tr>';
        $itemsData .= '<tr><th colspan="3" style="font-family: \'Open Sans\',\'Helvetica Neue\',Helvetica,Arial,sans-serif;text-align: right;vertical-align: top;padding: 10px;background-color: #f2f2f2;border-top: 1px solid #d1d1d1;font-weight: 400;">TVA (20%)</th>
                            <td style="font-family: \'Open Sans\',\'Helvetica Neue\',Helvetica,Arial,sans-serif;vertical-align: top;padding: 10px;background-color: #f2f2f2;border-top: 1px solid #d1d1d1;text-align: right;">' . round($order->getTaxAmount(), 2) . '</td></tr>';
        $itemsData .= '<tr><th colspan="3" style="font-family: \'Open Sans\',\'Helvetica Neue\',Helvetica,Arial,sans-serif;text-align: right;vertical-align: top;padding: 10px;background-color: #f2f2f2;border-top: 1px solid #d1d1d1;font-weight: 400;">Montant global (TTC)</th>
                            <td style="font-family: \'Open Sans\',\'Helvetica Neue\',Helvetica,Arial,sans-serif;vertical-align: top;padding: 10px;background-color: #f2f2f2;border-top: 1px solid #d1d1d1;text-align: right;">' . round($order->getGrandTotal(), 2) . '</td></tr>';
        $itemsData .= '</tfoot></table>';

        return $itemsData;
    }

    protected function getPaymentHtml($order, $storeId)
    {
        return $this->_paymentHelper->getInfoBlockHtml(
            $order->getPayment(),
            $storeId
        );
    }

    /**
     * @param Order $order
     * @return string|null
     */
    protected function getFormattedShippingAddress($order)
    {
        return $order->getIsVirtual()
            ? null
            : $this->_addressRenderer->format($order->getShippingAddress(), 'html');
    }

    /**
     * @param Order $order
     * @return string|null
     */
    protected function getFormattedBillingAddress($order)
    {
        return $this->_addressRenderer->format($order->getBillingAddress(), 'html');
    }
}