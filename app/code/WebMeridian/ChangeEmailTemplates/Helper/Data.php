<?php
namespace WebMeridian\ChangeEmailTemplates\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Customer\Model\Session;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Theme\Block\Html\Header\Logo;
use Magento\Catalog\Api\ProductRepositoryInterface;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const GROUP_GENERAL= 1;
    const GROUP_PROFESSIONAL = 5;
    const PDF_CGV_PRO = 'CGV_Pro.pdf';
    const PDF_CGV = 'CGV.pdf';
    const PDF_RETRACTION = 'FORMULAIRE_DE_RETRACTATION.pdf';
    const CUSTOMER_PRO = 'pro';
    const CUSTOMER_PART = 'no_pro';
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
    /**
     * @var Logo
     */
    protected $_logo;
    /**
     * @var ProductRepositoryInterface
     */
    protected $_productRepository;

    public function __construct(
        Context $context,
        Session $customerSession,
        StoreManagerInterface $storeManager,
        Logo $logo,
        ProductRepositoryInterface $productRepositoryInterface
    ){
        $this->_customerSession = $customerSession;
        $this->_storeManager = $storeManager;
        $this->_logo = $logo;
        $this->_productRepository = $productRepositoryInterface;
        parent::__construct($context);
    }

    public function getCustomerGroup()
    {
        return $this->_customerSession->getCustomerGroupId();
    }

    public function getMediaUrl()
    {
        return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }

    public function getStaticUrl()
    {
        return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_STATIC);
    }

    /**
     * Get store identifier
     *
     * @return  int
     */
    public function getStoreId()
    {
        return $this->_storeManager->getStore()->getId();
    }

    /**
     * Get logo image URL
     *
     * @return string
     */
    public function getLogoSrc()
    {
        return $this->_logo->getLogoSrc();
    }

    /**
     * Get logo text
     *
     * @return string
     */
    public function getLogoAlt()
    {
        return $this->_logo->getLogoAlt();
    }

    /**
     * Get logo width
     *
     * @return int
     */
    public function getLogoWidth()
    {
        return $this->_logo->getLogoWidth();
    }

    /**
     * Get logo height
     *
     * @return int
     */
    public function getLogoHeight()
    {
        return $this->_logo->getLogoHeight();
    }

    public function getProductImg($productId)
    {
        $product = $this->_productRepository->getById($productId);
        return $this->getMediaUrl() . 'catalog/product' . $product->getData('thumbnail');
    }

    public function getFooterImages()
    {
        return [
            'first' => $this->getMediaUrl() . 'footer/block2.png',
            'second' => $this->getMediaUrl() . 'footer/icon-11.png',
            'third' => $this->getMediaUrl() . 'footer/icon-10.png'
        ];
    }

    public function getLogoParameters()
    {
        return [
            'img' => $this->getLogoSrc(),
            'height' => $this->getLogoHeight(),
            'width' => $this->getLogoWidth(),
            'alt' => $this->getLogoAlt()
        ];
    }
}
