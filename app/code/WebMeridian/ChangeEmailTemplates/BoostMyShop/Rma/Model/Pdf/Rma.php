<?php

namespace WebMeridian\ChangeEmailTemplates\BoostMyShop\Rma\Model\Pdf;

class Rma extends \BoostMyShop\Rma\Model\Pdf\Rma
{
    public function __construct(
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\Stdlib\StringUtils $string,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Sales\Model\Order\Pdf\Config $pdfConfig,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Sales\Model\Order\Pdf\Total\Factory $pdfTotalFactory,
        \Magento\Sales\Model\Order\Pdf\ItemsFactory $pdfItemsFactory,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Sales\Model\Order\Address\Renderer $addressRenderer,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Locale\ResolverInterface $localeResolver,
        \BoostMyShop\Rma\Model\Config $config,
        array $data = []
    ){
        parent::__construct(
            $paymentData,
            $string,
            $scopeConfig,
            $pdfConfig,
            $filesystem,
            $pdfTotalFactory,
            $pdfItemsFactory,
            $localeDate,
            $inlineTranslation,
            $addressRenderer,
            $storeManager,
            $localeResolver,
            $config,
            $data);
    }

    protected function drawRmaInformation($page, $rma)
    {

        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(1));
        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0));
        $page->setLineWidth(0.5);
        $page->drawRectangle(25, $this->y, 570, $this->y - 90);

        $this->_setFontBold($page, 14);
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $page->drawText(__('RMA # %1', $rma->getRmaReference()), 30, $this->y - 20, 'UTF-8');

        $this->_setFontRegular($page, 12);
        $additionnalTxt = [];
        if ($rma->getOrder())
            $additionnalTxt[] = __('Original order # : %1', $rma->getOrder()->getincrement_id());

        $i = 0;
        foreach($additionnalTxt as $txt)
        {
            $page->drawText($txt, 60, $this->y - 40 - ($i * 13), 'UTF-8');
            $i++;
        }

        $this->y -= 100;

    }

    /**
     * Insert billto & shipto blocks
     *
     * @param $page
     * @param $order
     */
    protected function drawAddresses($page, $order)
    {
        /* Add table head */
        $this->_setFontBold($page, 14);
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));

        $this->y -= 15;
        $page->drawText(__('Customer :'), 30, $this->y, 'UTF-8');
        $page->drawText(__('Return to :'), 300, $this->y, 'UTF-8');

        $customerAddress = $this->splitTextForLineWeight($this->getCustomerAddress($order), 200, $this->getFontRegular(), 14);
        $returnAddress = explode("\n", $this->getReturnAddress($order));

        $this->_setFontRegular($page, 12);
        $i = 0;
        foreach($customerAddress as $line) {
            $line = str_replace("\r", "", $line);
            if ($line) {
                $page->drawText($line, 60, $this->y - 20 - ($i * 13), 'UTF-8');
                $i++;
            }
        }

        $j = 0;
        foreach($returnAddress as $line) {
            $line = str_replace("\r", "", $line);
            if ($line) {
                $page->drawText($line, 330, $this->y - 20 - ($j * 13), 'UTF-8');
                $j++;
            }
        }

        $maxLines = max(($i), ($j));

        $this->y -= $maxLines * 20;
    }

    /**
     * @param $item
     * @param $page
     * @param $order
     */
    protected function _drawProduct($item, $page, $rma)
    {
        /* Add table head */
        $this->_setFontRegular($page, 10);
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(1));

        //columns headers
        $lines[0][] = ['text' => $item->getRiQty(), 'feed' => 45, 'align' => 'right'];

        $lines[0][] = ['text' => $item->getri_sku(), 'feed' => 70, 'align' => 'left'];

        $lines[0][] = ['text' => $item->getri_name(), 'feed' => 200, 'align' => 'left'];

        $lines[0][] = ['text' => $item->getri_reason(), 'feed' => 450, 'align' => 'left'];

        $lineBlock = ['lines' => $lines, 'height' => 5];

        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $this->drawLineBlocks($page, [$lineBlock], ['table_header' => true]);
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $this->y -= 20;
    }

    /**
     * Draw header for item table
     *
     * @param \Zend_Pdf_Page $page
     * @return void
     */
    protected function _drawHeader(\Zend_Pdf_Page $page)
    {
        /* Add table head */
        $this->_setFontRegular($page, 10);
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(1));

        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0));
        $page->setLineWidth(0.5);
        $page->drawRectangle(25, $this->y, 570, $this->y - 15);
        $this->y -= 10;

        //columns headers
        $lines[0][] = ['text' => __('Qty'), 'feed' => 45, 'align' => 'right'];
        $lines[0][] = ['text' => __('SKU'), 'feed' => 70, 'align' => 'left'];
        $lines[0][] = ['text' => __('Product'), 'feed' => 200, 'align' => 'left'];
        $lines[0][] = ['text' => __('Reason'), 'feed' => 450, 'align' => 'left'];

        $lineBlock = ['lines' => $lines, 'height' => 5];

        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $this->drawLineBlocks($page, [$lineBlock], ['table_header' => true]);
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $this->y -= 20;
    }

}