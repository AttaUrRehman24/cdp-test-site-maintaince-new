<?php

namespace WebMeridian\ChangeEmailTemplates\BoostMyShop\Rma\Model\Rma;

use Magento\Framework\Api\Filter;

class CustomerNotification extends \BoostMyShop\Rma\Model\Rma\CustomerNotification
{
    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $_productRepository;
    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $_searchCriteriaBuilder;

    /**
     * CustomerNotification constructor.
     * @param \BoostMyShop\Rma\Model\Config $config
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        \BoostMyShop\Rma\Model\Config $config,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
    ){
        $this->_productRepository = $productRepository;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        parent::__construct($config, $storeManager, $transportBuilder);
    }

    /**
     * @param $rma
     * @return array
     */
    protected function buildParams($rma)
    {
        $datas = [];

        foreach ($rma->getData() as $k => $v) {
            if (is_string($v))
                $datas[$k] = $v;
        }

        if ($rma->getOrder())
        {
            foreach($rma->getOrder()->getData() as $k => $v)
            {
                if (is_string($v))
                    $datas['order_'.$k] = $v;
            }
        }

        $datas['company_name'] = $this->_config->getCompanyName($rma->getrma_store_id());

        $datas['download_pdf_url'] = $this->_storeManager->getStore($rma->getrma_store_id())->getUrl('rma/index/download', ['rma_id' => $rma->getId(), 'key' => $rma->getKey(), '_nosid' => 1]);

        if ($rma->getData()['rma_status'] == 'complete')
        {
            $productIds = [];
            $productQty = [];
            $rmaItems = $rma->getAllItems()->getData();
            foreach ($rmaItems as $item) {
                $productIds[] = $item['ri_product_id'];
                $productQty[$item['ri_product_id']] = $item['ri_qty'];
            }
            $products = $this->getProductByIds($productIds);
            $productsTemplate = $this->getProductsTemplate($products, $productQty);
            $datas['products_template'] = $productsTemplate;
        }

        return $datas;
    }

    public function getProductByIds($productIds)
    {
        $searchCriteria = $this->_searchCriteriaBuilder
            ->addFilter('entity_id', $productIds, 'in')
            ->create();
        $products = $this->_productRepository->getList($searchCriteria)->getItems();
        return $products;
    }

    public function getProductsTemplate($products, $productQty)
    {
        $template = '<table style="border-collapse:collapse;width:100%;border-spacing:0;max-width:100%;border:1px solid #eeeeee; margin-bottom: 100px;">';
        $template .= '<thead><tr style="background-color: #eeeeee;"><th style="padding-left: 20px;">Articles</th><th>Qté</th></tr></thead>';
        $template .= '<tbody>';
        $baseMediaUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        foreach ($products as $product) {
            $productImage = $baseMediaUrl . 'catalog/product' . $product->getThumbnail();
            $productQty = $productQty[$product->getId()];
            $template .= '<tr><td style="width: 90%;">' . '<img style="width: 50px;" src="' .$productImage . '"/></div>' . '<p style="display: inline-block; vertical-align: top; margin-top: 10px;">' . $product->getName() . '<br/>Ref:' . $product->getSku() . '</p></td><td>' . $productQty . '</td></tr>';
        }
        return $template;
    }
}