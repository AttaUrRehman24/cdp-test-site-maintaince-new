<?php

namespace WebMeridian\ChangeEmailTemplates\Magento\Sales\Model\Order\Email;


use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Sales\Model\Order\Email\Container\IdentityInterface;
use Magento\Sales\Model\Order\Email\Container\Template;
use WebMeridian\ChangeEmailTemplates\Helper\Data;
use WebMeridian\ChangeEmailTemplates\Model\PdfRendererFactory;

class SenderBuilder extends \Magento\Sales\Model\Order\Email\SenderBuilder
{
    protected $helper;

    protected $orderPdfRenderer;

    protected $customerRepositoryInterface;

    public function __construct(
        Template $templateContainer,
        IdentityInterface $identityContainer,
        TransportBuilder $transportBuilder,
        Data $helper,
        PdfRendererFactory $pdfRenderer,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface
    ){
        $this->helper = $helper;
        $this->orderPdfRenderer = $pdfRenderer;
        $this->customerRepositoryInterface = $customerRepositoryInterface;
        parent::__construct($templateContainer, $identityContainer, $transportBuilder);
    }

    public function send()
    {
        $this->configureEmailTemplate();

        $this->transportBuilder->addTo(
            $this->identityContainer->getCustomerEmail(),
            $this->identityContainer->getCustomerName()
        );

        $copyTo = $this->identityContainer->getEmailCopyTo();

        if ($this->transportBuilder->getTemplateIdentifier() == 'sales_email_order_template') {
            $mediaUrl = $this->helper->getMediaUrl();
            $order = $this->templateContainer->getTemplateVars()['order'];
            $order_pdf = $this->orderPdfRenderer->create()->getPdf([$order]);
            $customer = $this->customerRepositoryInterface->getById($order->getCustomerId());
            $customerType = $customer->getCustomAttributes()['customer_type']->getValue();



            if ($customerType == $this->helper::CUSTOMER_PART) {

                $this->transportBuilder->addAttachment(
                    file_get_contents($mediaUrl . 'email/pdf/' . $this->helper::PDF_CGV), $this->helper::PDF_CGV);
            } elseif ($customerType == $this->helper::CUSTOMER_PRO) {
                $this->transportBuilder->addAttachment(
                    file_get_contents($mediaUrl . 'email/pdf/' . $this->helper::PDF_CGV_PRO), $this->helper::PDF_CGV_PRO);
            }

            if (!empty($copyTo) && $this->identityContainer->getCopyMethod() == 'bcc') {
                foreach ($copyTo as $email) {
                    $this->transportBuilder->addBcc($email);
                }
            }

            $this->transportBuilder->addAttachment(
                $order_pdf->render(),
                __("Order"). '#' . $order->getId().'.pdf');
            $this->transportBuilder->addAttachment(
                file_get_contents($mediaUrl . 'email/pdf/' . $this->helper::PDF_RETRACTION), $this->helper::PDF_RETRACTION);
        }

        $transport = $this->transportBuilder->getTransport();
        $transport->sendMessage();
    }
}