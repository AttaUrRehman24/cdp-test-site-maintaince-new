<?php

namespace WebMeridian\ChangeAgreements\Plugin\Magento\CheckoutAgreements\Model;

use \WebMeridian\ChangeAgreements\Helper\Data;

class ChangeCheckoutAgreementsRepository
{
    protected $_helper;

    public function __construct(
        Data $helper
    )
    {
        $this->_helper = $helper;

    }

    public function afterGetList(
        \Magento\CheckoutAgreements\Model\CheckoutAgreementsRepository $subject,
        $result
    ){
        $customerType = $this->_helper->getCustomerType();

        switch ($customerType) {
            case $this->_helper::CUSTOMER_PART:
                foreach ($result as $key => $item) {
                    if ($item->getId() == $this->_helper::TERMS_PROF_ID) {
                        unset($result[$key]);
                    }
                }
                break;
            case $this->_helper::CUSTOMER_PRO:
                foreach ($result as $key => $item) {
                    if ($item->getId() == $this->_helper::TERMS_PART_ID) {
                        unset($result[$key]);
                    }
                }
                break;
        }
        foreach ($result as $item) {
            if ($item->getId() == $this->_helper::TERMS_PART_ID) {
                $item->setData('checkbox_url', $this->_helper->getBaseUrl() . 'cgv-particuliers');
            } else {
                $item->setData('checkbox_url', $this->_helper->getBaseUrl() .  'cgv-professionnels');
            }
        }
        return $result;
    }
}
