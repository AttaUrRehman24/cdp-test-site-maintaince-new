<?php
namespace WebMeridian\ChangeAgreements\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Customer\Model\Session;
use Magento\Store\Model\StoreManagerInterface;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const TERMS_PART_ID = 2;
    const TERMS_PROF_ID = 3;
    const CUSTOMER_PRO = 'pro';
    const CUSTOMER_PART = 'no_pro';
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    public function __construct(
        Context $context,
        Session $customerSession,
        StoreManagerInterface $storeManager
    ){
        $this->_customerSession = $customerSession;
        $this->_storeManager = $storeManager;
        parent::__construct($context);
    }

    public function getBaseUrl()
    {
        return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
    }

    public function getCustomerType()
    {
        return $this->_customerSession->getCustomer()->getCustomerType();
    }
}
