define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/additional-validators',
        'WebMeridian_ChangeAgreements/js/model/checkout-agreements-validator'
    ],
    function (Component, additionalValidators, checkoutAgreementsValidator) {
        'use strict';
        additionalValidators.registerValidator(checkoutAgreementsValidator);
        return Component.extend({});
    }
);