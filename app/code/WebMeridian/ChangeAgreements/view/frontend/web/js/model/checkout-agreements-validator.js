define(
    [
        'jquery',
        'ko',
        'mage/validation'
    ],
    function ($, ko, Component) {
        'use strict';
        return {
            /**
             * Validate something
             *
             * @returns {boolean}
             */
            validate: function() {
                //Put your validation logic here
                var chekoutBoxes = document.getElementsByClassName('checkout-agreement');
                var index, len, checkoutLength, trueCheckout;

                if (!$("#id_checkout-agreements").hasClass("id_checkout-agreements")) {
                    $("#id_checkout-agreements").addClass("id_checkout-agreements");
                };

                checkoutLength = document.getElementsByClassName('checkout-agreement').length;
                trueCheckout = 0;

                if (chekoutBoxes[0].children[0].checked == true) {
                    trueCheckout++;
                }

                if (checkoutLength == trueCheckout) {
                    return true;
                } else {
                    jQuery('.loading-mask').css('display', 'none');
                    jQuery("#id_checkout-agreements").removeClass("id_checkout-agreements");
                    return false;
                }
            }
        }
    }
);