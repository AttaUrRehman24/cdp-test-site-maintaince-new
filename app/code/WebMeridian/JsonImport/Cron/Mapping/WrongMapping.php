<?php


namespace WebMeridian\JsonImport\Cron\Mapping;

use WebMeridian\JsonImport\Cron\Core;

class WrongMapping
{
    /**
     * @var Core $core
     */
    protected $core;


    /**
     * KarcherImport constructor.
     * @param Core $core
     */

    public function __construct(
        Core $core
    )
    {
        $this->core = $core;
    }

    /**
     * @param $cronJobsModel \WebMeridian\JsonImport\Model\CronJobs
     */
    public function runImport($cronJobsModel)
    {
        $this->core->insertLog($cronJobsModel, __('Wrong mapping'));
        $cronJobsModel->setStatus('has_error');
        $cronJobsModel->save();
    }
}