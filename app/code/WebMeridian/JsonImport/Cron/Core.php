<?php


namespace WebMeridian\JsonImport\Cron;

use Braintree\Exception;

class Core
{

    /**
     * @var \Magento\Framework\Filter\TranslitUrl $translitUrl
     */
    protected $translitUrl;

    /**
     * @var \Magento\UrlRewrite\Model\UrlRewrite $urlRewrite
     */
    protected $urlRewrite;

    protected $_mediaDirectory;

    /**
     * @var \WebMeridian\JsonImport\Model\Log $log
     */
    protected $log;

    /**
     * @var array $_messages
     */
    protected $_messages = [];

    /**
     * @var bool
     */
    protected $isPrintProductKeys = false;


    /**
     * Core constructor.
     * @param \Magento\Framework\Filter\TranslitUrl $translitUrl
     * @param \Magento\UrlRewrite\Model\UrlRewrite $urlRewrite
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \WebMeridian\JsonImport\Model\Log $log
     */

    public function __construct(
        \Magento\Framework\Filter\TranslitUrl $translitUrl,
        \Magento\UrlRewrite\Model\UrlRewrite $urlRewrite,
        \Magento\Framework\Filesystem $filesystem,
        \WebMeridian\JsonImport\Model\Log $log
    )
    {
        $this->translitUrl = $translitUrl;
        $this->urlRewrite = $urlRewrite;
        $this->_mediaDirectory = $filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
        $this->log = $log;
    }


    /**
     * @param $url
     * @param $product
     * @return string
     */

    public function getMainImage($url, $product){

        $target = $this->_mediaDirectory->getAbsolutePath('jsonimport/');
        return $this->downloadFile($url, 'main_' . $product->getSku(), $target);
    }

    /**
     * @param $url
     * @return string
     * @throws Exception
     */

    public function getAdditionalImage($url, $product){
        $target = $this->_mediaDirectory->getAbsolutePath('jsonimport/' . $product->getSku() .'/');
        $path = '';

        /**
         * remove wrong symbols from url
         */
        /** if "//" at the begin add http */
        if(substr($url, 0, 2) == '//'){
            $url = 'http:'.$url;
        }

        $pos = strpos($url, '?');
        if($pos){
            $url = substr($url, 0, $pos);
        }

        preg_match('/:\/\/.*(\/.*)\.[\w-]+$/i',$url, $fileName);
        if(count($fileName)){
            $fileName = end($fileName);
            $fileName = str_replace('/', '' , $fileName);
            $path = $this->downloadFile($url, $fileName, $target);
            return $path;
        }else{
            throw new Exception(__('Image url is wrong. Url: ') . $url);
        }

        return $path;
    }

    /**
     * @param $name
     * @param $sku
     * @param $entityId
     * @return string
     * @throws Exception
     */
    public function getCorrectKey($name, $sku, $entityId){

        $translitUrl = $this->translitUrl;
        $requestPath = $translitUrl->filter($name . $this->convertSkuToKey($sku));
        if($this->isUrlRewriteExists($requestPath, $entityId)){
            throw new Exception('Url is exists. ' . $requestPath);
        }

        if($this->isPrintProductKeys){
            $link = $requestPath . '.html';
            echo '<a target="_blank" href="/' . $link . '">'. $link . '</a>';
            echo '<br />';
        }

        return $requestPath;
    }

    /**
     * @param string $sku
     * @return string
     */
    public function convertSkuToKey($sku){
        $string = preg_replace('#[^0-9a-z]+#i', '', $sku);
        $string = strtolower($string);
        $string = trim($string, '');
        return '-'.$string;
    }

    /**
     * @param string $url
     * @param integer $entityId
     * @param string $type
     * @return bool
     */
    public function isUrlRewriteExists($url, $entityId, $type = 'product'){
        $urlRewriteCollection = $this->urlRewrite->getCollection()
            ->addFieldToFilter('request_path' , $url. '.html')
            ->addFieldToFilter('entity_type', $type);
        $count = count($urlRewriteCollection);
        if($count){
            if($count = 1){
                if($entityId){
                    foreach ($urlRewriteCollection as $item){
                        if($item->getEntityId() == $entityId) {
                            return false;
                        }
                    }
                }
            }
            return true;
        }else{
            return false;
        }
    }


    /**
     * @param $url
     * @param $product
     * @return mixed|string
     */

    public function getPdfFile($url, $product){
        $target = $this->_mediaDirectory->getAbsolutePath('catalog/product/pdf/');
        $path = $this->downloadFile($url, $product->getSku(), $target, 'pdf');
        if(trim($path) != ''){
            return str_replace($target, '', $path);
        }else{
            return $path;
        }
    }

    /**
     * @param $url
     * @return string
     * @throws Exception
     */

    public function getWysiwygImage($url){
        $target = $this->_mediaDirectory->getAbsolutePath('wysiwyg/jsonimport/');
        $path = '';
        /**
         * remove wrong symbols from url
         */
        /** if "//" at the begin add http */

        if(substr($url, 0, 2) == '//'){
            $url = 'http:'.$url;
        }
        $pos = strpos($url, '?');
        if($pos){
            $url = substr($url, 0, $pos);
        }

        preg_match('/:\/\/.*(\/[\w-]+)/i',$url, $fileName);
        if(count($fileName)){
            $fileName = end($fileName);
            $fileName = str_replace('/', '' , $fileName);

            preg_match('/\.[0-9a-z]+$/i',$url, $types);
            if(count($types)){
                $type = $types[0];
                $path = $this->downloadFile($url, $fileName, $target);

                if(trim($path) != ''){
                    return '/pub/media/wysiwyg/jsonimport/'.$fileName.$type;
                }
            }
        }else{
            throw new Exception(__('Image url is wrong. Url: ') . $url);
        }

        return $path;
    }

    /**
     * @param $url
     * @param $name
     * @param $directory
     * @return string
     */
    public function downloadFile($url, $name, $directory, $fileType = null){
        sleep(1);
        $path = '';
        $file_headers = @get_headers( $url );
        $file_exists = strpos( $file_headers[0], ' 200 OK' ) !== false;
        if($file_exists) {
            preg_match('/\.[0-9a-z]+$/i', $url, $matches);
            $type = '';
            if (isset($matches[0]) && (trim($matches[0]) != '')) {
                $type = $matches[0];
                if (!file_exists($directory)) {
                    mkdir($directory, 0777);
                }

                if(!is_null($fileType)){
                    $allowTypes = ['pdf', 'jpg', 'svg', 'jpeg', 'png'];
                    if(!in_array($type, $allowTypes)){
                        $type = '.'.$fileType;
                    }
                }
                $path = $directory . $name . $type;
                file_put_contents($path, file_get_contents($url));
            }
        }
        return $path;
    }


    /**
     * @param $cronJobsModel \WebMeridian\JsonImport\Model\CronJobs
     * @param $message string
     */

    public function insertLog($cronJobsModel, $message)
    {
        $log = $this->log;
        $log->setData('cron_id', $cronJobsModel->getId());
        $log->setData('message', $message);
        $log->setData('file', $cronJobsModel->getFile());
        $log->setData('file_name', $cronJobsModel->getFileName());
        $log->save();
        $log->unsetData();
    }

    /**
     * @param $message string
     */

    public function addMessage($message)
    {
        $this->_messages[] = $message;
    }

    /**
     * @return array
     */
    public function getMessages()
    {
        return $this->_messages;
    }

    /**
     * @param bool $status
     */
    public function changePrintStatus($status = false){
        $this->isPrintProductKeys = $status;
    }

}