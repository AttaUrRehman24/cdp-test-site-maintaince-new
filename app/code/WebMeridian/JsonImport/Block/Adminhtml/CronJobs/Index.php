<?php
namespace WebMeridian\JsonImport\Block\Adminhtml\CronJobs;

class Index extends \Magento\Backend\Block\Widget\Grid\Container
{

    protected function _construct()
    {
        $this->_controller = 'adminhtml_post';
        $this->_blockGroup = 'WebMerididan_JsonImport';
        $this->_headerText = __('Json Cron Jobs');
        $this->_addButtonLabel = __('Create New Job');
        parent::_construct();
    }
}