<?php
namespace WebMeridian\JsonImport\Block\Adminhtml\Log;

class Index extends \Magento\Backend\Block\Widget\Grid\Container
{

    protected function _construct()
    {
        $this->_controller = 'adminhtml_post';
        $this->_blockGroup = 'WebMerididan_JsonImport';
        $this->_headerText = __('Logs');
//        $this->_addButtonLabel = __('Create New Log');
        parent::_construct();
    }
}