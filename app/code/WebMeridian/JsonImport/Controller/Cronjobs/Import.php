<?php

namespace WebMeridian\JsonImport\Controller\Cronjobs;

use Magento\Framework\App\Action\Context;
use WebMeridian\JsonImport\Model\CronJobs;

class Import extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \WebMeridian\JsonImport\Cron\Import
     */
    private $_jsonImport;

    private $_modelImport;

    public function __construct(
        Context $context,
        \WebMeridian\JsonImport\Cron\Import $import,
        CronJobs $cronJobs
    )
    {
        parent::__construct($context);
        $this->_jsonImport = $import;
        $this->_modelImport = $cronJobs;
    }

    public function execute()
    {
        $jobsId = $this->getRequest()->getParam('id');
        if(!$jobsId) {
            $this->_redirect('/');
            return;
        }

        $job = $this->_modelImport->load($jobsId);

        $printUrls = $this->getRequest()->getParam('print');
        if($printUrls == 1 || $printUrls == 'yes'){
            $this->_jsonImport->changePrintStatus(true);
        }

        $this->_jsonImport->runJob($job);
    }
}