<?php
/**
 * @copyright Copyright (c) 2016 www.magebuzz.com
 */

namespace WebMeridian\JsonImport\Controller\Adminhtml\Cronjobs;

use Magento\Backend\App\Action;

class Save extends \Magento\Backend\App\Action
{


    /**
     * @var \Magento\Framework\Controller\Result\RedirectFactory
     */
    protected $resultRedirectFactory;

    protected $_varDirectory;
    protected $_fileUploaderFactory;
    protected $import;


    /**
     * @param Action\Context $context
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(
        Action\Context $context,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->_varDirectory = $filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR);
        $this->_fileUploaderFactory = $fileUploaderFactory;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('WebMeridian_JsonImport::save');
    }

    /**
     * Save Cron Jobs
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        try {
            $id = $this->getRequest()->getParam('cron_id');

            /** @var \WebMeridian\JsonImport\Model\CronJobs $model */
            $model = $this->_objectManager->create('WebMeridian\JsonImport\Model\CronJobs');
            if ($id) {
                $model->load($id);
                if (!$model->getId()) {
                    $this->messageManager->addError(__('Cron job is missing.'));

                    return $this->resultRedirectFactory->create()->setPath(
                        '*/*/index', ['_secure' => $this->getRequest()->isSecure()]
                    );
                }
            }

            $data = $this->getRequest()->getParams();
            unset($data['key']);

            $target = $this->_varDirectory->getAbsolutePath('json_import/');
            /** @var $uploader \Magento\MediaStorage\Model\File\Uploader */
            $uploader = $this->_fileUploaderFactory->create(['fileId' => 'file']);

            /** Allowed extension types */
            $uploader->setAllowedExtensions(['json']);

            $uploader->setAllowRenameFiles(true);
            $result = $uploader->save($target);
            if ($result['file']) {
                $this->messageManager->addSuccess(__('File has been successfully uploaded'));
            }

            $fileFullPath = $result['path'] . $result['file'];
            if (file_exists($fileFullPath)) {
                $jsonString = file_get_contents($fileFullPath);
                $importData = json_decode($jsonString, true);
            }else{
                $importData = [];
            }

            $data['file'] = str_replace($this->_varDirectory->getAbsolutePath(), '' , $result['path']). $result['file'];
            $model->setData($data);
            $model->save();

        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
        return $this->resultRedirectFactory->create()->setPath(
            '*/*/index', ['_secure' => $this->getRequest()->isSecure()]
        );
    }
}