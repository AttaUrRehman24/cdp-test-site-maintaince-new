<?php
/**
 * @copyright Copyright (c) 2016 www.magebuzz.com
 */

namespace WebMeridian\JsonImport\Controller\Adminhtml\Log;

use Magento\Backend\App\Action;

use Magento\Backend\App\Action\Context;
use WebMeridian\JsonImport\Model\ResourceModel\Log\CollectionFactory;
use Magento\Framework\Controller\ResultFactory;

class MassDelete extends \Magento\Backend\App\Action
{



    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;


    /**
     * @param Action\Context $context
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(
        Context $context,
        CollectionFactory $collectionFactory
    )
    {
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('WebMeridian_JsonImport::massDelete');
    }

    public function execute()
    {

        /** @var \WebMeridian\JsonImport\Model\ResourceModel\Log\Collection $collection */
        $collection = $this->collectionFactory->create();
        $ids = $this->getRequest()->getParam('log_id');
        $collection->addFieldToFilter('log_id', ['in' => $ids]);

        $collectionSize = $collection->getSize();

        foreach ($collection as $item) {
            $item->delete();
        }

        $this->messageManager->addSuccess(__('A total of %1 element(s) have been deleted.', $collectionSize));

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }
}