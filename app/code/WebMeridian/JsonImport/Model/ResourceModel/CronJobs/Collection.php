<?php
namespace WebMeridian\JsonImport\Model\ResourceModel\CronJobs;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'cron_id';
    protected $_eventPrefix = 'webmeridian_jsonimport_cronjobs_collection';
    protected $_eventObject = 'cronJobs_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('WebMeridian\JsonImport\Model\CronJobs', 'WebMeridian\JsonImport\Model\ResourceModel\CronJobs');
    }

}