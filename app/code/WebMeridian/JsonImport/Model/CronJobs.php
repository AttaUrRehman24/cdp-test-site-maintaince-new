<?php
namespace WebMeridian\JsonImport\Model;
class CronJobs extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'webmeridian_jsonimport_cronjobs';

    protected $_cacheTag = 'webmeridian_jsonimport_cronjobs';

    protected $_eventPrefix = 'webmeridian_jsonimport_cronjobs';

    protected function _construct()
    {
        $this->_init('WebMeridian\JsonImport\Model\ResourceModel\CronJobs');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
}