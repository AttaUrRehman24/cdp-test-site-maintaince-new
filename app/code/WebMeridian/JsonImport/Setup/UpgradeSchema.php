<?php


namespace WebMeridian\JsonImport\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    CONST TABLE_CRON_JOBS = 'webmeridian_jsonimport_cronjobs';

    /**
     * {@inheritdoc}
     */
    public function upgrade(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    )
    {
        $setup->startSetup();

        $installer = $setup;
        if (version_compare($context->getVersion(), "1.0.1", "<")) {
            $table = $installer->getTable(self::TABLE_CRON_JOBS);
            $installer->getConnection()->addColumn($table, 'mapping',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'nullable => false',
                    'comment' => 'Mapping'
                ]
            );
        }

        $setup->endSetup();
    }
}