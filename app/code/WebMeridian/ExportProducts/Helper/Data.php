<?php

namespace WebMeridian\ExportProducts\Helper;

use Magento\Framework\App\Helper\Context;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const CATEGORY_ID_MARGUES = 122;
    /**
     * @var Context
     */
    protected $_context;
    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;
    /**
     * @var \Magento\Tax\Api\TaxCalculationInterface
     */
    protected $_taxCalculation;
    /**
     * @var \Magento\Catalog\Model\CategoryFactory
     */
    protected $_categoryFactory;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
    /**
     * @var \Magento\CatalogUrlRewrite\Model\ProductUrlPathGenerator
     */
    protected $_productUrlPathGenerator;
    /**
     * @var \Magento\Framework\UrlFactory
     */
    protected $_urlFactory;
    /**
     * @var \Magento\Store\Model\App\Emulation
     */
    protected $_appEmulation;
    /**
     * @var \Magento\Framework\View\Element\BlockFactory
     */
    protected $_blockFactory;
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    public function __construct(
        Context $context,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Tax\Api\TaxCalculationInterface $taxCalculation,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\CatalogUrlRewrite\Model\ProductUrlPathGenerator $pathGenerator,
        \Magento\Framework\UrlFactory $urlFactory,
        \Magento\Store\Model\App\Emulation $appEmulation,
        \Magento\Framework\View\Element\BlockFactory $blockFactory,
        \Psr\Log\LoggerInterface $logger
    ){
        $this->_context = $context;
        $this->_productFactory = $productFactory;
        parent::__construct($context);
        $this->_taxCalculation = $taxCalculation;
        $this->_categoryFactory = $categoryFactory;
        $this->_storeManager = $storeManager;
        $this->_productUrlPathGenerator = $pathGenerator;
        $this->_urlFactory = $urlFactory;
        $this->_appEmulation = $appEmulation;
        $this->_blockFactory = $blockFactory;
        $this->_logger = $logger;
    }

    /**
     * @return mixed
     */
    public function getProducts()
    {
        return $this->_productFactory->create()->getCollection()->addAttributeToSelect('*');
    }

    public function returnFileLink()
    {
        $fileName = 'pub/media/products'.date('d-m-y').'.txt';

        $file = $this->getCreateFile($fileName);
        if ($file == 'true') {
            $fileLink = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_DIRECT_LINK) . $fileName;
            return $fileLink;
        } else {
            return $file;
        }

    }

    public function getCreateFile($fileName)
    {
        $content = 'Identifiant_unique|Titre|Description|Prix_TTC|Prix_barre|Prix_solde|Categorie|Sous_categorie1|Sous_categorie2|Sous_categorie3|URL_produit|URL_image|EAN|MPN|Marque|Frais_de_livraison|Delais_de_livraison|Description_de_livraison|Quantite_en_Stock|Disponibilite|Garantie|Taille|Couleur|Matiere|Genre|Poids|Condition|Soldes|Promo_texte|Pourcentage_promo|Date_de_debut_promo|Date_de_fin_de_promo|Ecotaxe|Devise|gshopping_cat|c_price'."\r\n";

        file_put_contents($fileName, $content);
        $message = '';
        $products = $this->getProducts();
        foreach ($products as $product){
            try{
                $productString = '';
                $productManufacturer = $product->getResource()->getAttribute('manufacturer')->getFrontend()->getValue($product);
                $productIdentifiant =  strtoupper(substr($productManufacturer,0,3)) . '/' . $product->getSku() . '|';
                $productTitre = $product->getName() . '|';
                $productDecription = $product->getName() . ' ' . $productManufacturer . '|';
                $productPriceIncTax = round($this->getPriceInclTax($product, $product->getPrice()),2) . '|';
                $productSpecialPriceIncTax = ($product->getSpecialPrice() == 0) ? '|' : round($this->getPriceInclTax($product, $product->getSpecialPrice()),2) . '|';
                $productPriceSolde = '|';

                $productCategory = $this->getProductCategory($product->getCategoryCollection()->getItems());
                $catPath = explode(' > ', $productCategory);
                $productCategory1 = isset($catPath[0]) ? $catPath[0] . '|' : '|';
                $productCategory2 = isset($catPath[1]) ? $catPath[1] . '|' : '|';
                $productCategory3 = isset($catPath[2]) ? $catPath[2] . '|' : '|';

                $productUrl = $product->getProductUrl();
                $productImageUrl = $this->getProductImageUrl($product, 'product_base_image');

                $productEAN = $product->getData('ean') != 0 ? $product->getEan() . '|' : '|';
                $productMPN = $product->getId() != 0 ? $product->getId() . '|' : '|';

                $productFrais = $this->getProductShippingCost($product);
                $productGarantie = ($productManufacturer == 'Bosch') || ($productManufacturer == 'Festool') ? 3 : 2;
                $producWeight = round($product->getWeight(),2). '|';
                $productSpecialCost = empty($product->getCost()) ? '|' : round($product->getCost(), 2) . '|';
                $GshopingCategory = $this->getGshopingCategory($product->getCategoryCollection()->getItems());
                $productGshopingCategory = empty($GshopingCategory) ? '|' : $GshopingCategory . '|';


                $productString .= $productIdentifiant . $productTitre . $productDecription . $productPriceIncTax . $productSpecialPriceIncTax . $productPriceSolde;
                $productString .= $productCategory . '|' . $productCategory1 . $productCategory2 . $productCategory3;
                $productString .= $productUrl . '|' . $productImageUrl . '|';
                $productString .= $productEAN . $productMPN . $productManufacturer . '|';
                $productString .= $productFrais . '|' . '3-5|' . '|' . rand(1,9) .'|3-5|' . $productGarantie . '|' . '|' . '|' . '|' . '|';
                $productString .= $producWeight . '|' . '|' . '|' . '|' . '|' . '|' . '|' . 'EUR' . '|' . $productGshopingCategory . $productSpecialCost . "\r\n";

                file_put_contents($fileName, $productString, FILE_APPEND);
                $message = true;
            } catch (\Exception $e) {
                $this->_logger->critical($e->getMessage());
                $message .= $e->getMessage();
            }
        }
        return $message;
    }

    public function getGshopingCategory($categories)
    {
        $result = '';
        foreach ($categories as $category){
            if ($category->getData('level') == 4) {
                $result =  $category->getgshopping_cart();
            }
        }
        return $result;
    }

    public function getProductShippingCost($product)
    {
        $price = empty($product->getSpecialPrice()) ? $product->getPrice() : $product->getSpecialPrice();

        return $price > 144 ? 0 : 12;
    }

    public function getProductImageUrl($product, string $imageType= '')
    {
        $storeId = $this->_storeManager->getStore()->getId();
        $this->_appEmulation->startEnvironmentEmulation($storeId, \Magento\Framework\App\Area::AREA_FRONTEND, true);

        $imageBlock = $this->_blockFactory->createBlock('Magento\Catalog\Block\Product\ListProduct');
        $productImage = $imageBlock->getImage($product, $imageType);
        $imageUrl = $productImage->getImageUrl();

        $this->_appEmulation->stopEnvironmentEmulation();

        return $imageUrl;
    }

    public function getPriceInclTax($product, $price)
    {
        if ($taxAttribute = $product->getCustomAttribute('tax_class_id')) {
            // First get base price (=price excluding tax)
            $productRateId = $taxAttribute->getValue();
            $rate = $this->_taxCalculation->getCalculatedRate($productRateId);

            if ((int)$this->scopeConfig->getValue(
                    'tax/calculation/price_includes_tax',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE) === 1
            ) {
                // Product price in catalog is including tax.
                $priceExcludingTax = $price / (1 + ($rate / 100));
            } else {
                // Product price in catalog is excluding tax.
                $priceExcludingTax = $price;
            }

            $priceIncludingTax = $priceExcludingTax + ($priceExcludingTax * ($rate / 100));

            return $priceIncludingTax;
        }

        return $price;
    }

    private function getProductCategory($categories)
    {
        $categoryName = '';
        foreach ($categories as $category) {
            if ($category->getData('level') == 4) {
                $path = $category->getPath();
                $arrPath = explode('/', $path);
                unset($arrPath[0]);
                unset($arrPath[1]);
                foreach ($arrPath as $catId) {
                    $cat = $this->_categoryFactory->create()->load($catId);
                    $categoryName .= $cat->getName() . ' > ';
                }
                $categoryName = substr($categoryName, 0,-3);
            }
        }
        return $categoryName;
    }
    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param array $params
     * @return string
     */
    public function getFullProductUrl(\Magento\Catalog\Model\Product $product, $params = [])
    {
        $useCategoryUrl = $this->getStoreConfig(\Magento\Catalog\Helper\Product::XML_PATH_PRODUCT_URL_USE_CATEGORY);

        $routePath = '';
        $routeParams = $params;

        $storeId = 0;

        $categoryId = $_category = null;
        $categoryExcl = $this->getCategoryChildrenMargues();

        $cats = $product->getCategoryIds();
        if (count($cats)) {
            if ($cats[0] == 2) {
                $categoryId = $cats[1];
            } elseif (!in_array($cats[0], $categoryExcl)) {
                $categoryId = $cats[0];
            } else {
                $categoryId = end($cats);
            }
        }

        if ($categoryId) {
            $_category = $this->_categoryFactory->create()->load($categoryId);
        }

        $requestPath = $this->_productUrlPathGenerator->getUrlPathWithSuffix($product, $product->getStoreId(), $_category);

        if (isset($routeParams['_scope'])) {
            $storeId = $this->_storeManager->getStore($routeParams['_scope'])->getId();
        }

        if ($storeId != $this->_storeManager->getStore()->getId()) {
            $routeParams['_scope_to_url'] = true;
        }

        $routeParams['_direct'] = $requestPath;

        // reset cached URL instance GET query params
        if (!isset($routeParams['_query'])) {
            $routeParams['_query'] = [];
        }

        return $this->getUrlInstance()->setScope($storeId)->getUrl($routePath, $routeParams);
    }
    /**
     * @param $path
     * @param null $store
     * @param string $scope
     * @return mixed
     */
    public function getStoreConfig($path, $store = null, $scope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE)
    {
        $store = $this->_storeManager->getStore($store);
        return $this->scopeConfig->getValue(
            $path,
            $scope,
            $store
        );
    }
    /**
     * @return array
     */
    public function getCategoryChildrenMargues()
    {
        $catId = self::CATEGORY_ID_MARGUES;
        /** @var \Magento\Catalog\Model\Category $category */
        $category = $this->_categoryFactory->create();
        $category->load($catId);
        $children = $category->getChildrenCategories()->getAllIds();
        array_push($children, $catId);
        return $children;
    }

    /**
     * Retrieve URL Instance
     *
     * @return \Magento\Framework\UrlInterface
     */
    private function getUrlInstance()
    {
        return $this->_urlFactory->create();
    }
}