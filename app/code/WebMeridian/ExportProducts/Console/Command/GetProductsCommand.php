<?php

namespace WebMeridian\ExportProducts\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;

class GetProductsCommand extends Command
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    protected $_taxCalculation;
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    protected $_appState;

    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Framework\App\State $state
    ){
        $this->_storeManager = $storeManager;
        $this->_productFactory = $productFactory;
        $this->_appState = $state;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('webmer:exportproducts:getproducts')
            ->setDescription('Make export products to txt file');
        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->_appState->setAreaCode('adminhtml');
        $output->writeln('Start Export');
        $productCollection = $this->_productFactory->create()->getCollection()->addAttributeToSelect('*');

        $fileName = 'pub/media/products'.date('d-m-y').'.txt';
        $content = 'Identifiant_unique|Titre|Description|Prix_TTC|Prix_barre|Prix_solde|Categorie|Sous_categorie1|Sous_categorie2|Sous_categorie3|URL_produit|URL_image|EAN|MPN|Marque|Frais_de_livraison|Delais_de_livraison|Description_de_livraison|Quantite_en_Stock|Disponibilite|Garantie|Taille|Couleur|Matiere|Genre|Poids|Condition|Soldes|Promo_texte|Pourcentage_promo|Date_de_debut_promo|Date_de_fin_de_promo|Ecotaxe|Devise|gshopping_cat|c_price'."\r\n";
        file_put_contents($fileName, $content);

        foreach ($productCollection as $item) {
            $itemManufacturer = $item->getResource()->getAttribute('manufacturer')->getFrontend()->getValue($item);
            $itemIdentifiant =  strtoupper(substr($itemManufacturer,0,3)) . '/' . $item->getSku() . '|';
            $itemTitre = $item->getName() . '|';
            $itemDecription = $item->getName() . ' ' . $itemManufacturer . '|';
            $itemString = $itemIdentifiant . $itemTitre . $itemDecription;


            file_put_contents($fileName, $itemString . "\r\n",FILE_APPEND);
        }

        $output->writeln('End Export');
    }
}