<?php

namespace WebMeridian\ExportProducts\Controller\Adminhtml\System\Config;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;

class Export extends Action
{

    protected $_meridianHelper;

    public function __construct(
        Action\Context $context,
        \WebMeridian\ExportProducts\Helper\Data $helper
    ){
        $this->_meridianHelper = $helper;
        parent::__construct($context);
    }

    /**
     * Check admin permissions for this controller
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return true;
    }

    public function execute()
    {

        $result = $this->_meridianHelper->returnFileLink();
        $response = ['link' => $result];
        $this->getResponse()->setBody(json_encode($response));

    }
}