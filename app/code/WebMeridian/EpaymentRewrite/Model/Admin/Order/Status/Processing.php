<?php
namespace WebMeridian\EpaymentRewrite\Model\Admin\Order\Status;

use \Magento\Framework\App\Config\ScopeConfigInterface;

class Processing extends \Paybox\Epayment\Model\Admin\Order\Status\Processing
{
    protected $_stateStatuses = 'Pending Payment';

}