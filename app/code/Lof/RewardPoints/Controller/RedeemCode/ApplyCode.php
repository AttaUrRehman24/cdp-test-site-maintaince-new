<?php
/**
 * Landofcoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   Landofcoder
 * @package    Lof_RewardPoints
 * @copyright  Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license    http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\RewardPoints\Controller\RedeemCode;

use Lof\RewardPoints\Model\Email;
use Lof\RewardPoints\Model\Transaction;

class ApplyCode extends \Lof\RewardPoints\Controller\AbstractIndex
{
	/**
     * @var \Lof\RewardPoints\Model\ResourceModel\Transaction\CollectionFactory
     */
    protected $transactionCollectionFactory;

    /**
     * @var \Lof\RewardPoints\Helper\Data
     */
    protected $rewardsData;

    /**
     * @var \Lof\RewardPoints\Helper\Mail
     */
    protected $rewardsMail;

    /**
     * @var \Lof\RewardPoints\Model\Config
     */
    protected $rewardsConfig;

    /**
     * @param \Magento\Framework\App\Action\Context                               $context                      
     * @param \Lof\RewardPoints\Model\ResourceModel\Transaction\CollectionFactory $transactionCollectionFactory 
     * @param \Lof\RewardPoints\Helper\Data                                       $rewardsData                  
     * @param \Lof\RewardPoints\Helper\Mail                                       $rewardsMail                  
     * @param \Lof\RewardPoints\Model\Config                                      $rewardsConfig                
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Lof\RewardPoints\Model\ResourceModel\Transaction\CollectionFactory $transactionCollectionFactory,
        \Lof\RewardPoints\Helper\Data $rewardsData,
        \Lof\RewardPoints\Helper\Mail $rewardsMail,
        \Lof\RewardPoints\Model\Config $rewardsConfig
    ) {
        parent::__construct($context);
        $this->transactionCollectionFactory = $transactionCollectionFactory;
        $this->rewardsData                  = $rewardsData;
        $this->rewardsMail                  = $rewardsMail;
        $this->rewardsConfig                = $rewardsConfig;
    }

    public function execute()
    {
    	$post = $this->getRequest()->getPost();
    	if (isset($post['code'])) {
    		$escaper = $this->_objectManager->get('Magento\Framework\Escaper');
    		$code = $escaper->escapeHtml(trim($post['code']));
    		$transaction = $this->transactionCollectionFactory->create()
    		->addFieldToFilter('is_expired', [
                ['eq' => NULL],
                ['eq' => 0],
            ])->addFieldToFilter('customer_id', [
                ['eq' => NULL],
                ['eq' => 0],
            ])
            ->addFieldToFilter('code', ['eq' => $code])
            ->getFirstItem();

            if ($transaction->getId()) {
                $order = $this->_objectManager->create('Magento\Sales\Model\Order')->load($transaction->getOrderId());
                $status = $order->getStatus();
                if (in_array($status, $this->rewardsConfig->getGeneralEarnInStatuses())) {
                    $customer = $this->rewardsData->getCustomer();
                    $message  = __('You used reward code "%1".', $code);
                	$this->messageManager->addSuccess($message);
                	$transaction->setTitle($message)
                	->setCustomerId($customer->getId())
                	->setStatus(Transaction::STATE_COMPLETE)
                	->save();
                	$this->rewardsMail->sendNotificationBalanceUpdateEmail($transaction, '', Email::ACTION_APPLY_REWARD_CODE);
                }
            } else {
            	$this->messageManager->addError(__('The reward code "%1" is not valid.', $code));
            } 
    	}

        $this->_redirect('*/*/index');
        return;
    }

}