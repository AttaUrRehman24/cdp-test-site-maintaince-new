<?php
/**
 * Landofcoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   Landofcoder
 * @package    Lof_RewardPoints
 * @copyright  Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license    http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\RewardPoints\Model;

class Purchase extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Purchase cache tag
     */
    const CACHE_TAG = 'rewardpoints_purchase';
    
    const DISCOUNT  = 'discount';

    /**
     * @var string
     */
    protected $_cacheTag = 'rewardpoints_purchase';

    /**
     * @var \Magento\Quote\Model\Quote
     */
    protected $quote;

    /**
     * @var \Lof\RewardPoints\Helper\Data
     */
    protected $rewardsData;

    /**
     * @var \Lof\RewardPoints\Helper\Customer
     */
    protected $rewardsCustomer;

    /**
     * @var \Lof\RewardPoints\Logger\Logger
     */
    protected $rewardsLogger;

    /**
     * @param \Magento\Framework\Model\Context                               $context            
     * @param \Magento\Framework\Registry                                    $registry           
     * @param \Lof\RewardPoints\Model\ResourceModel\Purchase|null            $resource           
     * @param \Lof\RewardPoints\Model\ResourceModel\Purchase\Collection|null $resourceCollection 
     * @param \Lof\RewardPoints\Helper\Data                                  $rewardsData        
     * @param \Lof\RewardPoints\Helper\Customer                              $rewardsCustomer    
     * @param array                                                          $data               
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Lof\RewardPoints\Model\ResourceModel\Purchase $resource = null,
        \Lof\RewardPoints\Model\ResourceModel\Purchase\Collection $resourceCollection = null,
        \Lof\RewardPoints\Helper\Data $rewardsData,
        \Lof\RewardPoints\Helper\Customer $rewardsCustomer,
        \Lof\RewardPoints\Logger\Logger $rewardsLogger,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->rewardsData     = $rewardsData;
        $this->rewardsCustomer = $rewardsCustomer;
        $this->rewardsLogger   = $rewardsLogger;
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Lof\RewardPoints\Model\ResourceModel\Purchase');
    }

    public function toOptionArray($emptyOption = false)
    {
        return $this->getCollection()->toOptionArray($emptyOption);
    }

    public function setQuote($quote)
    {
        $this->quote = $quote;
        return $this;
    }

    public function getQuote()
    {
        return $this->quote;
    }

    public function getParams($code = '')
    {   
        $params = [];
        if ($this->getData('params')) {
            $params = unserialize($this->getData('params'));
        }
        if ($code && isset($params[$code])) {
            return $params[$code];
        }
        if ($code) {
            return;
        }
        return $params;
    }

    public function refreshDiscount()
    {
        $params   = $this->getParams();
        $discount = 0;

        // $object = new \Magento\Framework\DataObject(['discount' => $discount]);
        // $this->_eventManager->dispatch(
        //     'rewardpoints_purchase_refresh_discount',
        //     [
        //         'purchase' => $this,
        //         'obj'      => $object
        //     ]
        // );
        // $discount = $object->getDiscount();

        // $this->rewardsLogger->addError($discount . '|TEST');

        if (isset($params[\Lof\RewardPointsRule\Model\Config::SPENDING_CATALOG_RULE]['discount'])) {
            $discount += $params[\Lof\RewardPointsRule\Model\Config::SPENDING_CATALOG_RULE]['discount'];
        }
        if (isset($params[\Lof\RewardPointsRule\Model\Config::SPENDING_CART_RULE]['discount'])) {
            $discount += $params[\Lof\RewardPointsRule\Model\Config::SPENDING_CART_RULE]['discount'];
        }
        if (isset($params[Config::SPENDING_RATE]['discount']) && !isset($params[\Lof\RewardPointsRule\Model\Config::SPENDING_CART_RULE]['discount'])) {
            $discount += $params[Config::SPENDING_RATE]['discount'];
        }

        if (isset($params[Config::SPENDING_RATE]['discount'])) {
            $discount += $params[Config::SPENDING_RATE]['discount'];
        }

        // Debug
        if ($discount<0) {
            $this->rewardsLogger->addError('Purchase Discount is smaller than 0');
            $discount = 0;
        }

        $this->setData('discount', $discount);
        return $this;
    }

    public function verifyPointsWithCartItems()
    {
        $params     = $this->getParams();
        $quote      = $this->rewardsData->getQuote();
        $total      = $quote->getTotals();
        $price      = $total['subtotal']->getValue() + $this->getDiscount();
        $collection = $quote->getAllVisibleItems();
        $tmp = [];
        foreach ($collection as $item) {
            $tmp[strtolower($item->getSku())] = $item->getQty();
        }

        $object = new \Magento\Framework\DataObject(['params' => $params]);
        $this->_eventManager->dispatch(
            'rewardpoints_purchase_verify_points',
            [
                'purchase' => $this,
                'obj'   => $object,
                'quote' => $quote
            ]
        );
        $params = $object->getParams();


        // Spending product points
        if (isset($params[Config::SPENDING_PRODUCT_POINTS]['items'])) {
            $spendingProductPoints = $params[Config::SPENDING_PRODUCT_POINTS]['items']; 
            $newProductPoitns = [];
            $discount = 0;
            $points = 0;
            foreach ($spendingProductPoints as $sku => $item) {
                if (isset($tmp[$sku])) {
                    $item['qty']            = $tmp[$sku];
                    $discount               += ($item['qty'] * $item['discount']);
                    $points                 += ($item['qty'] * $item['points']);
                    $newProductPoitns[$sku] = $item;
                }
            }
            $params[Config::SPENDING_PRODUCT_POINTS]['items']    = $newProductPoitns;
            $params[Config::SPENDING_PRODUCT_POINTS]['points']   = $points;
            $params[Config::SPENDING_PRODUCT_POINTS]['discount'] = $discount;
        }

        $this->setParams($params);
        $this->refreshPoints();
        return $this;
    }


    public function refreshSpendingProductPoints()
    {
        $params = $this->getParams();
        if (isset($params[Config::SPENDING_PRODUCT_POINTS]['items'])) {
            $spendingProductPoitns = $params[Config::SPENDING_PRODUCT_POINTS]['items'];
            $discount = 0;
            $points = 0;
            foreach ($spendingProductPoitns as $item) {
                $discount += ($item['qty'] * $item['discount']);
                $points += ($item['qty'] * $item['points']);
            }
            $params[Config::SPENDING_PRODUCT_POINTS]['points'] = $points;
            $params[Config::SPENDING_PRODUCT_POINTS]['discount'] = $discount;
        }
        $this->setParams($params);
        return $this;
    }

    public function refreshSpendingRates()
    {
        $params = $this->getParams();
        $points     = 0;
        $discount   = 0;
        if (isset($params[Config::SPENDING_RATE]['rules'])) {
            $rules = $params[Config::SPENDING_RATE]['rules'];
            foreach ($rules as $ruleId => $rule) {
                if (isset($rule['status']) && $rule['status']) {
                    $points += (int) $rule['points'];
                    $discount += $rule['discount'];
                }
            }
            $params[Config::SPENDING_RATE]['rules']    = $rules;
            $params[Config::SPENDING_RATE]['discount'] = $discount; 
            $this->setSpendRatePoints($points);
        }
        $this->setParams($params);
        return $this;
    }

    

    public function refreshSpendPoints()
    {
        $params             = $this->getParams();
        $spendProductPoints = 0;
        if (isset($params[Config::SPENDING_PRODUCT_POINTS]['points'])) {
            $spendProductPoints = $params[Config::SPENDING_PRODUCT_POINTS]['points'];
        }
        $spendCatalogPoints = (int) $this->getData('spend_catalog_points');
        $spendCartPoints    = (int) $this->getData('spend_cart_points');
        $spendRatePoints    = (int) $this->getData('spend_rate_points');
        $spendPoints        = (int) ($spendCatalogPoints + $spendCartPoints + $spendProductPoints + $spendRatePoints);

        // Debug
        if ($spendPoints<0) {
            $this->rewardsLogger->addError('Purchase Spend Points is smaller than 0');
            $spendPoints = 0;
        }

        $this->setData('spend_points', $spendPoints);
        return $this;
    }

    public function refreshEarnPoints()
    {
        $earnCatalogPoints = (int) $this->getData('earn_catalog_points');
        $earnCartPoints    = (int) $this->getData('earn_cart_points');
        $earnPoints        = $earnCartPoints + $earnCatalogPoints;
        $this->setData('earn_points', $earnPoints);
        return $this;
    }

    public function getDiscount($fullDiscount = false) {
        $params   = $this->getParams();
        $discount = (int) $this->getData('discount');
        if ($fullDiscount && isset($params[Config::SPENDING_PRODUCT_POINTS]['discount'])) {
            $discount += $params[Config::SPENDING_PRODUCT_POINTS]['discount'];
        }
        return $discount;
    }

    public function setParams($newParams)
    {
        $params = [];
        if (is_array($params)) {
            $params = serialize($newParams);
        }
        $this->setData('params', $params);
        return $this;
    }


    /**
     * Refresh Points
     */
    public function refreshPoints()
    {
        // Refresh Earning Points
        $this->refreshEarningRatePoints();

        $this->_eventManager->dispatch(
            'rewardpoints_purchase_refresh_points',
            [
                'purchase' => $this
            ]
        );

        // Refresh Spending Points
        $this->refreshSpendingRates();
        $this->refreshSpendingProductPoints();
        $this->refreshSpendPoints();

        // Refresh Discount
        $this->refreshDiscount();


        if ($this->getData('discount') == 0) {
            $this->rewardsLogger->addError('test1');
        }

        return $this;
    }

    /**
     * Refresh Earning Points
     */
    public function refreshEarningRatePoints()
    {
        $params      = $this->getParams();
        $totalPoints = 0;
        if (isset($params[Config::EARNING_RATE]['rules'])) {
            $earningRate = $params[Config::EARNING_RATE];
            $rules       = $earningRate['rules'];
            foreach ($rules as $ruleId => $rule) {
                $rulePoints = 0;
                foreach ($rule['items'] as $productId => $product) {
                    $rulePoints += ($product['points'] * $product['qty']);
                }
                $rules[$ruleId]['points'] = $rulePoints;
                $totalPoints              += $rules[$ruleId]['points'];
            }
            $earningRate['rules']         = $rules;
            $earningRate['points']        = $totalPoints;
            $params[Config::EARNING_RATE] = $earningRate;
        }

        if (isset($params[Config::EARNING_PRODUCT_POINTS]['rules'])) {
            $earningProductPoints = [];
            if (isset($params[Config::EARNING_PRODUCT_POINTS]['rules']['items'])) {
                $earningProductPoints = $params[Config::EARNING_PRODUCT_POINTS]['rules']['items'];
            }
            $rulePoints = 0;
            foreach ($earningProductPoints as $sku => $item) {
                $rulePoints  += ($item['qty'] * $item['points']);
                $totalPoints += $rulePoints;
            }
            $params[Config::EARNING_PRODUCT_POINTS]['points'] = $rulePoints;
        }
        $this->setData('earn_catalog_points', $totalPoints);
        $this->refreshEarnPoints();
        $this->setParams($params);
        return $this;
    }

    public function resetFullData()
    {
        $this->setParams([]);
        $this->setEarnPoints(0);
        $this->setSpendPoints(0);
        $this->setEarnCatalogPoints(0);
        $this->setEarnCartPoints(0);
        $this->setSpendCartPoints(0);
        $this->setSpendCatalogPoints(0);
        $this->setSpendRatePoints(0);
        $this->setDiscount(0);
        $this->setSpendAmount(0);
        return $this;
    }
}