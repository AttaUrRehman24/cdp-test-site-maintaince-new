<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Lof\RewardPoints\Model;

use Lof\RewardPoints\Api\CreditManagementInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;


class CreditManagement implements CreditManagementInterface
{
    /**
     * Quote repository.
     *
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    protected $quoteRepository;
    
      /**
     * @var \Lof\RewardPoints\Helper\Checkout
     */
    protected $cart;
     /**
     * @var \Lof\RewardPoints\Helper\Checkout
     */
    protected $rewardsCheckout;
    
    /**
     * Constructs a coupon read service object.
     *
     * @param \Magento\Quote\Api\CartRepositoryInterface $quoteRepository Quote repository.
     */
    public function __construct(
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Lof\RewardPoints\Helper\Checkout $rewardsCheckout,
         \Magento\Checkout\Model\Cart $cart
    ) {
        $this->quoteRepository = $quoteRepository;
        $this->cart                = $cart;
         $this->rewardsCheckout     = $rewardsCheckout;
    }
    
    /**
     * {@inheritdoc}
     */
    public function get($cartId)
    {
        /** @var  \Magento\Quote\Model\Quote $quote */
        $quote = $this->quoteRepository->getActive($cartId);
        //return $quote->getCreditAmount();
    }
    
    /**
     * {@inheritdoc}
     */
    public function set($cartId, $creditAmount)
    {
        $this->rewardsCheckout->applyPoints($creditAmount);
        /** @var  \Magento\Quote\Model\Quote $quote */
        $quote = $this->quoteRepository->getActive($cartId);
        
        
            $quote->getShippingAddress()->setCollectShippingRates(true);
            $quote->collectTotals();
            $this->quoteRepository->save($quote);
        

       // return [$quote->getBaseCreditAmount(), $quote->getCreditAmount()];
    }
}
