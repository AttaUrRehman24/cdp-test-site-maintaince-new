<?php
namespace Lof\RewardPoints\Api;

interface RewardPointManagementInterface
{
    /**
     * GET for  total point
     * @param string $customer
     * @return string
     */
    public function getTotalCustomerPoints($customer);

    /**
     * GET spending total point by customer
     * @param string $customer_id, $order_id
     * @return string
     */
    public function getTotalSpentPoint();
    /**
     * GET for total earn points and total spent points by order id
     * @return string
     */
    public function getOrderEarnSpentPoints();
}