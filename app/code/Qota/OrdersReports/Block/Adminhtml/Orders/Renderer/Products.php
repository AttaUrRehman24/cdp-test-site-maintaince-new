<?php

namespace Qota\OrdersReports\Block\Adminhtml\Orders\Renderer;

class Products extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\Action
{

    protected $_coreHelper = null;
    protected $order;

    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        array $data = []
    )
    {
        parent::__construct($context, $jsonEncoder, $data);
    }

    public function render(\Magento\Framework\DataObject $row)
    {
       
		return $this->getProductName($row);
    }


    private function getProductName($order){
        $pname = $order->getName();
        if($pname){
                
                    return $pname;              
            }else{
        $items = $order->getAllItems();
        if($items){ 
   


            $total_qty = [];   
            $sku = [];   
                foreach($items as $itemId => $_item){                    
                    $name[][$itemId]= $_item->getName();
                }           
            $c=0;
            $html="<p>";               
                $cc=0;
                foreach($name as $itm){
                    $html.=($cc+1).' ) '.$itm[$cc].' <br/>';
                    $cc++;
                }                
            $html.="</p>";
            return $html;

            }
        }
    }

}