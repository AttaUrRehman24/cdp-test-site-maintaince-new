<?php

namespace Qota\OrdersReports\Block\Adminhtml\Orders;

use Magento\Backend\Block\Widget\Grid\Column;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    protected $_orderCollectionFactory;
    protected $timezone;
    protected $options;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Qota\OrdersReports\Block\Adminhtml\Orders\Options\Time $Time,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $date,
        array $data = []
    )
    {

        $this->_orderCollectionFactory=$orderCollectionFactory;        
        $this->optionszone=$date;        
        $this->options=$Time;        
        parent::__construct($context,$backendHelper,$data);
    }

    /**
     * Class constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('qotaGrid');
        $this->setDefaultSort('increment_id', 'DESC');
    }

    /**
     * @return $this
     */

    protected function _prepareCollection(){        
        $collection=  $this->_orderCollectionFactory->create();                     
        $this->setCollection($collection);
		parent::_prepareCollection();				
        return $this;            
    }

   
    /**
     * @return $this
     */


    protected function _prepareColumns()
    {
        //print_r($this->options->toOptionArray());

   

        $this->addColumn('increment_id', ['header' => __('Order ID #'), 'index' => 'increment_id', 'filter_index' => 'main_table.increment_id']);
        $this->addColumn('Products', ['header' => __(' Product Name'),'filter'=>false,  'renderer' => 'Qota\OrdersReports\Block\Adminhtml\Orders\Renderer\Products','filter_condition_callback' => [$this, '_filterCollection']]);
        $this->addColumn('ProductsPrice', ['header' => __('Margin'),'filter'=>false,'renderer' => 'Qota\OrdersReports\Block\Adminhtml\Orders\Renderer\ProductsPrice','filter_condition_callback' => [$this, '_filterCollectionPrice']]);
        $this->addColumn('ProductsSku', ['header' => __('Sku'),  'renderer' => 'Qota\OrdersReports\Block\Adminhtml\Orders\Renderer\ProductsSku','filter_condition_callback' => [$this, '_filterCollectionSku']]);
        $this->addColumn('created_at', ['header' => __('Created Date'), 'index' => 'created_at','type'=>'date','filter_index' => 'main_table.created_at']);
        $this->addColumn('updated_at', ['header' => __('Updated Date'), 'index' => 'updated_at','type'=>'date','filter_index' => 'main_table.updated_at']);       

        // $this->addColumn('cdays', ['header' => __('Created Date For'), 'index' => 'created_at','type'=>'options','options'=>$this->options->toOptionArray(),'filter_condition_callback' => [$this, '_filterCollectionTimeForStarting'],'filter_index' => 'main_table.created_at']);
        // $this->addColumn('udays', ['header' => __('Updated Date For'), 'index' => 'updated_at','type'=>'options','options'=>$this->options->toOptionArray(),'filter_condition_callback' => [$this, '_filterCollectionTimeForEnding'],'filter_index' => 'main_table.updated_at']);
         $this->addColumn('manufacturer', ['header' => __('Manufacturer'),'type'=>'options','options'=>$this->options->toOptionAttrArray(),'filter_condition_callback' => [$this, '_filterCollectionManufacturer'],'renderer' => 'Qota\OrdersReports\Block\Adminhtml\Orders\Renderer\Manufacturer']);

        $this->addColumn('status', ['header' => __('Status'), 'index' => 'status','type'=>'options','options'=>array(''=>'Select Options','canceled'=>'Annulée','closed'=>'Terminée','commande_livree'=>'Livré','complete'=>'Envoyée','fraud'=>'Bloquée','holded'=>'en Anomalie','lengow_technical_error'=>'Lengow Technical Error','new'=>'Nouveau','payment_review'=>'Vérif. en cours','paypal_canceled_reversal'=>'PayPal Canceled Reversal','paypal_reversed'=>'PayPal Reversed','pendig_payment_3xsansfrais'=>'En attente de paiement 3x sans frais','pending'=>'En attente de paiement','pendingpayment'=>'En attente de paiement','pending_payment'=>'En attente de paiement','pending_paypal'=>'Pending PayPal','processing'=>'Validée'),'filter_index' => 'main_table.status']);      
        


        $this->addExportType($this->getUrl('*/*/exportCsv', ['_current' => true]),__('CSV'));
        $this->addExportType($this->getUrl('*/*/exportExcel', ['_current' => true]),__('Excel XML'));
                  $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }
        return parent::_prepareColumns();

    }

    protected function _filterCollectionSku($collection, $column)
    {
            $value = trim($column->getFilter()->getValue());                                
            $collection->join(
                ["soi" => "sales_order_item"],
            'main_table.entity_id = soi.order_id AND soi.product_type in ("simple","downloadable") ',
            array('sku', 'name','price'))->addFieldToFilter(
                'sku',
                $value
            );
            return $this;
    }

    protected function _filterCollectionTimeForStarting($collection, $column)
    {
            $today= date("Y-m-d H:i:s");
            $value = trim($column->getFilter()->getValue()); 
            $collection->addFieldToFilter('main_table.created_at',['from'=>$value,'to'=>$today]);
            return $this;
    }

    protected function _filterCollectionTimeForEnding($collection, $column)
    {
            $today= date("Y-m-d H:i:s");
            $value = trim($column->getFilter()->getValue()); 
            $collection->addFieldToFilter('main_table.updated_at',['from'=>$value,'to'=>$today]);
            return $this;
    }


    protected function _filterCollectionManufacturer($collection, $column)
    {
        $value = trim($column->getFilter()->getValue()); 
        $req=$column->getFilter()->getData();      
       $d=$collection->join(
            ["soi" => "sales_order_item"],
        'main_table.entity_id = soi.order_id AND soi.product_type in ("simple","downloadable")',array('sku', 'name','price'))->join(
            ["cpie" => "catalog_product_index_eav"],
            'soi.product_id = cpie.entity_id',array('value','entity_id','attribute_id') 
        )->addFieldToFilter(
            'attribute_id','173'
        )->addFieldToFilter(
            'value',$value
        );
        return $this;
   }
    
  

    
}