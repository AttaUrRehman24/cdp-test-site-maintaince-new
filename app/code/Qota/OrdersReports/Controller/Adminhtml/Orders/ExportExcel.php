<?php

namespace Qota\OrdersReports\Block\Adminhtml\Orders;

use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Response\Http\FileFactory;

class ExportExcel extends \Magento\Backend\App\Action
{
    protected $_fileFactory;

    public function execute()
    {
        try{

        $fileName   = 'modele.xls';
        //$content    = $this->getLayout()->createBlock('inventory/adminhtml_modele_grid')->getCsv();
        $content    = $this->_view->getLayout()->createBlock('\Qota\Account\Block\Adminhtml\Lines\Grid')->getCsvFile();
  
        $file = fopen('/home/www/comptoirdespros.com/var/'.$content['value'],"r");
        $csv_final_content = array();
        $i = 0;
        $csv_head = '';

        $headsers = [];
		
        while(! feof($file))
          {
            $excel_content = fgetcsv($file);
			
			if($i == 0) {
                $headsers = $excel_content;
			}
            
            $inovice_id = $excel_content[0];
            $order_id = $excel_content[1];
            $date = $excel_content[2];
            $compte_general = explode('<br>',$excel_content[3]);
            $sens = explode('<br>',$excel_content[4]);
            $montant = explode('<br>',$excel_content[5]);
            $credit_inovice_id = $excel_content[6];            
            $credit_date = $excel_content[7];
            $credit_montant = explode('<br>',$excel_content[8]);
            $credit_sens = explode('<br>',$excel_content[9]);
            $journal = $excel_content[10];
            $date_echeance = explode('<br>',$excel_content[11]);

            if($i != 0){
                $csv_final_content[0] = $headsers;
                foreach($compte_general as $key => $value){
                    if($value != ""){
                        $csv_final_content[] = array(
                            $headsers[0] => $inovice_id,
							$headsers[1] => $order_id,
							$headsers[2] => $date,
                            $headsers[3] => trim($value),
                            $headsers[4] => (isset($sens[$key]) ? trim($sens[$key]) : ''),
                            $headsers[5] => (isset($montant[$key]) ? trim($montant[$key]) : ''),                            
                            $headsers[6] => $credit_inovice_id,
                            $headsers[7] => $credit_date,
                            $headsers[8] => (isset($credit_montant[$key]) ? trim($credit_montant[$key]) : ''),							
                            $headsers[9] => (isset($credit_sens[$key]) ? trim($credit_sens[$key]) : ''),
							$headsers[10] => $journal,
							$headsers[11] => (isset($date_echeance[$key]) ? $date_echeance[$key] : '')                            
                        );
                    }
                }
            }else{
                $csv_head = $excel_content;
            }
            $i++;
          }
        //   echo '<pre>';
        //   print_r($csv_final_content);
        //   exit();
            
			$date = $this->_objectManager->create('\Magento\Framework\Stdlib\DateTime\DateTime')->date('Y-m-d_H-i-s');
          	$this->convert_to_excel($csv_final_content, $date . '.xls', '\t');

        }catch(\Exception $e){
            $this->messageManager->addError(__('An error occurred : '.$e->getMessage()));
            $this->_redirect('*/*/index');
        }
    }

    function convert_to_excel($input_array, $output_file_name, $delimiter)
	{
        $temp_memory = fopen("php://memory", 'w');
        foreach ($input_array as $line)
        {
            fputcsv($temp_memory, $line,"\t");
        }
        //fclose($temp_memory);
		// $temp_memory = fopen('php://memory', 'w');
		// // loop through the array
		// foreach ($input_array as $line) {
		// // use the default csv handler
		// fputcsv($temp_memory, $line, $delimiter);
		// }
		fseek($temp_memory, 0);
        // modify the header to be CSV format
        header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
        header("Content-Disposition: attachment; filename=" . $output_file_name);  //File name extension was wrong
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private",false);
		// output the file to be downloaded
		fpassthru($temp_memory);
	}

    function convert_to_csv($input_array, $output_file_name, $delimiter)
	{
		$temp_memory = fopen('php://memory', 'w');
		// loop through the array
		foreach ($input_array as $line) {
		// use the default csv handler
		fputcsv($temp_memory, $line, $delimiter);
		}

		fseek($temp_memory, 0);
		// modify the header to be CSV format
		header('Content-Type: application/csv');
		header('Content-Disposition: attachement; filename="' . $output_file_name . '";');
		// output the file to be downloaded
		fpassthru($temp_memory);
	}

    public function execute1()
    {
        $this->_view->loadLayout(false);

        $fileName = 'account.xls';

        $exportBlock = $this->_view->getLayout()->createBlock('\Qota\Account\Block\Adminhtml\Lines\Grid');

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $this->_fileFactory = $objectManager->create('Magento\Framework\App\Response\Http\FileFactory');


        return $this->_fileFactory->create(
            $fileName,
            $exportBlock->getExcelFile(),
            DirectoryList::VAR_DIR
        );
    }
}