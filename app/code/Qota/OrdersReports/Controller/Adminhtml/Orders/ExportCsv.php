<?php

namespace Qota\OrdersReports\Block\Adminhtml\Orders;

use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Response\Http\FileFactory;

class ExportCsv extends \Magento\Backend\App\Action
{
    protected $_fileFactory;

    public function execute()
    {
        try{

        $fileName   = 'modele.csv';
        //$content    = $this->getLayout()->createBlock('inventory/adminhtml_modele_grid')->getCsv();
        $content    = $this->_view->getLayout()->createBlock('\Qota\OrdersReports\Block\Adminhtml\Orders\Grid')->getCsvFile();
  
        $file = fopen('/home/www/comptoirdespros.com/var/'.$content['value'],"r");
        $csv_final_content = array();
        $i = 0;
        $csv_head = '';
		
        while(! feof($file))
          {
            $csv_content = fgetcsv($file);
            
			if($i == 0) {
				$csv_final_content[] = $csv_content;
			}
			
            $inovice_id = $csv_content[0];
            $order_id = $csv_content[1];
            $date = $csv_content[2];
            $compte_general = explode('<br>',$csv_content[3]);
            $sens = explode('<br>',$csv_content[4]);
            $montant = explode('<br>',$csv_content[5]);
            $credit_inovice_id = $csv_content[6];            
            $credit_date = $csv_content[7];
            $credit_montant = explode('<br>',$csv_content[8]);
            $credit_sens = explode('<br>',$csv_content[9]);
            $journal = $csv_content[10];
            $date_echeance = explode('<br>',$csv_content[11]);
            
            if($i != 0){
                foreach($compte_general as $key => $value){
                    if($value != ""){						
						$csv_final_content[] = array(
							$inovice_id,
							$order_id,
							$date,
                            trim($value),
                            (isset($sens[$key]) ? trim($sens[$key]) : ''),
                            (isset($montant[$key]) ? trim($montant[$key]) : ''),                            
                            $credit_inovice_id,
                            $credit_date,
                            (isset($credit_montant[$key]) ? trim($credit_montant[$key]) : ''),							
                            (isset($credit_sens[$key]) ? trim($credit_sens[$key]) : ''),
							$journal,
							(isset($date_echeance[$key]) ? $date_echeance[$key] : '')
						);
                    }
                }
            }else{
                $csv_head = $csv_content;
            }
            $i++;
          }
        //   echo'<pre>';
        //   print_r($csv_final_content);
        //   exit();
			$date = $this->_objectManager->create('\Magento\Framework\Stdlib\DateTime\DateTime')->date('Y-m-d_H-i-s');
          	$this->convert_to_csv($csv_final_content, $date . '.csv', ',');

        }catch(\Exception $e){
            $this->messageManager->addError(__('An error occurred : '.$e->getMessage()));
            $this->_redirect('*/*/index');
        }
    }

    function convert_to_csv($input_array, $output_file_name, $delimiter)
	{
		$temp_memory = fopen('php://memory', 'w');
		// loop through the array
		foreach ($input_array as $line) {
		// use the default csv handler
		fputcsv($temp_memory, $line, $delimiter);
		}

		fseek($temp_memory, 0);
		// modify the header to be CSV format
		header('Content-Type: application/csv');
		header('Content-Disposition: attachement; filename="' . $output_file_name . '";');
		// output the file to be downloaded
		fpassthru($temp_memory);
	}

    public function execute1()
    {
        try{

    
           $csv =  str_replace("<br>", "\r\n", $this->_view->getLayout()->createBlock('\Qota\OrdersReports\Block\Adminhtml\Orders\Grid')->getCsv());

            $date = $this->_objectManager->create('\Magento\Framework\Stdlib\DateTime\DateTime')->date('Y-m-d_H-i-s');

            return $this->_objectManager->get('\Magento\Framework\App\Response\Http\FileFactory')->create(
                'supplier_return' . $date . '.csv',
                $csv,
                DirectoryList::VAR_DIR,
                'application/csv'
            );
        }catch(\Exception $e){
            $this->messageManager->addError(__('An error occurred : '.$e->getMessage()));
            $this->_redirect('*/*/index');
        }
    }
}