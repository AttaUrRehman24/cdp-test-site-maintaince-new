<?php
namespace Qota\AdditionalFieldorderlist\Cron;
use Magento\Sales\Model\Order;
class Orderstatus
{
    protected $logger;
    protected $collectionFactory;

    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $collectionFactory){
        
        $this->logger = $logger;
        $this->collectionFactory = $collectionFactory;

    }
    
    public function execute()
    {
      
        $this->SalesList();
       
           
    }

    public function SalesList(){
        
        $ThirtyMints = date("Y-m-d H:i:s", strtotime("+30 minutes"));
        $collection = $this->collectionFactory->create();
        $collection->addAttributeToSelect('increment_id');                     
        $collection->addAttributeToSelect('created_at'); 
        $collection->addAttributeToSelect('entity_id'); 
        $collection->getSelect()->join(["sop"=>"sales_order_payment"],'main_table.entity_id = sop.parent_id',array('method'))
       ->where("status='pending' AND method='pbxep_cb' OR method='pbxep_threetime' AND created_at >= '".$ThirtyMints."'");      
       $ids=[];

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
       
        if(count($collection)){
            foreach ($collection as $orderId) {
                $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
                $connection = $resource->getConnection();
                $tableName = $resource->getTableName('sales_order'); //gives table name with 
                $sales_order_grid = $resource->getTableName('sales_order_grid'); //gives table name with 
                $sales_order_status_history = $resource->getTableName('sales_order_status_history'); //gives table name with 
                $sql = "Update `" . $tableName . "` Set state ='canceled', status = 'canceled'  where increment_id =".$orderId->getIncrementid();
                $connection->query($sql);               
                $sales_order_grid_sql = "Update `" . $sales_order_grid . "` Set status = 'canceled'  where increment_id =".$orderId->getIncrementid();
                $connection->query($sales_order_grid_sql);                            
                 $sales_order_status_history_sql ="Insert into `".$sales_order_status_history."` (parent_id,is_customer_notified,comment,status,entity_name) values('".$orderId->getEntityid()."','1','cron updated','canceled','order')"; 
                 $connection->query($sales_order_status_history_sql);
                 $this->logger->addInfo('Empty'.$orderId->getEntityid());  
            }  

        }else{
            $this->logger->addInfo('Empty');  
            
        }
    }

  
}