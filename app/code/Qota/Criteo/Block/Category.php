<?php
/**
 * @author Muhammad Naseem <naseem@redsignal.biz>
 */
namespace Qota\Criteo\Block;

class Category extends Base
{
    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;

    /**
     * Category constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Qota\Criteo\Helper\Data $helper
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Qota\Criteo\Helper\Data $helper,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->_registry = $registry;
        parent::__construct($context, $helper, $data);
    }

    public function getItemsJson()
    {
        $items = $this->_registry->registry('criteo_catalog_block_product_list_collection');
        $arr = array();
        if ($items) {
            $i = 0;
            foreach ($items as $item) {
                // if (++$i > 3) {
                //     break;
                // }
                if ($item->getTypeId() == 'simple') {
                	$arr[] = $item->getSku(); 	
                 }                
                $i++;
            }
        }
        return json_encode($arr);        
    }
}
