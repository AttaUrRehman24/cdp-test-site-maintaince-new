<?php

namespace Qota\Account\Block\Adminhtml\Lines;

use Magento\Backend\Block\Widget\Grid\Column;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    //protected $_accountCollection;
    protected $_storeCollectionFactory;
    protected $_userList = null;
    protected $_orderCollectionFactory;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        //\Qota\Account\Model\ResourceModel\Lines\CollectionFactory $accountCollection,
        \Magento\Store\Model\ResourceModel\Store\CollectionFactory $storeCollectionFactory,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\User\Model\ResourceModel\User\CollectionFactory $userList,
        array $data = []
    ) {

        parent::__construct($context, $backendHelper, $data);

        //$this->_accountCollection = $accountCollection;
        $this->_storeCollectionFactory = $storeCollectionFactory;
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->_userList = $userList;
    }

    /**
     * Class constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('qotaGrid');
        $this->setDefaultSort('increment_id', 'DESC');
        //$this->setDefaultDir('DESC');
        //$this->setTitle(__('hello'));
    }


    /**
     * @return $this
     */
    
    protected function _prepareCollection(){
			$collection= $this->_orderCollectionFactory->create()
                        ->addAttributeToSelect('increment_id')                      
                        ->addAttributeToSelect('entity_id')                     
						->addAttributeToSelect('state')						
						->addAttributeToSelect('created_at')
						->addAttributeToSelect('base_grand_total')
						->addAttributeToSelect('base_subtotal')
						->addAttributeToSelect('base_tax_amount')
						->addAttributeToSelect('base_discount_invoiced')
						->addAttributeToSelect('base_discount_amount')
                        ->addAttributeToSelect('shipping_amount');
                        

			//$collection->getSelect()->join(array('so' => $this->getResource()->getTable('sales_invoice')), 'sales_invoice.order_id= main_table.entity_id ',array('invoice_increment_id' => 'sales_invoice.increment_id'));
            $collection->getSelect()->where('main_table.total_invoiced!="NULL"')
            ->joinLeft('sales_order_payment', 'main_table.entity_id = sales_order_payment.parent_id',array('payment_method' => 'sales_order_payment.method'))
            ->joinLeft('sales_creditmemo', 'main_table.entity_id = sales_creditmemo.order_id',array('credit_order_id' => 'sales_creditmemo.order_id','credit_created_at' => 'sales_creditmemo.created_at','credit_grand_total'=>'sales_creditmemo.grand_total'));

            $this->setCollection($collection);

			parent::_prepareCollection();				
			return $this;
	}	
    

    /**
     * @return $this
     */

    protected function _prepareColumns()
    {

        $this->addColumn('increment_id', ['header' => __('Order #'), 'index' => 'increment_id', 'filter_index' => 'main_table.increment_id']);
        $this->addColumn('invoice_increment_id', ['header' => __('Invoice Increment Id'), 'filter' => false, 'renderer' => 'Qota\Account\Block\Adminhtml\Lines\Renderer\Invoice']);
        $this->addColumn('created_at', ['header' => __('Order Created Date'), 'index' => 'created_at','type'=>'date','filter_index' => 'main_table.created_at']);
        $this->addColumn('compte_general', ['header' => __('Compte General'), 'filter' => false, 'renderer' => 'Qota\Account\Block\Adminhtml\Lines\Renderer\CompteGeneral']);
        $this->addColumn('sens', ['header' => __('Sens'), 'filter' => false, 'renderer' => 'Qota\Account\Block\Adminhtml\Lines\Renderer\Sens']);
        $this->addColumn('montant', ['header' => __('Montant'), 'filter' => false, 'renderer' => 'Qota\Account\Block\Adminhtml\Lines\Renderer\Montant']);
        $this->addColumn('credit_increment_id', ['header' => __('Credit Invoice Id'), 'filter' => false, 'renderer' => 'Qota\Account\Block\Adminhtml\Lines\Renderer\CreditInvoice']);
        $this->addColumn('credit_created_at', ['header' => __('Credit Note Created Date'), 'index' => 'credit_created_at','type'=>'date','filter_index' => 'sales_creditmemo.created_at']);
        $this->addColumn('credit_montant', ['header' => __('Credit Note Montant'), 'filter' => false, 'renderer' => 'Qota\Account\Block\Adminhtml\Lines\Renderer\CreditMontant']);
        $this->addColumn('credit_sens', ['header' => __('Credit Sens'), 'filter' => false, 'renderer' => 'Qota\Account\Block\Adminhtml\Lines\Renderer\CreditSens']);
        $this->addColumn('journal', ['header' => __('Journal'), 'filter' => false, 'renderer' => 'Qota\Account\Block\Adminhtml\Lines\Renderer\Journal']);
        $this->addColumn('date_heance', ['header' => __('date d\'échéance'), 'filter' => false, 'renderer' => 'Qota\Account\Block\Adminhtml\Lines\Renderer\Dateecheance','type'=>'date']);        
        $this->addExportType($this->getUrl('*/*/exportCsv', ['_current' => true]),__('CSV'));
        $this->addExportType($this->getUrl('*/*/exportExcel', ['_current' => true]),__('Excel XML'));
                  $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }
        return parent::_prepareColumns();

    }

   public function getRowUrl($row)
    {
        return '';//$this->getUrl('sales/order/view', ['order_id' => $row->getqota_comments_id()]);
    }   

    public function getStoreOptions()
    {
        $options = [];
        $options[''] = ' ';
        foreach($this->_storeCollectionFactory->create() as $item)
        {
            $options[$item->getId()] = $item->getname();
        }
        return $options;
    }

    public function getManagerOptions()
    {
        $users = [];
        foreach($this->_userList->create() as $user)
            $users[$user->getId()] = $user->getusername();
        return $users;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }


}