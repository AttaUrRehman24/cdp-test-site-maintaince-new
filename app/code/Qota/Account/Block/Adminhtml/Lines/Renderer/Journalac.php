<?php

namespace Qota\Account\Block\Adminhtml\Lines\Renderer;

class Journalac extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{

    protected $_coreHelper = null;

    public function __construct(
        \Magento\Backend\Block\Context $context,
        array $data = []
    )
    {

        parent::__construct($context, $data);
    }
    public function render(\Magento\Framework\DataObject $row)
    {
       $html = '<p>'.$row->getbase_tax_amount().'</br>'.$row->getbase_grand_total().'</br>'.$row->getbase_subtotal().'</p>';
        return $html;

    }


}
