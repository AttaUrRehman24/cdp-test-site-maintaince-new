<?php

namespace Qota\Account\Block\Adminhtml\Lines\Renderer;

class Action extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\Action
{

    protected $_coreHelper = null;

    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        array $data = []
    )
    {

        parent::__construct($context, $jsonEncoder, $data);
    }

    public function render(\Magento\Framework\DataObject $row)
    {

        $actions = [
            [// Preview 'caption' => __('Preview (%1 Order)', $this->getUrl("sales/order/view",['order_id' => $row->getqota_comments_id()])),
                'caption' => __('Accepted'),
                'url' => $this->getUrl("cancel/comments/Accept",['order_id' => $row->getqota_comments_id(),'qota_status' => $row->getqota_status()]),
                'field' => 'id',
                'popup' => true
            ],
            [// Edit
                'caption' => __('Decline'),
                'url' => [
                    'base' => '*/*/Decline'
                ],
                'field' => 'id',
            ],
            [// Preview 'caption' => __('Preview (%1 Order)', $this->getUrl("sales/order/view",['order_id' => $row->getqota_comments_id()])),
                'caption' => __('Preview ( Order )'),
                'url' => $this->getUrl("sales/order/view",['order_id' => $row->getqota_comments_id()]),
                'field' => 'id',
                'popup' => true
            ]
        ];


        $this->getColumn()->setActions($actions);
        return parent::render($row);
    }
}
