<?php

namespace Qota\Account\Block\Adminhtml\Lines\Renderer;

class CreditSens extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\Action
{

    protected $_coreHelper = null;
    protected $_resource;

    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Framework\App\ResourceConnection $resource,
        array $data = []
    )
    {
    	$this->_resource = $resource;
        parent::__construct($context, $jsonEncoder, $data);
    }

    public function render(\Magento\Framework\DataObject $row)
    {
		$payment_code = $row->getPaymentMethod();
		$grandTotal = $row->getcredit_grand_total();
		$return_str= '';
		if($grandTotal > '0'){
			if($payment_code == "pbxep_threetime" || $payment_code == 'comnpaypnf'){
				$return_str = "C<br>C<br>C<br>D<br>";
			}else{
				$return_str = "C<br>D<br>";
			}

			$connection  = $this->_resource->getConnection();
			$tableName   = $connection->getTableName('lof_rewardpoints_transaction'); // It will return "tabledata"
			$mapsDeleteQuery = "select params from ".$tableName.' where order_id='.$row->getEntity_id();
			$res=$connection->fetchAll($mapsDeleteQuery); 
			if($res){
				$params=unserialize($res[0]['params']);
				if(isset($params['spending_cart_rule']) && isset($params['spending_cart_rule']['discount'])) {
					$discountAmount = $params['spending_cart_rule']['discount'];
				}else {
					$discountAmount = 0;	
				}	
			}else{
				$discountAmount = 0;
			}		
			//$discountAmount = abs($row->getBaseDiscountAmount());
			if($discountAmount > 0){
				$return_str .= "C<br>";
			}
			$return_str .= "D<br>D";
		}else{
			$return_str= '<br>';
		}
		return $return_str;
    }
}