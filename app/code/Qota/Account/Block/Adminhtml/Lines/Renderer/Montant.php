<?php

namespace Qota\Account\Block\Adminhtml\Lines\Renderer;

class Montant extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\Action
{

    protected $_coreHelper = null;
 	protected $_resource;

    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Framework\App\ResourceConnection $resource,
        array $data = []
    )
    {
    	$this->_resource = $resource;
        parent::__construct($context, $jsonEncoder, $data);
    }

    public function render(\Magento\Framework\DataObject $row)
    {
		$connection  = $this->_resource->getConnection();
	    $tableName   = $connection->getTableName('lof_rewardpoints_transaction'); // It will return "tabledata"
	    $mapsDeleteQuery = "select params from ".$tableName.' where order_id='.$row->getEntity_id();
	   	$res=$connection->fetchAll($mapsDeleteQuery); 
	   	if($res){
			$params=unserialize($res[0]['params']);
			if(isset($params['spending_cart_rule']) && isset($params['spending_cart_rule']['discount'])) {
				$discountAmount = $params['spending_cart_rule']['discount'];
			}else{
				$discountAmount = 0;
			}
	   	}else{
	   		$discountAmount=0;
	   	}

    	$payment_code = $row->getPaymentMethod();
		if($payment_code == "pbxep_threetime" || $payment_code == 'comnpaypnf'){
			$amount = $row->getBaseGrandTotal() / 3;
			$return_str = number_format($amount,2,'.','').'<br>';
			$return_str .= number_format($amount,2,'.','').'<br>';
			$return_str .= number_format($amount,2,'.','').'<br>';
		}else{
			$return_str = number_format($row->getBaseGrandTotal(),2,'.','').'<br>';
		}
		$return_str .= number_format($row->getBaseSubtotal(),2,'.','').'<br>';
		//$discountAmount = abs($row->getBaseDiscountAmount());
		if($discountAmount > 0){
			$return_str .= number_format(($discountAmount/1.2),2,'.','').'<br>';
		}
		$return_str .= number_format($row->getShippingAmount(),2,'.','').'<br>';
		//$return_str .= number_format($row->getBaseTaxAmount(),2,'.','');
		$return_str .= number_format((($row->getBaseGrandTotal()/1.2)*0.2),2,'.','');
		return $return_str;
    }
}