<?php

namespace Qota\Account\Block\Adminhtml\Lines\Renderer;

class Dateecheance extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\Action
{

    protected $_coreHelper = null;

    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        array $data = []
    )
    {

        parent::__construct($context, $jsonEncoder, $data);
    }

    public function render(\Magento\Framework\DataObject $row)
    {
    	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    	$order_load = $objectManager->create('Magento\Sales\Model\Order')->loadByIncrementId($row->getIncrementId());
    	//$payment_code = $row->getPayment()->getMethodInstance()->getCode();
		$payment_code = $row->getPaymentMethod();
		$payment_data = $order_load->getPayment()->getPbxepAuthorization();

		//echo"<pre>";
		$payment_data = unserialize($payment_data);
		//var_dump($payment_data);
	
		$discoundAmount = abs($row->getBaseDiscountAmount());
		
		if($payment_code == 'pbxep_cb' || $payment_code == 'comnpay'){
			$return_str = $payment_data['date'].'<br>';			
		}elseif($payment_code == 'pbxep_threetime' || $payment_code == 'comnpaypnf'){
			$return_str = $payment_data['date'].'<br>';
			$payment_data['date'] = preg_replace('/^([0-9]{2})([0-9]{2})([0-9]{4})$/', '$1-$2-$3', $payment_data['date']);
			$return_str .= date("dmY",strtotime($payment_data['date'] . " +1 month")).'<br>';
			$return_str .= date("dmY",strtotime($payment_data['date'] . " +2 month"));
		}else {
			$return_str = '';
		}
		
		//print_r(unserialize($return_str));		
		return $return_str;
		
    }
}