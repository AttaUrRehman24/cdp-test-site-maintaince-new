<?php

namespace Qota\Account\Block\Adminhtml\Lines\Renderer;

class CompteGeneral extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\Action
{

    protected $_coreHelper = null;

    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        array $data = []
    )
    {

        parent::__construct($context, $jsonEncoder, $data);
    }

    public function render(\Magento\Framework\DataObject $row)
    {
    	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    	$order_load = $objectManager->create('Magento\Sales\Model\Order')->loadByIncrementId($row->getIncrementId());
		//$payment_code = $row->getPayment()->getMethodInstance()->getCode();
		$payment_code = $row->getPaymentMethod();
		$return_str = '';
		
		if($payment_code == 'paybox_system' || $payment_code == 'pbxep_cb'){
			$return_str .= '580001<br>';
		}elseif($payment_code == 'pbxep_threetime'){
			$return_str .= '580001<br>';
			$return_str .= '580001<br>';
			$return_str .= '580001<br>';
		}elseif($payment_code == 'bankpayment' || $payment_code == 'redbankpayment'){
			$return_str .= '580002<br>';
		}elseif($payment_code == 'paypal_standard' || $payment_code == 'paypal_express' ){
			$return_str .= '580003<br>';
		}elseif($payment_code == 'checkmo'){
			$return_str .= '580004<br>';
		}elseif(substr($payment_code,0,3) == 'kwx' || $payment_code == 'lengow'){
			$return_str .= '580005<br>';
		}
		
		$frnace_array = array('FR','GB','ES','IT','DE');
		$intra_array = array("BE","LU","NL","MC");
		$dom_export_array = array("SA","GP","GF","MQ","YT","PF","RE");
		$ship_country_code = $order_load->getShippingAddress()->getCountryId();
		
		$discoundAmount = abs($row->getBaseDiscountAmount());
		
		if(in_array($ship_country_code,$frnace_array)){
			if($row->getBaseTaxAmount() > 0){
				/// category France (take from xsl) with TAX
				$return_str .= '707001<br>';
				if($discoundAmount > 0){
					$return_str .= '707001<br>';
				}
				$return_str .= '708501<br>';
			}else{
				/// category DOM+export (take from xsl) because tax == 0 with France
				$return_str .= '707900<br>';
				if($discoundAmount > 0){
					$return_str .= '707900<br>';
				}
				$return_str .= '708590<br>';
			}
			
		}elseif(in_array($ship_country_code,$intra_array)){
			if($row->getBaseTaxAmount() > 0){
				/// category Intracommunautaire (take from xsl) with TAX
				$return_str .= '707920<br>';
				if($discoundAmount > 0){
					$return_str .= '707920<br>';
				}
				$return_str .= '708591<br>';
			}else{
				/// category Franchise TVA (take from xsl) because tax == 0 with Intracommunautaire
				$return_str .= '707910<br>';
				if($discoundAmount > 0){
					$return_str .= '707910<br>';
				}
				$return_str .= '708592<br>';
			}
			
		}elseif(in_array($ship_country_code,$dom_export_array)){
			/// category DOM+export (take from xsl) with HT
			$return_str .= '707900<br>';
			if($discoundAmount > 0){
				$return_str .= '707900<br>';
			}
			$return_str .= '708590<br>';
		}
		
		$return_str .= '445720';
		
		return $return_str;
    }
}