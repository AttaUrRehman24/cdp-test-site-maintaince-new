<?php

/**
 * Copyright © 2018 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\ElasticsearchAutocomplete\Block\Search;

/**
 * Display the categories search results in the search results page
 * @package Wyomind\ElasticsearchAutocomplete\Block\Search
 */
class Category extends \Magento\Framework\View\Element\Template
{

    /**
     * @var \Magento\Search\Model\QueryFactory
     */
    protected $queryFactory = null;

    /**
     * @var \Wyomind\ElasticsearchAutocomplete\Helper\Config
     */
    protected $configHelper = null;

    /**
     * Constructor
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Search\Model\QueryFactory $queryFactory
     * @param \Wyomind\ElasticsearchAutocomplete\Helper\Config $configHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Search\Model\QueryFactory $queryFactory,
        \Wyomind\ElasticsearchAutocomplete\Helper\Config $configHelper,
        array $data = []
    ) {
    
        $this->queryFactory = $queryFactory;
        $this->configHelper = $configHelper;
        parent::__construct($context, $data);
    }


    /**
     * Get the categories list matching the search term
     * @param string $storeCode the store code
     * @return array the list of categories
     */
    public function getCategoryCollection($storeCode)
    {
        try {
            $config = new \Wyomind\ElasticsearchCore\Helper\Autocomplete\Config($storeCode);
            $config->getData();
        } catch (\Exception $e) {
            return [];
        }

        $client = new \Wyomind\ElasticsearchCore\Model\Client($config, new \Psr\Log\NullLogger());
        $client->init($storeCode);

        $requester = new \Wyomind\ElasticsearchCore\Helper\Requester($client, $config);

        $query = $this->queryFactory->get();
        $collection = $requester->searchByType($storeCode, "category", $query->getQueryText(), $this->getLimit(), $this->configHelper->isHighlightEnabled());
        return $collection['docs'];
    }

    /**
     * Get the number of categories to display
     * @return int the limit of categories to display
     */
    public function getLimit()
    {
        return $this->configHelper->getCategoryPageSearchLimit();
    }
}
