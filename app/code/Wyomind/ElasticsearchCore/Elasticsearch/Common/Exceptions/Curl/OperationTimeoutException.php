<?php

namespace Wyomind\ElasticsearchCore\Elasticsearch\Common\Exceptions\Curl;

use Wyomind\ElasticsearchCore\Elasticsearch\Common\Exceptions\ElasticsearchException;
use Wyomind\ElasticsearchCore\Elasticsearch\Common\Exceptions\TransportException;

/**
 * Class OperationTimeoutException
 *
 * @category Elasticsearch
 * @package Wyomind\ElasticsearchCore\Elasticsearch\Common\Exceptions\Curl
 * @author   Zachary Tong <zach@elastic.co>
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache2
 * @link     http://elastic.co
  */
class OperationTimeoutException extends TransportException implements ElasticsearchException
{
}
