<?php

/**
 * Copyright © 2016 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\Elasticsearch\Observer\Category;

use Wyomind\Elasticsearch\Model\Indexer\Category as CategoryIndexer;
use Wyomind\Elasticsearch\Helper\Config as Config;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Wyomind\Elasticsearch\Model\Indexer\Product as ProductIndexer;

class Reindex extends AbstractObserver implements ObserverInterface
{

    public $productIndexer = null;
    
    public function __construct(
    CategoryIndexer $categoryIndexer,
        Config $configHelper,
        ProductIndexer $productIndexer)
    {
        parent::__construct($categoryIndexer,$configHelper);
        $this->productIndexer = $productIndexer;
    }
    
    /**
     * {@inheritdoc}
     */
    public function execute(Observer $observer)
    {
        if ($this->isElasticsearchEnabled) {
            $category = $observer->getEvent()->getCategory();
            $this->indexer->executeRow($category->getId());
            
            $products = $category->getProductCollection();
            $productIds = [];
            foreach ($products as $product) {
                $productIds[] = $product->getId();
            }
            $this->productIndexer->setCategoryId($category->getId());
            $this->productIndexer->execute($productIds);
        }
    }
}
