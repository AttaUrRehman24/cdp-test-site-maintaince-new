<?php

namespace Meridian\Video\Ui\DataProvider\Modifier;

use Magento\Catalog\Controller\Adminhtml\Product\Initialization\StockDataFilter;
use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\CatalogInventory\Api\Data\StockItemInterface;
use Magento\CatalogInventory\Api\StockConfigurationInterface;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Stdlib\ArrayManager;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Serialize\JsonValidator;

/**
 * Data provider for advanced inventory form
 */
class VideoPreview extends AbstractModifier
{

    /**
     * @var LocatorInterface
     */
    private $locator;

    /**
     * @var StockRegistryInterface
     */
    private $stockRegistry;

    /**
     * @var ArrayManager
     */
    private $arrayManager;

    /**
     * @var StockConfigurationInterface
     */
    private $stockConfiguration;

    /**
     * @var array
     */
    private $meta = [];

    /**
     * @var Json
     */
    private $serializer;

    /**
     * @var JsonValidator
     */
    private $jsonValidator;

    protected $imageuploader;

    protected $urlBuilder;

    /**
     * Constructor
     *
     * @param LocatorInterface $locator
     * @param StockRegistryInterface $stockRegistry
     * @param ArrayManager $arrayManager
     * @param StockConfigurationInterface $stockConfiguration
     * @param Json|null $serializer
     * @param JsonValidator|null $jsonValidator
     */
    public function __construct(
        LocatorInterface $locator,
        StockRegistryInterface $stockRegistry,
        ArrayManager $arrayManager,
        StockConfigurationInterface $stockConfiguration,
        \Meridian\Video\Model\ImageUploader $imageUploader,
        \Magento\Framework\UrlInterface $urlBuilder,
        Json $serializer = null,
        JsonValidator $jsonValidator = null
    ) {
        $this->locator = $locator;
        $this->stockRegistry = $stockRegistry;
        $this->arrayManager = $arrayManager;
        $this->stockConfiguration = $stockConfiguration;
        $this->serializer = $serializer ?: ObjectManager::getInstance()->get(Json::class);
        $this->jsonValidator = $jsonValidator ?: ObjectManager::getInstance()->get(JsonValidator::class);
        $this->imageuploader = $imageUploader;
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyData(array $data)
    {

        //VIDEO PREVIEW
        $fieldCode = 'video_preview';

        $model = $this->locator->getProduct();
        $modelId = $model->getId();
        $url = null;
        if ($model->getVideoPreview()) {
            if (isset($data[$modelId][self::DATA_SOURCE_DEFAULT][$fieldCode])) {
                unset($data[$modelId][self::DATA_SOURCE_DEFAULT][$fieldCode]);
            }
            $url = $this->urlBuilder->getBaseUrl(
                    ['_type' => \Magento\Framework\UrlInterface::URL_TYPE_MEDIA]
                ) . 'catalog/product/video_preview/' . $model->getVideoPreview();

            $data[$modelId][self::DATA_SOURCE_DEFAULT][$fieldCode][0] =
                [
                    'name' => $model->getVideoPreview(),
                    'url' => $url
                ];
        }

        //VIDEO
        $fieldCode = 'video';

        if ($model->getVideo()) {
            if (isset($data[$modelId][self::DATA_SOURCE_DEFAULT][$fieldCode])) {
                unset($data[$modelId][self::DATA_SOURCE_DEFAULT][$fieldCode]);
            }
            if (!$model->getVideoPreview() || !$url) {
                $url = $this->urlBuilder->getBaseUrl(
                        ['_type' => \Magento\Framework\UrlInterface::URL_TYPE_MEDIA]
                    ) . 'catalog/product/video/' . $model->getVideo();
            }

            $data[$modelId][self::DATA_SOURCE_DEFAULT][$fieldCode][0] =
                [
                    'name' => $model->getVideo(),
                    'url' => $url
                ];
        }


        return $data;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyMeta(array $meta)
    {
        return $meta;
    }

}
