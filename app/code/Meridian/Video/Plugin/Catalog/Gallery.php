<?php

namespace Meridian\Video\Plugin\Catalog;

class Gallery
{

    protected $_imageHelper;
    protected $_storeManager;
    public function __construct(
        \Magento\Catalog\Helper\Image $imageHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {
        $this->_imageHelper = $imageHelper;
        $this->_storeManager = $storeManager;
    }


    public function isMainImage($image, $subject)
    {
        $product = $subject->getProduct();
        return $product->getImage() == $image->getFile();
    }

    public function aroundGetGalleryImagesJson(
        $subject,
        \Closure $proceed
    ) {

        $imagesItems = [];
        foreach ($subject->getGalleryImages() as $image) {
            $imagesItems[] = [
                'thumb' => $image->getData('small_image_url'),
                'img' => $image->getData('medium_image_url'),
                'full' => $image->getData('large_image_url'),
                'caption' => $image->getLabel(),
                'position' => $image->getPosition(),
                'isMain' => $this->isMainImage($image, $subject),
                'type' => str_replace('external-', '', $image->getMediaType()),
                'videoUrl' => $image->getVideoUrl(),
            ];
        }
        if (empty($imagesItems)) {

            $imagesItems[] = [
                'thumb' => $this->_imageHelper->getDefaultPlaceholderUrl('thumbnail'),
                'img' => $this->_imageHelper->getDefaultPlaceholderUrl('image'),
                'full' => $this->_imageHelper->getDefaultPlaceholderUrl('image'),
                'caption' => '',
                'position' => '0',
                'isMain' => true,
                'type' => 'image',
                'videoUrl' => null,
            ];
        }

        $product = $subject->getProduct();


        if ($product->getVideo()) {

            $videoUrl = $this->_storeManager
                    ->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA)
                . 'catalog/product/video/' . $product->getVideo();

            $videoImg = $this->_storeManager
                    ->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA)
                . 'catalog/product/video_preview/' . $product->getVideoPreview();

            $imagesItems[] = [
                'thumb' => $videoImg,
                'img' => $videoImg,
                'full' => $videoImg,
                'caption' => '',
                'position' => '0',
                'isMain' => false,
                'type' => 'video',
                'videoUrl' => $videoUrl,
            ];
        }


        return json_encode($imagesItems);

    }
}
