<?php

namespace Meridian\Video\Plugin\Video;

/**
 * @api
 * @since 100.0.2
 */
class Gallery
{

    protected $_storeManager;

    protected $jsonEncoder;

    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder
    )
    {
        $this->_storeManager = $storeManager;
        $this->jsonEncoder = $jsonEncoder;
    }

    public function aroundGetMediaGalleryDataJson(
        $subject,
        \Closure $closure
    ) {
        $mediaGalleryData = [];
        foreach ($subject->getProduct()->getMediaGalleryImages() as $mediaGalleryImage) {
            $mediaGalleryData[] = [
                'mediaType' => $mediaGalleryImage->getMediaType(),
                'videoUrl' => $mediaGalleryImage->getVideoUrl(),
                'isBase' => $this->isMainImage($mediaGalleryImage, $subject),
            ];
        }

        $product = $subject->getProduct();

        if ($product->getVideo()) {

            $videoUrl = $this ->_storeManager
                    ->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA)
                . 'catalog/product/video/' . $product->getVideo();

            $mediaGalleryData[] = [
                'mediaType' => 'external-video',
                'videoUrl' => $videoUrl,
                'isBase' => false,
                'custom' => 'true',
            ];

        }

        return $this->jsonEncoder->encode($mediaGalleryData);
    }

    public function isMainImage($image, $subject)
    {
        $product = $subject->getProduct();
        return $product->getImage() == $image->getFile();
    }


    /**
     * Retrieve media gallery data in JSON format
     *
     * @return string
     */
    public function getMediaGalleryDataJson()
    {
        $mediaGalleryData = [];
        foreach ($this->getProduct()->getMediaGalleryImages() as $mediaGalleryImage) {
            $mediaGalleryData[] = [
                'mediaType' => $mediaGalleryImage->getMediaType(),
                'videoUrl' => $mediaGalleryImage->getVideoUrl(),
                'isBase' => $this->isMainImage($mediaGalleryImage),
            ];
        }

        $product = $this->getProduct();


        if ($product->getVideo()) {

            $videoUrl = $this ->_storeManager
                    ->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA)
                . 'catalog/product/video/' . $product->getVideo();

            $mediaGalleryData[] = [
                'mediaType' => 'external-video',
                'videoUrl' => $videoUrl,
                'isBase' => false,
                'custom' => 'true',
            ];

        }

        return $this->jsonEncoder->encode($mediaGalleryData);
    }
}
