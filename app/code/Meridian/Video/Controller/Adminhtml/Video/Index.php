<?php
namespace Meridian\Video\Controller\Adminhtml\Video;

use Magento\Framework\Controller\ResultFactory;

class Index extends \Magento\Backend\App\Action {
    /**
     * Image uploader
     *
     * @var \Meridian\Video\Model\ImageUploader
     */
    protected $imageUploader;
    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Meridian\Video\Model\ImageUploader $imageUploader
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Meridian\Video\Model\ImageUploader $imageUploader
    ) {
        parent::__construct($context);
        $this->imageUploader = $imageUploader;
    }

    public function execute()
    {
        try {
            $result = $this->imageUploader->saveFileToTmpDir('video');
        } catch (\Exception $e) {
            $result = ['error' => $e->getMessage(), 'errorcode' => $e->getCode()];
        }

        return $this->resultFactory->create(ResultFactory::TYPE_JSON)->setData($result);
    }
}