var config = {


    // Paths defines associations from library name (used to include the library,
    // for example when using "define") and the library file path.
    paths: {
        'videoMinJs': 'Meridian_Video/js/video.min',
        'flvMinJs': 'Meridian_Video/js/flv.min'
    },

    shim: {
        'videoMinJs': {
            deps: ['jquery']
        },
        'flvMinJs': {
            deps: ["jquery"]
        },
        'videoFlvMinJs': {
            deps: ["jquery"]
        }
    },

};
