<?php
/**
 * Verifone e-commerce Epayment module for Magento
 *
 * Feel free to contact Verifone e-commerce at support@paybox.com for any
 * question.
 *
 * LICENSE: This source file is subject to the version 3.0 of the Open
 * Software License (OSL-3.0) that is available through the world-wide-web
 * at the following URI: http://opensource.org/licenses/OSL-3.0. If
 * you did not receive a copy of the OSL-3.0 license and are unable
 * to obtain it through the web, please send a note to
 * support@paybox.com so we can mail you a copy immediately.
 *
 * @version   1.0.7-psr
 * @author    BM Services <contact@bm-services.com>
 * @copyright 2012-2017 Verifone e-commerce
 * @license   http://opensource.org/licenses/OSL-3.0
 * @link      http://www.paybox.com/
 */

namespace Meridian\Epayment\Block;


class Info extends \Paybox\Epayment\Block\Info
{

    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('Meridian_Epayment::pbxep/info/default.phtml');
    }

    /**
     * @return mixed
     */
    public function getOrder(){
        return $this->getParentBlock()->getParentBlock()->getOrder();
    }

    /**
     * @return mixed
     */
    public function getPaymentInformation(){
        return $this->getOrder()->getPayment()->getAdditionalInformation();
    }

    /**
     * @return string
     */
    public function getPaymentPhone(){
        $info = $this->getPaymentInformation();
        $result = '';
        if(count($info) && isset($info['payment_phone'])){
            $result = $info['payment_phone'];
        }

        return $result;
    }

    /**
     * @return string
     */
    public function getPhoneHtml(){
        $phone = $this->getPaymentPhone();
        $html = [];
        if(trim($phone) != false){
            $html[] = '<br />';
            $html[] = __('Payment Phone');
            $html[] = ': ';
            $html[] = '<storng>';
            $html[] = $phone;
            $html[] = '</storng>';
        }

        return implode('', $html);
    }
}
