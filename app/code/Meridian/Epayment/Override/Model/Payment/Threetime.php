<?php
/**
 * Verifone e-commerce Epayment module for Magento
 *
 * Feel free to contact Verifone e-commerce at support@paybox.com for any
 * question.
 *
 * LICENSE: This source file is subject to the version 3.0 of the Open
 * Software License (OSL-3.0) that is available through the world-wide-web
 * at the following URI: http://opensource.org/licenses/OSL-3.0. If
 * you did not receive a copy of the OSL-3.0 license and are unable
 * to obtain it through the web, please send a note to
 * support@paybox.com so we can mail you a copy immediately.
 *
 * @version   1.0.7
 * @author    BM Services <contact@bm-services.com>
 * @copyright 2012-2017 Verifone e-commerce
 * @license   http://opensource.org/licenses/OSL-3.0
 * @link      http://www.paybox.com/
 */

namespace Meridian\Epayment\Override\Model\Payment;

use \Magento\Framework\DataObject;

class Threetime extends \Paybox\Epayment\Model\Payment\Threetime
{
    const CODE = 'pbxep';
    const CALL_NUMBER = 'paybox_call_number';
    const TRANSACTION_NUMBER = 'paybox_transaction_number';
    const PBXACTION_DEFERRED = 'deferred';
    const PBXACTION_IMMEDIATE = 'immediate';
    const PBXACTION_MANUAL = 'manual';
    const PBXACTION_MODE_SHIPMENT = 'shipment';
    protected $_infoBlockType = 'Meridian\Epayment\Block\Info';

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Payment\Model\Method\Logger $logger,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * Assign corresponding data
     *
     * @param  \Magento\Framework\DataObject|mixed $data
     * @return $this
     * @throws LocalizedException
     */
    public function assignData(DataObject $data)
    {
        parent::assignData($data);
        if (!($data instanceof DataObject)) {
            $data = new DataObject($data);
        }

        $additionnalData = new DataObject($data->getAdditionalData());
        $info = $this->getInfoInstance();
        $info->setCcType($additionnalData->getCcType());
        $info->setAdditionalInformation(['payment_phone' => $additionnalData->getPaymentPhone()]);
        return $this;
    }
}
