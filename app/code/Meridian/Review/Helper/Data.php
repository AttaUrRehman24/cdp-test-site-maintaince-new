<?php
/**
 * Venustheme
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Venustheme.com license that is
 * available through the world-wide-web at this URL:
 * http://www.venustheme.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   Venustheme
 * @package    Ves_BlockBuilder
 * @copyright  Copyright (c) 2014 Venustheme (http://www.venustheme.com/)
 * @license    http://www.venustheme.com/LICENSE-1.0.html
 */
namespace Meridian\Review\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\ResourceConnection;
use Magento\Sales\Model\Order;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;
    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;

    protected $checkResult = null;
    protected $checkedProduct = false;

    protected $_reviewsColFactory;
    protected $_storeManager;

    public function __construct(
        Context $context,
        \Magento\Customer\Model\Session $customerSession,
        ResourceConnection $resourceConnection,
        \Magento\Review\Model\ResourceModel\Review\CollectionFactory $collectionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->_reviewsColFactory = $collectionFactory;
        $this->customerSession = $customerSession;
        $this->resourceConnection = $resourceConnection;
        $this->_storeManager = $storeManager;
        parent::__construct($context);
    }

    public function isOrderedProduct($product)
    {
        if ($this->checkedProduct) {
            return $this->checkResult;
        }

        if ($this->customerSession->isLoggedIn()) {

            $customer = $this->customerSession->getCustomer();

            $tableName = $this->resourceConnection->getTableName('sales_order');

            /** @var \Magento\Framework\DB\Adapter\Pdo\Mysql $connection */
            $connection = $this->resourceConnection->getConnection();

            $select = $connection->select('*')->from(['main_table' => $tableName])
                ->joinLeft(
                    ['items' => 'sales_order_item'],
                    'main_table.entity_id = items.order_id',
                    false
                )
                ->where("main_table.customer_id = ?",  $customer->getId())
                ->where('items.product_id = ?', $product->getId());

            $select->reset('columns');
            $select->columns(['entity_id' => 'entity_id']);

            $res = $connection->fetchAll($select);

            $this->checkedProduct = true;
            if (!empty($res)) {
                $this->checkResult = true;
                return true;
            } else {
                $this->checkResult = false;
                return false;
            }
        }

        return false;


    }

    public function getReviewCollection($product)
    {
        return $this->_reviewsCollection = $this->_reviewsColFactory->create()->addStoreFilter(
            $this->_storeManager->getStore()->getId()
        )->addStatusFilter(
            \Magento\Review\Model\Review::STATUS_APPROVED
        )->addEntityFilter(
            'product',
            $product->getId()
        )->setDateOrder()
        ->setPageSize(4);
    }

    public function resizeName($nickname){
        $rtnSrt = $nickname;
        if(strpos($nickname,' ') !== false) {
            $parts = explode(" ", $nickname);
            $rtnSrt = $parts[0]." ".substr($parts[1],0,1);
        }
        return $rtnSrt;
    }
}
