<?php
/**
 * Updated on 4th Nov 2017.
 * Bobcares Quote2Sales Quotes Items Block.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */
namespace Meridian\Quote2Sales\Block\Quote;

use Magento\Sales\Model\Order;
use Magento\Quote\Model\Quote;

/**
 * Updated on 4th Nov 2017.
 * Bobcares Quote2Sales Quotes Items Block.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */
class Totals extends \Bobcares\Quote2Sales\Block\Quote\Totals
{
    /**
     * Initialize quote totals array
     *
     * @return $this
     */
    protected function _initTotals()
    {
        $source = $this->getSource();

        $this->_totals = [];
        $this->_totals['subtotal'] = new \Magento\Framework\DataObject(
            ['code' => 'subtotal',
                'value' => $source->getSubtotal(),
                'label' => __('Subtotal')]
        );

        /**
         * change by Webmeridian
         */
        if($this->getSource()->getShippingAddress()->getTaxAmount() &&
            $this->getSource()->getShippingAddress()->getTaxAmount() != 0){

            $this->_totals['tax'] = new \Magento\Framework\DataObject(
                ['code' => 'tax',
                    'value' => $this->getSource()->getShippingAddress()->getTaxAmount(),
                    'label' => __('Tax')]
            );
        }
        /**
         * end by Webmeridian
         */


        /**
         * Add shipping
         */
        if (!$source->getIsVirtual() && ((double)$source->getShippingAmount() || $source->getShippingAddress()->getShippingDescription())) {
            $this->_totals['shipping'] = new \Magento\Framework\DataObject(
                [
                    'code' => 'shipping',
                    'field' => 'shipping_amount',
                    'value' => $this->getSource()->getShippingAddress()->getShippingAmount(),
                    'label' => __('Shipping & Handling'),
                ]
            );
        }

        /**
         * Add discount
         */
        if ((double)$this->getSource()->getShippingAddress()->getDiscountAmount() != 0) {
            if ($this->getSource()->getShippingAddress()->getDiscountDescription()) {
                $discountLabel = __('Discount (%1)', $source->getShippingAddress()->getDiscountDescription());
            } else {
                $discountLabel = __('Discount');
            }
            $this->_totals['discount'] = new \Magento\Framework\DataObject(
                [
                    'code' => 'discount',
                    'field' => 'discount_amount',
                    'value' => $source->getShippingAddress()->getDiscountAmount(),
                    'label' => $discountLabel,
                ]
            );
        }

        /**
         * Base grandtotal
         */
        $this->_totals['grand_total'] = new \Magento\Framework\DataObject(
            [
                'code' => 'grand_total',
                'field' => 'grand_total',
                'strong' => true,
                'value' => $source->getGrandTotal(),
                'label' => __('Grand Total'),
            ]
        );

        /**
         * Base grandtotal
         */
        //if ($this->getOrder()->isCurrencyDifferent()) {
//            $this->_totals['base_grandtotal'] = new \Magento\Framework\DataObject(
//                [
//                    'code' => 'base_grandtotal',
//                    'value' => $source->getBaseGrandTotal(),
//                    'label' => __('Grand Total to be Charged'),
//                    'is_formated' => true,
//                ]
//            );
        //}
        return $this;
    }

}
