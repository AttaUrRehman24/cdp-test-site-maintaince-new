<?php


namespace Meridian\Quote2Sales\Observer\Backend\Controller;

class SalesQuoteSaveCommitAfter implements \Magento\Framework\Event\ObserverInterface
{
    protected $_request;

    protected $_rateFactory;

    /**
     * SalesQuoteAddressCollectTotalsAfter constructor.
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Quote\Model\Quote\Address\RateFactory $_rateFactory
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Quote\Model\Quote\Address\RateFactory $_rateFactory
    ) {
        $this->_request = $request;
        $this->_rateFactory = $_rateFactory;
    }

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {

        $addresesArrayId = array_keys($observer->getQuote()->getAddressesCollection()->getItems());
        $factoryCreate = $this->_rateFactory->create();
        $collectionRatesShipp = $factoryCreate->getCollection()->addFieldToFilter('address_id', array('in' =>$addresesArrayId));
        foreach ($collectionRatesShipp as $collectionRateShipp){

            $collectionRateShipp->setPrice($observer->getQuote()->getShippingAmount());
            $collectionRateShipp->save();
        }
        $collectionRatesShipp->save();

    }
}