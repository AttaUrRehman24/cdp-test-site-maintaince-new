<?php


namespace Meridian\Quote2Sales\Observer\Backend\Controller;

class SalesQuoteAddressCollectTotalsAfter implements \Magento\Framework\Event\ObserverInterface
{
    protected $_request;

    protected $_rateFactory;

    /**
     * SalesQuoteAddressCollectTotalsAfter constructor.
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Quote\Model\Quote\Address\RateFactory $_rateFactory
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Quote\Model\Quote\Address\RateFactory $_rateFactory
    ) {
        $this->_request = $request;
        $this->_rateFactory = $_rateFactory;
    }

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {
        if($observer->getTotal()->getShippingAmount()){
            $shippingAmount = $observer->getTotal()->getShippingAmount();
            $shipping_method_value = (float)$this->_request->getParam('shipping_method_value');

            /**
             * Change shipping amount
             */
            if($shipping_method_value && $shippingAmount != $shipping_method_value){

                $observer->getTotal()->setShippingAmount($this->_request->getParam('shipping_method_value'));
                $observer->getTotal()->setShippingTaxCalculationAmount($this->_request->getParam('shipping_method_value'));

                $observer->getTotal()->setBaseShippingAmount($this->_request->getParam('shipping_method_value'));
                $observer->getTotal()->setBaseShippingTaxCalculationAmount($this->_request->getParam('shipping_method_value'));

                $grand_total = $observer->getTotal()->getGrandTotal();
                $new_grand_total = $grand_total-$shippingAmount+$shipping_method_value;
                $observer->getTotal()->setGrandTotal($new_grand_total);

            }
        }

    }
}