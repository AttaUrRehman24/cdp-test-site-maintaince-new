<?php

namespace Meridian\ProductCounting\Model\Wyomind\Elasticsearch;
use Wyomind\Elasticsearch\Model\Index\TypeInterface;

class QueryBuilder extends \Wyomind\Elasticsearch\Model\QueryBuilder
{
    public function buildCat(
        $boolQuery,
        TypeInterface $type,
        $store = null
    )
    {

        $must = $boolQuery->getMust();
        $should = $boolQuery->getShould();

        $q = $must['category']->getReference()->getValue();

        $queries = [];

        $params = [
            'from' => 0,
            'size' => 10000,
            'body' => [
                'query' => []
            ]
        ];

        if (!is_array($q)) {
            $params['body']['query']['constant_score']['filter']['bool']['should'][] = [
                'term' => [
                    \Wyomind\Elasticsearch\Helper\Config::PRODUCT_CATEGORIES_ID => $q
                ]
            ];
            $params['body']['query']['constant_score']['filter']['bool']['should'][] = [
                'term' => [
                    \Wyomind\Elasticsearch\Helper\Config::PRODUCT_CATEGORIES_PARENT_ID => $q
                ]
            ];
        } else {
            $params['body']['query']['constant_score']['filter']['bool']['should'][] = [
                'terms' => [
                    \Wyomind\Elasticsearch\Helper\Config::PRODUCT_CATEGORIES_ID => $q
                ]
            ];
            $params['body']['query']['constant_score']['filter']['bool']['should'][] = [
                'terms' => [
                    \Wyomind\Elasticsearch\Helper\Config::PRODUCT_CATEGORIES_PARENT_ID => $q
                ]
            ];
        }


        foreach ($should as $key => $info) {
            $reference = $info->getReference();
            $field = $reference->getField();
            if ($reference instanceof \Magento\Framework\Search\Request\Filter\Range) {
                if ($field == "price") {
                    $field = "final_price";
                }
                $params['body']['query']['constant_score']['filter']['bool']['must'][] = [
                    'range' => [
                        $field => [
                            "from" => $reference->getFrom(),
                            "to" => $reference->getTo()
                        ]
                    ]
                ];
            } else {
                if ($this->coreHelper != null && $this->coreHelper->moduleIsEnabled("Amasty_Shopby")) { // fix for Amasty_Shopby !
                    $tmp = $reference->getValue();
                    $value = $tmp[0];
                } else {
                    $value = $reference->getValue();
                }
                if (isset($value['in'])) {
                    $value = $value['in'];
                }
                if (is_array($value)) {
                    $params['body']['query']['constant_score']['filter']['bool']['must'][] = [
                        'terms' => [
                            $field . ($field != "visibility" ? "_ids" : "") =>  array('0'=>'1','1'=>'2','2'=>'4')
                        ]
                    ];
                } else {
                    $params['body']['query']['constant_score']['filter']['bool']['must'][] = [
                        'term' => [
                            $field . ($field != "visibility" ? "_ids" : "") =>  array('0'=>'1','1'=>'2','2'=>'4')
                        ]
                    ];
                }
            }
        }

        foreach ($must as $key => $info) {
            if ($info->getReference()->getField() != "category_ids") {
                if (method_exists($info->getReference(), "getFrom")) {
                    $params['body']['query']['constant_score']['filter']['bool']['must'][] = [
                        'range' => [
                            $info->getReference()->getField() => [
                                "from" => $info->getReference()->getFrom(),
                                "to" => $info->getReference()->getTo()
                            ]
                        ]
                    ];
                } else {

                    $reference = $info->getReference();

                    if($reference->getName()=='visibility_filter' && is_array($reference->getValue())){
                        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                        $referenceCust = $objectManager->create(
                            \Magento\Framework\Search\Request\Filter\Term::class,
                            [
                                'name' => 'visibility_filter',
                                'field' => 'visibility',
                                'value' => array('1','2','3','4')
                            ]
                        );
                    }
                    if(isset($referenceCust)){
                        $reference = $referenceCust;
                        unset($referenceCust);
                    }




                    $field = $reference->getField();
                    if ($this->coreHelper != null && $this->coreHelper->moduleIsEnabled("Amasty_Shopby")) { // fix for Amasty_Shopby !
                        $tmp = $reference->getValue();
                        $value = $tmp[0];
                    } else {
                        $value = $reference->getValue();
                    }
                    if (isset($value['in'])) {
                        $value = $value['in'];
                    }
                    if (is_array($value)) {
                        $params['body']['query']['constant_score']['filter']['bool']['must'][] = [
                            'terms' => [
                                $field . ($field != "visibility" ? "_ids" : "") =>  $value
                            ]
                        ];
                    } else {
                        $params['body']['query']['constant_score']['filter']['bool']['must'][] = [
                            'term' => [
                                $field . ($field != "visibility" ? "_ids" : "") => array('1','2','3','4')
                            ]
                        ];
                    }
                }
            }
        }

        return $params;
    }
	
}
	
	