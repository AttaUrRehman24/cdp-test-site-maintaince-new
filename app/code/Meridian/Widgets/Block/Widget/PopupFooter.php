<?php


namespace Meridian\Widgets\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class PopupFooter extends Template implements BlockInterface
{

    protected $_template = "widget/popupfooter.phtml";

    /**
     * @var \Magento\Cms\Model\Template\FilterProvider
     */
    protected $_filterProvider;

    /**
     * Storage for used widgets
     *
     * @var array
     */
    protected static $_widgetUsageMap = [];

    /**
     * Block factory
     *
     * @var \Magento\Cms\Model\BlockFactory
     */
    protected $_blockFactory;

    protected $_blockModel;
    protected $_dataFilterHelper;
    protected $_layout;

    public function __construct(
        Template\Context $context,
        \Magento\Cms\Model\Template\FilterProvider $filterProvider,
        \Magento\Cms\Model\Block $blockModel,
        \Meridian\Widgets\Helper\Data $dataHelper,
        \Magento\Cms\Model\BlockFactory $blockFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        // used singleton (instead factory) because there exist dependencies on \Magento\Cms\Helper\Page
        $this->_filterProvider = $filterProvider;
        $this->_blockModel = $blockModel;
        $this->_dataFilterHelper = $dataHelper;
        $this->_layout = $context->getLayout();
        $this->_blockFactory = $blockFactory;
    }

    public function contentToHtml($html = null)
    {
        if(is_null($html)) return;
        $html = $this->_filterProvider->getPageFilter()->filter($html);
        return $html;
    }
    public function getConfig($key, $default = NULL){
        if($this->hasData($key)){
            return $this->getData($key);
        }
        return $default;
    }
    public function getDataFilterHelper() {
        return $this->_dataFilterHelper;
    }
    public function getLayout() {
        return $this->_layout;
    }

    public function getData($key = '', $index = null)
    {
        if ('' === $key) {
            $data = $this->_dataFilterHelper->decodeWidgetValues($this->_data);
        } else {
            $data = parent::getData($key, $index);
            $dataEncode = $this->_dataFilterHelper->decodeWidgetValues($data);
            $isDecoded = $this->_isDecoded($data);
            if(!is_null($dataEncode) && !empty($dataEncode)) {
                if (is_scalar($data)) {
                    $data = $this->_dataFilterHelper->decodeWidgetValues($data);
                }
            }
            $data = $this->_filterProvider->getPageFilter()->filter($data);
        }

        return $data;
    }

    public function _isDecoded($data){
        // Check if there are valid base64 characters
        if (!preg_match('/^[a-zA-Z0-9\/\r\n+]*={0,2}$/', $data)) return false;

        // Decode the string in strict mode and check the results
        $decoded = base64_decode($data, true);
        if(false === $decoded) return false;

        // Encode the string again
        if(base64_encode($decoded) != $data) return false;

        return true;
        //return (bool) preg_match('/^[a-zA-Z0-9\/\r\n+]*={0,2}$/', $data);
        /*if ( base64_encode(base64_decode($data, true)) === $data){
            //echo '$data is valid';
            return true;
        } else {
            //echo '$data is NOT valid';
            return false;
        }*/
    }
}
