<?php

namespace Meridian\Widgets\Block\Widget;

use Magento\Widget\Block\BlockInterface;

class Sitemap extends \Magento\Framework\View\Element\Template implements BlockInterface
{
    protected $shipconfig;

    protected $scopeConfig;

    protected $customerSession;

    protected $categoryFactory;

    protected $categoryResourceFactory;

    protected $pageFactory;

    protected $_storeManager;

    /**
     * @var Category
     */
    protected $_categoryInstance;

    /**
     * @var \Meridian\Widgets\Helper\Data
     */
    protected $_dataFilterHelper;

    /**
     * @var \Magento\Cms\Model\Template\FilterProvider
     */
    protected $_filterProvider;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Shipping\Model\Config $shipconfig,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryResourceFactory,
        \Magento\Cms\Model\ResourceModel\Page\CollectionFactory $pageFactory,
        \Meridian\Widgets\Helper\Data $dataHelper,
        \Magento\Cms\Model\Template\FilterProvider $filterProvider,
        array $data = []
    ) {
        $this->scopeConfig = $context->getScopeConfig();
        $this->shipconfig = $shipconfig;
        $this->customerSession = $customerSession;
        $this->categoryFactory = $categoryFactory;
        $this->pageFactory = $pageFactory;
        $this->_storeManager = $context->getStoreManager();
        $this->_categoryInstance = $categoryFactory->create();
        $this->categoryResourceFactory = $categoryResourceFactory;
        $this->_dataFilterHelper = $dataHelper;
        $this->_filterProvider = $filterProvider;

        parent::__construct($context, $data);
    }

    public function isUserLoggedIn()
    {
        return $this->customerSession->isLoggedIn();
    }

    public function getAllCmsPages()
    {
        $cmscollection = $this->getCmsPageCollection();
        $pages = [];
        foreach ($cmscollection as $page) {
            if ($page->getIdentifier() == 'no-route') {
                continue;
            }

            $cmsId = $page->getId();
            $pages[$cmsId]['id'] = $page->getId();
            $pages[$cmsId]['title'] = $page->getTitle();
            $pages[$cmsId]['link_url'] = $page->getIdentifier();
        }

        return $pages;
    }

    protected function getCmsPageCollection()
    {
        $pageCollection = $this->pageFactory->create();
        // add Filter if you want
        $pageCollection->addFieldToFilter('is_active', \Magento\Cms\Model\Page::STATUS_ENABLED);
        $pageCollection->addStoreFilter($this->_storeManager->getStore()->getId());
        return $pageCollection;
    }

    public function catagorylistrecursiveHtml($categoryy, $levelClass = 'top')
    {
        $html = '';
        $count = is_array($categoryy) ? count($categoryy) : $categoryy->count();
        if($count) {
            $html .= '<ul class="sitemap__'.$levelClass.'-list">';
            foreach ($categoryy as $catt) :
                $category = $this->_loadCategory($catt->getId());
                if ($category->getIncludeInMenu() && $category->getIsActive()) {
                    $haschildren = '';
                    if (count($catt->getChildrenCategories())) {
                        $haschildren = ' isparent';
                    }
                    $level = $catt->getLevel();

                    $html .= '<li class="sitemap__'.$levelClass.'-item level-' . $level . $haschildren . '">';
                    $html .= '<a class="sitemap__'.$levelClass.'-category" href="' . $catt->getUrl() . '" title="' . $catt->getName() . '">';
                    $html .= $catt->getName();
                    $html .= '</a>';
                    if ($haschildren) :
                        $html .= $this->catagorylistrecursiveHtml($catt->getChildrenCategories(),'sub');
                    endif;
                    $html .= '</li>';
                    if ($catt->getLevel() == 2) {
                        $html .= '</ul><ul class="sitemap__'.$levelClass.'-list">';
                    }
                }
            endforeach;
            $html .= '</ul>';
        }
        return $html;
    }

    protected function _loadCategory($catId = null)
    {
        if(is_null($catId)) return null;
        $category = $this->categoryFactory->create();
        $category->load($catId);
        return $category;
    }

    public function getCatCollection()
    {

        $categories = $this->categoryResourceFactory->create()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('is_active', 1)
            ->addAttributeToFilter('include_in_menu', 1)
            ->addLevelFilter(1)
            ->addAttributeToSort('position')
            ;

        return $categories;
    }

    public function getCategoryCollection()
    {
        $categories = $this->categoryFactory->create()->getCollection()
            ->addAttributeToSelect('*')
            ->addFieldToFilter('is_active', 1)
            ->addFieldToFilter('include_in_menu', 1)
            ->setOrder('position','ASC');

        return $categories;
    }

    /**
     * Get url for category data
     *
     * @param Category $category
     * @return string
     */
    public function getCategoryUrl($category)
    {
        if ($category instanceof \Magento\Catalog\Model\Category) {
            $url = $category->getUrl();
        } else {
            $url = $this->_categoryInstance->setData($category->getData())->getUrl();
        }

        return $url;
    }

    public function getData($key = '', $index = null)
    {
        if ('' === $key) {
            $data = $this->_dataFilterHelper->decodeWidgetValues($this->_data);
        } else {
            $data = parent::getData($key, $index);
            if (is_scalar($data)) {
                $data = $this->_dataFilterHelper->decodeWidgetValues($data);
            }

            $data = $this->_filterProvider->getPageFilter()->filter($data);
        }

        return $data;
    }

}
