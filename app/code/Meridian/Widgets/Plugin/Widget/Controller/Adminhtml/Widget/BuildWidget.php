<?php

namespace Meridian\Widgets\Plugin\Widget\Controller\Adminhtml\Widget;

class BuildWidget
{
    /**
     * @var \Magento\Widget\Model\Widget
     */
    protected $widget;

    /**
     * @var \Meridian\Widgets\Helper\Data
     */
    protected $helper;

    /**
     * @param \Magento\Widget\Model\Widget $widget
     * @param \Meridian\Widgets\Helper\Data $helper
     */
    public function __construct(
        \Magento\Widget\Model\Widget $widget,
        \Meridian\Widgets\Helper\Data $helper
    ) {
        $this->widget = $widget;
        $this->helper = $helper;
    }

    /**
     * Format widget pseudo-code for inserting into wysiwyg editor
     *
     * @return void
     */
    public function aroundExecute(
        \Magento\Widget\Controller\Adminhtml\Widget\BuildWidget $subject,
        \Closure $proceed
    ) {
        $type = $subject->getRequest()->getPost('widget_type');
        $params = $subject->getRequest()->getPost('parameters', []);

        $typeExplode = explode('\\',$type);
        $moduleName = $typeExplode[0].'_'.$typeExplode[1];
        $widgetName = end($typeExplode);
        //if($type == 'Meridian\Widgets\Block\Widget\PopupFooter') {
        if($moduleName == 'Meridian_Widgets') {

            foreach($params as $key => $value) {
                $pos = strpos($key, 'image_chooser');
                if(($pos !== false) && !empty($value)){
                    if(strpos($value,'/directive/___directive/') !== false) {

                        $parts = explode('/', $value);
                        $keySearch   = array_search("___directive", $parts);
                        if($keySearch !== false) {

                            $url = $parts[$keySearch+1];
                            $url = base64_decode(strtr($url, '-_,', '+/='));

                            $parts = explode('"', $url);
                            $keySearch   = array_search("{{media url=", $parts);
                            $url   = $parts[$keySearch+1];

                            $value = $url;
                        }
                    }
                }
                if(in_array($key,['conditions_encoded','conditions','conditions_serialized'])) continue;
                $params[$key] = $this->helper->encodeWidgetValues($value);
            }

        }
        $asIs = $subject->getRequest()->getPost('as_is');
        $html = $this->widget->getWidgetDeclaration($type, $params, $asIs);
        $subject->getResponse()->setBody($html);
    }
}