<?php

namespace Meridian\Widgets\Plugin\Widget\Controller\Adminhtml\Widget;

use Magento\Framework\Json\Helper\Data as JsonHelper;
use Magento\Widget\Helper\Conditions as conditionsHelper;

class LoadOptions
{
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var \Magento\Framework\App\ViewInterface
     */
    protected $view;

    /**
     * @var \Magento\Widget\Helper\Conditions
     */
    private $conditionsHelper;

    private $widgetsHelper;
    private $jsonHelper;

    /**
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Framework\App\ViewInterface $view
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Meridian\Widgets\Helper\Data $widgetsHelper,
        JsonHelper $jsonHelper,
        conditionsHelper $conditionsHelper,
        \Magento\Framework\App\ViewInterface $view
    ) {
        $this->view = $view;
        $this->objectManager = $objectManager;
        $this->widgetsHelper = $widgetsHelper;
        $this->jsonHelper = $jsonHelper;
        $this->conditionsHelper = $conditionsHelper;
    }
    /**
     * Ajax responder for loading plugin options form
     *
     * @return void
     */
    public function aroundExecute(
        \Magento\Widget\Controller\Adminhtml\Widget\LoadOptions $subject,
        \Closure $proceed
    ) {
        try {
            $this->view->loadLayout();
            if ($paramsJson = $subject->getRequest()->getParam('widget')) {
                $request = $this->jsonHelper->jsonDecode($paramsJson);
                if (is_array($request)) {
                    $optionsBlock = $this->view->getLayout()->getBlock('wysiwyg_widget.options');
                    if (isset($request['widget_type'])) {
                        $optionsBlock->setWidgetType($request['widget_type']);
                    }
                    if (isset($request['values'])) {
                        $typeExplode = explode('\\',$optionsBlock->getWidgetType());
                        $moduleName = $typeExplode[0].'_'.$typeExplode[1];
                        //if($optionsBlock->getWidgetType() === 'Meridian\Widgets\Block\Widget\PopupFooter') {
                        if($moduleName == 'Meridian_Widgets') {
                            $helper = $this->widgetsHelper;
                            foreach($request['values'] as $key => $value) {
                                if(in_array($key,['conditions_encoded','conditions','conditions_serialized'])) continue;
                                $request['values'][$key] = $helper->decodeWidgetValues($value);
                            }
                        } else {
                            $request['values'] = array_map('htmlspecialchars_decode', $request['values']);
                            if (isset($request['values']['conditions_encoded'])) {
                                $request['values']['conditions'] =
                                    $this->conditionsHelper->decode($request['values']['conditions_encoded']);
                            }
                        }
                        $optionsBlock->setWidgetValues($request['values']);
                    }
                }
                $this->view->renderLayout();
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $result = ['error' => true, 'message' => $e->getMessage()];
            $subject->getResponse()->representJson(
                $this->jsonHelper->jsonEncode($result)
            );
        }
    }
}