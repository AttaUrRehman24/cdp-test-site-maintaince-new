<?php

namespace Meridian\ProductRelation\Model\Product\Link\CollectionProvider;

class CompareType implements \Magento\Catalog\Model\ProductLink\CollectionProviderInterface
{
    /**
     * {@inheritdoc}
     */
    public function getLinkedProducts(\Magento\Catalog\Model\Product $product)
    {
        $products = $product->getCompareProducts();

        if (!isset($products)) {
            return [];
        }

        return $products;
    }
}