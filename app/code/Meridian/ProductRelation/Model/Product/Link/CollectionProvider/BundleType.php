<?php

namespace Meridian\ProductRelation\Model\Product\Link\CollectionProvider;

class BundleType implements \Magento\Catalog\Model\ProductLink\CollectionProviderInterface
{
    /**
     * {@inheritdoc}
     */
    public function getLinkedProducts(\Magento\Catalog\Model\Product $product)
    {
        $products = $product->getBundletypeProducts();

        if (!isset($products)) {
            return [];
        }

        return $products;
    }
}