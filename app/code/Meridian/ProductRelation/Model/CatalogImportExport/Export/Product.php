<?php

namespace Meridian\ProductRelation\Model\CatalogImportExport\Export;

class Product extends \Magento\CatalogImportExport\Model\Export\Product
{
    /**
     * {@inheritdoc}
     */
    protected function setHeaderColumns($customOptionsData, $stockItemRows)
    {
        if (!$this->_headerColumns) {
            parent::setHeaderColumns($customOptionsData, $stockItemRows);

            $this->_headerColumns = array_merge(
                $this->_headerColumns,
                [
                    'customtype_skus',
                    'customtype_position',
                    'bundletype_skus',
                    'bundletype_position',
                    'compare_skus',
                    'compare_position'
                ]
            );
        }
    }
}
