<?php

namespace Meridian\ProductRelation\Model\Catalog\Product\Link;

class Proxy extends \Magento\Catalog\Model\Product\Link\Proxy
{
    /**
     * {@inheritdoc}
     */
    public function useCustomtypeLinks()
    {
        return $this->_getSubject()->useCustomtypeLinks();
    }

    public function useBundletypeLinks()
    {
        return $this->_getSubject()->useBundletypeLinks();
    }

    public function useCompareLinks()
    {
        return $this->_getSubject()->useCompareLinks();
    }
}