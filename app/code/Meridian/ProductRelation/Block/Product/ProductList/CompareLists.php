<?php

namespace Meridian\ProductRelation\Block\Product\ProductList;

use Magento\Catalog\Model\ResourceModel\Product\Collection;

class CompareLists extends \Magento\Catalog\Block\Product\AbstractProduct
{
    /**
     * @var int
     */
    protected $_columnCount = 4;

    /**
     * @var  \Magento\Framework\DataObject[]
     */
    protected $_items;
    protected $_itemsCompare;

    /**
     * @var Collection
     */
    protected $_itemCollection;

    /**
     * @var array
     */
    protected $_itemLimits = [];

    /**
     * Checkout session
     *
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * Catalog product visibility
     *
     * @var \Magento\Catalog\Model\Product\Visibility
     */
    protected $_catalogProductVisibility;

    /**
     * Checkout cart
     *
     * @var \Magento\Checkout\Model\ResourceModel\Cart
     */
    protected $_checkoutCart;

    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * Comparable attributes cache
     *
     * @var array
     */
    protected $_comparableAttributes;

    /**
     * Catalog product compare list
     *
     * @var \Magento\Catalog\Model\Product\Compare\ListCompare
     */
    protected $_catalogProductCompareList;

    /**
     * Catalog product compare list
     *
     * @var \Magento\Catalog\Model\ResourceModel\Product\Compare\Item\CollectionFactory
     */
    protected $_itemCompareCollectionFactory;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory
     */
    protected $_attributeCollectionFactory;

    /**
     * Customer Filter
     *
     * @var int
     */
    protected $_customerId = 0;

    /**
     * Visitor Filter
     *
     * @var int
     */
    protected $_visitorId = 0;

    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Eav\Model\Config
     */
    protected $_eavConfig;

    protected $_resourceConnection;

    /**
     * Compare Products comparable attributes cache
     *
     * @var array
     */
    protected $_attributes;

    /**
     * Entity object to define collection's attributes
     *
     * @var \Magento\Eav\Model\Entity\AbstractEntity
     */
    protected $_entity;

    /**
     * Catalog product compare
     *
     * @var \Magento\Catalog\Helper\Product\Compare
     */
    protected $_catalogProductCompare = null;

    /**
     * Current scope (store Id)
     *
     * @var int
     */
    protected $_storeId;

    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $_currentProduct;

    /**
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Magento\Checkout\Model\ResourceModel\Cart $checkoutCart
     * @param \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Checkout\Model\ResourceModel\Cart $checkoutCart,
        \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\Module\Manager $moduleManager,
        \Magento\Catalog\Model\Product\Compare\ListCompare $catalogProductCompareList,
        \Magento\Catalog\Model\ResourceModel\Product\Compare\Item\CollectionFactory $itemCompareCollectionFactory,
        \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory $attributeCollectionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Eav\Model\Config $eavConfig,
        \Magento\Catalog\Helper\Product\Compare $catalogProductCompare,
        array $data = []
    ) {
        $this->_checkoutCart = $checkoutCart;
        $this->_catalogProductVisibility = $catalogProductVisibility;
        $this->_checkoutSession = $checkoutSession;
        $this->moduleManager = $moduleManager;
        $this->_catalogProductCompareList = $catalogProductCompareList;
        $this->_itemCompareCollectionFactory = $itemCompareCollectionFactory;
        $this->_attributeCollectionFactory = $attributeCollectionFactory;
        $this->_storeManager = $storeManager;
        $this->_eavConfig = $eavConfig;
        $this->_resourceConnection = $resourceConnection;
        $this->_catalogProductCompare = $catalogProductCompare;
        parent::__construct(
            $context,
            $data
        );

        $this->setTabTitle();
    }

    /**
     * @return $this
     */
    protected function _prepareData()
    {
        /* @var $product \Magento\Catalog\Model\Product */
        $product = $this->getCurrentProduct();
        $this->_itemCollection = $product->getCompareProductCollection()->setPositionOrder()->addStoreFilter();
        if ($this->moduleManager->isEnabled('Magento_Checkout')) {
            $this->_addProductAttributesAndPrices($this->_itemCollection);
        }

        $this->_itemCollection->setVisibility($this->_catalogProductVisibility->getVisibleInCatalogIds());

        $this->_itemCollection->load();

        /**
         * Updating collection with desired items
         */
        $this->_eventManager->dispatch(
            'catalog_product_compare',
            ['product' => $product, 'collection' => $this->_itemCollection, 'limit' => null]
        );

        foreach ($this->_itemCollection as $product) {
            $product->setDoNotUseCategoryId(true);
        }

        return $this;
    }

    /**
     * @return $this
     */
    protected function _beforeToHtml()
    {
        $this->_prepareData();
        return parent::_beforeToHtml();
    }

    /**
     * @return Collection
     */
    public function getItemCollection()
    {
        /**
         * getIdentities() depends on _itemCollection populated, but it can be empty if the block is hidden
         * @see https://github.com/magento/magento2/issues/5897
         */
        if (is_null($this->_itemCollection)) {
            $this->_prepareData();
        }
        return $this->_itemCollection;
    }

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getItems()
    {
        if (is_null($this->_items)) {
            $this->_items = $this->getItemCollection()->getItems();
        }
        return $this->_items;
    }

    /**
     * @param string $type
     * @return array|int
     */
    public function getItemLimit($type = '')
    {
        if ($type == '') {
            return $this->_itemLimits;
        }
        if (isset($this->_itemLimits[$type])) {
            return $this->_itemLimits[$type];
        } else {
            return 0;
        }
    }

    public function getCompareItems()
    {
        $collection = $this->getCompareCollections();
        return $collection;
    }

    public function getCompareCollections()
    {
        if ($this->_itemsCompare === null) {
            /** @var \Magento\Catalog\Model\Product $currentProduct */
            $currentProduct = $this->getProduct();

            $this->_itemsCompare = $currentProduct->getCompareProductCollection()->setPositionOrder()->addStoreFilter();
            $this->_itemsCompare->addAttributeToSelect(
                $this->_catalogConfig->getProductAttributes()
            );
            $this->loadComparableAttributes($this->_itemsCompare);
            $this->_itemsCompare->addMinimalPrice()->addTaxPercents();
            /*$this->_itemsCompare->setVisibility(
                $this->_catalogProductVisibility->getVisibleInSiteIds()
            );*/
        }

        return $this->_itemsCompare;
    }

    protected function loadComparableAttributes(
        \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
    )
    {
        $comparableAttributes = $this->getComparableAttributes();
        $attributes = [];
        foreach ($comparableAttributes as $attribute) {
            $attributes[] = $attribute->getAttributeCode();
        }
        $collection->addAttributeToSelect($attributes);

        return $collection;$this->_coreRegistry->registry('product');
    }

    public function getCurrentProduct()
    {
        if($this->_currentProduct === null) {
            /* @var $product \Magento\Catalog\Model\Product */
            $this->_currentProduct = $this->_coreRegistry->registry('product');
        }
        return $this->_currentProduct;
    }

    public function getConnection()
    {
        return $this->_resourceConnection->getConnection();
    }

    public function getStoreId()
    {
        if ($this->_storeId === null) {
            $this->_storeId = $this->_storeManager->getStore()->getId();
        }
        return $this->_storeId;
    }

    protected function _getAttributeSetIds()
    {
        // prepare compare items table conditions
        $compareConds = ['compare.linked_product_id=entity.entity_id'];
        $compareConds[] = $this->getConnection()->quoteInto('compare.product_id = ?', $this->getCurrentProduct()
            ->getId());
        // prepare website filter
        $websiteId = (int)$this->_storeManager->getStore($this->getStoreId())->getWebsiteId();
        $websiteConds = [
            'website.product_id = entity.entity_id',
            $this->getConnection()->quoteInto('website.website_id = ?', $websiteId),
        ];
        $catalogEntityTable  = $this->getConnection()->getTableName('catalog_product_entity');
        $catalogProductWebsite  = $this->getConnection()->getTableName('catalog_product_website');
        //$catalogCompareItem  = $this->getConnection()->getTableName('catalog_compare_item');
        $catalogCompareItem  = $this->getConnection()->getTableName('catalog_product_link');
        // retrieve attribute sets
        $select = $this->getConnection()->select()->distinct(
            true
        )->from(
            ['entity' => $catalogEntityTable],
            'attribute_set_id'
        )->join(
            ['website' => $catalogProductWebsite],
            join(' AND ', $websiteConds),
            []
        )->join(
            ['compare' => $catalogCompareItem],
            join(' AND ', $compareConds),
            []
        );
        return $this->getConnection()->fetchCol($select);
    }

    protected function _getAttributeIdsBySetIds(array $setIds)
    {
        $select = $this->getConnection()->select()->distinct(
            true
        )->from(
            $this->getConnection()->getTableName('eav_entity_attribute'),
            'attribute_id'
        )->where(
            'attribute_set_id IN(?)',
            $setIds
        );
        return $this->getConnection()->fetchCol($select);
    }

    public function getComparableAttributes()
    {
        if ($this->_comparableAttributes === null) {
            $this->_comparableAttributes = [];
            $setIds = $this->_getAttributeSetIds();
            if ($setIds) {
                $attributeIds = $this->_getAttributeIdsBySetIds($setIds);

                $select = $this->getConnection()->select()->from(
                    ['main_table' => $this->getConnection()->getTableName('eav_attribute')]
                )->join(
                    ['additional_table' => $this->getConnection()->getTableName('catalog_eav_attribute')],
                    'additional_table.attribute_id=main_table.attribute_id'
                )->joinLeft(
                    ['al' => $this->getConnection()->getTableName('eav_attribute_label')],
                    'al.attribute_id = main_table.attribute_id AND al.store_id = ' . (int)$this->getStoreId(),
                    [
                        'store_label' => $this->getConnection()->getCheckSql(
                            'al.value IS NULL',
                            'main_table.frontend_label',
                            'al.value'
                        )
                    ]
                )->where(
                    'additional_table.is_comparable=?',
                    1
                )->where(
                    'main_table.attribute_id IN(?)',
                    $attributeIds
                );
                $attributesData = $this->getConnection()->fetchAll($select);
                if ($attributesData) {
                    $entityType = \Magento\Catalog\Model\Product::ENTITY;
                    $this->_eavConfig->importAttributesData($entityType, $attributesData);
                    foreach ($attributesData as $data) {
                        $attribute = $this->_eavConfig->getAttribute($entityType, $data['attribute_code']);
                        $this->_comparableAttributes[$attribute->getAttributeCode()] = $attribute;
                    }
                    unset($attributesData);
                }
            }
        }
        return $this->_comparableAttributes;
    }

    public function getCompareAttributes()
    {
        $collection = $this->_attributeCollectionFactory->create()->addVisibleFilter()->addFieldToFilter('is_comparable',1);
        return $collection;
    }

    /**
     * Retrieve Product Compare Attributes
     *
     * @return array
     */
    public function getAttributes()
    {
        //if ($this->_attributes === null) {
            //$this->_attributes = $this->getItems()->getComparableAttributes();
            $this->_attributes = $this->getComparableAttributes();
        //}

        return $this->_attributes;
    }

    public function getListUrlCompare()
    {
        $itemIds = $this->getItemCollection()->getAllIds();
        $params = [
            'items' => implode(',', $itemIds),
            \Magento\Framework\App\ActionInterface::PARAM_NAME_URL_ENCODED =>
                $this->_catalogProductCompare->getEncodedUrl()
        ];
        $url = $this->getUrl('catalog/product_compare', $params);
        if(strpos($url,'%2C')  !== false) {
            $url = str_replace('%2C',',',$url);
        }
        return $url;
    }

    public function getProductAttributeValue($product, $attribute)
    {
        if (!$product->hasData($attribute->getAttributeCode())) {
            return __('N/A');
        }

        if ($attribute->getSourceModel() || in_array(
                $attribute->getFrontendInput(),
                ['select', 'boolean', 'multiselect']
            )
        ) {
            //$value = $attribute->getSource()->getOptionText($product->getData($attribute->getAttributeCode()));
            $value = $attribute->getFrontend()->getValue($product);
        } else {
            $value = $product->getData($attribute->getAttributeCode());
        }
        return (string)$value == '' ? __('No') : $value;
    }

    /**
     * Set tab title
     *
     * @return void
     */
    public function setTabTitle()
    {
        $count = $this->getCompareCollections()->count();
//        if($this->getCurrentProduct()->getTypeId() === \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE) {
//            $productsCompare = $this->getCurrentProduct()->getTypeInstance(true)->getUsedProducts($this->getCurrentProduct());
//            $count = count($productsCompare);
//        }
        $title = $count
            ? __('Modèles %1', '<span class="counter">' . $count . '</span>')
            : __('Modèles');
        $this->setTitle($title);
    }
}
