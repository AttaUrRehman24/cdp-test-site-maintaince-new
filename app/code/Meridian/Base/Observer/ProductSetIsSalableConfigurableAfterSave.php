<?php
/**
 * Created by PhpStorm.
 * User: kate
 * Date: 6/14/18
 * Time: 12:03 PM
 */

namespace Meridian\Base\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\CatalogInventory\Model\Stock\Status;

class ProductSetIsSalableConfigurableAfterSave implements ObserverInterface
{
    /**
     * @var \Magento\CatalogInventory\Helper\Stock
     */
    protected $stockHelper;

    /**
     * @var \Magento\CatalogInventory\Model\ResourceModel\Stock\Status
     */
    protected $resourceStockStatus;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resourceConnection;

    /**
     * @param \Magento\CatalogInventory\Helper\Stock $stockHelper
     */
    public function __construct(
        \Magento\CatalogInventory\Helper\Stock $stockHelper,
        \Magento\CatalogInventory\Model\ResourceModel\Stock\Status $resourceStockStatus,
        \Magento\Framework\App\ResourceConnection $resourceConnection
    ) {
        $this->stockHelper = $stockHelper;
        $this->resourceStockStatus = $resourceStockStatus;
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * Add stock information to product
     *
     * @param EventObserver $observer
     * @return void
     */
    public function execute(EventObserver $observer)
    {
        $product = $observer->getEvent()->getProduct();
        if ($product instanceof \Magento\Catalog\Model\Product) {
            if($product->getTypeId() === \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE) {
                $attributes = (array)$product->getConfigurableAttributesAsArray($product);
                if(!$product->getIsSalable() && !$attributes) {
                    $this->stockHelper->assignStatusToProduct($product,true);

                    $values = $this->getCatalogInventoryWebsiteIds();
                    if (!empty($values)) {
                        foreach ($values as $value) {
                            $this->resourceStockStatus->saveProductStatus(
                                $product->getId(),
                                Status::STATUS_OUT_OF_STOCK,
                                0,
                                $value['website_id'],
                                $value['stock_id']
                            );
                        }
                    }
                }
            } elseif ($product->getTypeId() === \Magento\Bundle\Model\Product\Type::TYPE_CODE) {
                $this->stockHelper->assignStatusToProduct($product,true);

                $values = $this->getCatalogInventoryWebsiteIds();
                if (!empty($values)) {
                    foreach ($values as $value) {
                        $this->resourceStockStatus->saveProductStatus(
                            $product->getId(),
                            Status::STATUS_IN_STOCK,
                            0,
                            $value['website_id'],
                            $value['stock_id']
                        );
                    }
                }
            }
        }
    }

    protected function getCatalogInventoryWebsiteIds()
    {
        $connection = $this->resourceConnection->getConnection();
        $collection = $connection->select()
            ->from($connection->getTableName('cataloginventory_stock'));
        $result = $connection->fetchAll($collection);
        return $result;
    }
}