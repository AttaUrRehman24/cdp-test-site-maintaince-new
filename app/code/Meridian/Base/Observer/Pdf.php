<?php

namespace Meridian\Base\Observer;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Event\ObserverInterface;

class Pdf implements ObserverInterface
{

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;
    protected $_uploaderFactory;
    protected $_filesystem;
    protected $_fileUploaderFactory;
    protected $_logger;

    /**
     * @var \Magento\Backend\App\Action\Context
     */
    private $context;

    /**
     * Construct
     *
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Magento\Backend\App\Action\Context $context
    ) {
        $this->_filesystem = $filesystem;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->_logger = $logger;
        $this->context = $context;
        $this->request = $request;
    }

    /**
     * Generate urls for UrlRewrite and save it in storage
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {

        /** @var Product $product */
        $product = $observer->getEvent()->getProduct();

        $files = $this->request->getFiles('product');
        $productRequest = $this->request->getParam('product');

        if (isset($productRequest['pdf_file'])) {
            $pdf = $productRequest['pdf_file'];
            if (isset($pdf[0]['name']) && $pdf[0]['name'] != $product->getPdfFile()) {
                $product->setData('pdf_file', $pdf[0]['name']);
                $product->getResource()->saveAttribute($product, 'pdf_file');
            }
        } else {
            $product->setData('pdf_file', '');
            $product->getResource()->saveAttribute($product, 'pdf_file');
        }
    }

    protected function getFileData($files,$code)
    {
        $file = [];
        foreach ($files as $key => $value) {
            if (isset($value[$code])) {
                $file[$key] = $value[$code];
            }
        }

        return $file;
    }
}
