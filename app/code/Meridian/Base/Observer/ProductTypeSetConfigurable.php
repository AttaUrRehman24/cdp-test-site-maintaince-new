<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Meridian\Base\Observer;

use Magento\Catalog\Model\Product;
use Magento\CatalogUrlRewrite\Model\ProductUrlPathGenerator;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class ProductTypeSetConfigurable implements ObserverInterface
{
    /**
     * @var \Magento\CatalogUrlRewrite\Model\ProductUrlPathGenerator
     */
    protected $productUrlPathGenerator;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    /**
     * @var \Magento\CatalogInventory\Helper\Stock
     */
    protected $stockHelper;

    /**
     * @param ProductUrlPathGenerator $productUrlPathGenerator
     */
    public function __construct(
        ProductUrlPathGenerator $productUrlPathGenerator,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\CatalogInventory\Helper\Stock $stockHelper
    )
    {
        $this->productUrlPathGenerator = $productUrlPathGenerator;
        $this->_request = $request;
        $this->stockHelper = $stockHelper;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var \Magento\Catalog\ModelProduct $product */
        $product = $observer->getEvent()->getProduct();
        $productTypeId = $this->_request->getParam('type');
        //$reqeustParams = $this->_request->getParams();
        $attributes = $this->_request->getParam('attributes');

        if($productTypeId === \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE) {
            $product->setTypeId(\Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE);
            if(!$product->getHasOptions()){
                $product->setHasOptions(true);
            }
            if(!$product->getIsSalable() && empty($attributes)){
                $product->setIsSalable(true);
                $this->stockHelper->assignStatusToProduct($product,true);
            }
        } elseif ($product->getTypeId() === \Magento\Bundle\Model\Product\Type::TYPE_CODE) {
            $this->stockHelper->assignStatusToProduct($product,true);
        }
    }
}
