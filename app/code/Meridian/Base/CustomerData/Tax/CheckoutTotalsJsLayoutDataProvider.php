<?php

namespace Meridian\Base\CustomerData\Tax;


class CheckoutTotalsJsLayoutDataProvider extends \Magento\Tax\CustomerData\CheckoutTotalsJsLayoutDataProvider
{
    /**
     * @var \Meridian\Base\Helper\Data
     */
    protected $_helperBase;

    public function __construct(\Magento\Tax\Model\Config $taxConfig)
    {
        parent::__construct($taxConfig);
    }

    /**
     * Get totals config
     *
     * @return array
     */
    protected function getTotalsConfig()
    {
        //$currentPirceDisplay = $this->getHelper()->currentPriceDisplayType();
        return [
            'display_cart_subtotal_incl_tax' => (int)$this->taxConfig->displayCartSubtotalInclTax(),
            'display_cart_subtotal_excl_tax' => (int)$this->taxConfig->displayCartSubtotalExclTax(),
        ];
    }

    /**
     * @return \Meridian\Base\Helper\Data|mixed
     */
    public function getHelper()
    {
        if($this->_helperBase === null){
            $this->_helperBase = \Magento\Framework\App\ObjectManager::getInstance()->get(
                \Meridian\Base\Helper\Data::class
            );
        }
        return $this->_helperBase;
    }
}