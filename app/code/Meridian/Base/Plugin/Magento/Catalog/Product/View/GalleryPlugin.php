<?php

namespace Meridian\Base\Plugin\Magento\Catalog\Product\View;

use Magento\Catalog\Model\Product;
use Magento\Framework\Serialize\Serializer\Json;

class GalleryPlugin
{
    /**
     * @var \Meridian\Base\Helper\Data
     */
    private $helperBase;

    /**
     * @var Json
     */
    private $json;

    public function __construct(
        \Meridian\Base\Helper\Data $helperBase,
        Json $json
    )
    {
        $this->helperBase = $helperBase;
        $this->json = $json;
    }

    public function afterGetGalleryImagesJson(
        \Magento\Catalog\Block\Product\View\Gallery $subject,
        $result
    )
    {
        $resultNew = [];
        $resultTmp = $this->json->unserialize($result);
        $parentProduct = $subject->getProduct();
        foreach ($resultTmp as $item){
            if(isset($item['caption'])){
                $item['caption'] = $this->helperBase->getProductImageAlt($parentProduct);
            }
            $resultNew[] = $item;
        }

        return $this->json->serialize($resultNew);
    }
}