<?php

namespace Meridian\Base\Block;

use \Magento\Framework\View\Element\Template;
use \Meridian\Base\Helper\Data as MeridianBaseHelper;
//use \Magento\Catalog\Block\Product\View as ProductView;

class PrixForm extends Template
{
    protected $helper;

    public function __construct(
        MeridianBaseHelper $helper,
        Template\Context $context,
        array $data = []
    )
    {
        $this->helper        = $helper;
        parent::__construct($context, $data);
    }

    public function getFormAction() {
        return $this->getUrl('webmerattach/prixform/send');
    }

    public function getCurrentUrl() {
        return $this->_storeManager->getStore()->getCurrentUrl();
    }
}