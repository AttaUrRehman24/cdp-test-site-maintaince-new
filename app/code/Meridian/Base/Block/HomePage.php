<?php
/**
 * Created by PhpStorm.
 * User: kate
 * Date: 5/21/18
 * Time: 3:35 PM
 */

namespace Meridian\Base\Block;

use Meridian\Base\Helper\Data as MeridianBaseHelper;
use Magento\Customer\Model\Context;


class HomePage extends \Magento\Framework\View\Element\Template
{

    protected $helper;
    /**
     * @var \Magento\Framework\App\Http\Context
     */
    protected $httpContext;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\Http\Context $httpContext,
        MeridianBaseHelper $helper,
        array $data = []
    ) {
        $this->httpContext = $httpContext;
        $this->helper = $helper;
        parent::__construct($context, $data);
    }

    public function isHomePage()
    {
        return $this->helper->isHomePage();
    }
}