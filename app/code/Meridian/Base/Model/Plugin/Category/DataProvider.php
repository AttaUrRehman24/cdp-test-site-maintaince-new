<?php

namespace Meridian\Base\Model\Plugin\Category;

use Magento\Eav\Model\Config;

class DataProvider extends \Magento\Catalog\Model\Category\DataProvider
{

    /**
     * @var Config
     */
    private $eavConfig;

    public function __construct(
        Config $eavConfig
    ) {
        $this->eavConfig = $eavConfig;
    }

    /**
     * @param \Magento\Catalog\Model\Category\DataProvider $subject
     * @param $result
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function afterPrepareMeta(\Magento\Catalog\Model\Category\DataProvider $subject, $result)
    {
        $meta = $result;
        $meta = array_replace_recursive($meta, $this->prepareFieldsMeta(
            $this->getFieldsMap(),
            $subject->getAttributesMeta($this->eavConfig->getEntityType('catalog_category'))
        ));
        return $meta;

    }

    /**
     * @param array $fieldsMap
     * @param array $fieldsMeta
     * @return array
     */
    private function prepareFieldsMeta($fieldsMap, $fieldsMeta)
    {
        $result = [];
        foreach ($fieldsMap as $fieldSet => $fields) {
            foreach ($fields as $field) {
                if (isset($fieldsMeta[$field])) {
                    $result[$fieldSet]['children'][$field]['arguments']['data']['config'] = $fieldsMeta[$field];
                }
            }
        }
        return $result;
    }

    /**
     * @return array
     */
    protected function getFieldsMap()
    {
        $fields = [];
        $fields['megamenu'][] = 'menu_featured_content';
        $fields['megamenu'][] = 'menu_footer_content';
        $fields['content'][] = 'garantie_description';
        $fields['content'][] = 'thumbnail';

        return $fields;
    }
}