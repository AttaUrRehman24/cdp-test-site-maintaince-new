<?php

namespace Meridian\Base\Model;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Event\ObserverInterface;

class ImageUploader
{

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;
    protected $_uploaderFactory;
    /** @var \Magento\Framework\Filesystem  */
    protected $_filesystem;
    protected $_fileUploaderFactory;
    protected $_logger;

    /**
     * @var \Magento\Backend\App\Action\Context
     */
    private $context;
    protected $storeManager;

    /**
     * Construct
     *
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Backend\App\Action\Context $context
    ) {
        $this->_filesystem = $filesystem;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->_logger = $logger;
        $this->context = $context;
        $this->request = $request;
        $this->storeManager = $storeManager;
    }

    /**
     * Generate urls for UrlRewrite and save it in storage
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function saveFileToTmpDir($attributeName = null)
    {
        $files = $this->request->getFiles('product');

        $path = $this->_filesystem->getDirectoryRead(
            DirectoryList::MEDIA
        )->getAbsolutePath(
            'catalog/product/pdf/'
        );
        $files = $this->request->getFiles('product');

        try {
            /** @var $uploader \Magento\MediaStorage\Model\File\Uploader */
            $uploader = $this->_fileUploaderFactory->create(['fileId' => $files[$attributeName]]);
//            $uploader->setAllowedExtensions(['pdf']);
            $uploader->setAllowRenameFiles(true);
            $result = $uploader->save($path);
            $result['tmp_name'] = str_replace('\\', '/', $result['tmp_name']);
            $result['path'] = str_replace('\\', '/', $result['path']);
            $result['url'] = $this->storeManager
                    ->getStore()
                    ->getBaseUrl(
                        \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
                    ) . $this->getFilePath('catalog/product/pdf', $result['file']);
            $result['name'] = $result['file'];
            $this->_logger->info($result['file']);
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __($e->getMessage())
            );
        }

        return $result;
    }

    public function getFilePath($path, $imageName)
    {
        return rtrim($path, '/') . '/' . ltrim($imageName, '/');
    }

    protected function getFileData($files,$code)
    {
        $file = [];
        foreach ($files as $key => $value) {
            if (isset($value[$code])) {
                $file[$key] = $value[$code];
            }
        }

        return $file;
    }
}
