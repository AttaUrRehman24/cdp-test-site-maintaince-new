<?php

namespace Meridian\Base\Model\Product\Attribute\Backend;

use Magento\Framework\App\Filesystem\DirectoryList;

class Pdf extends \Magento\Eav\Model\Entity\Attribute\Backend\AbstractBackend {

    protected $_uploaderFactory;
    protected $_filesystem;
    protected $_fileUploaderFactory;
    protected $_logger;

    /**
     * @var \Magento\Backend\App\Action\Context
     */
    private $context;

    /**
     * Construct
     *
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Magento\Backend\App\Action\Context $context
    ) {
        $this->_filesystem = $filesystem;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->_logger = $logger;
        $this->context      = $context;
    }

    public function afterSave($object)
    {
        $attrCode = $this->getAttribute()->getName();
        $value = $object->getData($attrCode);
        $downloads = $this->context->getRequest()->getFiles('product');
        //$value = $object->getData($this->getAttribute()->getName() . '_additional_data');

        // if no image was set - nothing to do
        if (empty($value) && empty($_FILES)) {
            return $this;
        }

        if (is_array($value) && !empty($value['delete'])) {
            $object->setData($this->getAttribute()->getName(), '');
            $this->getAttribute()->getEntity()->saveAttribute($object, $attrCode);
            return $this;
        }
        if (isset($downloads[$attrCode]) && isset($downloads[$attrCode]['tmp_name']) && !empty($downloads[$attrCode]['tmp_name'])) {
            $path = $this->_filesystem->getDirectoryRead(
                DirectoryList::MEDIA
            )->getAbsolutePath(
                'catalog/product/pdf/'
            );
            $this->_logger->debug($path);
            $file = $this->getFileData($_FILES, $attrCode);
            try {
                /** @var $uploader \Magento\MediaStorage\Model\File\Uploader */
                $uploader = $this->_fileUploaderFactory->create(['fileId' => $file]);
                $uploader->setAllowedExtensions(['pdf']);
                $uploader->setAllowRenameFiles(true);
                $result = $uploader->save($path);

                $object->setData($this->getAttribute()->getName(), $result['file']);
                $this->getAttribute()->getEntity()->saveAttribute($object, $this->getAttribute()->getName());
            } catch (\Exception $e) {
                if ($e->getCode() != \Magento\MediaStorage\Model\File\Uploader::TMP_NAME_EMPTY) {
                    $this->_logger->critical($e);
                }
            }

        }
        return $this;
    }

    /**
     * Receiving uploaded file data
     *
     * @return array
     * @since 100.1.0
     */
    /**
     * @param $files
     * @param $code
     * @return array
     */
    protected function getFileData($files,$code)
    {
        $file = [];
//        $file['tmp_name'] = $files['product']['tmp_name'][$code];
//        $file['name'] = isset($files['product']['value'][$code]) ? $files['product']['value'][$code] : $files['product']['name'][$code];
        if(is_array($files) && isset($files['product']) && isset($files['product']['tmp_name'][$code])) {
            foreach ($files['product'] as $key => $value) {
//                $file[$key] = $value[$code];
            }
        }

        return $file;
    }
}