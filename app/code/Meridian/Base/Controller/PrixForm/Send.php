<?php

namespace Meridian\Base\Controller\PrixForm;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Contact\Model\MailInterface;
use Meridian\Base\Model\Email\Prix\Notification as PrixModelEmail;
use Magento\Framework\DataObject;
use Magento\Framework\App\ObjectManager;

class Send extends \Magento\Catalog\Controller\Product
{
    const EMAIL_TEMPLATE_ID = 'comptoir_config_product_page_email_template';

    /**
     * Recipient email config path
     */
    const XML_PATH_EMAIL_RECIPIENT = 'comptoir_config/product_page/recipient_email';

    /**
     * Sender email config path
     */
    const XML_PATH_EMAIL_SENDER = 'comptoir_config/product_page/sender_email_identity';

    /**
     * Email template config path
     */
    const XML_PATH_EMAIL_TEMPLATE = 'comptoir_config/product_page/email_template';

    /**
     * @var \Magento\Catalog\Helper\Product\View
     */
    protected $viewHelper;

    /**
     * @var \Magento\Framework\Controller\Result\ForwardFactory
     */
    protected $resultForwardFactory;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * @var MailInterface
     */
    private $mail;

    /**
     * @var PrixModelEmail
     */
    private $mailPrix;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $_transportBuilder;

    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    protected $inlineTranslation;

    /**
     * @var \Meridian\Base\Helper\Data
     */
    protected $helperBase;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    public function __construct(
        Context $context,
        \Magento\Catalog\Helper\Product\View $viewHelper,
        \Magento\Framework\Controller\Result\ForwardFactory $resultForwardFactory,
        PageFactory $resultPageFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        MailInterface $mail,
        PrixModelEmail $mailPrix,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Meridian\Base\Helper\Data $helperBase,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->viewHelper = $viewHelper;
        $this->resultForwardFactory = $resultForwardFactory;
        $this->resultPageFactory = $resultPageFactory;
        $this->_storeManager = $storeManager;
        $this->mail = $mail;
        $this->mailPrix = $mailPrix;
        $this->_transportBuilder = $transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
        $this->helperBase = $helperBase;
        $this->_logger = $logger;
        parent::__construct($context);
    }

    public function execute()
    {
        $post = $this->getRequest()->getPostValue();
        $resultRedirect = $this->resultRedirectFactory->create();
        if (!$post) {
            $url = $this->_redirect->getRefererUrl();
            $this->_redirect($url);
            return;
        }
        try {
            $postObject = new \Magento\Framework\DataObject();
            $postObject->setData($post);

            $error = false;

            if (!\Zend_Validate::is(trim($post['firstname']), 'NotEmpty')) {
                $error = true;
            }
            if (!\Zend_Validate::is(trim($post['lastname']), 'NotEmpty')) {
                $error = true;
            }
            if (!\Zend_Validate::is(trim($post['productname']), 'NotEmpty')) {
                $error = true;
            }
            if (!\Zend_Validate::is(trim($post['prixproduct']), 'NotEmpty')) {
                $error = true;
            }
            if (!\Zend_Validate::is(trim($post['addressurl']), 'NotEmpty')) {
                $error = true;
            }
            if (!\Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
                $error = true;
            }

            $this->validateEmailFormat(trim($post['email']));

            if ($error) {
                throw new \Exception();
            }

            //$this->sendEmail($post);
            $this->sendCustomEmail($post);

            $this->messageManager->addSuccessMessage(
                __('Your request has been sent')
            );
            $this->getDataPersistor()->clear('prix_form');
            $this->_redirect($this->_redirect->getRefererUrl());
            return;
        } catch (\Exception $e) {
            $this->_logger->critical($e);
            $this->messageManager->addError(
                __('We can\'t process your request right now. Sorry, that\'s all we know.')
            );
            $this->getDataPersistor()->set('prix_form', $post);
            $this->_redirect($this->_redirect->getRefererUrl());
            return;
        }
    }

    /**
     * @param $email
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Validate_Exception
     */
    protected function validateEmailFormat($email)
    {
        if (!\Zend_Validate::is($email, 'EmailAddress')) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Please enter a valid email address.'));
        }
    }

    /**
     * Get Data Persistor
     *
     * @return DataPersistorInterface
     */
    private function getDataPersistor()
    {
        if ($this->dataPersistor === null) {
            $this->dataPersistor = ObjectManager::getInstance()
                ->get(DataPersistorInterface::class);
        }

        return $this->dataPersistor;
    }

    /**
     * @param array $post Post data from contact form
     * @return void
     */
    private function sendEmail($post)
    {
        $this->mail->send(
            $post['email'],
            ['data' => new DataObject($post)]
        );
    }

    private function sendCustomEmail($post)
    {

        $this->mailPrix->send(
            $post['email'],
            ['data' => new DataObject($post)]
        );

        return $this;
    }
}