<?php

namespace Meridian\Customer\Block;

/**
 * Customer login info block
 *
 * @api
 * @since 100.0.2
 */
class Guestpage extends \Magento\Framework\View\Element\Template
{
}
