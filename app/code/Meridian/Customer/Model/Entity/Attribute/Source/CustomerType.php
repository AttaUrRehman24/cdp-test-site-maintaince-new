<?php

namespace Meridian\Customer\Model\Entity\Attribute\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class CustomerType extends AbstractSource
{
    public function getAllOptions()
    {
        return [
            'pro' => [
                'label' => 'Professionnel',
                'value' => 'pro'
            ],
            'no_pro' => [
                'label' => 'Particulier',
                'value' => 'no_pro'
            ]
        ];
    }
}