<?php

namespace Meridian\Customer\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Magento\Customer\Model\Customer;

/**
 * Upgrade Data script
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * EAV setup factory
     *
     * @var \Magento\Eav\Setup\EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @var CustomerSetupFactory
     */
    private $customerSetupFactory;

    /**
     * @var AttributeSetFactory
     */
    private $attributeSetFactory;

    public function __construct(
        \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory,
        CustomerSetupFactory $customerSetupFactory,
        AttributeSetFactory $attributeSetFactory
    )
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->customerSetupFactory = $customerSetupFactory;
        $this->attributeSetFactory = $attributeSetFactory;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if ($context->getVersion() && version_compare($context->getVersion(), '1.0.3') < 0) {
            /** @var \Magento\Customer\Setup\CustomerSetup $customerSetup */
            $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
            $customerEntity = $customerSetup->getEavConfig()->getEntityType(Customer::ENTITY);
            $attributeSetId = $customerEntity->getDefaultAttributeSetId();
            $attributeSet = $this->attributeSetFactory->create();
            $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);
            $attributesToAdd = [];

            $attrCode = 'subscribe_partners';
            $customerSetup->addAttribute(Customer::ENTITY, $attrCode,
                [
                    'label' => 'J’accepte de recevoir les offres de vos partenaires',
                    'type' => 'int',
                    'visible' => true,
                    'required' => false,
                    'unique' => false,
                    'filterable' => 1,
                    'sort_order' => 700,
                    'default' => 0,
                    'input' => 'select',
                    'system' => 0,
                    'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                    'grid' => true,
                    'comment' => '',
                    'sort_order' => 999
                ]);
            $attributesToAdd[] = $attrCode;

            /** Add the attributes to the attribute set and the common forms */
            foreach ($attributesToAdd as $code) {
                $attribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, $code);
                $attribute->addData([
                    'attribute_set_id' => $attributeSetId,
                    'attribute_group_id' => $attributeGroupId,
                    'used_in_forms' => ['adminhtml_customer', 'customer_account_create']
                ]);
                $attribute->save();
            }
        }

        if ($context->getVersion() && version_compare($context->getVersion(), '1.0.5') < 0) {
            /** @var \Magento\Customer\Setup\CustomerSetup $customerSetup */
            $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
            $customerEntity = $customerSetup->getEavConfig()->getEntityType(Customer::ENTITY);
            $attributeSetId = $customerEntity->getDefaultAttributeSetId();
            $attributeSet = $this->attributeSetFactory->create();
            $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);
            $attributesToAdd = [];

            $attrCode = 'customer_type';
            $attributesToAdd[] = $attrCode;

            /** Add the attributes to the attribute set and the common forms */
            foreach ($attributesToAdd as $code) {
                $attribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, $code);
                $attribute->addData([
                    'attribute_set_id' => $attributeSetId,
                    'attribute_group_id' => $attributeGroupId,
                    'used_in_forms' => ['adminhtml_customer', 'customer_account_create', 'customer_account_edit']
                ]);
                $attribute->save();
            }
        }

        if ($context->getVersion() && version_compare($context->getVersion(), '1.0.6') < 0) {
            /** @var \Magento\Customer\Setup\CustomerSetup $customerSetup */
            $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
            $customerEntity = $customerSetup->getEavConfig()->getEntityType(Customer::ENTITY);
            $attributeSetId = $customerEntity->getDefaultAttributeSetId();
            $attributeSet = $this->attributeSetFactory->create();
            $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);
            $attributesToAdd = [];

            $attrCode = 'subscribe_partners';
            $attributesToAdd[] = $attrCode;

            /** Add the attributes to the attribute set and the common forms */
            foreach ($attributesToAdd as $code) {
                $attribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, $code);
                $attribute->addData([
                    'attribute_set_id' => $attributeSetId,
                    'attribute_group_id' => $attributeGroupId,
                    'used_in_forms' => ['adminhtml_customer', 'customer_account_create', 'customer_account_edit']
                ]);
                $attribute->save();
            }
        }

        if ($context->getVersion() && version_compare($context->getVersion(), '1.0.7') < 0) {

            /** @var \Magento\Customer\Setup\CustomerSetup $customerSetup */
            $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
            $customerEntity = $customerSetup->getEavConfig()->getEntityType(Customer::ENTITY);
            $attributeSetId = $customerEntity->getDefaultAttributeSetId();
            $attributeSet = $this->attributeSetFactory->create();
            $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);
            $attributesToAdd = [];

            $attrCode = 'company';
            $customerSetup->addAttribute(Customer::ENTITY, $attrCode,
                [
                    'label' => 'Company',
                    'type' => 'text',
                    'visible' => true,
                    'required' => true,
                    'unique' => false,
                    'filterable' => 1,
                    'default' => '',
                    'input' => 'text',
                    'system' => 0,
                    'sort_order' => 999
                ]);
            $attributesToAdd[] = $attrCode;

            /** Add the attributes to the attribute set and the common forms */
            foreach ($attributesToAdd as $code) {
                $attribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, $code);
                $attribute->addData([
                    'attribute_set_id' => $attributeSetId,
                    'attribute_group_id' => $attributeGroupId,
                    'used_in_forms' => ['adminhtml_customer', 'customer_account_create', 'customer_account_edit']
                ]);
                $attribute->save();
            }
        }


        if ($context->getVersion() && version_compare($context->getVersion(), '1.0.8') < 0) {
            $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
            $customerEntity = $customerSetup->getEavConfig()->getEntityType(Customer::ENTITY);

            $customerSetup->updateAttribute(
                'customer',
                'company',
                'is_required',
                0
            );
            $attributeSetId = $customerEntity->getDefaultAttributeSetId();
            $attributeSet = $this->attributeSetFactory->create();
            $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);

            $attribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, 'company');
            $attribute->addData([
                'attribute_set_id' => $attributeSetId,
                'attribute_group_id' => $attributeGroupId,
                'used_in_forms' => ['adminhtml_customer', 'customer_account_create', 'customer_account_edit']
            ]);
            $attribute->save();
        }
        $setup->endSetup();
    }
}
