<?php

namespace Meridian\Customer\Observer;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Event\ObserverInterface;

class RedirectOldCustomer implements ObserverInterface
{
    /**
     * @var \Magento\Backend\App\Action\Context
     */
    private $context;

    /**
     * @var \Magento\Framework\App\ResponseFactory
     */
    private $responseFactory;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    private $url;

    /**
     * @var \Magento\Customer\Model\Session
     */
    private $customerSession;

    /**
     * @var \Magento\Framework\Controller\Result\RedirectFactory
     */
    private $redirectFactory;


    /**
     * RedirectOldCustomer constructor.
     * @param \Magento\Framework\App\ResponseFactory $responseFactory
     * @param \Magento\Framework\UrlInterface $url
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Framework\Controller\Result\RedirectFactory $redirectFactory
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        \Magento\Framework\App\ResponseFactory $responseFactory,
        \Magento\Framework\UrlInterface $url,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Controller\Result\RedirectFactory $redirectFactory,
        \Magento\Backend\App\Action\Context $context
    ) {
        $this->context = $context;
        $this->responseFactory = $responseFactory;
        $this->url = $url;
        $this->customerSession = $customerSession;
        $this->redirectFactory = $redirectFactory;
    }

    /**
     * Redirect customers without customer type to account edit page
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $request = $observer->getRequest();
        if ($this->isLoggedIn() && $this->isEmptyCustomerType() && !$this->isAllowPage($request)) {
            /** @var \Magento\Checkout\Controller\Index\Index $controllerAction */
            $controllerAction = $observer->getControllerAction();
            $redirectionUrl = $this->url->getUrl('customer/account/edit');
            $controllerAction->getResponse()->setRedirect($redirectionUrl);
        }

        return $this;
    }

    /**
     * Check is customer is logged in
     * @return bool
     */
    public function isLoggedIn(){
        return $this->customerSession->isLoggedIn();
    }

    /**
     * Check if customer has customer type
     * @return bool
     */
    public function isEmptyCustomerType(){
        $customer = $this->customerSession->getCustomer();
        $customerType = $customer->getCustomerType();
        return is_null($customerType) ? true : false;
    }

    /**
     * Check allow pages
     * @param $request
     * @return bool
     */
    public function isAllowPage($request){
        $actionName = $request->getFullActionName();
        $data = in_array($actionName, $this->getAllowFullActionNames());
        return $data;
    }

    /**
     * Allow pages
     * @return array
     */
    public function getAllowFullActionNames(){
        return [
            'customer_account_edit',
            'customer_section_load',
            'customer_account_logout',
            'customer_account_editPost',
            'customer_account_loginPost'
        ];
    }
}
