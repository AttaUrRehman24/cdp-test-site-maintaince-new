<?php

namespace Meridian\Customer\Plugin\Magento\Newsletter\Manage;

use Magento\Framework\App\Request\Http;
use Magento\Customer\Api\CustomerRepositoryInterface as CustomerRepository;

class SaveNewsletterPlugin
{
    /**
     * @var Http
     */
    protected $request;

    /**
     * @var \Magento\Framework\Data\Form\FormKey\Validator
     */
    protected $formKeyValidator;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $_customerRepository;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $customerFactory;

    /**
     * Customer session
     *
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    public function __construct(
        Http $request,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        CustomerRepository $customerRepository,
        \Magento\Customer\Model\CustomerFactory $customerFactory
    )
    {
        $this->request = $request;
        $this->storeManager = $storeManager;
        $this->formKeyValidator = $formKeyValidator;
        $this->_customerRepository = $customerRepository;
        $this->_customerSession = $customerSession;
        $this->customerFactory = $customerFactory;
    }

    public function beforeExecute(
        \Magento\Newsletter\Controller\Manage\Save $subject
    ){
        $customerId = $this->_customerSession->getCustomerId();
        if($customerId) {
            $currentCustomer = $this->_customerRepository->getById($customerId);

            if ((boolean)$this->request->getParam('subscribe_partners', false)) {
                $currentCustomer->setCustomAttribute('subscribe_partners',1);
            } else {
                $currentCustomer->setCustomAttribute('subscribe_partners',null);
            }
            $this->_customerRepository->save($currentCustomer);
        }

        return $this;
    }
}