<?php

namespace Meridian\ProductCanShow\Model\MageWorx\SeoXTemplates\Model\DbWriter\Product\Eav;


class Url extends \MageWorx\SeoXTemplates\Model\DbWriter\Product\Eav\Url
{
    protected function updateProductUrl($data)
    {
        $connect =  $this->dataProvider->getCollectionIds();
        $linkField = $this->linkFieldResolver->getLinkField(ProductInterface::class, 'entity_id');
        $product = $this->_collection->getItemById($connect[$data[$linkField]]);

        $this->urlPersist->deleteByData([
            UrlRewrite::ENTITY_ID => $product->getId(),
            UrlRewrite::ENTITY_TYPE => ProductUrlRewriteGenerator::ENTITY_TYPE,
            UrlRewrite::REDIRECT_TYPE => 0,
            UrlRewrite::STORE_ID => $product->getStoreId()
        ]);

        $product->setUrlKey($data['value']);

        //if ($product->isVisibleInSiteVisibility()) {
        $this->urlPersist->replace($this->productUrlRewriteGenerator->generate($product));
        //}
    }
}