<?php

namespace Meridian\ProductCanShow\Model\Magento\CatalogUrlRewrite;


class ProductUrlRewriteGenerator extends \Magento\CatalogUrlRewrite\Model\ProductUrlRewriteGenerator
{
    public function generate(\Magento\Catalog\Model\Product $product, $rootCategoryId = null)
    {
//        if ($product->getVisibility() == Visibility::VISIBILITY_NOT_VISIBLE) {
//            return [];
//        }

        $storeId = $product->getStoreId();

        $productCategories = $product->getCategoryCollection()
            ->addAttributeToSelect('url_key')
            ->addAttributeToSelect('url_path');

        $urls = $this->isGlobalScope($storeId)
            ? $this->generateForGlobalScope($productCategories, $product, $rootCategoryId)
            : $this->generateForSpecificStoreView($storeId, $productCategories, $product, $rootCategoryId);

        return $urls;
    }
}