<?php

namespace Meridian\ProductCanShow\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateFriendlyUrls extends Command
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $_resourceConnection;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory
     */
    protected $_categoryCollectionFactory;

    /**
     * @var \Magento\Framework\App\State $appState
     */
    protected $_appState;

    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Magento\Framework\App\State $appState
    ){
        $this->storeManager = $storeManager;
        $this->_productFactory = $productFactory;
        $this->_resourceConnection = $resourceConnection;
        $this->_categoryCollectionFactory = $categoryCollectionFactory;
        $this->_appState = $appState;
        parent::__construct();
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('webmer:productcanshow:urls:friendyfix')
            ->setDescription('Regenerate friendly urls for categories/products');
        parent::configure();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        set_time_limit(0);
        $allStores = $this->getAllStoreIds();
        $storesList = [];
        $output->writeln('Regenerating of Url rewrites:');

        // get store Id (if was set)
        $storeId = $input->getArgument('storeId');

        // if store ID is not specified the re-generate for all stores
        if (is_null($storeId)) {
            $storesList = $allStores;
        }
        // we will re-generate URL only in this specific store (if it exists)
        elseif (strlen($storeId) && ctype_digit($storeId)) {
            if (isset($allStores[$storeId])) {
                $storesList = array(
                    $storeId => $allStores[$storeId]
                );
            } else {
                $this->displayError($output, 'ERROR: store with this ID not exists.');
                return;
            }
        }
        // disaply error if user set some incorrect value
        else {
            $this->displayError($output, 'ERROR: store ID should have a integer value.', true);
            return;
        }

        // remove all current url rewrites
        if (count($storesList) > 0) {
            $this->removeAllUrlRewrites($storesList);
        }

        // set area code if needed
        try {
            $areaCode = $this->_appState->getAreaCode();
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            // if area code is not set then magento generate exception "LocalizedException"
            $this->_appState->setAreaCode('adminhtml');
        }

        foreach ($storesList as $storeId => $storeCode) {
            $output->writeln('');
            $output->write("[Store ID: {$storeId}, Store View code: {$storeCode}]:");
            $output->writeln('');
            $step = 0;

            // get categories collection
            $categories = $this->_categoryCollectionFactory->create()
                ->addAttributeToSelect('*')
                ->setStore($storeId)
                ->addFieldToFilter('level', array('gt' => '1'))
                ->setOrder('level', 'DESC');

            foreach ($categories as $category) {
                try {
                    // we use save() action to start all before/after 'save' events (includes a regenerating of url rewrites)
                    // and we set orig "url_key" as empty to pass checks if data was updated
                    $category->setStoreId($storeId);
                    $category->setOrigData('url_key', '');
                    $category->save();

                    $step++;
                    $output->write('.');
                    if ($step > 19) {
                        $output->writeln('');
                        $step = 0;
                    }
                } catch (\Exception $e) {
                    // debugging
                    $output->writeln($e->getMessage());
                }
            }
        }

        $output->writeln('');
        $output->writeln('');
        $output->writeln('Reindexation...');
        shell_exec('php bin/magento indexer:reindex');

        $output->writeln('Cache refreshing...');
        shell_exec('php bin/magento cache:clean');
        shell_exec('php bin/magento cache:flush');
        $output->writeln('Finished');
    }

    /**
     * Remove all current Url rewrites of categories/products from DB
     * Use a sql queries to speed up
     *
     * @param array $storesList
     * @return void
     */
    public function removeAllUrlRewrites($storesList) {
        $storeIds = implode(',', array_keys($storesList));
        $sql = "DELETE FROM {$this->_resourceConnection->getTableName('url_rewrite')} WHERE `entity_type` IN ('category', 'product') AND `store_id` IN ({$storeIds});";
        $this->getConnection()->query($sql);

        $sql = "DELETE FROM {$this->_resourceConnection->getTableName('catalog_url_rewrite_product_category')} WHERE `url_rewrite_id` NOT IN (
            SELECT `url_rewrite_id` FROM {$this->_resourceConnection->getTableName('url_rewrite')}
        );";
        $this->getConnection()->query($sql);
    }

    /**
     * Get list of all stores id/code
     *
     * @return array
     */
    public function getAllStoreIds() {
        $result = [];

        $sql = $this->getConnection()->select()
            ->from($this->_resourceConnection->getTableName('store'), array('store_id', 'code'))
            ->order('store_id', 'ASC');

        $queryResult = $this->getConnection()->fetchAll($sql);

        foreach ($queryResult as $row) {
            $result[(int)$row['store_id']] = $row['code'];
        }

        return $result;
    }

    /**
     * Display error message
     * @param  OutputInterface  $output
     * @param  string  $errorMsg
     * @param  boolean $displayHint
     * @return void
     */
    private function displayError(&$output, $errorMsg, $displayHint = false)
    {
        $output->writeln('');
        $output->writeln($errorMsg);

        if ($displayHint) {
            $output->writeln('Correct command is: bin/magento ok:urlrewrites:regenerate 19');
        }

        $output->writeln('Finished');
    }

    public function getConnection()
    {
        return $this->_resourceConnection->getConnection();
    }
}