<?php

namespace Meridian\ProductCanShow\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\CatalogUrlRewrite\Model\ProductUrlRewriteGenerator;
use Magento\UrlRewrite\Model\UrlPersistInterface;
use Meridian\ProductCanShow\Helper\ProductUrlRewrite;

class RegenerateUrlRewrites extends Command
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @var ProductUrlRewriteGenerator
     */
    protected $productUrlRewriteGenerator;

    /**
     * @var UrlPersistInterface
     */
    protected $urlPersist;

    /**
     * @var ProductUrlRewrite
     */
    protected $_helper;

    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        ProductUrlRewriteGenerator $productUrlRewriteGenerator,
        UrlPersistInterface $urlPersist,
        ProductUrlRewrite $helper
    ){
        $this->storeManager = $storeManager;
        $this->_productFactory = $productFactory;
        $this->productUrlRewriteGenerator = $productUrlRewriteGenerator;
        $this->urlPersist = $urlPersist;
        $this->_helper = $helper;
        parent::__construct();
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('webmer:productcanshow:urls:regenerate')
            ->setDescription('Generate products url with status not visible individuality');
        parent::configure();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->_helper->getAppStateFromHelper()->setAreaCode('adminhtml');

        $output->writeln('Regenerating of Product url:');
        foreach ($this->storeManager->getStores(true, true) as $store) {
            $output->writeln('');
            $output->write("[Store ID: {$store->getId()}, Store View code: {$store->getCode()}]:");
            $output->writeln('');

            $collection = $this->_productFactory->create()->getCollection()->addAttributeToSelect(
                '*'
            )->setStore(
                $store
            );
            $collection->addStoreFilter($store);
            /** @var $product \Magento\Catalog\Model\Product */
            foreach ($collection as $product) {
                if (($store->getId() == \Magento\Store\Model\Store::DEFAULT_STORE_ID) && $product->getVisibility() == \Magento\Catalog\Model\Product\Visibility::VISIBILITY_NOT_VISIBLE) {

                    $select = $this->_helper->getConnection()->select()->from('url_rewrite')
                        ->where('entity_id = ?',$product->getId())
                        ->reset(\Zend_Db_Select::COLUMNS)
                        ->columns(array(
                            'entity_id' => new \Zend_Db_Expr("entity_id")));

                    $result = $this->_helper->getConnection()->fetchOne($select);

                    if(empty($result) || $result === ''){
                        $this->urlPersist->replace($this->productUrlRewriteGenerator->generate($product));
                    }
                    $output->write('.');
                }
            }
        }

        $output->writeln('');
        $output->writeln('Finished');
    }
}