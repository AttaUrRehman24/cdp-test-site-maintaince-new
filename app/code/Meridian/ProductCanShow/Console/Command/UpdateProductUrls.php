<?php

namespace Meridian\ProductCanShow\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateProductUrls extends Command
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $_resourceConnection;

    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Framework\App\ResourceConnection $resourceConnection
    ){
        $this->storeManager = $storeManager;
        $this->_productFactory = $productFactory;
        $this->_resourceConnection = $resourceConnection;
        parent::__construct();
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('webmer:productcanshow:urls:update')
            ->setDescription('Update empty products url on store view');
        parent::configure();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Update Product urls:');
        $output->writeln('');
        foreach ($this->storeManager->getStores(true, true) as $store) {
            $output->write("[Store ID: {$store->getId()}, Store View code: {$store->getCode()}]:");
            $output->writeln('');
            $step = 0;
            $collection = $this->_productFactory->create()->getCollection()->addAttributeToSelect(
                '*'
            )->setStore(
                $store
            );
            $collection->addStoreFilter($store);
            /** @var $product \Magento\Catalog\Model\Product */
            foreach ($collection as $product) {
                if($store->getId() != \Magento\Store\Model\Store::DEFAULT_STORE_ID) {
                    $product->addAttributeUpdate('url_key','',$store->getId());
                }
                $step++;
                //$output->write('.');
                $message = sprintf('ProductId: %s, StoreId: %s, UrlKey: %s',$product->getId(), $product->getStoreId(). '<>' .$store->getId(), $product->getUrlKey());
                $output->write($message);
                $output->writeln('');
                if ($step > 19) {
                    $output->writeln('');
                    $step = 0;
                }
            }
        }
        /*$output->writeln('');

        $output->writeln('Reindexation...');
        shell_exec('php bin/magento indexer:reindex');

        $output->writeln('Cache refreshing...');
        shell_exec('php bin/magento cache:clean');
        shell_exec('php bin/magento cache:flush');*/

        $output->writeln('');
        $output->writeln('Finished');
    }

    public function getConnection()
    {
        return $this->_resourceConnection->getConnection();
    }
}