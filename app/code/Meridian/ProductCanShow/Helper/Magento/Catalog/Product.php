<?php

namespace Meridian\ProductCanShow\Helper\Magento\Catalog;


class Product extends \Magento\Catalog\Helper\Product
{

    public function canShow($product, $where = 'catalog')
    {
        if (is_int($product)) {
            try {
                $product = $this->productRepository->getById($product);
            } catch (NoSuchEntityException $e) {
                return false;
            }
        } else {
            if (!$product->getId()) {
                return false;
            }
        }
        //return $product->isVisibleInCatalog() && $product->isVisibleInSiteVisibility();
        return true;
    }
}
	
	