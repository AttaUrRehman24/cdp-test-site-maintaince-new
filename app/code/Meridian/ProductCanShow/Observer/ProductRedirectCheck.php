<?php

namespace Meridian\ProductCanShow\Observer;


class ProductRedirectCheck implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $request = $observer->getEvent()->getRequest();

        if (!$request) {
            return;
        }

        $action = $request->getFullActionName();
        if ($action != 'catalog_product_view') {
            return $this;
        }

        return $this;
    }
}