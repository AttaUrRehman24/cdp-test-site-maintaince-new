<?php
namespace Meridian\ProductAvailable\Cron;
use \Psr\Log\LoggerInterface;

class Decrement {
    protected $logger;

    public function __construct(LoggerInterface $logger) {
        $this->logger = $logger;
    }

    public function execute() {
        $this->logger->info('- one day');
        $this->decrementProductDelay();
        $this->decrementManufDelay();
    }

    private function decrementProductDelay(){
        $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $productCollection = $_objectManager->create('Magento\Catalog\Model\ResourceModel\Product\CollectionFactory');
        $collection = $productCollection->create()
            ->addAttributeToSelect('date_available_delay_i')
            ->load();
        foreach ($collection as $product) {
            $nowDelay = $product->getData('date_available_delay_i');
            if($nowDelay != "" && $nowDelay != null && $nowDelay != "0" && $nowDelay >= 0){
                $product->setData('date_available_delay_i',$nowDelay-1);
                $product->save();
            }
        }
    }

    private function decrementManufDelay(){
        $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $productCollection = $_objectManager->create('Magento\Catalog\Model\ResourceModel\Category\CollectionFactory');
        $collection = $productCollection->create()
            ->addAttributeToSelect('*')
            ->load();
        foreach ($collection as $category) {
            $nowDelay = $category->getData('date_available_delay');
            if ($nowDelay != "" && $nowDelay != null && $nowDelay != "0" && $nowDelay >= 0) {
                $category->setData('date_available_delay', $nowDelay - 1);
                $category->save();
            }
        }
    }
}