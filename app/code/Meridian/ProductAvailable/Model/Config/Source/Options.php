<?php

namespace Meridian\ProductAvailable\Model\Config\Source;

use Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory;

use Magento\Framework\DB\Ddl\Table;

/**
 * Custom Attribute Renderer
 */
class Options extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource

{

    /**
     * @var OptionFactory
     */

    protected $optionFactory;

    /**
     * @param OptionFactory $optionFactory
     */

    /**
     * Get all options
     *
     * @return array
     */

    public function getAllOptions()

    {
        $this->_options = [
            ['label' => 'Yes', 'value' => '1'],
            ['label' => 'No', 'value' => '0'],
        ];
        return $this->_options;
    }

}
