<?php
/**
 * Created by PhpStorm.
 * User: margio
 * Date: 09.02.18
 * Time: 12:23
 */

namespace Meridian\ProductAvailable\Model;

use \BoostMyShop\AvailabilityStatus\Model\AvailabilityStatus as OriginalData;

class ProductAvailable extends OriginalData
{
    public function moduleStatus($storeId)
    {
        return $this->_config->getSetting('general_settings/enable', $storeId);
    }
    public function baseOn($storeId)
    {
        return $this->_config->getSetting('general_settings/bse_on', $storeId);
    }

    public function getInStockMessageC($storeId)
    {
        return $this->_config->getSetting('in_stock_settings/message', $storeId);
    }

    public function getInStockDesc($storeId)
    {
        return $this->_config->getSetting('in_stock_settings/addition_message', $storeId);
    }

    public function getOutOfStockMessageC($storeId)
    {
        return $this->_config->getSetting('out_of_stock_settings/message', $storeId);
    }

    public function getOutOfStockDesc($storeId)
    {
        return $this->_config->getSetting('out_of_stock_settings/addition_message', $storeId);
    }

    public function getKnownOutOfStockMessage($storeId)
    {
        return $this->_config->getSetting('known_out_of_stock_settings/message', $storeId);
    }

    public function getKnownOutOfStockDesc($storeId)
    {
        return $this->_config->getSetting('known_out_of_stock_settings/addition_message', $storeId);
    }

    public function getUnknownOutOfStockMessage($storeId)
    {
        return $this->_config->getSetting('unknown_out_of_stock_settings/message', $storeId);
    }

    public function getUnknownOutOfStockDesc($storeId)
    {
        return $this->_config->getSetting('unknown_out_of_stock_settings/addition_message', $storeId);
    }

    public function getCountInStock($product)
    {
        return $this->_stockRegistry->getStockStatus($product->getId(), $product->getStore()->getWebsiteId())->getQty();
    }
}