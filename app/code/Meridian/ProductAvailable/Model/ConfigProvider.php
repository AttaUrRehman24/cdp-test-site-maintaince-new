<?php

namespace Meridian\ProductAvailable\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\View\LayoutInterface;

class ConfigProvider implements ConfigProviderInterface
{
    /** @var LayoutInterface  */
    protected $_layout;
    protected $blocks;

    public function __construct(LayoutInterface $layout)
    {
        $this->_layout = $layout;
        $this->blocks = $this->constructBlock();
    }

    public function constructBlock()
    {
        $blocks = [];
        $blocks['delivery_message'] = $this->_layout->createBlock('Meridian\ProductAvailable\Block\Product\Cart\DeliveryMessage')->toHtml();
        return $blocks;
    }

    public function getConfig()
    {
        return $this->blocks;
    }
}