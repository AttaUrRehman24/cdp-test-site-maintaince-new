<?php

namespace Meridian\ProductAvailable\Helper;

use Magento\Framework\App\Helper\Context;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $_availabilityStatus;
    protected $_storeId;
    protected $categoryManager;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $timezone;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $date;

    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Meridian\Base\Helper\Data
     */
    protected $_helperBase;

    public function __construct(
        Context $context,
        \Meridian\ProductAvailable\Model\ProductAvailable $availabilityStatus,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {
        parent::__construct($context);
        $this->_availabilityStatus = $availabilityStatus;
        $this->timezone = $timezone;
        $this->date = $date;
        $this->categoryManager = $categoryFactory;
        $this->_storeManager = $storeManager;
        $this->_storeId = $this->_storeManager->getStore()->getId();
    }

    private function getManufactureDelivery($product)
    {
        $date = null;
        if ($product instanceof \Magento\Catalog\Model\Product) {
            $productCategoriesIds = $product->getCategoryIds();

            foreach ($productCategoriesIds as $categoryId) {
                $category = $this->categoryManager->create()->load($categoryId);
                if ($category->getData('manuf_avail') != NULL && substr($category->getData('manuf_avail'), 0, 10) > date('o-m-d')) {
                    $date = substr($category->getData('manuf_avail'), 0, 10);
                    break;
                }
            }
        }
        return $date;
    }

    private function getManufactureDeliveryDelay($product)
    {
        $data = null;
        if ($product instanceof \Magento\Catalog\Model\Product) {
            $productCategoriesIds = $product->getCategoryIds();
            foreach ($productCategoriesIds as $categoryId) {
                $category = $this->categoryManager->create()->load($categoryId);
                if ($category->getData('date_available_delay') > 0) {
                    $data = $category->getData('date_available_delay');
                    break;
                }
            }
        }
        return $data;
    }

    private function getProductDelivery($product)
    {
        $date = null;
        if ($product instanceof \Magento\Catalog\Model\Product) {
            if ($product->getData('date_available_product_i') != null && substr($product->getData('date_available_product_i'), 0, 10) > date('o-m-d')) {
                $date = substr($product->getData('date_available_product_i'), 0, 10);
            }
        }
        return $date;
    }

    private function getFuturePurchase($product)
    {
        $data = null;
        if ($product instanceof \Magento\Catalog\Model\Product) {
            $data = $product->getData('future_purchase_i');
        }
        return $data;
    }

    private function getProductDeliveryDelay($product)
    {
        $data = null;
        if ($product instanceof \Magento\Catalog\Model\Product) {
            if ($product->getData('date_available_delay_i') > 0) {
                $data = $product->getData('date_available_delay_i');
            }
        }
        return $data;
    }

    private function formateDate($date)
    {
        $date = str_replace("-", '/', $date);
        $parts = explode("/", $date);
        return $parts[2] . '/' . $parts[1] . '/' . $parts[0];
    }

    /**
     * @return \Meridian\Base\Helper\Data|mixed
     */
    public function getHelper()
    {
        if($this->_helperBase === null){
            $this->_helperBase = \Magento\Framework\App\ObjectManager::getInstance()->get(
                \Meridian\Base\Helper\Data::class
            );
        }
        return $this->_helperBase;
    }

    public function getMessageFrontCart($product = null)
    {
        $storeId = $this->_storeId;
        $data = [];
        if ($this->_availabilityStatus->moduleStatus($storeId) == 1) {
            $product = $this->getHelper()->getProductDetail($product->getId());
            $baseOn = $this->_availabilityStatus->baseOn($storeId);
            $getFuturePurchase = $this->getFuturePurchase($product);
            $getProductDelivery = $this->getProductDelivery($product);
            $getProductDeliveryDelay = $this->getProductDeliveryDelay($product);
            $getManufactureDelivery = $this->getManufactureDelivery($product);
            $getManufactureDeliveryDelay = $this->getManufactureDeliveryDelay($product);
            if ($product->isAvailable() && $this->_availabilityStatus->getCountInStock($product) > 0) {
                $data['status'] = 'green';
                //$oldMessage = $this->_availabilityStatus->getInStockMessageC($storeId);
                $message = __('En stock');
                $addition_message = $this->_availabilityStatus->getInStockDesc($storeId);
                $data['message'] = $message;
                $data['addition_message'] = $addition_message;
            } elseif (!$product->isAvailable() && $this->getFuturePurchase($product) == '0') {
                $data['status'] = 'red';
                $data['message'] = $this->_availabilityStatus->getOutOfStockMessageC($storeId);
                $data['addition_message'] = $this->_availabilityStatus->getOutOfStockDesc($storeId);
            } elseif ($getFuturePurchase == '1' || $getFuturePurchase == ' ' || !$getFuturePurchase) {
                $message = str_replace('Disponible', 'Expédié', $this->_availabilityStatus->getKnownOutOfStockMessage($storeId));
                $message2 = str_replace('Disponible', 'Expédié', $this->_availabilityStatus->getKnownOutOfStockDesc($storeId));
                if ($getProductDelivery != null || $getProductDeliveryDelay != null) {
                    if ($baseOn == "1" || $baseOn == "") {
                        if ($getProductDeliveryDelay != null) {
                            $data['status'] = 'yellow';
                            $tmp_date = date('o-m-d', strtotime('+ ' . $getProductDeliveryDelay . ' days'));
                            $data['message'] = $message . " " . $this->formateDate($tmp_date);
                            $data['addition_message'] = $message2 . " " . $this->formateDate($tmp_date);
                        } else {
                            $data['status'] = 'yellow';
                            $data['message'] = $message . " " . $this->formateDate($getProductDelivery);
                            $data['addition_message'] = $message2 . " " . $this->formateDate($getProductDelivery);
                        }
                    } elseif ($baseOn == "2") {
                        if ($getProductDelivery != null) {
                            $data['status'] = 'yellow';
                            $data['message'] = $message . " " . $this->formateDate($getProductDelivery);
                            $data['addition_message'] = $message2 . " " . $this->formateDate($getProductDelivery);
                        } else {
                            $data['status'] = 'yellow';
                            $tmp_date = date('o-m-d', strtotime('+ ' . $getProductDeliveryDelay . ' days'));
                            $data['message'] = $message . " " . $this->formateDate($tmp_date);
                            $data['addition_message'] = $message2 . " " . $this->formateDate($tmp_date);
                        }
                    } elseif ($baseOn == "3") {
                        if ($getProductDelivery != null && $getProductDeliveryDelay != null) {
                            $data['status'] = 'yellow';
                            $tmp_date = date('o-m-d', strtotime($getProductDelivery . ' + ' . $getProductDeliveryDelay . ' days'));
                            $data['message'] = $message . " " . $this->formateDate($tmp_date);
                            $data['addition_message'] = $message2 . " " . $this->formateDate($tmp_date);
                        } elseif ($getProductDeliveryDelay != null) {
                            $data['status'] = 'yellow';
                            $tmp_date = date('o-m-d', strtotime('+ ' . $getProductDeliveryDelay . ' days'));
                            $data['message'] = $message . " " . $this->formateDate($tmp_date);
                            $data['addition_message'] = $message2 . " " . $this->formateDate($tmp_date);
                        } else {
                            $data['status'] = 'yellow';
                            $data['message'] = $message . " " . $this->formateDate($getProductDelivery);
                            $data['addition_message'] = $message2 . " " . $this->formateDate($getProductDelivery);
                        }
                    }
                } elseif ($getManufactureDelivery != null || $getManufactureDeliveryDelay != null) {
                    if ($baseOn == "1" || $baseOn == "") {
                        if ($getManufactureDeliveryDelay != null) {
                            $data['status'] = 'yellow';
                            $tmp_date = date('o-m-d', strtotime('+ ' . $getManufactureDeliveryDelay . ' days'));
                            $data['message'] = $message . " " . $this->formateDate($tmp_date);
                            $data['addition_message'] = $message2 . " " . $this->formateDate($tmp_date);
                        } else {
                            $data['status'] = 'yellow';
                            $data['message'] = $message . " " . $this->formateDate($getManufactureDelivery);
                            $data['addition_message'] = $message2 . " " . $this->formateDate($getManufactureDelivery);
                        }
                    } elseif ($baseOn == "2") {
                        if ($getManufactureDelivery != null) {
                            $data['status'] = 'yellow';
                            $data['message'] = $message . " " . $this->formateDate($getManufactureDelivery);
                            $data['addition_message'] = $message2 . " " . $this->formateDate($getManufactureDelivery);
                        } else {
                            $data['status'] = 'yellow';
                            $tmp_date = date('o-m-d', strtotime('+ ' . $getManufactureDeliveryDelay . ' days'));
                            $data['message'] = $message . " " . $this->formateDate($tmp_date);
                            $data['addition_message'] = $message2 . " " . $this->formateDate($tmp_date);
                        }
                    } elseif ($baseOn == "3") {
                        if ($getManufactureDelivery != null && $getManufactureDeliveryDelay != null) {
                            $data['status'] = 'yellow';
                            $tmp_date = date('o-m-d', strtotime($getManufactureDelivery . ' + ' . $getManufactureDeliveryDelay . ' days'));
                            $data['message'] = $message . " " . $this->formateDate($tmp_date);
                            $data['addition_message'] = $message2 . " " . $this->formateDate($tmp_date);
                        } elseif ($getManufactureDeliveryDelay != null) {
                            $data['status'] = 'yellow';
                            $tmp_date = date('o-m-d', strtotime('+ ' . $getManufactureDeliveryDelay . ' days'));
                            $data['message'] = $message . " " . $this->formateDate($tmp_date);
                            $data['addition_message'] = $message2 . " " . $this->formateDate($tmp_date);
                        } else {
                            $data['status'] = 'yellow';
                            $data['message'] = $message . " " . $this->formateDate($getManufactureDelivery);
                            $data['addition_message'] = $message2 . " " . $this->formateDate($getManufactureDelivery);
                        }
                    }
                } else {
                    $data['status'] = 'yellow';
                    //$message = $this->_availabilityStatus->getUnknownOutOfStockMessage($storeId);
                    $currentDay = $this->timezone->date()->modify('+5 days')->format('d/m/Y');
                    $lastDay = $this->timezone->date()->modify('+9 days')->format('d/m/Y');
                    $isWeekDay = $this->timezone->date()->modify('+5 days')->format('l');
                    if($isWeekDay == 'Sunday'){
                        $currentDay = $this->timezone->date()->modify('+6 days')->format('d/m/Y');
                        $lastDay = $this->timezone->date()->modify('+10 days')->format('d/m/Y');
                    }
                    $message = sprintf('Expédié entre le %s et le %s', $currentDay, $lastDay);
                    $data['message'] = $message;
                    $data['addition_message'] = $this->_availabilityStatus->getUnknownOutOfStockDesc($storeId);
                }
            }
            return $data;
        } else {
            return null;
        }
    }
}