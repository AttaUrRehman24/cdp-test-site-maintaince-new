define(
    [
        'jquery',
        'ko',
        'uiComponent'
    ],
    function(
        $,
        ko,
        Component
    ) {
        'use strict';
        return Component.extend({
            defaults: {
                template: 'Meridian_ProductAvailable/delivery_message'
            },

            initialize: function () {
                var self = this;
                this._super();
            }

        });
    }
);