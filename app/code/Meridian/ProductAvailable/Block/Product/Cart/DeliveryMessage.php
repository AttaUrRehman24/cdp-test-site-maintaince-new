<?php

namespace Meridian\ProductAvailable\Block\Product\Cart;

use Magento\Framework\View\Element\Template;

class DeliveryMessage extends Template
{
    protected $_template = "product/cart/delivery_message.phtml";

    /**
     * @var \Magento\Quote\Model\Quote|null
     */
    protected $quote = null;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var \Meridian\ProductAvailable\Block\Product\View\Type\Simple
     */
    protected $_simpleType;
    /**
     * @var \Meridian\ProductAvailable\Helper\Data
     */
    private $helperAvailable;
    /**
     * @var \Meridian\Base\Helper\Data
     */
    private $helperBase;

    /**
     * DeliveryMessage constructor.
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Meridian\ProductAvailable\Block\Product\View\Type\Simple $simpleType
     * @param \Meridian\ProductAvailable\Helper\Data $helperAvailable
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Meridian\ProductAvailable\Block\Product\View\Type\Simple $simpleType,
        \Meridian\ProductAvailable\Helper\Data $helperAvailable,
        \Meridian\Base\Helper\Data $helperBase,
        Template\Context $context,
        array $data = []
    )
    {
        $this->checkoutSession = $checkoutSession;
        $this->_simpleType = $simpleType;
        parent::__construct($context, $data);
        $this->helperAvailable = $helperAvailable;
        $this->helperBase = $helperBase;
    }

    /**
     * Get active quote
     *
     * @return \Magento\Quote\Model\Quote
     */
    protected function getQuote()
    {
        if (null === $this->quote) {
            $this->quote = $this->checkoutSession->getQuote();
        }
        return $this->quote;
    }

    public function displayCheckoutMessage()
    {
        $dates = [];
        $items = $this->getQuote()->getAllVisibleItems();
        if(count($items)) {
            $counter = 0;
            $fullDates = [];
            foreach (array_reverse($items) as $quoteItem) {
                /* @var $quoteItem \Magento\Quote\Model\Quote\Item */
                $message = $this->helperAvailable->getMessageFrontCart($quoteItem->getProduct());
                if(count($message)){
                    if(!isset($message['message'])){
                        continue;
                    }
                }
                $counter ++;
                preg_match_all('(\d{2}\/\d{2}\/\d{4})', $message['message'], $matches);
                if(count($matches[0]) == 0) continue;
                $data = $matches[0][0];
                if (count($matches[0]) == 2) {
                    $data = $matches[0][1];
                    $fullDate = sprintf("%s et le %s",
                        str_replace('/','-',
                            $this->getCorrectDate($matches[0][0])
                        ),
                        str_replace('/','-',
                            $this->getCorrectDate($matches[0][1])
                            ));
                }else{
                    $fullDate = str_replace('/','-',
                        $this->getCorrectDate($data));
                }
                $data = str_replace('/','-',$data);
                $dates[$counter] = $data;
                $fullDates[$counter] = $fullDate;
            }
            if(count($dates)) {
                $maxKey = $this->getMaxKeyFromDates($dates);
                $returnDate = $fullDates[$maxKey];
                $returnDate = str_replace('-', '/', $returnDate);
            } else {
                $returnDate = str_replace('-', '/', $this->getCorrectDate());
            }
        } else {
            $returnDate = false;
        }
        return $returnDate;
    }

    private function getCorrectDate($text = null){

        if(is_null($text)){
            $date = new \Zend_Date();
        }else{
            $date = new \Zend_Date($text);
        }
        $shortDay = $date->toString(\Zend_Date::WEEKDAY_DIGIT);
        if($shortDay == 6){
            $date->add('2', \Zend_Date::DAY);
        } else {
            $date->add('1', \Zend_Date::DAY);
        }

        $format= sprintf("%s-%s-%s", \Zend_Date::DAY, \Zend_Date::MONTH, \Zend_Date::YEAR);
        $result = $date->toString($format);
        return $result;
    }

    private function getMaxKeyFromDates($dates){
        $maxKey = 0;
        $maxValue = 0;
        foreach ($dates as $key => $value){
            $maxValueTmp = strtotime($value);
            if($maxValue < $maxValueTmp){
                $maxValue = $maxValueTmp;
                $maxKey = $key;
            }
        }
        return $maxKey;
    }
}