<?php

namespace Meridian\ProductAvailable\Block\Product\View\Type;

use \BoostMyShop\AvailabilityStatus\Block\Product\View\Type\Simple as OriginalData;

class Simple extends OriginalData
{

    protected $_availabilityStatus;
    protected $_storeId;
    protected $categoryManager;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $timezone;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $date;

    /**
     * @var \Meridian\Base\Helper\Data
     */
    protected $_helperBase;

    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\Stdlib\ArrayUtils $arrayUtils,
        \Meridian\ProductAvailable\Model\ProductAvailable $availabilityStatus,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Meridian\Base\Helper\Data $helperBase,
        array $data = []
    )
    {
        parent::__construct($context, $arrayUtils, $availabilityStatus, $data);
        $this->_availabilityStatus = $availabilityStatus;
        $this->_storeId = $this->_storeManager->getStore()->getId();
        $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->categoryManager = $_objectManager->create('Magento\Catalog\Model\Category');
        $this->timezone = $timezone;
        $this->date = $date;
        $this->_helperBase = $helperBase;
    }

    private function getManufactureDelivery()
    {
        $date = null;
        $productCategoriesIds = $this->getProduct()->getCategoryIds();

        foreach ($productCategoriesIds as $categoryId) {
            if ($this->categoryManager->load($categoryId)->getData('manuf_avail') != NULL && substr($this->categoryManager->load($categoryId)->getData('manuf_avail'), 0, 10) > date('o-m-d')) {
                $date = substr($this->categoryManager->load($categoryId)->getData('manuf_avail'), 0, 10);
                break;
            }
        }
        return $date;
    }

    private function getManufactureDeliveryDelay()
    {
        $data = null;
        $productCategoriesIds = $this->getProduct()->getCategoryIds();
        foreach ($productCategoriesIds as $categoryId) {
            if ($this->categoryManager->load($categoryId)->getData('date_available_delay') > 0) {
                $data = $this->categoryManager->load($categoryId)->getData('date_available_delay');
                break;
            }
        }
        return $data;
    }

    private function getProductDelivery()
    {
        $date = null;
        if ($this->getProduct()->_getData('date_available_product_i') != null && substr($this->getProduct()->_getData('date_available_product_i'), 0, 10) > date('o-m-d')) {
            $date = substr($this->getProduct()->_getData('date_available_product_i'), 0, 10);
        }
        return $date;
    }

    private function getFuturePurchase()
    {
        return $this->getProduct()->getData('future_purchase_i');
    }

    private function getProductDeliveryDelay()
    {
        $data = null;
        if ($this->getProduct()->getData('date_available_delay_i') > 0) {
            $data = $this->getProduct()->getData('date_available_delay_i');
        }
        return $data;
    }

    private function formateDate($date)
    {
        $date = str_replace("-", '/', $date);
        $parts = explode("/", $date);
        return $parts[2] . '/' . $parts[1] . '/' . $parts[0];
    }

    public function getMessageFront()
    {
        $product = $this->getProduct();
        $storeId = $this->_storeId;
        $data = [];
        if ($this->_availabilityStatus->moduleStatus($storeId) == 1) {
            $baseOn = $this->_availabilityStatus->baseOn($storeId);
            $getFuturePurchase = $this->getFuturePurchase();
            $getProductDelivery = $this->getProductDelivery();
            $getProductDeliveryDelay = $this->getProductDeliveryDelay();
            $getManufactureDelivery = $this->getManufactureDelivery();
            $getManufactureDeliveryDelay = $this->getManufactureDeliveryDelay();
            if ($product->isAvailable() && $this->_availabilityStatus->getCountInStock($product) > 0) {
                $data['status'] = 'green';
                $data['message'] = $this->_availabilityStatus->getInStockMessageC($storeId);
                $data['addition_message'] = $this->_availabilityStatus->getInStockDesc($storeId);
            } elseif (!$product->isAvailable() && $getFuturePurchase == '0') {
                $data['status'] = 'red';
                $data['message'] = $this->_availabilityStatus->getOutOfStockMessageC($storeId);
                $data['addition_message'] = $this->_availabilityStatus->getOutOfStockDesc($storeId);
            } elseif ($getFuturePurchase == '1' || $getFuturePurchase == ' ' || !$this->getFuturePurchase()) {
                if ($getProductDelivery != null || $getProductDeliveryDelay != null) {
                    if ($baseOn == "1" || $baseOn == "") {
                        if ($getProductDeliveryDelay != null) {
                            $data['status'] = 'yellow';
                            $tmp_date = date('o-m-d', strtotime('+ ' . $getProductDeliveryDelay . ' days'));
                            $data['message'] = $this->_availabilityStatus->getKnownOutOfStockMessage($storeId) . " " . $this->formateDate($tmp_date);
                            $data['addition_message'] = $this->_availabilityStatus->getKnownOutOfStockDesc($storeId) . " " . $this->formateDate($tmp_date);
                        } else {
                            $data['status'] = 'yellow';
                            $data['message'] = $this->_availabilityStatus->getKnownOutOfStockMessage($storeId) . " " . $this->formateDate($getProductDelivery);
                            $data['addition_message'] = $this->_availabilityStatus->getKnownOutOfStockDesc($storeId) . " " . $this->formateDate($getProductDelivery);
                        }
                    } elseif ($baseOn == "2") {
                        if ($getProductDelivery != null) {
                            $data['status'] = 'yellow';
                            $data['message'] = $this->_availabilityStatus->getKnownOutOfStockMessage($storeId) . " " . $this->formateDate($getProductDelivery);
                            $data['addition_message'] = $this->_availabilityStatus->getKnownOutOfStockDesc($storeId) . " " . $this->formateDate($getProductDelivery);
                        } else {
                            $data['status'] = 'yellow';
                            $tmp_date = date('o-m-d', strtotime('+ ' . $getProductDeliveryDelay . ' days'));
                            $data['message'] = $this->_availabilityStatus->getKnownOutOfStockMessage($storeId) . " " . $this->formateDate($tmp_date);
                            $data['addition_message'] = $this->_availabilityStatus->getKnownOutOfStockDesc($storeId) . " " . $this->formateDate($tmp_date);
                        }
                    } elseif ($baseOn == "3") {
                        if ($getProductDelivery != null && $getProductDeliveryDelay != null) {
                            $data['status'] = 'yellow';
                            $tmp_date = date('o-m-d', strtotime($getProductDelivery . ' + ' . $getProductDeliveryDelay . ' days'));
                            $data['message'] = $this->_availabilityStatus->getKnownOutOfStockMessage($storeId) . " " . $this->formateDate($tmp_date);
                            $data['addition_message'] = $this->_availabilityStatus->getKnownOutOfStockDesc($storeId) . " " . $this->formateDate($tmp_date);
                        } elseif ($getProductDeliveryDelay != null) {
                            $data['status'] = 'yellow';
                            $tmp_date = date('o-m-d', strtotime('+ ' . $getProductDeliveryDelay . ' days'));
                            $data['message'] = $this->_availabilityStatus->getKnownOutOfStockMessage($storeId) . " " . $this->formateDate($tmp_date);
                            $data['addition_message'] = $this->_availabilityStatus->getKnownOutOfStockDesc($storeId) . " " . $this->formateDate($tmp_date);
                        } else {
                            $data['status'] = 'yellow';
                            $data['message'] = $this->_availabilityStatus->getKnownOutOfStockMessage($storeId) . " " . $this->formateDate($getProductDelivery);
                            $data['addition_message'] = $this->_availabilityStatus->getKnownOutOfStockDesc($storeId) . " " . $this->formateDate($getProductDelivery);
                        }
                    }
                } elseif ($getManufactureDelivery != null || $getManufactureDeliveryDelay != null) {
                    if ($baseOn == "1" || $baseOn == "") {
                        if ($getManufactureDeliveryDelay != null) {
                            $data['status'] = 'yellow';
                            $tmp_date = date('o-m-d', strtotime('+ ' . $getManufactureDeliveryDelay . ' days'));
                            $data['message'] = $this->_availabilityStatus->getKnownOutOfStockMessage($storeId) . " " . $this->formateDate($tmp_date);
                            $data['addition_message'] = $this->_availabilityStatus->getKnownOutOfStockDesc($storeId) . " " . $this->formateDate($tmp_date);
                        } else {
                            $data['status'] = 'yellow';
                            $data['message'] = $this->_availabilityStatus->getKnownOutOfStockMessage($storeId) . " " . $this->formateDate($getManufactureDelivery);
                            $data['addition_message'] = $this->_availabilityStatus->getKnownOutOfStockDesc($storeId) . " " . $this->formateDate($getManufactureDelivery);
                        }
                    } elseif ($baseOn == "2") {
                        if ($getManufactureDelivery != null) {
                            $data['status'] = 'yellow';
                            $data['message'] = $this->_availabilityStatus->getKnownOutOfStockMessage($storeId) . " " . $this->formateDate($getManufactureDelivery);
                            $data['addition_message'] = $this->_availabilityStatus->getKnownOutOfStockDesc($storeId) . " " . $this->formateDate($getManufactureDelivery);
                        } else {
                            $data['status'] = 'yellow';
                            $tmp_date = date('o-m-d', strtotime('+ ' . $getManufactureDeliveryDelay . ' days'));
                            $data['message'] = $this->_availabilityStatus->getKnownOutOfStockMessage($storeId) . " " . $this->formateDate($tmp_date);
                            $data['addition_message'] = $this->_availabilityStatus->getKnownOutOfStockDesc($storeId) . " " . $this->formateDate($tmp_date);
                        }
                    } elseif ($baseOn == "3") {
                        if ($getManufactureDelivery != null && $getManufactureDeliveryDelay != null) {
                            $data['status'] = 'yellow';
                            $tmp_date = date('o-m-d', strtotime($getManufactureDelivery . ' + ' . $getManufactureDeliveryDelay . ' days'));
                            $data['message'] = $this->_availabilityStatus->getKnownOutOfStockMessage($storeId) . " " . $this->formateDate($tmp_date);
                            $data['addition_message'] = $this->_availabilityStatus->getKnownOutOfStockDesc($storeId) . " " . $this->formateDate($tmp_date);
                        } elseif ($getManufactureDeliveryDelay != null) {
                            $data['status'] = 'yellow';
                            $tmp_date = date('o-m-d', strtotime('+ ' . $getManufactureDeliveryDelay . ' days'));
                            $data['message'] = $this->_availabilityStatus->getKnownOutOfStockMessage($storeId) . " " . $this->formateDate($tmp_date);
                            $data['addition_message'] = $this->_availabilityStatus->getKnownOutOfStockDesc($storeId) . " " . $this->formateDate($tmp_date);
                        } else {
                            $data['status'] = 'yellow';
                            $data['message'] = $this->_availabilityStatus->getKnownOutOfStockMessage($storeId) . " " . $this->formateDate($getManufactureDelivery);
                            $data['addition_message'] = $this->_availabilityStatus->getKnownOutOfStockDesc($storeId) . " " . $this->formateDate($getManufactureDelivery);
                        }
                    }
                } else {
                    $data['status'] = 'yellow';
                    $data['message'] = $this->_availabilityStatus->getUnknownOutOfStockMessage($storeId);
                    $data['addition_message'] = $this->_availabilityStatus->getUnknownOutOfStockDesc($storeId);
                }
            }
            return $data;
        } else {
            return null;
        }
    }

    public function getMessageFrontCart()
    {
        $product = $this->_helperBase->getProductDetail($this->getProduct()->getId());
        $storeId = $this->_storeId;
        $data = [];
        if ($this->_availabilityStatus->moduleStatus($storeId) == 1) {
            $baseOn = $this->_availabilityStatus->baseOn($storeId);
            $getFuturePurchase = $this->getFuturePurchase();
            $getProductDelivery = $this->getProductDelivery();
            $getProductDeliveryDelay = $this->getProductDeliveryDelay();
            $getManufactureDelivery = $this->getManufactureDelivery();
            $getManufactureDeliveryDelay = $this->getManufactureDeliveryDelay();
            if ($product->isAvailable() && $this->_availabilityStatus->getCountInStock($product) > 0) {
                $data['status'] = 'green';
                //$oldMessage = $this->_availabilityStatus->getInStockMessageC($storeId);
                $message = __('En stock');
                $addition_message = $this->_availabilityStatus->getInStockDesc($storeId);
                $data['message'] = $message;
                $data['addition_message'] = $addition_message;
            } elseif (!$product->isAvailable() && $getFuturePurchase == '0') {
                $data['status'] = 'red';
                $data['message'] = $this->_availabilityStatus->getOutOfStockMessageC($storeId);
                $data['addition_message'] = $this->_availabilityStatus->getOutOfStockDesc($storeId);
            } elseif ($getFuturePurchase == '1' || $getFuturePurchase == ' ' || !$this->getFuturePurchase()) {
                $message = str_replace('Disponible', 'Expédié', $this->_availabilityStatus->getKnownOutOfStockMessage($storeId));
                $message2 = str_replace('Disponible', 'Expédié', $this->_availabilityStatus->getKnownOutOfStockDesc($storeId));
                if ($getProductDelivery != null || $getProductDeliveryDelay != null) {
                    if ($baseOn == "1" || $baseOn == "") {
                        if ($getProductDeliveryDelay != null) {
                            $data['status'] = 'yellow';
                            $tmp_date = date('o-m-d', strtotime('+ ' . $getProductDeliveryDelay . ' days'));
                            $data['message'] = $message . " " . $this->formateDate($tmp_date);
                            $data['addition_message'] = $message2 . " " . $this->formateDate($tmp_date);
                        } else {
                            $data['status'] = 'yellow';
                            $data['message'] = $message. " " . $this->formateDate($getProductDelivery);
                            $data['addition_message'] = $message2 . " " . $this->formateDate($getProductDelivery);
                        }
                    } elseif ($baseOn == "2") {
                        if ($getProductDelivery != null) {
                            $data['status'] = 'yellow';
                            $data['message'] = $message . " " . $this->formateDate($getProductDelivery);
                            $data['addition_message'] = $message2 . " " . $this->formateDate($getProductDelivery);
                        } else {
                            $data['status'] = 'yellow';
                            $tmp_date = date('o-m-d', strtotime('+ ' . $getProductDeliveryDelay . ' days'));
                            $data['message'] = $message . " " . $this->formateDate($tmp_date);
                            $data['addition_message'] = $message2 . " " . $this->formateDate($tmp_date);
                        }
                    } elseif ($baseOn == "3") {
                        if ($getProductDelivery != null && $getProductDeliveryDelay != null) {
                            $data['status'] = 'yellow';
                            $tmp_date = date('o-m-d', strtotime($getProductDelivery . ' + ' . $getProductDeliveryDelay . ' days'));
                            $data['message'] = $message . " " . $this->formateDate($tmp_date);
                            $data['addition_message'] = $message2 . " " . $this->formateDate($tmp_date);
                        } elseif ($getProductDeliveryDelay != null) {
                            $data['status'] = 'yellow';
                            $tmp_date = date('o-m-d', strtotime('+ ' . $getProductDeliveryDelay . ' days'));
                            $data['message'] = $message . " " . $this->formateDate($tmp_date);
                            $data['addition_message'] = $message2 . " " . $this->formateDate($tmp_date);
                        } else {
                            $data['status'] = 'yellow';
                            $data['message'] = $message . " " . $this->formateDate($getProductDelivery);
                            $data['addition_message'] = $message2 . " " . $this->formateDate($getProductDelivery);
                        }
                    }
                } elseif ($getManufactureDelivery != null || $getManufactureDeliveryDelay != null) {
                    if ($baseOn == "1" || $baseOn == "") {
                        if ($getManufactureDeliveryDelay != null) {
                            $data['status'] = 'yellow';
                            $tmp_date = date('o-m-d', strtotime('+ ' . $getManufactureDeliveryDelay . ' days'));
                            $data['message'] = $message . " " . $this->formateDate($tmp_date);
                            $data['addition_message'] = $message2 . " " . $this->formateDate($tmp_date);
                        } else {
                            $data['status'] = 'yellow';
                            $data['message'] = $message . " " . $this->formateDate($getManufactureDelivery);
                            $data['addition_message'] = $message2 . " " . $this->formateDate($getManufactureDelivery);
                        }
                    } elseif ($baseOn == "2") {
                        if ($getManufactureDelivery != null) {
                            $data['status'] = 'yellow';
                            $data['message'] = $message . " " . $this->formateDate($getManufactureDelivery);
                            $data['addition_message'] = $message2 . " " . $this->formateDate($getManufactureDelivery);
                        } else {
                            $data['status'] = 'yellow';
                            $tmp_date = date('o-m-d', strtotime('+ ' . $getManufactureDeliveryDelay . ' days'));
                            $data['message'] = $message . " " . $this->formateDate($tmp_date);
                            $data['addition_message'] = $message2 . " " . $this->formateDate($tmp_date);
                        }
                    } elseif ($baseOn == "3") {
                        if ($getManufactureDelivery != null && $getManufactureDeliveryDelay != null) {
                            $data['status'] = 'yellow';
                            $tmp_date = date('o-m-d', strtotime($getManufactureDelivery . ' + ' . $getManufactureDeliveryDelay . ' days'));
                            $data['message'] = $message . " " . $this->formateDate($tmp_date);
                            $data['addition_message'] = $message2 . " " . $this->formateDate($tmp_date);
                        } elseif ($getManufactureDeliveryDelay != null) {
                            $data['status'] = 'yellow';
                            $tmp_date = date('o-m-d', strtotime('+ ' . $getManufactureDeliveryDelay . ' days'));
                            $data['message'] = $message . " " . $this->formateDate($tmp_date);
                            $data['addition_message'] = $message2 . " " . $this->formateDate($tmp_date);
                        } else {
                            $data['status'] = 'yellow';
                            $data['message'] = $message . " " . $this->formateDate($getManufactureDelivery);
                            $data['addition_message'] = $message2 . " " . $this->formateDate($getManufactureDelivery);
                        }
                    }
                } else {
                    $data['status'] = 'yellow';
                    //$message = $this->_availabilityStatus->getUnknownOutOfStockMessage($storeId);
                    $currentDay = $this->timezone->date()->modify('+5 days')->format('d/m/Y');
                    $lastDay = $this->timezone->date()->modify('+9 days')->format('d/m/Y');
                    $isWeekDay = $this->timezone->date()->modify('+5 days')->format('l');
                    if($isWeekDay == 'Sunday'){
                        $currentDay = $this->timezone->date()->modify('+6 days')->format('d/m/Y');
                        $lastDay = $this->timezone->date()->modify('+10 days')->format('d/m/Y');
                    }
                    $message = sprintf('Expédié entre le %s et le %s', $currentDay, $lastDay);
                    $data['message'] = $message;
                    $data['addition_message'] = $this->_availabilityStatus->getUnknownOutOfStockDesc($storeId);
                }
            }
            return $data;
        } else {
            return null;
        }
    }
}