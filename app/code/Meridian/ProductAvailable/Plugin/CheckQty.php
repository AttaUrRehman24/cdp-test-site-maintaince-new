<?php

namespace Meridian\ProductAvailable\Plugin;


class CheckQty
{

    public function beforeCheckQuoteItemQty($subject, $stockItem, $qty, $summaryQty, $origQty)
    {
        $stockItem->setIsInStock(true);
        return [$stockItem, $qty, $summaryQty, $origQty];
    }
}


