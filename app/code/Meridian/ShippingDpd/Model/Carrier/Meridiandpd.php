<?php

namespace Meridian\ShippingDpd\Model\Carrier;

use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Shipping\Model\Rate\Result;

class Meridiandpd extends \Magento\Shipping\Model\Carrier\AbstractCarrier implements
    \Magento\Shipping\Model\Carrier\CarrierInterface
{
    /**
     * @var \Meridian\ProductAvailable\Model\ProductAvailable
     */
    protected $_availabilityStatus;

    /**
     * @var string
     */
    protected $_code = 'meridiandpd';

    /**
     * Meridiandpd constructor.
     * @param \Meridian\ProductAvailable\Model\ProductAvailable $availabilityStatus
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory
     * @param \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory
     * @param array $data
     */
    public function __construct(
        \Meridian\ProductAvailable\Model\ProductAvailable $availabilityStatus,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory,
        \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory,
        array $data = []
    )
    {
        $this->_availabilityStatus = $availabilityStatus;
        $this->_rateResultFactory = $rateResultFactory;
        $this->_rateMethodFactory = $rateMethodFactory;
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
    }

    /**
     * @return array
     */
    public function getAllowedMethods()
    {

        return ['meridiandpd' => $this->getConfigData('name')];
    }

    /**
     * @param RateRequest $request
     * @return bool|\Magento\Framework\DataObject|Result|null
     */
    public function collectRates(RateRequest $request)
    {
        if (!$this->getConfigFlag('active')) {
            return false;
        }

        foreach ($request->getAllItems() as $item) {
            $product = $item->getProduct();
            if ($request->getPackageWeight() >=  $this->getConfigData('weight_limit')) {
                return false;
            }
//            if ($product->isAvailable() && $this->_availabilityStatus->getCountInStock($product) <= 0) {
//                return false;
//            }
        }

        $packageValueWithDiscount = $request->getPackageValueWithDiscount();
        $freeDpdPrice = $this->getConfigData('free_dpd_price');
        if ($freeDpdPrice > $packageValueWithDiscount) {
            $amount = $this->getConfigData('price');
        } else {
            $amount = '0';
        }

        /** @var \Magento\Shipping\Model\Rate\Result $result */
        $result = $this->_rateResultFactory->create();

        /** @var \Magento\Quote\Model\Quote\Address\RateResult\Method $method */
        $method = $this->_rateMethodFactory->create();

        $method->setCarrier('meridiandpd');
        $method->setCarrierTitle($this->getConfigData('title'));

        $method->setMethod('meridiandpd');
        $method->setMethodTitle($this->getConfigData('name'));

        $method->setPrice($amount);
        $method->setCost($amount);

        $result->append($method);

        return $result;
    }
}