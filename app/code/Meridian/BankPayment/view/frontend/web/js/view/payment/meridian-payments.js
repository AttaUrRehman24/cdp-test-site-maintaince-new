define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'meridian_bankpayment',
                component: 'Meridian_BankPayment/js/view/payment/method-renderer/meridian_bankpayment-method'
            }
        );
        return Component.extend({});
    }
);