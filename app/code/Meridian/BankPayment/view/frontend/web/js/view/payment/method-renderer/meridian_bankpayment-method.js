define(
    [
        'Magento_Checkout/js/view/payment/default'
    ],
    function (Component) {
        'use strict';
        return Component.extend({
            defaults: {
                template: 'Meridian_BankPayment/payment/meridian_bankpayment'
            },
            getFormCmsUrl: function () {
                return window.checkoutConfig.payment.meridian_bankpayment.formcmsurl;
            },
            showAccounts: function () {
                return !window.checkoutConfig.payment.meridian_bankpayment.formcmsurl && window.checkoutConfig.payment.meridian_bankpayment.accounts.length > 0;
            },
            getInstructions: function () {
                return window.checkoutConfig.payment.meridian_bankpayment.instructions;
            },
            getAccounts: function () {
                return window.checkoutConfig.payment.meridian_bankpayment.accounts;
            },
            getCustomText: function () {
                return window.checkoutConfig.payment.meridian_bankpayment.customtext;
            },
        });
    }
);
