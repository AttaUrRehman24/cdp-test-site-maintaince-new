<?php

namespace Meridian\BankPayment\Model;

/**
 * Class Serialized - is used to unserialize values and ensure compatibility to magento 2.1.x and 2.2
 * @package Meridian\BankPayment\Model
 */
class Serialized
{
    /**
     * @param string $value
     * @return array
     */
    public function unserialize($value)
    {
        $result = json_decode($value, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            // not in json format, try serialized array as fallback for pre magento 2.2 versions
            $result = @unserialize($value);
        }
        if (!is_array($result)) {
            $result = [];
        }
        return $result;
    }
}
