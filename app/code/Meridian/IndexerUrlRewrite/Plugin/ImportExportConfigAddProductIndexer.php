<?php

namespace Meridian\IndexerUrlRewrite\Plugin;


class ImportExportConfigAddProductIndexer
{
    public function aroundGetRelatedIndexers(
        \Magento\ImportExport\Model\Import\Config $subject,
        \Closure $proceed,
        $entity
    )
    {
        if($entity !== 'catalog_product') {
            return $proceed();
        }
        $entities = $subject->getEntities();
        $result = [];
        if (isset($entities[$entity])) {
            $result = $entities[$entity]['relatedIndexers'];
            $merged = ['product_url_rewrite' => ['name' => 'product_url_rewrite']];
            $result = array_merge($result, $merged);
        }

        return $result;
    }
}