<?php

namespace BoostMyShop\SupplierReturn\Block\Adminhtml;

class Supplierreturn extends \Magento\Backend\Block\Widget\Container
{
    /**
     * @var string
     */
    protected $_template = 'supplierreturn/supplierreturn.phtml';

    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param array $data
     */
    public function __construct(\Magento\Backend\Block\Widget\Context $context,array $data = [])
    {
        parent::__construct($context, $data);
    }

    /**
     * Prepare button and grid
     *
     * @return \Magento\Catalog\Block\Adminhtml\Product
     */
    protected function _prepareLayout()
    {

        $this->buttonList->add(
                'new_return',
                [
                    'label' => __('Create new return'),
                    'onclick' => 'setLocation(\'' . $this->_getCreateUrl() . '\')',
                    'class' => 'add primary'
                ]
            );
		

        $this->setChild(
            'grid',
            $this->getLayout()->createBlock('BoostMyShop\SupplierReturn\Block\Adminhtml\Supplierreturn\Grid', 'boostmyshop.supplierreturn.grid')
        );
        return parent::_prepareLayout();
    }

    /**
     *
     *
     * @param string $type
     * @return string
     */
    protected function _getCreateUrl()
    {
        return $this->getUrl(
            'supplierreturn/*/new'
        );
    }

    /**
     * Render grid
     *
     * @return string
     */
    public function getGridHtml()
    {
        return $this->getChildHtml('grid');
    }

}