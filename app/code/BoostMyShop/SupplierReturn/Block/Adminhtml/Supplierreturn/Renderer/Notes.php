<?php

namespace BoostMyShop\SupplierReturn\Block\Adminhtml\Supplierreturn\Renderer;

use Magento\Framework\DataObject;
class Notes extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    public function render(DataObject $row)
    {

        $html = '<textarea name="products['.$row->getId().'][bsrp_notes]" rows="2" cols="25">'.$row->getbsrp_notes().'</textarea>';
        return $html;
    }
}