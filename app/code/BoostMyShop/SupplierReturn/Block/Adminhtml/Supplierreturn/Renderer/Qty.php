<?php

namespace BoostMyShop\SupplierReturn\Block\Adminhtml\Supplierreturn\Renderer;

use Magento\Framework\DataObject;

class Qty extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{

    public function render(DataObject $row)
    {
        $html = '<input size="6" type="textbox" name="products['.$row->getId().'][bsrp_qty]" id="products['.$row->getId().'][bsrp_qty]" value="'.$row->getbsrp_qty().'">';

        return $html;
    }
}