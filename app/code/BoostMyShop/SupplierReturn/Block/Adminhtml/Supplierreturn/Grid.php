<?php
namespace BoostMyShop\SupplierReturn\Block\Adminhtml\Supplierreturn;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    
    protected $moduleManager;

    
    protected $_supplierreturnFactory;

    
    protected $_status;
    protected $paymentStatus;
    protected $warehouse;
    protected $_supplier;
    
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \BoostMyShop\SupplierReturn\Model\SupplierreturnFactory $SupplierreturnFactory,
        \BoostMyShop\SupplierReturn\Model\Supplierreturn\Status $status,
        \BoostMyShop\SupplierReturn\Model\Supplierreturn\Paymentstatus $paymentStatus,
        \BoostMyShop\Supplier\Model\Source\Supplier $_supplier,
        \BoostMyShop\SupplierReturn\Model\Supplierreturn\Warehouse $warehouse,
        \Magento\Framework\Module\Manager $moduleManager,
        array $data = []
    ) {
        $this->_supplierreturnFactory = $SupplierreturnFactory;
        $this->_status = $status;
        $this->paymentStatus = $paymentStatus;
        $this->_supplier = $_supplier;
        $this->warehouse  = $warehouse;
        $this->moduleManager = $moduleManager;
        parent::__construct($context, $backendHelper, $data);
    }

    
    protected function _construct()
    {
        parent::_construct();
        $this->setId('postGrid');
        $this->setDefaultSort('bsr_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
        $this->setVarNameFilter('post_filter');
    }

    
    protected function _prepareCollection()
    {
        $collection = $this->_supplierreturnFactory->create()->getCollection();
        $this->setCollection($collection);

        parent::_prepareCollection();

        return $this;
    }

   
    protected function _prepareColumns()
    {
		      $this->addColumn("bsr_created_at", array(
                "header" => __("Created at"),
                "index" => "bsr_created_at",
                "type"  => "date",
                ));
                $_supplier  = $this->_supplier->toArray();
                $this->addColumn("bsr_supplier_id", array(
                "header" => __("Supplier"),
                "index" => "bsr_supplier_id",
                'type'      => 'options',
                "renderer" => "BoostMyShop\SupplierReturn\Block\Adminhtml\Supplierreturn\Renderer\Suppliername",
                "options" => $_supplier,
                ));

                $this->addColumn("bsr_reference", array(
                "header" => __("Reference"),
                "index" => "bsr_reference",
                ));

                $statuses = $this->_status->getOptionArray();
                $this->addColumn("status", array(
                "header" => __("Status"),
                "index" => "status",
                'type'      => 'options',
                'options'   => $statuses,
                ));

                $paymentstatuses = $this->paymentStatus->getOptionArray();
                $this->addColumn("bsr_payment_status", array(
                "header" => __("Payment Status"),
                "index" => "bsr_payment_status",
                'type'      => 'options',
                'options'   => $paymentstatuses,
                ));
                $this->addColumn("bsr_products_count", array(
                "header" => __("Products count"),
                "index" => "bsr_products_count",
                "type" => "number",
                ));

                $this->addColumn("bsr_shipped_date", array(
                "header" => __("Shipped at"),
                "index" => "bsr_shipped_date",
                "type" => "date",
                ));

                $this->addColumn("bsr_tracking_number", array(
                "header" => __("Tracking"),
                "index" => "bsr_tracking_number",
                ));
		
		   $this->addExportType($this->getUrl('supplierreturn/*/exportCsv', ['_current' => true]),__('CSV'));
		   $this->addExportType($this->getUrl('supplierreturn/*/exportExcel', ['_current' => true]),__('Excel XML'));

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('supplierreturn/*/index', ['_current' => true]);
    }

    public function getRowUrl($row)
    {
		
        return $this->getUrl(
            'supplierreturn/*/edit',
            ['bsr_id' => $row->getId()]
        );
		
    }

	

}