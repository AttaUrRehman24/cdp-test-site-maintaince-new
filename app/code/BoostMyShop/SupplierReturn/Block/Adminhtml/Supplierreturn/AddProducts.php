<?php

namespace BoostMyShop\SupplierReturn\Block\Adminhtml\Supplierreturn;

use Magento\Backend\Block\Widget\Grid\Column;

class AddProducts extends \Magento\Backend\Block\Widget\Grid\Extended
{
    protected $_coreRegistry = null;
    protected $_productFactory = null;
    protected $_supplierConfig = null;
    protected $_productCollectionFactory = null;
    protected $_jsonEncoder;
    protected $_resource;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param \Magento\Authorization\Model\ResourceModel\Role\CollectionFactory $userRolesFactory
     * @param \Magento\Framework\Registry $coreRegistry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productFactory,
        \BoostMyShop\SupplierReturn\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \BoostMyShop\Supplier\Model\Config $supplierConfig,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\App\ResourceConnection $resource,
        array $data = []
    ) {
        $this->_jsonEncoder = $jsonEncoder;
        $this->_productFactory = $productFactory;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_coreRegistry = $coreRegistry;
        $this->_supplierConfig = $supplierConfig;
        $this->_resource = $resource;

        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * Class constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('productsGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('desc');
        $this->setTitle(__('Add products to supplier return'));
        $this->setUseAjax(true);

    }

    protected function getSupplierReturn()
    {
        return $this->_coreRegistry->registry('current_supplier_return');
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {

        $collection = $this->_productFactory->create();
        $collection->addAttributeToSelect('name');
        $collection->addAttributeToSelect('image');
        $collection->addAttributeToSelect('thumbnail');
        if ($this->_supplierConfig->getBarcodeAttribute())
            $collection->addAttributeToSelect($this->_supplierConfig->getBarcodeAttribute());
        $collection->addFieldToFilter('type_id', array('in' => array('simple')));

        $alreadyAddedProducts = $this->_productCollectionFactory->create()->getAlreadyAddedProductIds($this->getSupplierReturn()->getId());
        if (count($alreadyAddedProducts) > 0)
        $collection->addFieldToFilter('entity_id', array('nin' => $alreadyAddedProducts));

        //link to product / supplier
        $connection  = $this->_resource->getConnection();
        $tableName   = $connection->getTableName('bms_supplier_product');
        $collection->getSelect()->joinLeft(
            $tableName,
            'sp_product_id = entity_id and sp_sup_id = '.$this->getSupplierReturn()->getbsr_supplier_id(),
            array('*')
        );

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * @return $this
     */
    protected function _prepareColumns()
    {

        $this->addColumn(
            'in_products',
            [
                'header' => __('Select'),
                'renderer' => '\BoostMyShop\SupplierReturn\Block\Adminhtml\Supplierreturn\AddProducts\Renderer\Checkbox',
                'index' => 'entity_id',
                'sortable' => false,
                'filter' => false,
                'align' => 'center',
            ]
        );

        $this->addColumn(
            'qty',
            [
                'filter' => false,
                'sortable' => false,
                'header' => __('Quantity'),
                'renderer' => '\BoostMyShop\SupplierReturn\Block\Adminhtml\Supplierreturn\AddProducts\Renderer\Qty',
                'name' => 'qty',
                'inline_css' => 'qty',
                'filter' => false,
                'align' => 'center',
                'type' => 'input',
                'validate_class' => 'validate-number',
                'index' => 'qty'
            ]
        );

        $this->addColumn('image', ['header' => __('Image'),'filter' => false, 'sortable' => false, 'type' => 'renderer', 'renderer' => '\BoostMyShop\SupplierReturn\Block\Adminhtml\Supplierreturn\AddProducts\Renderer\Image']);

        if ($this->_supplierConfig->getBarcodeAttribute())
            $this->addColumn('barcode', ['header' => __('Barcode'), 'filter' => false, 'index' => $this->_supplierConfig->getBarcodeAttribute(), 'type' => 'text']);

        $this->addColumn('sku', ['header' => __('Sku'), 'index' => 'sku', 'type' => 'text', 'renderer' => '\BoostMyShop\SupplierReturn\Block\Adminhtml\Supplierreturn\AddProducts\Renderer\Sku']);
        $this->addColumn('name', ['header' => __('Product'), 'index' => 'name', 'type' => 'text']);
        $this->addColumn('sp_sku', ['header' => __('Supplier Sku'), 'filter' => false, 'index' => 'sp_sku']);
        $this->addColumn('stock_details', ['header' => __('Stock details'),'filter' => false, 'sortable' => false, 'type' => 'renderer', 'renderer' => '\BoostMyShop\SupplierReturn\Block\Adminhtml\Supplierreturn\AddProducts\Renderer\StockDetails']);
        $this->addColumn('suppliers', ['header' => __('Suppliers'), 'index' => 'entity_id', 'sortable' => false, 'filter' => '\BoostMyShop\SupplierReturn\Block\Adminhtml\Supplierreturn\AddProducts\Filter\Suppliers', 'type' => 'renderer', 'renderer' => '\BoostMyShop\SupplierReturn\Block\Adminhtml\Supplierreturn\AddProducts\Renderer\Suppliers']);

        return parent::_prepareColumns();
    }


    /**
     * Get grid url
     *
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/addProductsGrid', ['bsr_id' => $this->getSupplierReturn()->getId()]);
    }

}
