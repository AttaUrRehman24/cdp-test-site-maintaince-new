<?php
namespace BoostMyShop\SupplierReturn\Block\Adminhtml\Supplierreturn\Edit;

/**
 * Admin page left menu
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    

    protected $_coreRegistry;


    /**
     * Class constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('supplierreturn_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Supplier Return'));
    }

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param \Magento\Backend\Model\Auth\Session $authSession
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Backend\Model\Auth\Session $authSession,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;

        parent::__construct($context, $jsonEncoder, $authSession, $data);
    }

    protected function getSupplierReturn()
    {
        return $this->_coreRegistry->registry('supplierreturn');
    }

    protected function _beforeToHtml()
    {
        $this->addTab(
            'main_section',
            [
                'label' => __('General'),
                'title' => __('General'),
                'content' => $this->getLayout()->createBlock('BoostMyShop\SupplierReturn\Block\Adminhtml\Supplierreturn\Edit\Tab\Main')->toHtml(),
                'active' => true
            ]
        );

        if ($this->getSupplierReturn()->getId())
        {
            $this->addTab(
                'products_section',
                [
                    'label' => __('Products'),
                    'title' => __('Products'),
                    'content' => $this->getLayout()->createBlock('BoostMyShop\SupplierReturn\Block\Adminhtml\Supplierreturn\Products')->toHtml()
                ]
            );

            $this->addTab(
                'add_products_section',
                [
                    'label' => __('Add Products'),
                    'title' => __('Add Products'),
                    'url'       => $this->getUrl('*/*/addProductsGrid', array('_current'=>true)),
                    'class'     => 'ajax'
                ]
            );
        }

        return parent::_beforeToHtml();
    }
}