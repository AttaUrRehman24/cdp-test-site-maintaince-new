<?php

namespace BoostMyShop\SupplierReturn\Block\Adminhtml\Supplierreturn\Edit\Tab;

/**
 * Supplierreturn edit form main tab
 */
class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    protected $_systemStore;
    protected $_status;
    protected $paymentStatus;
    protected $warehouse;
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \BoostMyShop\SupplierReturn\Model\Supplierreturn\Status $status,
        \BoostMyShop\SupplierReturn\Model\Supplierreturn\Paymentstatus $paymentStatus,
        \BoostMyShop\SupplierReturn\Model\Supplierreturn\Warehouse $warehouse,
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        $this->_status = $status;
        $this->paymentStatus = $paymentStatus;
        $this->warehouse  = $warehouse;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('supplierreturn');

        $isElementDisabled = false;

        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('page_');
		
        $fieldset = $form->addFieldset("supplierreturn_form", ['legend' => __('Information')]);
        $fieldsetshipping = $form->addFieldset("supplierreturn_form_shipping",['legend' => __('Shipping')]);
        $fieldsetpayment = $form->addFieldset("supplierreturn_form_payment", ['legend' => __('Payment')]);

        if ($model->getBsrSupplierId() && $model->getBsrSupplierId() != '') {

            $fieldset->addField("supplier_id",'hidden', array(
                "name" => "supplier_id",
                "value" => $model->getBsrSupplierId()
            ));
            $model->setsupplier_id($model->getBsrSupplierId());

            $fieldset->addField("supplier_name",'label', array(
                "name" => "supplier_name",
                "label" => __("Supplier"),
                "value" => $model->getSupplier()->getsup_name()
            ));
            $model->setsupplier_name($model->getSupplier()->getsup_name());
        }

        if($this->getRequest()->getParam('bsr_id')){
            $fieldset->addField('bsr_id', 'hidden', ['name' => 'bsr_id']);
            $fieldset->addField('products_to_add', 'hidden', ['name' => 'products_to_add']);
        }
        

        $fieldset->addField("bsr_created_at", "date", array(
            "label" => __("Created at"),                    
            "name" => "bsr_created_at",
            'style' => "border:none;",
            'readonly' => true,
            'date_format' => $this->_localeDate->getDateFormat(\IntlDateFormatter::SHORT),
            'time_format' => $this->_localeDate->getTimeFormat(\IntlDateFormatter::SHORT),
        ));

        $fieldset->addField("bsr_reference", "text", array(
        "label" => __("Reference"),                 
        "class" => "required-entry",
        "required" => true,
        "name" => "bsr_reference",
        ));

        $statuses = $this->_status->getOptionArray();
        $fieldset->addField('status', 'select', array(
          'label' => __('Status'),
          'name' => 'status',
          'values' => $statuses,
        ));
        
        $wh = $this->warehouse->getOptionArray();
        $fieldset->addField('bsr_warehouse_id', 'select', 
            array(
            'label' => __('Warehouse'),
            'name' => 'bsr_warehouse_id',
            'values' => $wh,
        ));

        $fieldset->addField('bsr_store_id', 'select', 
            array(
             'label' => __('Store'),
             'name' => 'bsr_store_id',
             'values' => $this->_systemStore->getStoreValuesForForm(false, true),
        ));

        $fieldset->addField('bsr_notes', 'textarea',
            array(
                'label' => __('Notes'),
                'name' => 'bsr_notes',
                'note' => __('Displayed on the PDF')
            ));

        $fieldsetshipping->addField(
            'bsr_shipped_date',
            'date',
            [
                'name' => 'bsr_shipped_date',
                'label' => __('Shipped at'),
                'title' => __('Shipped at'),
                'time' => false,
                'date_format' => $this->_localeDate->getDateFormat(\IntlDateFormatter::SHORT),
                'class' => 'validate-date'
            ]
        );

        $fieldsetshipping->addField("bsr_shipping_method", "text", 
            array(
            "label" => __("Shipping method"),                   
            "name" => "bsr_shipping_method",
        ));

        $fieldsetshipping->addField("bsr_tracking_number", "text", 
            array(
            "label" => __("Tracking number"),                   
            "name" => "bsr_tracking_number",
        ));

        $fieldsetpayment->addField(
            'bsr_payment_date',
            'date',
            [
                'name' => 'bsr_payment_date',
                'label' => __('Payment date'),
                'time' => false,
                'title' => __('Payment date'),
                'date_format' => $this->_localeDate->getDateFormat(\IntlDateFormatter::SHORT),
                'class' => 'validate-date'
            ]
        );

        $fieldsetpayment->addField("bsr_payment_method", "text", 
            array(
            "label" => __("Payment method"),                    
            "name" => "bsr_payment_method",
        ));

        $paymentstatuses = $this->paymentStatus->getOptionArray();
        $fieldsetpayment->addField('bsr_payment_status', 'select', array(
          'label' => __('Payment Status'),
          'name' => 'bsr_payment_status',
          'values' => $paymentstatuses,
        ));
					

        if (!$model->getId()) {
            $model->setData('is_active', $isElementDisabled ? '0' : '1');
        }

        $form->setValues($model->getData());
        $this->setForm($form);
		
        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Item Information');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Item Information');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
    
    public function getTargetOptionArray(){
    	return array(
    				'_self' => "Self",
					'_blank' => "New Page",
    				);
    }
}
