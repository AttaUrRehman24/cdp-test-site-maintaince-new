<?php

namespace BoostMyShop\SupplierReturn\Block\Adminhtml\Supplierreturn;

class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     * Initialize supplierreturn edit block
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_objectId = 'bsr_id';
        $this->_blockGroup = 'BoostMyShop_SupplierReturn';
        $this->_controller = 'adminhtml_supplierreturn';

        parent::_construct();

        $this->buttonList->update('save', 'label', __('Save'));

        $this->buttonList->update('delete', 'label', __('Delete'));

        if ($this->_coreRegistry->registry('supplierreturn')->getId())
            $this->addButtons();
    }

    public function addButtons()
    {
        $id = $this->_coreRegistry->registry('supplierreturn')->getId();
        $message = __('Please confirm the shipment creation, this will create the stock movements.');
        $this->buttonList->add("Ship", array(
            "label"     => __("Ship"),
            "class"     => "Ship",
            'onclick'   => "confirmSetLocation('{$message}', '{$this->getUrl('*/*/ship',array('id'=>$id))}')",
            "style"    => $this->getStyle(),
        ), -1, 0);

        $this->buttonList->add("notify", array(
            "label"     => __("Notify"),
            'onclick'   => "setLocation('{$this->getUrl('*/*/notifySupplier',array('id'=>$id))}')",
            "class"     => "notify",
        ), -1, 0);

        $this->buttonList->add("Print", array(
            "label"     => __("Print"),
            'onclick'   => "setLocation('{$this->getUrl('*/*/printSupplier',array('id'=>$id))}')",
            "class"     => "Print",
        ), -1, 0);

    }

    /**
     * Retrieve text for header element depending on loaded post
     *
     * @return \Magento\Framework\Phrase
     */
    public function getHeaderText()
    {
        if ($this->_coreRegistry->registry('supplierreturn')->getId()) {
            return __("Edit Supplierreturn '%1'", $this->escapeHtml($this->_coreRegistry->registry('supplierreturn')->getTitle()));
        } else {
            return __('New Supplierreturn');
        }
    }

    /**
     * Getter of url for "Save and Continue" button
     * tab_id will be replaced by desired by JS later
     *
     * @return string
     */
    protected function _getSaveAndContinueUrl()
    {
        return $this->getUrl('supplierreturn/*/save', ['_current' => true, 'back' => 'edit', 'active_tab' => '{{tab_id}}']);
    }

    /**
     * Prepare layout
     *
     * @return \Magento\Framework\View\Element\AbstractBlock
     */
    protected function _prepareLayout()
    {
        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('page_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'content');
                }
            };
        ";
        return parent::_prepareLayout();
    }

    public function getStyle(){
        $bsr_is_shipped = $this->_coreRegistry->registry('supplierreturn')->getbsr_is_shipped();
        if($bsr_is_shipped == 0){
            return "display:inline;";
        }else{
            return "display:none;";
        }
    }

}