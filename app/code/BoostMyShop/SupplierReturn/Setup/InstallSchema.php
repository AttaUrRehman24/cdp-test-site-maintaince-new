<?php


namespace BoostMyShop\SupplierReturn\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;
        $installer->startSetup();

        /**
         * Create table 'bms_supplier_return'
         */
        $table = $installer->getConnection()
            ->newTable($installer->getTable('bms_supplier_return'))
            ->addColumn('bsr_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Supplier id')
            ->addColumn('bsr_created_at', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, [], 'Created at')
            ->addColumn('bsr_updated_at', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, [], 'Updated at')
            ->addColumn('bsr_supplier_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, [], 'bsr_supplier_id')
            ->addColumn('bsr_reference', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 50, [], 'bsr_reference')
            ->addColumn('bsr_private_comments', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, null, [], 'bsr_private_comments')
            ->addColumn('bsr_public_comments', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, null, [], 'bsr_public_comments')
            ->addColumn('bsr_warehouse_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, [], 'bsr_warehouse_id')
            ->addColumn('bsr_shipped_date', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, [], 'bsr_shipped_date')
            ->addColumn('bsr_supplier_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, [], 'bsr_supplier_id')
            ->addColumn('bsr_store_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, [], 'bsr_store_id')
            ->addColumn('bsr_shipping_method', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'bsr_shipping_method')
            ->addColumn('bsr_tracking_number', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, null, [], 'bsr_tracking_number')
            ->addColumn('bsr_products_count', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, [], 'bsr_products_count')
            ->addColumn('bsr_is_shipped', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, null, array('unsigned'  => true,'nullable'  => true,'default'=>'0'), 'bsr_is_shipped')
            ->addColumn('status', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 20, [], 'status')
            ->setComment('supplier return');
        $installer->getConnection()->createTable($table);

        /**
         * Create table 'bms_supplier_return_product'
         */
        $table = $installer->getConnection()
            ->newTable($installer->getTable('bms_supplier_return_product'))
            ->addColumn('bsrp_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'bsrp_id')
            ->addColumn('bsrp_return_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, [], 'bsrp_return_id')
            ->addColumn('bsrp_product_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, [], 'bsrp_product_id')
            ->addColumn('bsrp_qty', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, [], 'bsrp_qty')
            ->addColumn('bsrp_notes', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 400, [], 'bsrp_notes')
            ->setComment('supplier return product');
        $installer->getConnection()->createTable($table);
        $setup->getConnection()->addIndex(
                $setup->getTable('bms_supplier_return'),
                $setup->getIdxName('bsr_supplier_id', 'bsr_supplier_id'),
                ['bsr_supplier_id'],
                \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_INDEX
            );
        $setup->getConnection()->addIndex(
                $setup->getTable('bms_supplier_return'),
                $setup->getIdxName('status', 'status'),
                ['status'],
                \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_INDEX
            );

        $setup->getConnection()->addIndex(
                $setup->getTable('bms_supplier_return_product'),
                $setup->getIdxName('bsrp_return_id', 'bsrp_return_id'),
                ['bsrp_return_id'],
                \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_INDEX
            );
        $setup->getConnection()->addIndex(
                $setup->getTable('bms_supplier_return_product'),
                $setup->getIdxName('bsrp_product_id', 'bsrp_product_id'),
                ['bsrp_product_id'],
                \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_INDEX
            );

        $setup->endSetup();
    }
}
