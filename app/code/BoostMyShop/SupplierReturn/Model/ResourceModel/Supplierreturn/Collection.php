<?php

namespace BoostMyShop\SupplierReturn\Model\ResourceModel\Supplierreturn;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('BoostMyShop\SupplierReturn\Model\Supplierreturn', 'BoostMyShop\SupplierReturn\Model\ResourceModel\Supplierreturn');
        $this->_map['fields']['page_id'] = 'main_table.page_id';
    }

}
?>