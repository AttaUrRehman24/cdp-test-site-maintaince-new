<?php
namespace BoostMyShop\SupplierReturn\Model\ResourceModel\Product;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('BoostMyShop\SupplierReturn\Model\Product', 'BoostMyShop\SupplierReturn\Model\ResourceModel\Product');
    }

    public function getAlreadyAddedProductIds($return_id)
    {
        $this->getSelect()->reset()->from($this->getMainTable(), ['bsrp_product_id'])->where("bsrp_return_id = ".$return_id);
        $ids = $this->getConnection()->fetchCol($this->getSelect());

        return $ids;
    }
    public function addSupReturnFilter($sup_ret_id)
    {

        $this->getSelect()->where("bsrp_return_id = ".$sup_ret_id);

        return $this;
    }

}