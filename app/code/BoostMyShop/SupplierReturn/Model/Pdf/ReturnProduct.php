<?php

namespace BoostMyShop\SupplierReturn\Model\Pdf;


class ReturnProduct extends AbstractPdf
{

    protected $_storeManager;
    protected $_localeResolver;
    protected $_config;
    protected $_productFactory;
    protected $_supplierProductFactory;

    public function __construct(
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\Stdlib\StringUtils $string,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Sales\Model\Order\Pdf\Total\Factory $pdfTotalFactory,
        \Magento\Sales\Model\Order\Pdf\ItemsFactory $pdfItemsFactory,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Sales\Model\Order\Address\Renderer $addressRenderer,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Locale\ResolverInterface $localeResolver,
        \BoostMyShop\Supplier\Model\ConfigFactory $config,
        \BoostMyShop\Supplier\Model\Supplier\ProductFactory $supplierProductFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        array $data = []
    ) {
        $this->_storeManager = $storeManager;
        $this->_localeResolver = $localeResolver;
        $this->_config = $config;
        $this->_productFactory = $productFactory;
        $this->_supplierProductFactory = $supplierProductFactory;

        parent::__construct(
            $paymentData,
            $string,
            $scopeConfig,
            $filesystem,
            $pdfTotalFactory,
            $pdfItemsFactory,
            $localeDate,
            $inlineTranslation,
            $addressRenderer,
            $data
        );
    }

    /**
     * Draw header for item table
     *
     * @param \Zend_Pdf_Page $page
     * @return void
     */
    protected function _drawHeader(\Zend_Pdf_Page $page)
    {
        /* Add table head */
        $this->_setFontRegular($page, 10);
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(1));

        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0));
        $page->setLineWidth(0.5);
        $page->drawRectangle(25, $this->y, 570, $this->y - 15);
        $this->y -= 10;

        //columns headers
        $lines[0][] = ['text' => __('Qty'), 'feed' => 45, 'align' => 'right'];
        $lines[0][] = ['text' => __('Supplier Sku'), 'feed' => 80, 'align' => 'left'];
        $lines[0][] = ['text' => __('Product'), 'feed' => 200, 'align' => 'left'];
        $lines[0][] = ['text' => __('Notes'), 'feed' => 400, 'align' => 'left'];

        $lineBlock = ['lines' => $lines, 'height' => 5];

        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $this->drawLineBlocks($page, [$lineBlock], ['table_header' => true]);
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $this->y -= 20;
    }

    /**
     * Return PDF document
     *
     * @param array|Collection $invoices
     * @return \Zend_Pdf
     */
    public function getPdf($returns = [])
    {
        $this->_beforeGetPdf();

        $pdf = new \Zend_Pdf();
        $this->_setPdf($pdf);
        $style = new \Zend_Pdf_Style();
        $this->_setFontBold($style, 10);

        foreach ($returns as $return) {
            if ($return->getBsrStoreId()) {
                $this->_localeResolver->emulate($return->getBsrStoreId());
                $this->_storeManager->setCurrentStore($return->getBsrStoreId());
            }
            $page = $this->newPage();

            /* Add image */
            $this->insertLogo($page, $return->getBsrStoreId());

            $this->_drawHeaderText($page, $return);

            /* Add document text and number */
            $this->drawBsrInformation($page, $return);

            /* Add public comments */
            if($return->getbsr_public_comments()){
                $this->drawPublicComments($page, $return);
            }

            $this->_drawHeader($page);

            /* Add body */
            foreach ($return->getAllItems() as $item) {

                /* Draw item */
                $this->_drawItem($item, $page, $return);

                $page = end($pdf->pages);
            }

            /* Add totals */
            $this->_drawFooter($pdf);

            if ($return->getBsrStoreId()) {
                $this->_localeResolver->revert();
            }

        }
        $this->_afterGetPdf();
        return $pdf;
    }

    protected function _drawHeaderText(\Zend_Pdf_Page $page, $return)
    {
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(1));
        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0));
        $page->setLineWidth(0.5);
        $page->drawRectangle(25, $this->y, 570, $this->y - 35);        
        $this->_setFontBold($page, 18);
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $page->drawText(__('Supplier return #%1', $return->getbsr_reference()), 30, $this->y - 21, 'UTF-8');

        $this->y -= 30;
    }

    /**
     * Add page number in pdf
     *
     * @param  $pdf
     */
    protected function _drawFooter($pdf)
    {
        $number_pages = sizeof($pdf->pages);
        foreach($pdf->pages AS $key => $page) {
            $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
            $page->drawText(($key + 1) . "/$number_pages", 540, 15, 'UTF-8');
        }

    }

    /**
     * Create new page and assign to PDF object
     *
     * @param  array $settings
     * @return \Zend_Pdf_Page
     */
    public function newPage(array $settings = [])
    {
        /* Add new table head */
        $page = $this->_getPdf()->newPage(\Zend_Pdf_Page::SIZE_A4);
        $this->_getPdf()->pages[] = $page;
        $this->y = 800;
        if (!empty($settings['table_header'])) {
            $this->_drawHeader($page);
        }
        return $page;
    }

    /**
     * @param $item
     * @param $page
     * @param $order
     */
    protected function _drawItem($item, $page, $return)
    {
        /* Add table head */
        $this->_setFontRegular($page, 10);
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(1));

        $product = $this->getProduct($item->getbsrp_product_id(), $return);

        $lines[0][] = ['text' => $item->getbsrp_qty(), 'feed' => 45, 'align' => 'right'];
        $lines[0][] = ['text' => $product->getsp_sku(), 'feed' => 80];

        $proSkuArr = array($product->getSku(), $product->getName());
        if ($proSkuArr) {
            $tempLine = 0;
            foreach ($proSkuArr as $value) {
                $lines[$tempLine][] = ['text' => $this->string->split($value, 50, true, true), 'feed' => 200, 'align' => 'left'];
                $tempLine++;
            }
        }

        $tempLine = 0;
        $notes = $item->getbsrp_notes();
        if ($notes) {
            $notesArr = explode("\n", $notes);
            
            foreach ($notesArr as $value) {
                $lines[$tempLine][] = ['text' => $this->string->split($value, 50, true, true), 'feed' => 400, 'align' => 'left'];
                $tempLine++;
            }
        }
        $lineBlock = ['lines' => $lines, 'height' => 8];

        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $this->drawLineBlocks($page, [$lineBlock], ['table_header' => true]);
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $this->y -= 20;
    }

    protected function drawBsrInformation($page, $return)
    {

        $savedY = $this->y;

        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $this->_setFontRegular($page, 12);
        $additionnalTxt = [];
        $additionnalTxt[] = __('Date : %1', $return->getbsr_created_at());
        $additionnalTxt[] = __('Supplier : %1', $return->getSupplier()->getsup_name());
        $additionnalTxt[] = __('Products count : %1', $return->getbsr_products_count());
        
        if($return->getbsr_shipping_method()){
            $additionnalTxt[] = __('Shipping Method : %1', $return->getbsr_shipping_method());
        }
        if($return->getbsr_tracking_number()){
            $additionnalTxt[] = __('Tracking Number : %1', $return->getbsr_tracking_number());
        }

        if($return->getbsr_notes()){
            $additionnalTxt[] = " ";
            $additionnalTxt[] = __('Notes :');
            $notes =  $this->string->split($return->getbsr_notes(), 100, true, true);
            foreach($notes as $note)
            {
                $additionnalTxt[] = $note;
            }
        }

        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(1));
        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0));
        $page->setLineWidth(0.5);
        $page->drawRectangle(25, $this->y, 570, $this->y - 20 - count($additionnalTxt) * 13);

        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $i = 0;
        $this->y -= 20;
        foreach($additionnalTxt as $txt)
        {
            $page->drawText($txt, 35, $this->y, 'UTF-8');
            $i++;

            $this->y -= 13;
        }

        $this->y -= 30;


    }

    protected function drawPublicComments($page, $return)
    {
        $comments = $return->getbsr_public_comments();

        if (!$comments)
            return $this;

        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(1));
        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0));
        $page->setLineWidth(0.5);

        $comments = explode("\n", $comments);
        
        $lineCount = count($comments) + 1;
        $page->drawRectangle(25, $this->y, 570, $this->y - 50 - ($lineCount * 13));

        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $this->_setFontBold($page, 14);
        $page->drawText(__('Special instructions :'), 30, $this->y - 20, 'UTF-8');

        $this->_setFontRegular($page, 12);

        foreach ($comments as $i => $line)
            $page->drawText($line, 60, ($this->y - 40 - ($i *13)), 'UTF-8');

        $this->y -= 60 + ($lineCount * 13);
    }

    public function getPdfObject()
    {
        return $this->_getPdf();
    }

    public function setFontBold($page, $size)
    {
        $this->_setFontBold($page, $size);
        return $this;
    }

    public function setFontRegular($page, $size)
    {
        $this->_setFontRegular($page, $size);
        return $this;
    }

    public function getProduct($proId, $return)
    {
        $product = $this->_productFactory->create()->load($proId);

        $productSupplier = $this->_supplierProductFactory->create()->loadByProductSupplier($proId, $return->getSupplier()->getId());
        $product->setsp_sku($productSupplier->getsp_sku());

        return $product;
    }
}
