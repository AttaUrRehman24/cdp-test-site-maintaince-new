<?php
namespace BoostMyShop\SupplierReturn\Model;

class Supplierreturn extends \Magento\Framework\Model\AbstractModel
{
    protected $_objectManager;
    protected $config;
    protected $objDate;
    protected $_warehouseItemFactory;
    protected $_productloader;  
    protected $_supplierloader;
    protected $_stockMovement;
    protected $_messageManager;
    protected $_supplier = null;

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('BoostMyShop\SupplierReturn\Model\ResourceModel\Supplierreturn');
    }

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\ObjectManagerInterface $objectmanager,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Config\ScopeConfigInterface $config,
        \Magento\Framework\Stdlib\DateTime\DateTime $objDate,
        \Magento\Catalog\Model\ProductFactory $_productloader,
        \BoostMyShop\Supplier\Model\SupplierFactory $_supplierloader,
        \BoostMyShop\AdvancedStock\Model\StockMovementFactory $_stockMovement,
        \BoostMyShop\AdvancedStock\Model\Warehouse\ItemFactory $warehouseItemFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        array $data = []
    )
    {
        parent::__construct($context, $registry, null, null, $data);
        $this->objDate = $objDate;
        $this->config  = $config;
        $this->_productloader = $_productloader;
        $this->_objectManager = $objectmanager;
        $this->_supplierloader = $_supplierloader;
        $this->_stockMovement  = $_stockMovement;
        $this->_warehouseItemFactory = $warehouseItemFactory;
        $this->_messageManager = $messageManager;
    }

    public function applyDefaultData($supplierId)
    {
		$date = $this->objDate->gmtDate();
		
		$this->setbsr_supplier_id($supplierId);
        $this->setbsr_created_at($date);
        $this->setbsr_warehouse_id($this->config->getValue('supplier/supplierreturn/default_warehouse'));
        $this->setstatus(\BoostMyShop\SupplierReturn\Model\Supplierreturn\Status::DRAFT);
        return $this;
    }


    public function addProduct($productId, $qty)
    {
        $obj = $this->_objectManager->create('BoostMyShop\SupplierReturn\Model\Product');
        $obj->setbsrp_return_id($this->getId());
        $obj->setbsrp_product_id($productId);
        $obj->setbsrp_qty($qty);
        $obj->save();
        return $this;
    }
    
    public function removeProduct($productId){
        $model = $this->getAllItems()
            ->addFieldToFilter("bsrp_product_id",$productId)
            ->getFirstItem()->delete();
    }

    public function getAllItems(){
        $collection = $this->_objectManager->create('BoostMyShop\SupplierReturn\Model\Product')->getCollection()->addFieldToFilter("bsrp_return_id",$this->getId());
        return $collection;
    }

    public function getSupplier(){
        if ($this->_supplier == null) {
            $this->_supplier = $this->_supplierloader->create()->load($this->getbsr_supplier_id());
        }
        return $this->_supplier;
    }

    public function ship($id){
        $return = $this->load($id);
        $returnItemCollection = $this->getAllItems();
        $warehouse_id = $return->getbsr_warehouse_id();
        $checkQty = false;
        $stockModel = $this->_warehouseItemFactory->create();
        foreach ($returnItemCollection as $key => $item) {
            $sourceStock = $stockModel->loadByProductWarehouse($item->getbsrp_product_id(),$warehouse_id);
            if($item->getbsrp_qty() > ($sourceStock->getwi_available_quantity() - $sourceStock->getwi_reserved_quantity())){
                $producName = $this->_productloader->create()->load($item->getbsrp_product_id())->getName();
                $this->_messageManager->addError('Quantity for '.$producName.' is not available');
                $checkQty = false;
                return false;
            }else{
                $checkQty = true;
            }
        }
        if($checkQty == true){
            $desciption = __("Supplier return #".$return->getbsr_reference()." for ".$this->getSupplier()->getSupName());
            foreach ($returnItemCollection as $key => $item) {
                $stockMovement = $this->_stockMovement->create();
                $movement = $stockMovement->create($item->getbsrp_product_id(),$warehouse_id,0,$item->getbsrp_qty(),\BoostMyShop\AdvancedStock\Model\StockMovement\Category::adjustment,$desciption,0);
            }
            $this->setbsr_shipped_date(date("Y-m-d H:i:s"))
            ->setbsr_is_shipped('1')
            ->setstatus(\BoostMyShop\SupplierReturn\Model\Supplierreturn\Status::SHIPED)
            ->save();
            return true;
        }
        return false;
    }

    public function getToken()
    {
        return sha1($this->getId().' bsr '.$this->getbsr_created_at());
    }
}
?>