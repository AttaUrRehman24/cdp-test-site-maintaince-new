<?php

namespace BoostMyShop\SupplierReturn\Model\Supplierreturn;


class Notification
{
    protected $_config;
    protected $_transportBuilder;
    protected $_storeManager;
    protected $_state;


    public function __construct(
        \BoostMyShop\Supplier\Model\Config $config,
        \Magento\Framework\App\State $state,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {
        $this->_config = $config;
        $this->_transportBuilder = $transportBuilder;
        $this->_storeManager = $storeManager;
        $this->_state = $state;
    }

    public function notifyToSupplier($supplierReturn)
    {

        $email = $supplierReturn->getSupplier()->getsup_email();
        $name = $supplierReturn->getSupplier()->getsup_contact();
        $storeId = ($supplierReturn->getbsr_store_id() ? $supplierReturn->getbsr_store_id() : 1);
        if (!$email)
            throw new \Exception('No email configured for this supplier');

        $template = $this->_config->getSetting('supplierreturn/email_template', $storeId);
        $sender = $this->_config->getSetting('supplierreturn/email_identity', $storeId);

        $params = $this->buildParams($supplierReturn);

        $this->_sendEmailTemplate($template, $sender, $params, $storeId, $email, $name);
    }

    protected function _sendEmailTemplate($template, $sender, $templateParams = [], $storeId, $recipientEmail, $recipientName)
    {
        $transport = $this->_transportBuilder->setTemplateIdentifier(
            $template
        )->setTemplateOptions(
            ['area' => \Magento\Framework\App\Area::AREA_FRONTEND, 'store' => $storeId]
        )->setTemplateVars(
            $templateParams
        )->setFrom(
            $sender
        )->addTo(
            $recipientEmail,
            $recipientName
        )->getTransport();
        $transport->sendMessage();

        return $this;
    }

    protected function buildParams($supplierReturn)
    {
        $datas = [];

        foreach($supplierReturn->getData() as $k => $v)
            $datas[$k] = $v;

        foreach($supplierReturn->getSupplier()->getData() as $k => $v)
            $datas[$k] = $v;

        $datas['company_name'] = $this->_config->getGlobalSetting('general/store_information/name', $supplierReturn->getbsr_store_id());
        $datas['supplierreturn'] = $supplierReturn;
        $datas['supplier'] = $supplierReturn->getSupplier();

        $datas['pdf_url'] = $this->getDownloadPdfUrl($supplierReturn);

        return $datas;
    }

    protected function getDownloadPdfUrl($supplierReturn)
    {
        //getUrl from admi doesnt work, dirty workaround below (git issue : https://github.com/magento/magento2/issues/5322)
        $url = $this->_storeManager->getStore($supplierReturn->getbsr_store_id())->getBaseUrl().'supplierreturn/pdf/download/bsr_id/'.$supplierReturn->getId().'/token/'.$supplierReturn->getToken();
        return $url;
    }

}