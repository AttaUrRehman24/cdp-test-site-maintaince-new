<?php

namespace BoostMyShop\SupplierReturn\Model\Supplierreturn;

class Status
{
    /**#@+
     * Status values
     */
    const DRAFT = 1;
    const TOPREPARE = 2;
    const SHIPED = 3;
    const CANCELED = 4;

    /**
     * Retrieve option array
     *
     * @return string[]
     */
    public static function getOptionArray()
    {
        return array(
           SELF::DRAFT =>__('Draft'),
           SELF::TOPREPARE =>__('To prepare'),
           SELF::SHIPED =>__('Shipped'),
           SELF::CANCELED =>__('Canceled'),
        );
    }

    /**
     * Retrieve option array with empty value
     *
     * @return string[]
     */
    public function getAllOptions()
    {
        $result = [];

        foreach (self::getOptionArray() as $index => $value) {
            $result[] = ['value' => $index, 'label' => $value];
        }

        return $result;
    }

    /**
     * Retrieve option text by option value
     *
     * @param string $optionId
     * @return string
     */
    public function getOptionText($optionId)
    {
        $options = self::getOptionArray();

        return isset($options[$optionId]) ? $options[$optionId] : null;
    }
}