<?php

namespace BoostMyShop\SupplierReturn\Controller\Adminhtml\Supplierreturn;

class Supplierajax extends \BoostMyShop\SupplierReturn\Controller\Adminhtml\SpReturn
{
    public function execute()
    {
        $sup_id = $this->_request->getParam('sup_id');
        if($sup_id){
            $resultLayout = $this->_resultLayoutFactory->create();
            $block = $resultLayout->getLayout()->getBlock('supplierreturn.supplierreturn.returngrid');
            $block->getHtmlData($sup_id);
            $block->setUseAjax(true);
            return $resultLayout;
        }
    }
}
?>