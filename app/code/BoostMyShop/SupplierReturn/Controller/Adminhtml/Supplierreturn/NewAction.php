<?php
namespace BoostMyShop\SupplierReturn\Controller\Adminhtml\Supplierreturn;

class NewAction extends \BoostMyShop\Supplier\Controller\Adminhtml\Order
{
    /**
     * @return void
     */
    public function execute()
    {
        $breadcrumb = __('New Supplier Return');
        $this->_initAction()->_addBreadcrumb($breadcrumb, $breadcrumb);
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__('New Supplier Return'));
        $this->_view->getPage()->getConfig()->getTitle()->prepend( __('New Supplier Return'));
        $this->_view->renderLayout();
    }
}