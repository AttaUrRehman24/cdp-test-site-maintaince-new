<?php

namespace BoostMyShop\SupplierReturn\Controller\Adminhtml\Supplierreturn;

class Ship extends \BoostMyShop\SupplierReturn\Controller\Adminhtml\SpReturn
{
    public function execute()
    {
        $id = $this->_request->getParam("id");

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        $model = $this->_objectManager->create('BoostMyShop\SupplierReturn\Model\Supplierreturn')->ship($id);

        if($model){
            $this->messageManager->addSuccess(__('Ship has been saved.'));
        }

        return $resultRedirect->setPath('*/*/edit', ['bsr_id' => $id]);

    }
}