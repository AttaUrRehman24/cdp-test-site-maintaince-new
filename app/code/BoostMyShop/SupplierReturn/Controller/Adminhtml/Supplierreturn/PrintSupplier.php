<?php

namespace BoostMyShop\SupplierReturn\Controller\Adminhtml\Supplierreturn;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\Filesystem\DirectoryList;

class PrintSupplier extends \BoostMyShop\SupplierReturn\Controller\Adminhtml\SpReturn
{
    /**
     * @var \Magento\Backend\Model\View\Result\ForwardFactory
     */
    protected $resultForwardFactory;

    public function execute()
    {
        $id = (int)$this->getRequest()->getParam('id');
        
        if ($id) {
            $model = $this->_supplierreturnFactory->create()->load($id);
            
            if ($model) {
                $pdf = $this->_objectManager->create('\BoostMyShop\SupplierReturn\Model\Pdf\ReturnProduct')->getPdf([$model]);
                $date = $this->_objectManager->get('Magento\Framework\Stdlib\DateTime\DateTime')->date('Y-m-d_H-i-s');
                return $this->_objectManager->get('\Magento\Framework\App\Response\Http\FileFactory')->create(
                    'supplierreturn_' . $date . '.pdf',
                    $pdf->render(),
                    DirectoryList::VAR_DIR,
                    'application/pdf'
                );
            }
        } else {
            return $this->resultForwardFactory->create()->forward('noroute');
        }
    }
}
?>