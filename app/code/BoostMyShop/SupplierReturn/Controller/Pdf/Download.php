<?php

namespace BoostMyShop\SupplierReturn\Controller\Pdf;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Registry;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\Filesystem\DirectoryList;

class Download extends \Magento\Framework\App\Action\Action
{

    protected $registry;
    protected $resultPageFactory;
    protected $httpContext;
    protected $_customerSession;
    protected $_adminNotification;
    protected $supplierReturnFactory;


    public function __construct(
        Context $context,
        Registry $registry,
        PageFactory $resultPageFactory,
        \Magento\Framework\App\Http\Context $httpContext,
        \BoostMyShop\SupplierReturn\Model\SupplierreturnFactory $supplierReturnFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);

        $this->registry = $registry;
        $this->supplierReturnFactory = $supplierReturnFactory;
        $this->httpContext = $httpContext;

    }

    public function execute()
    {
        $id = $this->getRequest()->getParam('bsr_id');
        $token = $this->getRequest()->getParam('token');

        $sp = $this->supplierReturnFactory->create()->load($id);
        if (($token) && ($token == $sp->getToken()))
        {
            $pdf = $this->_objectManager->create('\BoostMyShop\SupplierReturn\Model\Pdf\ReturnProduct')->getPdf([$sp]);
            return $this->_objectManager->get('\Magento\Framework\App\Response\Http\FileFactory')->create(
                'supplierreturn_' . $sp->getbsr_reference(). '.pdf',
                $pdf->render(),
                DirectoryList::VAR_DIR,
                'application/pdf'
            );
        }

    }

}
