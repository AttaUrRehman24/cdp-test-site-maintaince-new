<?php

namespace BoostMyShop\SupplierReturn\Observer;
 
use Magento\Framework\Event\ObserverInterface;
 
class AddSupplierReturnTab implements ObserverInterface
{
	protected $_layout;
    protected $layoutFactory;
    protected $_request;

	public function __construct(
    	\Magento\Framework\View\Element\Context $context,
        \Magento\Framework\View\LayoutFactory $layoutFactory
	)
	{
	    $this->_request = $context->getRequest();
	    $this->_layout = $context->getLayout();
        $this->layoutFactory = $layoutFactory;
	}

    /**
     * custom event handler
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
    	$sup_id = $this->_request->getParam('sup_id');
        try{
            if($sup_id){
	    		$tabs = $observer->getEvent()->getData('tabs');
			    $tabs->addTab('tab_supplier_returns', array(
			        'label' => __('Supplier returns'),
			        'content' => $this->layoutFactory->create()->createBlock('\BoostMyShop\SupplierReturn\Block\Adminhtml\Supplier\Edit\ReturnGrid')->getHtmlData($sup_id)->toHtml(),
			    ));
	    	}
        }catch(Exception $e){
            $this->messageManager->addError($e->getMessage());
        }
    }
}
