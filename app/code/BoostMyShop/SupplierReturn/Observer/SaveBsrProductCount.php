<?php

namespace BoostMyShop\SupplierReturn\Observer;
 
use Magento\Framework\Event\ObserverInterface;
 
class SaveBsrProductCount implements ObserverInterface
{
	protected $supplierReturn;

    public function __construct(
        \BoostMyShop\SupplierReturn\Model\Supplierreturn $supplierReturn
    )
    {
        $this->supplierReturn = $supplierReturn;
    }

    /**
     * save product total count
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
    	$id = $observer->getData('id');
        $supplierReturn = $this->supplierReturn->load($id);
        $returnProducts = $supplierReturn->getAllItems();

        $totalQty = 0;
        if(count($returnProducts) > 0){
            foreach($returnProducts as $item)
            {
                $qty = $item['bsrp_qty'];
                if (isset($qty) && $qty > 0)
                {
                    $totalQty += $qty;
                }
            }    
        }
        
        $supplierReturn->setbsr_products_count($totalQty);
        $supplierReturn->save();
        
    }
}
