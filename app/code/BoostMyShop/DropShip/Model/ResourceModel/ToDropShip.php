<?php

namespace BoostMyShop\DropShip\Model\ResourceModel;

use Magento\Sales\Api\Data\OrderSearchResultInterface;
use Magento\Sales\Model\ResourceModel\Collection\AbstractCollection;

class ToDropShip extends \Magento\Sales\Model\ResourceModel\Order\Collection
{
    protected $_extendedOrderItemCollectionFactory;
    protected $_config;

    public function __construct(
        \BoostMyShop\AdvancedStock\Model\ResourceModel\ExtendedSalesFlatOrderItem\CollectionFactory $extendedOrderItemCollectionFactory,
        \BoostMyShop\DropShip\Model\Config $config,
        \Magento\Framework\Data\Collection\EntityFactory $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Framework\Model\ResourceModel\Db\VersionControl\Snapshot $entitySnapshot,
        \Magento\Framework\DB\Helper $coreResourceHelper,
        \Magento\Framework\DB\Adapter\AdapterInterface $connection = null,
        \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource = null
    ) {
        parent::__construct(
            $entityFactory,
            $logger,
            $fetchStrategy,
            $eventManager,
            $entitySnapshot,
            $coreResourceHelper,
            $connection,
            $resource
        );

        $this->_extendedOrderItemCollectionFactory = $extendedOrderItemCollectionFactory;
        $this->_config = $config;
    }

    public function addToDropShipFilter($warehouse)
    {
        $orderItemIdsInDropShipWarehouse = $this->getOrderItemIdsInDropShipWarehouse($warehouse);
        $orderItemIdsBeingDrop = $this->getOrderItemIdsBeingDrop();
        $availableItemIdsForDropShip = array_diff($orderItemIdsInDropShipWarehouse, $orderItemIdsBeingDrop);

        $availableOrdersForDropShip = $this->getOrdersFromOrderItemIds($availableItemIdsForDropShip);

        $availableOrdersForDropShip[] = -1;     //prevent crash if no orders available
        $this->addFieldToFilter('entity_id', ['in' => $availableOrdersForDropShip]);

        $statuses = $this->_config->getOrderStatuses();
        if ($statuses)
            $this->addFieldToFilter('status', ['in' => $statuses]);

    }

    public function getOrderItemIdsBeingDrop()
    {
        $connection = $this->getConnection();
        $statuses = ["'".\BoostMyShop\Supplier\Model\Order\Status::toConfirm."'", "'".\BoostMyShop\Supplier\Model\Order\Status::expected."'"];
        $select = $connection
            ->select()
            ->from($this->getTable('bms_purchase_order'), [])
            ->join($this->getTable('bms_purchase_order_product'), 'po_id = pop_po_id',array('pop_dropship_order_item_id'))
            ->where('po_status in ('.implode(',', $statuses).')')
            ->where('po_type = "ds"');
        $result = $connection->fetchCol($select);
        return $result;

    }

    public function getOrderItemIdsInDropShipWarehouse($warehouseId)
    {
        $collection = $this->_extendedOrderItemCollectionFactory->create()->addQtyToShipFilter()->joinOrderItem()->addWarehouseFilter($warehouseId);
        $collection->getSelect()->reset(\Magento\Framework\DB\Select::COLUMNS);
        $collection->getSelect()->columns(new \Zend_Db_Expr('distinct item_id'));
        return $this->getConnection()->fetchCol($collection->getSelect());
    }

    protected function getOrdersFromOrderItemIds($orderItemIds)
    {
        $orderItemIds[] = -1;   //prevent crash if $orderItemIds empty
        $select = $this->getConnection()
            ->select()
            ->from($this->getTable('sales_order_item'), ['order_id'])
            ->where('item_id in ('.implode(',', $orderItemIds).')');
        $result = $this->getConnection()->fetchCol($select);
        return $result;
    }

}