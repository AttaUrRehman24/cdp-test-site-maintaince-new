<?php

namespace BoostMyShop\DropShip\Model\ResourceModel\DropShipper;

class Collection extends \BoostMyShop\Supplier\Model\ResourceModel\Supplier\Product\Collection
{

    public function init()
    {
        $this->joinSupplier();

        return $this;
    }

    public function joinSupplier()
    {
        $this->getSelect()->join($this->getTable('bms_supplier'), 'sp_sup_id = sup_id');
        return $this;
   }

    /**
     * Post inject qty_to_order
     * Todo : use regular collection sql query to allow future filter / sort on this value
     *
     * @return $this
     */
    protected function _afterLoad()
    {
        parent::_afterLoad();

        foreach ($this as $product) {
            //$value = $product->getqty_for_backorder() + $product->getqty_for_low_stock() - $product->getqty_to_receive();
            //$product->setData('qty_to_order', max($value, 0));
        }

        return $this;
    }


    public function addProductFilter($productId)
    {
        $this->addFieldToFilter('sp_product_id', $productId);
        return $this;
    }

}
