<?php

namespace BoostMyShop\DropShip\Model\ResourceModel\ExtendedSalesFlatOrderItem;


class Collection extends \BoostMyShop\AdvancedStock\Model\ResourceModel\ExtendedSalesFlatOrderItem\Collection
{
    public function joinPoProduct()
    {
        $this->getSelect()->joinLeft(
            $this->getTable('bms_purchase_order_product'),
            'item_id = pop_dropship_order_item_id'
        );
        $this->getSelect()->joinLeft(
            $this->getTable('bms_purchase_order'),
            'pop_po_id = po_id',
            ['po_reference']
        );
        return $this;
    }

}
