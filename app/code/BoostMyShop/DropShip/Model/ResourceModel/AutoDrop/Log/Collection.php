<?php namespace BoostMyShop\DropShip\Model\ResourceModel\AutoDrop\Log;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {

    protected function _construct()
    {
        $this->_init('BoostMyShop\DropShip\Model\AutoDrop\Log', 'BoostMyShop\DropShip\Model\ResourceModel\AutoDrop\Log');
    }

}