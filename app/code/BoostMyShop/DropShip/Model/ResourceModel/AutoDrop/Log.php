<?php namespace BoostMyShop\DropShip\Model\ResourceModel\AutoDrop;


class Log extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {

    protected function _construct()
    {
        $this->_init('bms_dropship_log', 'dsl_id');
    }

}