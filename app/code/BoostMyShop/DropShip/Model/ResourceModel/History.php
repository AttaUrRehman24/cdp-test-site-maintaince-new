<?php

namespace BoostMyShop\DropShip\Model\ResourceModel;

use Magento\Sales\Api\Data\OrderSearchResultInterface;
use Magento\Sales\Model\ResourceModel\Collection\AbstractCollection;

class History extends \BoostMyShop\Supplier\Model\ResourceModel\Order\Collection
{

    public function addHistoryFilter()
    {
        $this->getSelect()->where("po_status = '".\BoostMyShop\Supplier\Model\Order\Status::complete."'");
        $this->getSelect()->where("po_type = '".\BoostMyShop\Supplier\Model\Order\Type::dropShip."'");

        $this->getSelect()->join($this->getTable('sales_order'), 'po_dropship_order_id = entity_id');

        $this->getSelect()->columns(new \Zend_Db_Expr('concat(customer_firstname, " ", customer_lastname) as customer_name'));

        return $this;

    }

}