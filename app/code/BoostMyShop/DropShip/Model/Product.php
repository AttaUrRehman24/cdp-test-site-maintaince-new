<?php

namespace BoostMyShop\DropShip\Model;

use Magento\CatalogInventory\Model\Spi\StockRegistryProviderInterface;
use Magento\CatalogInventory\Api\StockConfigurationInterface;

class Product
{
    protected $_productAction;
    protected $_productFactory;
    protected $_stockRegistryProvider;
    protected $_stockConfiguration;
    protected $_currencyFactory;
    protected $_productResourceModel;


    /*
     * @var \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        StockRegistryProviderInterface $stockRegistryProvider,
        StockConfigurationInterface $stockConfiguration,
        \Magento\Catalog\Model\Product\Action $productAction,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \BoostMyShop\DropShip\Model\ResourceModel\Product $productResourceModel,
        \Magento\Directory\Model\CurrencyFactory $currencyFactory
    ){
        $this->_productAction = $productAction;
        $this->_stockRegistryProvider = $stockRegistryProvider;
        $this->_stockConfiguration = $stockConfiguration;
        $this->_productFactory = $productFactory;
        $this->_currencyFactory = $currencyFactory;
        $this->_productResourceModel = $productResourceModel;
    }

    public function productIsDeleted($productId)
    {
        $result = $this->_productResourceModel->productIsDeleted($productId);
        return $result;
    }


}