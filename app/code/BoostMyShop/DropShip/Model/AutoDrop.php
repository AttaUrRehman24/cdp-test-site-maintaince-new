<?php

namespace BoostMyShop\DropShip\Model;

class AutoDrop
{
    protected $_debug = [];
    protected $_logFactory;
    protected $_config;
    protected $_dropShipper;
    protected $_orderHelper;
    protected $_toDropShipCollectionFactory;
    protected $_dropShip;
    protected $_supplierFactory;

    public function __construct(
        \BoostMyShop\DropShip\Model\AutoDrop\LogFactory $logFactory,
        \BoostMyShop\DropShip\Model\Config $config,
        \BoostMyShop\DropShip\Model\Order $orderHelper,
        \BoostMyShop\DropShip\Model\DropShipper $dropShipper,
        \BoostMyShop\DropShip\Model\DropShip $dropShip,
        \BoostMyShop\Supplier\Model\SupplierFactory $supplierFactory,
        \BoostMyShop\DropShip\Model\ResourceModel\ToDropShipFactory $toDropShipCollectionFactory
    ){
        $this->_logFactory = $logFactory;
        $this->_config = $config;
        $this->_orderHelper = $orderHelper;
        $this->_dropShipper = $dropShipper;
        $this->_toDropShipCollectionFactory = $toDropShipCollectionFactory;
        $this->_dropShip = $dropShip;
        $this->_supplierFactory = $supplierFactory;
    }

    public function process()
    {
        $this->_debug = [];

        try
        {

            $orders = $this->getOrdersCollection();
            $this->_debug[] = $orders->getSize().' orders found';

            foreach($orders as $order)
            {
                $result = $this->processOrder($order);
                $this->_debug[] = 'Order #'.$order->getincrement_id().': '.$result;
            }
        }
        catch(\Exception $ex)
        {
            $this->_debug[] = 'An error occured : '.$ex->getMessage();
        }

        $this->addLog(implode(', ', $this->_debug));

    }

    protected function getOrdersCollection()
    {
        $collection = $this->_toDropShipCollectionFactory->create();

        $warehouse = $this->_config->getDropShipWarehouse();
        $collection->addToDropShipFilter($warehouse);

        return $collection;
    }

    protected function processOrder($order)
    {
        $result = [];
        $itemsPerSupplier = [];

        foreach($this->_orderHelper->getOrderItemCollection($order) as $orderItem)
        {
            $suppliers = $this->_dropShipper->getSuppliersForOrderItem($order, $orderItem);
            $orderItemStatus = $this->_orderHelper->getOrderItemStatus($order, $orderItem, $suppliers);

            switch($orderItemStatus)
            {
                case 'has_supplier':
                    $defaultSupplier = $this->getDefaultSupplier($suppliers);
                    if (!isset($itemsPerSupplier[$defaultSupplier->getsup_id()]))
                        $itemsPerSupplier[$defaultSupplier->getsup_id()] = [];
                    $itemsPerSupplier[$defaultSupplier->getsup_id()][] = $orderItem;
                    $result[] = 'Sku "'.$orderItem->getSku().'" added to PO for '.$defaultSupplier->getsup_name();
                    break;
                case 'no_supplier':
                    $result[] = 'Sku "'.$orderItem->getSku().'" has no supplier';
                    break;
            }

        }

        foreach($itemsPerSupplier as $supplierId => $orderItems)
        {
            $supplier = $this->_supplierFactory->create()->load($supplierId);
            $items = [];
            foreach($orderItems as $orderItem)
            {
                $items[] = ['order_item' => $orderItem, 'order_item_id' => $orderItem->getId(), 'qty' => $orderItem->getqty_ordered(), 'price' => ''];
            }
            $po = $this->_dropShip->create($supplier, $order, $items);
            if (!$this->_config->supplierValidationEnabled())
                $this->_dropShip->confirmPending($po->getId());
            $result[] = 'PO #"'.$po->getpo_reference().'" created';
        }

        return implode(',', $result);
    }

    protected function addLog($message)
    {
        $log = $this->_logFactory->create();
        $log->setdsl_message($message);
        $log->save();
    }

    public function getDebug()
    {
        return $this->_debug;
    }

    protected function getDefaultSupplier($suppliers)
    {
        foreach($suppliers as $supplier)
        {
            if ($supplier->getIsDefault())
                return $supplier;
        }
    }


}