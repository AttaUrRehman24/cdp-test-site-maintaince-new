<?php

namespace BoostMyShop\DropShip\Model;

class DropShipper
{
    protected $_dropShipperCollection;
    protected $_config;

    public function __construct(
        \BoostMyShop\DropShip\Model\ResourceModel\DropShipper\CollectionFactory $dropShipperCollection,
        \BoostMyShop\DropShip\Model\Config $config
    ){
        $this->_resourceModel = $dropShipperCollection;
        $this->_config = $config;
    }

    public function getSuppliersForProduct($productId)
    {
        $collection = $this->_resourceModel->create()->init()->addProductFilter($productId);

        if ($this->_config->supplierMustHaveDropShip())
            $collection->addFieldToFilter('sp_can_dropship', 1);
        if ($this->_config->supplierMustHaveStock())
            $collection->addFieldToFilter('sp_stock', ['gt' => 0]);
        if ($this->_config->supplierMustHavePrice())
            $collection->addFieldToFilter('sp_price', ['gt' => 0]);

        return $collection;
    }

    public function getSuppliersForOrderItem($order, $orderItem)
    {
        $collection = $this->getSuppliersForProduct($orderItem->getproduct_id());
        $collection->setOrder('sp_price', 'asc');
        $this->assignDefaultSupplier($collection);
        return $collection;
    }

    public function assignDefaultSupplier($collection)
    {
        $cheapestItem = null;

        foreach($collection as $item)
        {
            if ($this->_config->priorizePrimary() && $item->getsp_primary())
            {
                $item->setis_default(1);
                return;
            }

            if ($this->_config->priorizeCheapest())
            {
                $cheapestPrice = ($cheapestItem ? $cheapestItem->getsp_price() : 99999);
                if ($item->getsp_price() < $cheapestPrice)
                    $cheapestItem = $item;
            }
        }

        if ($cheapestItem)
            $cheapestItem->setis_default(1);
    }


}
