<?php

namespace BoostMyShop\DropShip\Model;

class Order
{
    protected $_config;
    protected $_product;
    protected $_orderItemCollectionFactory;

    /**
     * @param \Magento\Backend\Block\Context $context
     * @param array $data
     */
    public function __construct(
                                \BoostMyShop\DropShip\Model\Config $config,
                                \BoostMyShop\DropShip\Model\ResourceModel\ExtendedSalesFlatOrderItem\CollectionFactory $orderItemCollectionFactory,
                                \BoostMyShop\DropShip\Model\Product $product
                                )
    {
        $this->_config = $config;
        $this->_product = $product;
        $this->_orderItemCollectionFactory = $orderItemCollectionFactory;
    }

    public function getOrderItemCollection($order)
    {
        $collection = $this->_orderItemCollectionFactory
            ->create()
            ->addOrderFilter($order->getentity_id())
            ->joinOrderItem()
            ->joinPoProduct()
            ->addFieldToFilter('product_type', ['nin' => ['configurable', 'bundle']]);
        return $collection;
    }

    public function getOrderItemStatus($order, $item, $suppliers)
    {
        if ($this->_product->productIsDeleted($item->getproduct_id()))
            return 'deleted';

        if ($item->getesfoi_qty_to_ship() <= 0)
            return 'not_drop_ship';

        if ($item->getesfoi_warehouse_id() != $this->_config->getDropShipWarehouse())
            return 'not_drop_ship';

        if ($item->getpop_dropship_order_item_id())
            return 'already_dropship';

        if ($suppliers->getSize() == 0)
            return 'no_supplier';
        else
            return 'has_supplier';
    }


}