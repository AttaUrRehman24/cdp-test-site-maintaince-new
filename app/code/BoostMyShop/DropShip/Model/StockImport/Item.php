<?php namespace BoostMyShop\DropShip\Model\StockImport;

/**
 * Class Item
 *
 * @package   BoostMyShop\DropShip\Model\StockImport
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Item extends \Magento\Framework\Model\AbstractModel {

    const STATUS_PRODUCT_NOT_FOUND = 'product_not_found';
    const STATUS_SUCCESS = 'success';
    const STATUS_ERROR = 'error';

    protected function _construct()
    {
        $this->_init('BoostMyShop\DropShip\Model\ResourceModel\StockImport\Item');
    }

}