<?php namespace BoostMyShop\DropShip\Model\AutoDrop;


class Log extends \Magento\Framework\Model\AbstractModel {

    protected $_dateTime = null;

    protected function _construct()
    {
        $this->_init('BoostMyShop\DropShip\Model\ResourceModel\AutoDrop\Log');
    }

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        array $data = []
    )
    {
        parent::__construct($context, $registry, null, null, $data);

        $this->_dateTime = $dateTime;
    }
    public function beforeSave()
    {
        if (!$this->getId())
            $this->setdsl_created_at($this->_dateTime->gmtDate());

        parent::beforeSave();
    }

}