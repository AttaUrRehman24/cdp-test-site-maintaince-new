<?php

namespace BoostMyShop\DropShip\Model\DropShip;

class Notification extends \BoostMyShop\Supplier\Model\Order\Notification
{
    protected $_orderFactory;
    protected $_addressRenderer;

    public function __construct(
        \BoostMyShop\Supplier\Model\Config $config,
        \Magento\Framework\App\State $state,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Sales\Model\Order\Address\Renderer $addressRenderer,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Sales\Model\OrderFactory $orderFactory
    )
    {
        parent::__construct($config, $state, $transportBuilder, $storeManager);

        $this->_orderFactory = $orderFactory;
        $this->_addressRenderer = $addressRenderer;
    }

    public function notifyToSupplier($purchaseOrder)
    {

        $email = $purchaseOrder->getSupplier()->getsup_email();
        $name = $purchaseOrder->getSupplier()->getsup_contact();
        $storeId = ($purchaseOrder->getpo_store_id() ? $purchaseOrder->getpo_store_id() : 1);
        if (!$email)
            throw new \Exception('No email configured for this supplier');

        $template = $this->_config->getGlobalSetting('dropship/supplier_notification/email_template', $storeId);
        $sender = $this->_config->getGlobalSetting('dropship/supplier_notification/email_identity', $storeId);

        $params = $this->buildParams($purchaseOrder);

        $this->_sendEmailTemplate($template, $sender, $params, $storeId, $email, $name);
    }

    protected function buildParams($purchaseOrder)
    {
        $datas = parent::buildParams($purchaseOrder);

        $datas['management_url'] = $this->getManageUrl($purchaseOrder);

        $datas['shipping_address'] = $this->getShippingAddress($purchaseOrder);

        return $datas;
    }

    protected function getManageUrl($purchaseOrder)
    {
        //getUrl from admi doesnt work, dirty workaround below (git issue : https://github.com/magento/magento2/issues/5322)
        $url = $this->_storeManager->getStore($purchaseOrder->getpo_store_id())->getBaseUrl().'dropship/manage/index/po_id/'.$purchaseOrder->getId().'/token/'.$purchaseOrder->getToken();
        return $url;
    }

    protected function getShippingAddress($po)
    {
        $salesOrder = $this->_orderFactory->create()->load($po->getpo_dropship_order_id());
        $shippingAddress = $this->_addressRenderer->format($salesOrder->getShippingAddress(), 'html');
        return $shippingAddress;
    }

}
