<?php namespace BoostMyShop\DropShip\Model;

/**
 * Class StockImport
 *
 * @package   BoostMyShop\DropShip\Model
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class StockImport extends \Magento\Framework\Model\AbstractModel {

    const STATUS_COMPLETE = 'complete';
    const STATUS_IN_PROGRESS = 'in_progress';
    const STATUS_ERROR = 'error';

    /**
     * @var \BoostMyShop\DropShip\Model\StockImport\ItemFactory
     */
    protected $_itemFactory;

    /**
     * @var \BoostMyShop\DropShip\Model\ResourceModel\StockImport\Item\CollectionFactory
     */
    protected $_itemCollectionFactory;

    /**
     * StockImport constructor.
     * @param \BoostMyShop\DropShip\Model\ResourceModel\StockImport\Item\CollectionFactory $itemCollectionFactory
     * @param \BoostMyShop\DropShip\Model\StockImport\ItemFactory $itemFactory
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \BoostMyShop\DropShip\Model\ResourceModel\StockImport\Item\CollectionFactory $itemCollectionFactory,
        \BoostMyShop\DropShip\Model\StockImport\ItemFactory $itemFactory,
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ){
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->_itemFactory = $itemFactory;
        $this->_itemCollectionFactory = $itemCollectionFactory;
    }

    protected function _construct()
    {
        $this->_init('BoostMyShop\DropShip\Model\ResourceModel\StockImport');
    }

    /**
     * @return array
     */
    public function getStatuses(){

        return [
            self::STATUS_COMPLETE => __('Complete'),
            self::STATUS_IN_PROGRESS => __('In Progress'),
            self::STATUS_ERROR => __('Error')
        ];

    }

    /**
     * @param int $lineId
     * @param string $status
     */
    public function addRow($lineId, $status) {

        $this->_itemFactory->create()
            ->setsid_import_id($this->getId())
            ->setsid_line_id($lineId)
            ->setsid_status($status)
            ->save();

    }

    /**
     * @return \BoostMyShop\DropShip\Model\ResourceModel\StockImport\Item\Collection
     */
    public function getItems(){

        return $this->_itemCollectionFactory->create()
            ->addFieldToFilter('sid_import_id', $this->getId());

    }

    /**
     * @return string $csv
     */
    public function getCsv(){

        $csv = '"Line","Status"'."\n";

        foreach($this->getItems() as $item){

            $csv .= '"'.$item->getsid_line_id().'","'.$item->getsid_status().'"'."\n";

        }

        return $csv;

    }

}