define([
    "jquery",
    "mage/translate",
    "prototype",
    "Magento_Ui/js/modal/alert"
], function(jQuery, translate, prototype, alert){

    window.AdminDropShip = new Class.create();

    AdminDropShip.prototype = {

        initialize : function(){

        },

        init: function(dropShipUrl, cancelDropShipUrl, confirmPendingUrl, confirmShippingUrl, priceUrl, supplierValidationEnabled, showConfirmationPopup)
        {
            this.dropShipUrl = dropShipUrl;
            this.cancelDropShipUrl = cancelDropShipUrl;
            this.confirmPendingUrl = confirmPendingUrl;
            this.confirmShippingUrl = confirmShippingUrl;
            this.priceUrl = priceUrl;
            this.supplierValidationEnabled = supplierValidationEnabled;
            this.showConfirmationPopup = showConfirmationPopup;

            setInterval(function(){ objDropShip.updateTabTotals(); }, 3000);

        },


        dropShip: function(orderId)
        {
            //basic test
            if (!this.hasProductConfigured(orderId))
            {
                alert({content: 'Configure at least one product'});
                return;
            }

            //process
            var fields = $('table_todropship_' + orderId).select('select', 'input');
            var data = Form.serializeElements(fields, true);
            jQuery.ajax({
                url: this.dropShipUrl,
                data: data,
                dataType: 'json',
                success: function (resp) {
                    if (!resp.success) {
                        alert({content: resp.message});
                    }
                    else
                    {
                        if (objDropShip.supplierValidationEnabled)
                        {
                            if (objDropShip.showConfirmationPopup)
                                alert({content: 'Drop ship sent to supplier, order is now in the pending supplier validation tab.'});
                            tab_pendingvalidationJsObject.doFilter();
                        }
                        else
                        {
                            if (objDropShip.showConfirmationPopup)
                                alert({content: 'Drop ship sent to supplier, order is now in the pending supplier shipping tab.'});
                            tab_pendingshippingJsObject.doFilter();
                        }

                        for(var i=0;i < resp.order_items.length; i++)
                        {
                            jQuery('#td_ds_supplier_orderitem_' + resp.order_items[i])[0].innerHTML = 'Submitted';
                            jQuery('#td_ds_price_orderitem_' + resp.order_items[i])[0].innerHTML = '';
                        }

                        objDropShip.updateTabTotals();
                    }
                },
                failure: function (resp) {
                    jQuery('#debug').html('An error occured.');
                }
            });



        },

        hasProductConfigured: function(orderId)
        {
            var fields = $('table_todropship_' + orderId).select('select');
            var hasProductConfigured = false;
            jQuery.each( fields, function( key, value )
            {
                if (value.value)
                    hasProductConfigured = true;
            });
            return hasProductConfigured;
        },

        cancelDropShip: function(poId)
        {
            //process
            var data = {po_id: poId};
            jQuery.ajax({
                url: this.cancelDropShipUrl,
                data: data,
                dataType: 'json',
                success: function (resp) {
                    if (!resp.success) {
                        alert({content: resp.message});
                    }
                    else
                    {
                        if (objDropShip.showConfirmationPopup)
                            alert({content: 'Dropship has been canceled, order is now in the items to dropship tab'});
                        jQuery('.row-dropship-'+resp.po_id).first().remove();
                        tab_todropshipJsObject.doFilter();
                        tab_pendingshippingJsObject.doFilter();
                        tab_pendingvalidationJsObject.doFilter();
                        objDropShip.updateTabTotals();
                    }
                },
                failure: function (resp) {
                    jQuery('#debug').html('An error occured.');
                }
            });

        },

        confirmPending: function(poId)
        {
            var fields = $('table_dropship_' + poId).select('select', 'input');
            var data = Form.serializeElements(fields, true);
            data.po_id = poId;

            //process
            jQuery.ajax({
                url: this.confirmPendingUrl,
                data: data,
                dataType: 'json',
                success: function (resp) {
                    if (!resp.success) {
                        alert({content: resp.message});
                    }
                    else
                    {
                        if (objDropShip.showConfirmationPopup)
                            alert({content: 'Supplier validation acknowledged, dropship is now in the pending supplier shipping tab.'});
                        jQuery('.row-dropship-'+resp.po_id).first().remove();
                        tab_pendingshippingJsObject.doFilter();
                        objDropShip.updateTabTotals();
                    }
                },
                failure: function (resp) {
                    jQuery('#debug').html('An error occured.');
                }
            });

        },

        confirmShipping: function(poId)
        {
            var fields = $('table_dropship_' + poId).select('select', 'input');
            var data = Form.serializeElements(fields, true);

            data.po_id = poId;
            data.tracking = jQuery('#tracking_' + poId).val();
            data.shipping_cost = jQuery('#shipping_' + poId).val();
            data.notify_customer = jQuery('#notify_' + poId).val();

            //process
            jQuery.ajax({
                url: this.confirmShippingUrl,
                data: data,
                dataType: 'json',
                success: function (resp) {
                    if (!resp.success) {
                        alert({content: resp.message});
                    }
                    else
                    {
                        if (objDropShip.showConfirmationPopup)
                            alert({content: 'Drop shipping confirmed, drop is now in the history tab.'});
                        jQuery('.row-dropship-'+resp.po_id).first().remove();
                    }
                },
                failure: function (resp) {
                    jQuery('#debug').html('An error occured.');
                }
            });

        },

        updatePrice: function(orderId, orderItemId, productId)
        {
            var currentSupplierId = jQuery('#dropship_' + orderId + '_' + orderItemId + '_supplier').val();
            if (!currentSupplierId)
                return;
            var data = {
                order_id: orderId,
                order_item_id: orderItemId,
                product_id: productId,
                supplier_id: currentSupplierId
            }

            jQuery.ajax({
                url: this.priceUrl,
                data: data,
                dataType: 'json',
                success: function (resp) {
                    jQuery('#dropship_' + resp.order_id + '_' + resp.order_item_id + '_price').val(resp.price);
                },
                failure: function (resp) {
                    jQuery('#debug').html('An error occured.');
                }
            });
        },

        updateTabTotals: function()
        {
            jQuery('#tab_todropship_count').html(jQuery('#tab_todropship-total-count').html());
            jQuery('#tab_pending_validation_count').html(jQuery('#tab_pendingvalidation-total-count').html());
            jQuery('#tab_pending_shipping_count').html(jQuery('#tab_pendingshipping-total-count').html());
        }

    };

});
