<?php

namespace BoostMyShop\DropShip\Controller\Adminhtml;

abstract class Supplier extends \Magento\Backend\App\AbstractAction
{
    protected $_coreRegistry;
    protected $_resultLayoutFactory;
    protected $_dropShip;
    protected $_supplierFactory;
    protected $_supplierProductFactory;
    protected $_salesOrderFactory;
    protected $_salesOrderItemFactory;
    protected $_layoutFactory;
    protected $_config;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\User\Model\UserFactory $userFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory,
        \BoostMyShop\DropShip\Model\DropShip $dropShip,
        \BoostMyShop\Supplier\Model\SupplierFactory $supplierFactory,
        \BoostMyShop\Supplier\Model\Supplier\ProductFactory $supplierProductFactory,
        \Magento\Sales\Model\OrderFactory $salesOrderFactory,
        \Magento\Framework\View\LayoutFactory $layoutFactory,
        \Magento\Sales\Model\Order\ItemFactory $salesOrderItemFactory,
        \BoostMyShop\DropShip\Model\Config $config
    ) {
        parent::__construct($context);
        $this->_coreRegistry = $coreRegistry;
        $this->_resultLayoutFactory = $resultLayoutFactory;
        $this->_dropShip = $dropShip;
        $this->_supplierFactory = $supplierFactory;
        $this->_salesOrderFactory = $salesOrderFactory;
        $this->_salesOrderItemFactory = $salesOrderItemFactory;
        $this->_layoutFactory = $layoutFactory;
        $this->_config = $config;
        $this->_supplierProductFactory = $supplierProductFactory;
    }

    /**
     * @return $this
     */
    protected function _initAction()
    {
        $this->_view->loadLayout();

        return $this;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }
}
