<?php

namespace BoostMyShop\DropShip\Controller\Adminhtml\Main;

class CancelDropShip extends \BoostMyShop\DropShip\Controller\Adminhtml\Main
{

    /**
     * @return void
     */
    public function execute()
    {
        $result = [];
        $poId = $this->getRequest()->getPostValue('po_id');

        try
        {
            $this->_dropShip->cancelDropShip($poId);

            $result['po_id'] = $poId;
            $result['success'] = true;
        }
        catch(\Exception $ex)
        {
            $result['success'] = false;
            $result['message'] = $ex->getMessage();
            $result['stack'] = $ex->getTraceAsString();
        }

        return $this->_resultJsonFactory->create()->setData($result);
    }

}
