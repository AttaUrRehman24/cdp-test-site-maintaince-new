<?php

namespace BoostMyShop\DropShip\Controller\Adminhtml\Main;

class Price extends \BoostMyShop\DropShip\Controller\Adminhtml\Main
{

    /**
     * @return void
     */
    public function execute()
    {

        $productId = $this->getRequest()->getPost('product_id');
        $supplierId = $this->getRequest()->getPost('supplier_id');

        $result = [];
        $result['success'] = true;
        $result['order_id'] = $this->getRequest()->getPost('order_id');
        $result['order_item_id'] = $this->getRequest()->getPost('order_item_id');

        try
        {
            $price = '';
            $productSupplier = $this->_supplierProductFactory->create()->loadByProductSupplier($productId, $supplierId);
            if ($productSupplier->getId() && $productSupplier->getsp_price())
                $price = $productSupplier->getsp_price();

            $result['price'] = $price;
            $result['success'] = true;
        }
        catch(\Exception $ex)
        {
            $result['success'] = false;
            $result['message'] = $ex->getMessage();
            $result['stack'] = $ex->getTraceAsString();
        }

        return $this->_resultJsonFactory->create()->setData($result);
    }

}
