<?php

namespace BoostMyShop\DropShip\Controller\Adminhtml\Main;

class Index extends \BoostMyShop\AdvancedStock\Controller\Adminhtml\Routing
{

    /**
     * @return void
     */
    public function execute()
    {
        $this->_initAction();
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Drop Shipping'));
        $this->_view->renderLayout();
    }
}
