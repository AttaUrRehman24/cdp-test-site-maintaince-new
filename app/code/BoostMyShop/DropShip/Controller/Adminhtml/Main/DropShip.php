<?php

namespace BoostMyShop\DropShip\Controller\Adminhtml\Main;

class DropShip extends \BoostMyShop\DropShip\Controller\Adminhtml\Main
{

    /**
     * @return void
     */
    public function execute()
    {
        $result = [];
        $data = $this->getRequest()->getPostValue('dropship');
        $mainOrderId = null;

        try
        {
            $orderItemIds = [];
            $pos = $this->buildPosArray($data);
            foreach($pos as $supplierId => $supplierPos)
            {
                if ($supplierId) {
                    foreach ($supplierPos as $orderId => $items) {
                        $this->dropShip($supplierId, $orderId, $items);
                        $mainOrderId = $orderId;

                        foreach($items as $item)
                            $orderItemIds[] =  $item['order_item_id'];
                    }
                }
            }

            $result['order_id'] = $mainOrderId;
            $result['order_items'] = $orderItemIds;
            $result['success'] = true;
        }
        catch(\Exception $ex)
        {
            $result['success'] = false;
            $result['message'] = $ex->getMessage();
            $result['stack'] = $ex->getTraceAsString();
        }


        return $this->_resultJsonFactory->create()->setData($result);
    }

    protected function dropShip($supplierId, $orderId, $items)
    {
        $supplier = $this->_supplierFactory->create()->load($supplierId);
        $salesOrder = $this->_salesOrderFactory->create()->load($orderId);
        $po = $this->_dropShip->create($supplier, $salesOrder, $items);

        if (!$this->_config->supplierValidationEnabled())
            $this->_dropShip->confirmPending($po->getId());

        return $po;
    }

    protected function buildPosArray($data)
    {
        $pos = [];

        foreach($data as $orderId => $orderData)
        {
            $pos = [];
            foreach($orderData as $itemId => $itemDetails)
            {
                if (!$itemDetails['supplier'])
                    continue;
                if (!isset($pos[$itemDetails['supplier']][$orderId]))
                    $pos[$itemDetails['supplier']][$orderId] = [];
                $orderItem = $this->_salesOrderItemFactory->create()->load($itemId);
                $pos[$itemDetails['supplier']][$orderId][] = ['order_item' => $orderItem, 'order_item_id' => $orderItem->getId(), 'qty' => $orderItem->getqty_ordered(),  'price' => $itemDetails['price']];
            }
        }


        return $pos;
    }

}
