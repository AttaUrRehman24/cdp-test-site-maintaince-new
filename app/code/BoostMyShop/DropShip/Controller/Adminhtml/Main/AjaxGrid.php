<?php

namespace BoostMyShop\DropShip\Controller\Adminhtml\Main;

use Magento\Framework\Controller\ResultFactory;

class AjaxGrid extends \BoostMyShop\DropShip\Controller\Adminhtml\Main
{

    /**
     * @return void
     */
    public function execute()
    {
        $grid = $this->getRequest()->getParam('grid');

        $blockClass = null;
        switch($grid)
        {
            case 'todropship':
                $blockClass = 'BoostMyShop\DropShip\Block\Main\Tab\ToDropShip';
                break;
            case 'pendingvalidation':
                $blockClass = 'BoostMyShop\DropShip\Block\Main\Tab\PendingValidation';
                break;
            case 'pendingshipping':
                $blockClass = 'BoostMyShop\DropShip\Block\Main\Tab\PendingShipping';
                break;
            case 'history':
                $blockClass = 'BoostMyShop\DropShip\Block\Main\Tab\History';
                break;
        }

        $layout = $this->_layoutFactory->create();
        $resultRaw = $this->resultFactory->create(ResultFactory::TYPE_RAW);
        $resultRaw->setContents($layout->createBlock($blockClass)->toHtml());

        return $resultRaw;
    }
}
