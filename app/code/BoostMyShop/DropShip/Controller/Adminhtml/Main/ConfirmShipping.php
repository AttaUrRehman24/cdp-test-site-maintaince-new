<?php

namespace BoostMyShop\DropShip\Controller\Adminhtml\Main;

class ConfirmShipping extends \BoostMyShop\DropShip\Controller\Adminhtml\Main
{

    /**
     * @return void
     */
    public function execute()
    {
        $result = [];
        $poId = $this->getRequest()->getPostValue('po_id');
        $items = $this->getRequest()->getPostValue('poProducts');
        $tracking = $this->getRequest()->getPostValue('tracking');
        $notifyCustomer = $this->getRequest()->getPostValue('notify_customer');
        $shippingCost = $this->getRequest()->getPostValue('shipping_cost');
        $items = $items[$poId];

        try
        {
            $po = $this->_dropShip->confirmShipping($poId, $tracking, $notifyCustomer);
            $po->setpo_shipping_cost($shippingCost)->save();

            $result['po_id'] = $poId;
            $result['success'] = true;
        }
        catch(\Exception $ex)
        {
            $result['success'] = false;
            $result['message'] = $ex->getMessage();
            $result['stack'] = $ex->getTraceAsString();
        }

        return $this->_resultJsonFactory->create()->setData($result);
    }

}
