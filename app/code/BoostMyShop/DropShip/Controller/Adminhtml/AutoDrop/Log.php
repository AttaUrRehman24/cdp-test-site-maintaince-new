<?php

namespace BoostMyShop\DropShip\Controller\Adminhtml\AutoDrop;

class Log extends \BoostMyShop\DropShip\Controller\Adminhtml\AutoDrop
{
    /**
     * @return void
     */
    public function execute()
    {
        $this->_initAction();
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__('AutoDrop - Logs'));
        $this->_view->renderLayout();
    }
}
