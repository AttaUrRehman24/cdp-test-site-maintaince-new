<?php namespace BoostMyShop\DropShip\Controller\Adminhtml\Supplier\StockImport;

use Magento\Framework\App\Filesystem\DirectoryList;

/**
 * Class Download
 *
 * @package   BoostMyShop\DropShip\Controller\Adminhtml\Supplier\StockImport
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Download extends \BoostMyShop\DropShip\Controller\Adminhtml\Supplier {

    /**
     * @var \BoostMyShop\DropShip\Model\StockImportFactory
     */
    protected $_stockImportFactory;

    /**
     * Download constructor.
     * @param \BoostMyShop\DropShip\Model\StockImportFactory $stockImportFactory
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory
     * @param \BoostMyShop\DropShip\Model\DropShip $dropShip
     * @param \BoostMyShop\Supplier\Model\SupplierFactory $supplierFactory
     * @param \BoostMyShop\Supplier\Model\Supplier\ProductFactory $supplierProductFactory
     * @param \Magento\Sales\Model\OrderFactory $salesOrderFactory
     * @param \Magento\Framework\View\LayoutFactory $layoutFactory
     * @param \Magento\Sales\Model\Order\ItemFactory $salesOrderItemFactory
     * @param \BoostMyShop\DropShip\Model\Config $config
     */
    public function __construct(
        \BoostMyShop\DropShip\Model\StockImportFactory $stockImportFactory,
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory,
        \BoostMyShop\DropShip\Model\DropShip $dropShip,
        \BoostMyShop\Supplier\Model\SupplierFactory $supplierFactory,
        \BoostMyShop\Supplier\Model\Supplier\ProductFactory $supplierProductFactory,
        \Magento\Sales\Model\OrderFactory $salesOrderFactory,
        \Magento\Framework\View\LayoutFactory $layoutFactory,
        \Magento\Sales\Model\Order\ItemFactory $salesOrderItemFactory,
        \BoostMyShop\DropShip\Model\Config $config
    ){
        parent::__construct($context, $coreRegistry, $resultLayoutFactory, $dropShip, $supplierFactory, $supplierProductFactory, $salesOrderFactory, $layoutFactory, $salesOrderItemFactory, $config);
        $this->_stockImportFactory = $stockImportFactory;
    }

    public function execute(){

        if($id = filter_var($this->getRequest()->getParam('id'), FILTER_VALIDATE_INT)){

            $stockImport = $this->_stockImportFactory->create()->load($id);

            if($stockImport->getId()){

                $csv = $stockImport->getCsv();
                $date = $this->_objectManager->create('\Magento\Framework\Stdlib\DateTime\DateTime')->date('Y-m-d_H-i-s');
                return $this->_objectManager->get('\Magento\Framework\App\Response\Http\FileFactory')->create(
                    'stock_import_'.$stockImport->getId().'_'.$date.'.csv',
                    $csv,
                    DirectoryList::VAR_DIR,
                    'application/pdf'
                );

            }else{

                $this->messageManager->addError('Not able to load stock import with id '.$id);
                return $this->resultRedirectFactory->create()->setPath('*/*/index', []);

            }

        }else{

            $this->messageManager->addError('Bad input');
            return $this->resultRedirectFactory->create()->setPath('*/*/index', []);

        }

    }

}