<?php namespace BoostMyShop\DropShip\Controller\Adminhtml\Supplier\StockImport;

/**
 * Class Process
 *
 * @package   BoostMyShop\DropShip\Controller\Adminhtml\Supplier\StockImport
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Process extends \BoostMyShop\DropShip\Controller\Adminhtml\Supplier {

    /**
     * @var \BoostMyShop\DropShip\Model\SupplierStockImport
     */
    protected $_supplierStockImport;

    /**
     * Process constructor.
     * @param \BoostMyShop\DropShip\Model\SupplierStockImport $supplierStockImport
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory
     * @param \BoostMyShop\DropShip\Model\DropShip $dropShip
     * @param \BoostMyShop\Supplier\Model\SupplierFactory $supplierFactory
     * @param \BoostMyShop\Supplier\Model\Supplier\ProductFactory $supplierProductFactory
     * @param \Magento\Sales\Model\OrderFactory $salesOrderFactory
     * @param \Magento\Framework\View\LayoutFactory $layoutFactory
     * @param \Magento\Sales\Model\Order\ItemFactory $salesOrderItemFactory
     * @param \BoostMyShop\DropShip\Model\Config $config
     */
    public function __construct(
        \BoostMyShop\DropShip\Model\SupplierStockImport $supplierStockImport,
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory,
        \BoostMyShop\DropShip\Model\DropShip $dropShip,
        \BoostMyShop\Supplier\Model\SupplierFactory $supplierFactory,
        \BoostMyShop\Supplier\Model\Supplier\ProductFactory $supplierProductFactory,
        \Magento\Sales\Model\OrderFactory $salesOrderFactory,
        \Magento\Framework\View\LayoutFactory $layoutFactory,
        \Magento\Sales\Model\Order\ItemFactory $salesOrderItemFactory,
        \BoostMyShop\DropShip\Model\Config $config
    ){
        parent::__construct($context, $coreRegistry, $resultLayoutFactory, $dropShip, $supplierFactory, $supplierProductFactory, $salesOrderFactory, $layoutFactory, $salesOrderItemFactory, $config);
        $this->_supplierStockImport = $supplierStockImport;
    }

    /**
     * @return $this
     */
    public function execute(){

        try {
            if ($id = filter_var($this->getRequest()->getParam('id'), FILTER_VALIDATE_INT)) {

                $this->_supplierStockImport->run($id);
                $this->messageManager->addSuccess('Stock Import successfully processed');
                return $this->resultRedirectFactory->create()->setPath('supplier/supplier/edit', ['sup_id' => $id]);

            } else {

                $this->messageManager->addError('Supplier no more exists');
                return $this->resultRedirectFactory->create()->setPath('supplier/supplier/index', []);

            }
        }catch(\Exception $e){

            $this->messageManager->addError('An error occured during stock import : '.$e->getMessage());
            return $this->resultRedirectFactory->create()->setPath('supplier/supplier/edit', ['sup_id' => $id]);

        }

    }

}