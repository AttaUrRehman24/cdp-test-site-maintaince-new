<?php

namespace BoostMyShop\DropShip\Controller\Manage;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Registry;

abstract class AbstractFront extends \Magento\Framework\App\Action\Action
{

    protected $registry;
    protected $resultPageFactory;
    protected $httpContext;
    protected $_customerSession;
    protected $_adminNotification;
    protected $_orderFactory;
    protected $_dropShip;
    protected $_logger;


    public function __construct(
        Context $context,
        Registry $registry,
        PageFactory $resultPageFactory,
        \BoostMyShop\Supplier\Model\OrderFactory $orderFactory,
        \BoostMyShop\DropShip\Model\DropShip $dropShip,
        \BoostMyShop\DropShip\Helper\Logger $logger,
        \Magento\Framework\App\Http\Context $httpContext
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
        $this->registry = $registry;
        $this->httpContext = $httpContext;
        $this->_orderFactory = $orderFactory;
        $this->_dropShip = $dropShip;
        $this->_logger = $logger;
    }

    public function loadSupplier()
    {
        $poId = $this->getRequest()->getParam('po_id');
        $token = $this->getRequest()->getParam('token');

        $po = $this->_orderFactory->create()->load($poId);
        if (($token) && ($token == $po->getToken()))
        {
            $supplier = $po->getSupplier();
            $this->registry->register('current_supplier', $supplier);
            return $supplier;
        }
        else
            throw new \Exception('Invalid datas');

    }

}
