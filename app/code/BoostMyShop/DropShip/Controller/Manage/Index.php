<?php

namespace BoostMyShop\DropShip\Controller\Manage;


class Index extends AbstractFront
{

    public function execute()
    {
        try
        {
            $supplier = $this->loadSupplier();

            $resultPage = $this->resultPageFactory->create();
            $resultPage->getConfig()->getTitle()->set(__('DropShip Dashboard - %1', $supplier->getsup_name()));
            return $resultPage;
        }
        catch(\Exception $ex)
        {
            $this->_logger->logException($ex);

            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('');
            return $resultRedirect;
        }
    }

}
