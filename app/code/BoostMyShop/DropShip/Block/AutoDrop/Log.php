<?php

namespace BoostMyShop\DropShip\Block\AutoDrop;

use Magento\Backend\Block\Widget\Grid\Column;

class Log extends \Magento\Backend\Block\Widget\Grid\Extended
{
    protected $_logCollectionFactory;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \BoostMyShop\DropShip\Model\ResourceModel\AutoDrop\Log\CollectionFactory $logCollectionFactory,
        array $data = []
    ) {

        parent::__construct($context, $backendHelper, $data);

        $this->_logCollectionFactory = $logCollectionFactory;
    }

    /**
     * Class constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('autoDropLogGrid');
        $this->setDefaultSort('dsl_created_at');
        $this->setDefaultDir('desc');
        $this->setTitle(__('AutoDrop Logs'));
    }


    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->_logCollectionFactory->create();

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * @return $this
     */
    protected function _prepareColumns()
    {
        $this->addColumn('dsl_created_at', ['header' => __('Date'), 'width' => '200px', 'index' => 'dsl_created_at', 'type' => 'datetime']);
        $this->addColumn('dsl_message', ['header' => __('Message'), 'index' => 'dsl_message']);

        return parent::_prepareColumns();
    }


}
