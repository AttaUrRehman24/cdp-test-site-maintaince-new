<?php

namespace BoostMyShop\DropShip\Block\Main\Tab\Renderer\ToDropShip;

use Magento\Framework\DataObject;

class Action extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{

    /**
     * @param \Magento\Backend\Block\Context $context
     * @param array $data
     */
    public function __construct(\Magento\Backend\Block\Context $context,
                                array $data = [])
    {
        parent::__construct($context, $data);
    }

    public function render(DataObject $order)
    {
        $html = [];
        $html[] = '<center><input type="button" value="apply" onclick="objDropShip.dropShip('.$order->getId().')"></center>';
        return implode('', $html);
    }

}