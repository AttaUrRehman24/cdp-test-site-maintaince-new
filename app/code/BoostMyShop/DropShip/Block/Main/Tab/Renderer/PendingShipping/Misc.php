<?php

namespace BoostMyShop\DropShip\Block\Main\Tab\Renderer\PendingShipping;

use Magento\Framework\DataObject;

class Misc extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{

    public function render(DataObject $order)
    {
        $html = [];
        $html[] = '<table border="0" width="100%" id="table_pendingshipping_misc_'.$order->getId().'">';

        $html[] = '<tr>';
        $html[] = '<td>Tracking #</td>';
        $html[] = '<td><input type="textbox" id="tracking_'.$order->getId().'" name="poMisc['.$order->getId().'][tracking]"></td>';
        $html[] = '</tr>';

        $html[] = '<tr>';
        $html[] = '<td>Shipping cost</td>';
        $html[] = '<td><input type="textbox" id="shipping_'.$order->getId().'" name="poMisc['.$order->getId().'][shipping]"></td>';
        $html[] = '</tr>';

        $html[] = '<tr>';
        $html[] = '<td>Notify customer</td>';
        $html[] = '<td><select id="notify_'.$order->getId().'" name="poMisc['.$order->getId().'][notify]" id="">';
        $html[] = '<option value="1">Yes</option>';
        $html[] = '<option value="0">No</option>';
        $html[] = '</select></td>';
        $html[] = '</tr>';

        $html[] = '</table>';

        return implode('', $html);
    }

}