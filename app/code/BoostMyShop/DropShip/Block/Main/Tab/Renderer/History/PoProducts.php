<?php

namespace BoostMyShop\DropShip\Block\Main\Tab\Renderer\History;

use Magento\Framework\DataObject;

class PoProducts extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{

    public function render(DataObject $order)
    {
        $html = [];
        $html[] = '<table border="0" width="100%" id="table_dropship_'.$order->getId().'">';
        $html[] = '<tr>';
        $html[] = '<th>Sku</th>';
        $html[] = '<th>Product</th>';
        $html[] = '<th>Qty</th>';
        $html[] = '<th>Buying price</th>';
        $html[] = '</tr>';

        foreach ($order->getAllItems() as $item) {
            $html[] .= $this->renderItem($order, $item);
        }

        $html[] = '</table>';

        return implode('', $html);
    }

    public function renderItem($order, $item)
    {
        $productUrl = $this->getUrl('catalog/product/edit', ['id' => $item->getpop_product_id()]);

        $html = [];
        $html[] = '<tr>';
        $html[] = '<td><a href="'.$productUrl.'">'.$item->getpop_sku().'</a></td>';
        $html[] = '<td>'.$item->getpop_name().'</td>';
        $html[] = '<td>'.$item->getpop_qty().'</td>';
        $html[] = '<td>'.$item->getpop_price().'</td>';
        $html[] = '</tr>';

        return implode('', $html);
    }

}