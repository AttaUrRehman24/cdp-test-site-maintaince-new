<?php

namespace BoostMyShop\DropShip\Block\Main\Tab\Renderer\ToDropShip;

use Magento\Framework\DataObject;

class Products extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    protected $_dropShipper;
    protected $_supplierCollectionFactory;
    protected $_config;
    protected $_currencyFactory;
    protected $_orderHelper;

    /**
     * @param \Magento\Backend\Block\Context $context
     * @param array $data
     */
    public function __construct(\Magento\Backend\Block\Context $context,
                                \BoostMyShop\DropShip\Model\DropShipper $dropShipper,
                                \BoostMyShop\DropShip\Model\Order $orderHelper,
                                \BoostMyShop\Supplier\Model\ResourceModel\Supplier\CollectionFactory $supplierCollectionFactory,
                                \BoostMyShop\DropShip\Model\Config $config,
                                \Magento\Directory\Model\CurrencyFactory $currencyFactory,
                                array $data = [])
    {
        parent::__construct($context, $data);

        $this->_dropShipper = $dropShipper;
        $this->_orderHelper = $orderHelper;
        $this->_supplierCollectionFactory = $supplierCollectionFactory;
        $this->_config = $config;
        $this->_currencyFactory = $currencyFactory;
    }

    public function render(DataObject $order)
    {
        $html = [];
        $html[] = '<table border="0" width="100%" id="table_todropship_'.$order->getId().'">';
        $html[] = '<tr>';
        $html[] = '<th>Sku</th>';
        $html[] = '<th>Product</th>';
        $html[] = '<th>Qty</th>';
        $html[] = '<th>Supplier</th>';
        $html[] = '<th>Buying price</th>';
        $html[] = '</tr>';

        $collection = $this->_orderHelper->getOrderItemCollection($order);
        foreach ($collection as $item) {
            $html[] .= $this->renderItem($order, $item);
        }

        $html[] = '</table>';

        return implode('', $html);
    }


    public function renderItem($order, $item)
    {

        $productUrl = $this->getUrl('catalog/product/edit', ['id' => $item->getproduct_id()]);

        $html = [];
        $html[] = '<tr>';
        $html[] = '<td><a href="'.$productUrl.'">'.$item->getSku().'</a></td>';
        $html[] = '<td>'.$item->getName().'</td>';
        $html[] = '<td>'.$item->getesfoi_qty_to_ship().'</td>';

        $suppliers = $this->_dropShipper->getSuppliersForOrderItem($order, $item);
        $allSuppliers = $this->_supplierCollectionFactory->create()->setOrder('sup_name', 'asc');

        $status = $this->_orderHelper->getOrderItemStatus($order, $item, $suppliers);
        switch($status)
        {
            case 'no_supplier':
            case 'has_supplier':
                $html[] = '<td id="td_ds_supplier_orderitem_'.$item->getItemId().'">'.$this->getSupplierDropDown($suppliers, $allSuppliers, $order, $item).'</td>';
                $html[] = '<td id="td_ds_price_orderitem_'.$item->getItemId().'"><input type="textbox" value="'.$this->getDefaultPrice($suppliers).'" name="dropship['.$order->getId().']['.$item->getItemId().'][price]" id="dropship_'.$order->getId().'_'.$item->getItemId().'_price"></td>';
                break;
            case 'not_drop_ship':
                $html[] = '<td colspan="2">'.__('Not available for drop ship').'</td>';
                break;
            case 'deleted':
                $html[] = '<td colspan="2">'.__('This product is deleted').'</td>';
                break;
            case 'already_dropship':
                $html[] = '<td colspan="2">'.__('Already part of a dropship, PO #%1', $item->getpo_reference()).'</td>';
                break;
        }

        $html[] = '</tr>';

        return implode('', $html);
    }

    public function getSupplierDropDown($dropShippers, $allSuppliers, $order, $orderItem)
    {
        $html = [];

        $html[] = '<select onchange="objDropShip.updatePrice('.$order->getId().', '.$orderItem->getItemId().', '.$orderItem->getproduct_id().');" name="dropship['.$order->getId().']['.$orderItem->getItemId().'][supplier]" id="dropship_'.$order->getId().'_'.$orderItem->getItemId().'_supplier">';
        $html[] = '<option value=""> </option>';
        $html[] = '<optgroup label="Associated suppliers">';
        foreach($dropShippers as $item)
        {
            $style = '';
            $label = $item->getSupName();
            if ($item->getsp_price() > 0)
                $label .= ' (' . $this->formatPrice($item) . ')';
            if ($item->getis_default())
                $style = 'color: green; font-weight: bold;';
            $selected = '';
            if ($this->_config->selectDefaultSupplier() && $item->getIsDefault())
                $selected = 'selected="selected"';
            $html[] = '<option style="'.$style.'" value="'.$item->getSupId().'" '.$selected.'>'.$label.'</option>';
        }
        if ($dropShippers->getSize() == 0)
            $html[] = '<option value="">There is no supplier associated</option>';
        $html[] = '</optgroup>';
        $html[] = '<optgroup label="All suppliers">';
        foreach($allSuppliers as $item)
        {
            $html[] = '<option value="'.$item->getSupId().'">'.$item->getSupName().'</option>';
        }
        $html[] = '</optgroup>';
        $html[] = '</select>';

        return implode('', $html);
    }

    public function getDefaultPrice($suppliers)
    {
        foreach($suppliers as $supplier)
        {
            if ($supplier->getIsDefault())
                return $supplier->getsp_price();
        }
    }

    protected function formatPrice($supplier)
    {
        $price = $supplier->getsp_price();
        $currencyCode = $supplier->getsup_currency();
        $currency = $this->_currencyFactory->create()->load($currencyCode);
        return $currency->format($price, [], false);
    }



}