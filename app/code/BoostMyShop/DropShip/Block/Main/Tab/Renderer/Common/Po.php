<?php

namespace BoostMyShop\DropShip\Block\Main\Tab\Renderer\Common;

use Magento\Framework\DataObject;

class Po extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{

    public function render(DataObject $purchaseOrder)
    {
        $url = $this->getUrl('supplier/order/edit', ['po_id' => $purchaseOrder->getId()]);
        return '<a href="'.$url.'">'.$purchaseOrder->getpo_reference().'</a>';
    }

}