<?php

namespace BoostMyShop\DropShip\Block\Main\Tab;

use Magento\Backend\Block\Widget\Grid\Column;

class ToDropShip extends \Magento\Backend\Block\Widget\Grid\Extended
{
    protected $_coreRegistry = null;
    protected $_collectionFactory;
    protected $_jsonEncoder;
    protected $_config;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Framework\Registry $coreRegistry,
        \BoostMyShop\DropShip\Model\ResourceModel\ToDropShipFactory $collectionFactory,
        \BoostMyShop\DropShip\Model\Config $config,
        array $data = []
    ) {
        $this->_jsonEncoder = $jsonEncoder;
        $this->_coreRegistry = $coreRegistry;
        $this->_collectionFactory = $collectionFactory;
        $this->_config = $config;

        parent::__construct($context, $backendHelper, $data);

        $this->setMessageBlockVisibility(false);
    }

    /**
     * Class constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();

        $this->setId('tab_todropship');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('desc');
        $this->setUseAjax(true);
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->_collectionFactory->create();

        $this->setCollection($collection);
        $warehouse = $this->_config->getDropShipWarehouse();
        $collection->addToDropShipFilter($warehouse);
        return parent::_prepareCollection();
    }

    /**
     * @return $this
     */
    protected function _prepareColumns()
    {

        $this->addColumn('created_at', ['header' => __('Date'), 'index' => 'created_at']);
        $this->addColumn('increment_id', ['header' => __('#'), 'index' => 'increment_id', 'renderer' => 'BoostMyShop\DropShip\Block\Main\Tab\Renderer\Common\SalesOrder']);
        $this->addColumn('customer_name', ['header' => __('Customer'), 'index' => 'customer_name', 'renderer' => '\BoostMyShop\OrderPreparation\Block\Preparation\Renderer\CustomerName']);
        $this->addColumn('status', ['header' => __('Order Status'), 'index' => 'status']);
        $this->addColumn('products', ['header' => __('Products'), 'index' => 'index_id', 'filter' => false, 'renderer' => '\BoostMyShop\DropShip\Block\Main\Tab\Renderer\ToDropShip\Products', 'filter' => false]);
        $this->addColumn('action', ['header' => __('Action'), 'index' => 'index_id', 'renderer' => '\BoostMyShop\DropShip\Block\Main\Tab\Renderer\ToDropShip\Action', 'filter' => false]);

        return parent::_prepareColumns();
    }

    public function getMainButtonsHtml()
    {
        //nothing, hide buttons
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/ajaxGrid', ['_current' => true, 'grid' => 'todropship']);
    }

    public function getRowClass($item)
    {
        return 'row-todropship-'.$item->getId();
    }

}
