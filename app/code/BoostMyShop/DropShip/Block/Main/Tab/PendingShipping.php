<?php

namespace BoostMyShop\DropShip\Block\Main\Tab;

use Magento\Backend\Block\Widget\Grid\Column;

class PendingShipping extends \Magento\Backend\Block\Widget\Grid\Extended
{
    protected $_coreRegistry = null;
    protected $_collectionFactory;
    protected $_jsonEncoder;
    protected $_supplierList;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Framework\Registry $coreRegistry,
        \BoostMyShop\DropShip\Model\ResourceModel\PendingShippingFactory $collectionFactory,
        \BoostMyShop\Supplier\Model\Source\Supplier $supplierList,
        array $data = []
    ) {
        $this->_jsonEncoder = $jsonEncoder;
        $this->_coreRegistry = $coreRegistry;
        $this->_collectionFactory = $collectionFactory;
        $this->_supplierList = $supplierList;

        parent::__construct($context, $backendHelper, $data);

        $this->setMessageBlockVisibility(false);
    }

    /**
     * Class constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();

        $this->setId('tab_pendingshipping');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('desc');
        $this->setUseAjax(true);
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->_collectionFactory->create();
        $collection->addPendingShippingFilter();

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * @return $this
     */
    protected function _prepareColumns()
    {

        $this->addColumn('po_created_at', ['header' => __('Date'), 'index' => 'po_created_at']);
        $this->addColumn('po_reference', ['header' => __('Purchase order'), 'index' => 'po_reference', 'renderer' => 'BoostMyShop\DropShip\Block\Main\Tab\Renderer\Common\Po']);
        $this->addColumn('po_sup_id', ['header' => __('Supplier'), 'type' => 'options', 'options' => $this->_supplierList->toArray(), 'index' => 'po_sup_id']);
        $this->addColumn('increment_id', ['header' => __('Sales order'), 'index' => 'increment_id', 'renderer' => 'BoostMyShop\DropShip\Block\Main\Tab\Renderer\Common\SalesOrder']);
        $this->addColumn('customer_name', ['header' => __('Customer'), 'index' => 'customer_name']);
        $this->addColumn('product', ['header' => __('Products'), 'index' => 'product', 'renderer' => 'BoostMyShop\DropShip\Block\Main\Tab\Renderer\Common\PoProducts', 'filter' => false]);
        $this->addColumn('misc', ['header' => __('Miscellaneous'), 'index' => 'product', 'renderer' => 'BoostMyShop\DropShip\Block\Main\Tab\Renderer\PendingShipping\Misc', 'filter' => false]);
        $this->addColumn('action', ['header' => __('Action'), 'index' => 'index_id', 'renderer' => '\BoostMyShop\DropShip\Block\Main\Tab\Renderer\PendingShipping\Action', 'filter' => false]);

        return parent::_prepareColumns();
    }

    public function getMainButtonsHtml()
    {
        //nothing, hide buttons
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/ajaxGrid', ['_current' => true, 'grid' => 'pendingshipping']);
    }

    public function getRowClass($item)
    {
        return 'row-dropship-'.$item->getId();
    }

}
