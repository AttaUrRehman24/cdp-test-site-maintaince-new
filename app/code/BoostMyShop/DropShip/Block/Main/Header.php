<?php
namespace BoostMyShop\DropShip\Block\Main;

class Header extends \Magento\Backend\Block\Template
{
    protected $_template = 'Main/Header.phtml';

    protected $_coreRegistry = null;
    protected $_config = null;
    protected $_request;

    public function __construct(\Magento\Backend\Block\Template\Context $context,
                                \Magento\Framework\Registry $registry,
                                \BoostMyShop\DropShip\Model\Config $config,
                                array $data = [],
                                \Magento\Framework\App\Request\Http $request
    )
    {
        parent::__construct($context, $data);
        $this->_config = $config;
        $this->_coreRegistry = $registry;
        $this->_request = $request;
    }

    public function getDropShipUrl()
    {
        return $this->getUrl('*/*/dropShip');
    }

    public function getCancelDropShipUrl()
    {
        return $this->getUrl('*/*/cancelDropShip');
    }

    public function getConfirmPendingUrl()
    {
        return $this->getUrl('*/*/confirmPending');
    }

    public function getConfirmShippingUrl()
    {
        return $this->getUrl('*/*/confirmShipping');
    }

    public function getPriceUrl()
    {
        return $this->getUrl('*/*/price');
    }

    public function getAutoDropLogsUrl()
    {
        return $this->getUrl('*/autodrop/log');
    }

    public function getAutoDropUrl()
    {
        return $this->getUrl('*/autodrop/process');
    }

    public function supplierValidationEnabled()
    {
        return ($this->_config->supplierValidationEnabled() ? "1" : "0");
    }

    public function showConfirmationPopup()
    {
        return ($this->_config->showConfirmationPopup() ? "1" : "0");
    }

}