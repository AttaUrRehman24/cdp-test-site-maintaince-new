<?php
namespace BoostMyShop\DropShip\Block\Supplier\Order\Edit\Tab;

class DropShip extends \Magento\Backend\Block\Template
{
    protected $_template = 'Supplier/Order/Edit/Tab/DropShip.phtml';

    protected $_coreRegistry;
    protected $_orderFactory;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        array $data = [])
    {
        parent::__construct($context, $data);

        $this->_coreRegistry = $coreRegistry;
        $this->_orderFactory = $orderFactory;
    }

    public function getPurchaseOrder()
    {
        $po = $this->_coreRegistry->registry('current_purchase_order');
        return $po;
    }

    public function getSalesOrder()
    {
        return $this->_orderFactory->create()->load($this->getPurchaseOrder()->getpo_dropship_order_id());
    }

    public function getSalesOrderUrl()
    {
        return $this->getUrl('sales/order/view', ['order_id' => $this->getSalesOrder()->getId()]);
    }
}