<?php

namespace BoostMyShop\DropShip\Block\Supplier\ProductSupplier\Renderer;

use Magento\Framework\DataObject;

class DropShip extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{

    public function render(DataObject $row)
    {
        if ($row->getsp_id())
        {
            $name = 'products[' . $row->getsup_id() . '][' . $row->getentity_id() . '][sp_can_dropship]';
            $html = '<select  name="' . $name . '" id="' . $name . '">';
            $html .= '<option value="0" ' . ($row->getsp_can_dropship() ? '' : ' selected ') . '>No</option>';
            $html .= '<option value="1" ' . ($row->getsp_can_dropship() ? ' selected ' : '') . '>Yes</option>';
            $html .= '</select>';
            return $html;
        }
    }

    public function renderExport(DataObject $row)
    {
        return $row->getsp_can_dropship();
    }

}