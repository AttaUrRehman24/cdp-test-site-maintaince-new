<?php

namespace BoostMyShop\DropShip\Block\Supplier\ProductSupplier\Renderer;

use Magento\Framework\DataObject;

class Stock extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{

    public function render(DataObject $row)
    {
        if ($row->getsp_id())
        {
            $name = 'products['.$row->getsup_id().']['.$row->getentity_id().'][sp_stock]';
            return '<input type="text" name="'.$name.'" id="'.$name.'" value="'.$row->getsp_stock().'">';
        }
    }

    public function renderExport(DataObject $row)
    {
        return $row->getsp_stock();
    }

}