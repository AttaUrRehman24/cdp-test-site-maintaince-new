<?php namespace BoostMyShop\DropShip\Block\Supplier\Edit\tabs\StockImport\History\Widget\Grid\Column\Renderer;

/**
 * Class DownloadReport
 *
 * @package   BoostMyShop\DropShip\Block\Supplier\Edit\tabs\StockImport\History\Widget\Grid\Column\Renderer
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class DownloadReport extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer {

    /**
     * @param \Magento\Framework\DataObject $row
     * @return string $html
     */
    public function render(\Magento\Framework\DataObject $row) {

        $html = '<a href="'.$this->getUrl('dropship/supplier_stockimport/download', ['id' => $row->getsi_id()]).'">'.__('Download Report').'</a>';
        return $html;

    }

}