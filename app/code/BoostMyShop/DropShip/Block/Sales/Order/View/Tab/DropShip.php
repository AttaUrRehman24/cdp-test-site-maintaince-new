<?php


namespace BoostMyShop\DropShip\Block\Sales\Order\View\Tab;


class DropShip extends \Magento\Sales\Block\Adminhtml\Order\AbstractOrder implements
    \Magento\Backend\Block\Widget\Tab\TabInterface
{
    protected $_template = 'Sales/Order/Edit/Tab/DropShip.phtml';

    protected $_purchaseOrderCollectionFactory;
    protected $_purchaseOrders = null;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Sales\Helper\Admin $adminHelper,
        \BoostMyShop\Supplier\Model\ResourceModel\Order\CollectionFactory $purchaseOrderCollectionFactory,
        array $data = []
    ) {
        parent::__construct($context, $registry, $adminHelper, $data);

        $this->_purchaseOrderCollectionFactory = $purchaseOrderCollectionFactory;
    }


    /**
     * Retrieve order model instance
     *
     * @return \Magento\Sales\Model\Order
     */
    public function getOrder()
    {
        return $this->_coreRegistry->registry('current_order');
    }

    /**
     * Retrieve source model instance
     *
     * @return \Magento\Sales\Model\Order
     */
    public function getSource()
    {
        return $this->getOrder();
    }

    public function getPurchaseOrders()
    {
        if (!$this->_purchaseOrders)
            $this->_purchaseOrders = $this->_purchaseOrderCollectionFactory->create()->addFieldToFilter('po_dropship_order_id', $this->getOrder()->getId());
        return $this->_purchaseOrders;
    }

    public function hasPurchaseOrders()
    {
        return ($this->getPurchaseOrders()->getSize() > 0);
    }

    public function getPurchaseOrderUrl($item)
    {
        return $this->getUrl('supplier/order/edit', ['po_id' => $item->getId()]);
    }

    /**
     * ######################## TAB settings #################################
     */

    /**
     * {@inheritdoc}
     */
    public function getTabLabel()
    {
        return __('Drop Ship');
    }

    /**
     * {@inheritdoc}
     */
    public function getTabTitle()
    {
        return __('Drop Ship');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return $this->hasPurchaseOrders();
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return !$this->hasPurchaseOrders();
    }
}
