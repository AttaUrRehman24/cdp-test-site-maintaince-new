<?php

namespace BoostMyShop\DropShip\Block\Front\Manage;

use Magento\Framework\Registry;

class Index extends \Magento\Framework\View\Element\Template
{

    protected $_template = 'Manage/Index.phtml';

    protected $_orderCollectionFactory;
    protected $_addressRenderer;
    protected $_config;
    protected $registry;
    protected $_storeManager;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \BoostMyShop\Supplier\Model\Config $config,
        \BoostMyShop\Supplier\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        Registry $registry,
        array $data = []
    ) {
        $this->registry = $registry;
        parent::__construct($context, $data);

        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->_config = $config;
        $this->_storeManager = $storeManager;
    }

    public function getSupplier()
    {
        return $this->registry->registry('current_supplier');
    }

    public function getOrders()
    {
        $collection = $this->_orderCollectionFactory->create();
        $collection->addSupplierFilter($this->getSupplier()->getId());
        $collection->addFieldToFilter('po_type', 'DS');
        $collection->setOrder('po_status', 'desc');
        return $collection;
    }

    public function canAction($order)
    {
        return (in_array($order->getpo_status(), ['to_confirm', 'expected']));
    }

    public function getActions($order)
    {
        $actions = [];

        switch($order->getpo_status())
        {
            case 'to_confirm':
                $actions['accept'] = 'Accept';
                $actions['cancel'] = 'Cancel';
            case 'expected':
                $actions['confirm_shipment'] = 'Confirm shipment';
                break;
        }

        return $actions;
    }

    public function getSaveUrl()
    {
        return $this->getUrl('*/*/save');
    }

    public function getPrintUrl($po)
    {
        return $this->_storeManager->getStore($po->getpo_store_id())->getBaseUrl().'supplier/po/download/po_id/'.$po->getId().'/token/'.$po->getToken();
    }

    public function getParam($paramName)
    {
        return $this->getRequest()->getParam($paramName);
    }

}
