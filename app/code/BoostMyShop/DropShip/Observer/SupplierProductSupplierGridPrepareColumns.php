<?php

namespace BoostMyShop\DropShip\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;

class SupplierProductSupplierGridPrepareColumns implements ObserverInterface
{


    public function execute(EventObserver $observer)
    {
        $grid = $observer->getEvent()->getGrid();

        $grid->addColumn('sp_stock', ['header' => __('Supplier Stock'), 'align' => 'center', 'index' => 'sp_stock', 'renderer' => '\BoostMyShop\DropShip\Block\Supplier\ProductSupplier\Renderer\Stock']);
        $grid->addColumn('sp_can_dropship', ['header' => __('Dropship'), 'align' => 'center', 'index' => 'sp_can_dropship', 'renderer' => '\BoostMyShop\DropShip\Block\Supplier\ProductSupplier\Renderer\DropShip']);

        return $this;
    }

}
