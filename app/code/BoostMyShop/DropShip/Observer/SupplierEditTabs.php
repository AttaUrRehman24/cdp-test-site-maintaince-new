<?php namespace BoostMyShop\DropShip\Observer;

/**
 * Class SupplierEditTabs
 *
 * @package   BoostMyShop\DropShip\Observer
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class SupplierEditTabs implements \Magento\Framework\Event\ObserverInterface
{

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $_urlBuilder;

    /**
     * SupplierEditTabs constructor.
     * @param \Magento\Framework\UrlInterface $urlBuilder
     */
    public function __construct(
        \Magento\Framework\UrlInterface $urlBuilder
    )
    {
        $this->_urlBuilder = $urlBuilder;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this|void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {

        $supplier = $observer->getEvent()->getSupplier();
        $tabs = $observer->getEvent()->getTabs();
        $layout = $observer->getEvent()->getLayout();

        $tabs->addTab(
            'stock_import_settings',
            [
                'label' => __('Stock Import Settings'),
                'content' => $layout->createBlock('BoostMyShop\DropShip\Block\Supplier\Edit\Tabs\StockImport\Settings')->setSupplier($supplier)->toHtml()
            ]
        );

        $tabs->addTab(
            'stock_import_history',
            [
                'label' => __('Stock Import History'),
                'url' => $this->_urlBuilder->getUrl('dropship/supplier_stockimport_history/grid', ['id' => $supplier->getId()]),
                'class' => 'ajax'
            ]
        );

        return $this;

    }

}