<?php

namespace BoostMyShop\DropShip\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;


class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '0.0.2', '<'))  {

            $setup->getConnection()->addColumn(
                $setup->getTable('bms_purchase_order'),
                'po_dropship_order_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'length' => null,
                    'nullable' => true,
                    'comment' => 'Drop ship sales order id'
                ]
            );
        }

        if (version_compare($context->getVersion(), '0.0.3', '<'))  {

            $setup->getConnection()->addColumn(
                $setup->getTable('bms_purchase_order_product'),
                'pop_dropship_order_item_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'length' => null,
                    'nullable' => true,
                    'comment' => 'Drop ship sales order item id'
                ]
            );
        }

        if (version_compare($context->getVersion(), '0.0.4', '<'))  {

            $setup->getConnection()->addColumn(
                $setup->getTable('bms_supplier_product'),
                'sp_stock',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'length' => null,
                    'nullable' => true,
                    'default'   => 0,
                    'comment' => 'Supplier stock'
                ]
            );

            $setup->getConnection()->addColumn(
                $setup->getTable('bms_supplier_product'),
                'sp_can_dropship',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'length' => null,
                    'nullable' => true,
                    'default'   => 0,
                    'comment' => 'Can drop ship'
                ]
            );

        }

        if (version_compare($context->getVersion(), '0.0.5', '<'))  {

            $table = $setup->getConnection()
                ->newTable($setup->getTable('bms_supplier_stock_import'))
                ->addColumn(
                    'si_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'Supplier import ID'
                )
                ->addColumn('si_supplier_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['unsigned' => true, 'nullable' => false], 'Supplier Id')
                ->addColumn('si_date', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, [], 'Date of import')
                ->addColumn('si_status', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 20, [], 'Import status')
                ->addColumn('si_summary', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, null, [], 'Summary')
                ->addIndex($setup->getIdxName('bms_supplier_stock_import', ['si_status']), ['si_status'])
                ->addForeignKey(
                    $setup->getFkName('bms_supplier_stock_import', 'si_supplier_id', $setup->getTable('bms_supplier'), 'sup_id'),
                    'si_supplier_id',
                    $setup->getTable('bms_supplier'),
                    'sup_id',
                    \Magento\Framework\DB\Ddl\Table::ACTION_NO_ACTION
                )
                ->setComment('Supplier Stock Imports');
            $setup->getConnection()->createTable($table);

            $table = $setup->getConnection()
                ->newTable($setup->getTable('bms_supplier_stock_import_details'))
                ->addColumn(
                    'sid_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'Supplier Import Detail ID'
                )
                ->addColumn('sid_import_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['nullable' => false, 'unsigned' => true], 'Associate Import ID')
                ->addColumn('sid_line_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, [], 'Import Line ID')
                ->addColumn('sid_status', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 20, [], 'Import Detail Status')
                ->addIndex($setup->getIdxName('bms_supplier_stock_import_details', ['sid_status']), ['sid_status'])
                ->addForeignKey(
                    $setup->getFkName('bms_supplier_stock_import_details', 'sid_import_id', 'bms_supplier_stock_import', 'si_id'),
                    'sid_import_id',
                    $setup->getTable('bms_supplier_stock_import'),
                    'si_id',
                    \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
                )
                ->setComment('Supplier Stock Import Details');
            $setup->getConnection()->createTable($table);

        }

        if (version_compare($context->getVersion(), '0.0.6', '<'))  {

            $setup->getConnection()->addColumn(
                $setup->getTable('bms_supplier'),
                'sup_ftp_host',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => null,
                    'nullable' => true,
                    'comment' => 'FTP Host'
                ]
            );

            $setup->getConnection()->addColumn(
                $setup->getTable('bms_supplier'),
                'sup_ftp_port',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'length' => null,
                    'nullable' => true,
                    'comment' => 'FTP Port'
                ]
            );

            $setup->getConnection()->addColumn(
                $setup->getTable('bms_supplier'),
                'sup_ftp_login',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => null,
                    'nullable' => true,
                    'comment' => 'FTP Login'
                ]
            );

            $setup->getConnection()->addColumn(
                $setup->getTable('bms_supplier'),
                'sup_ftp_password',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => null,
                    'nullable' => true,
                    'comment' => 'FTP Password'
                ]
            );

            $setup->getConnection()->addColumn(
                $setup->getTable('bms_supplier'),
                'sup_ftp_path',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => null,
                    'nullable' => true,
                    'comment' => 'FTP Path'
                ]
            );

            $setup->getConnection()->addColumn(
                $setup->getTable('bms_supplier'),
                'sup_ftp_passive_mode',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'length' => null,
                    'nullable' => false,
                    'unsigned' => true,
                    'comment' => 'Passive mode'
                ]
            );

        }

        if (version_compare($context->getVersion(), '0.0.7', '<'))  {

            $setup->getConnection()->addColumn(
                $setup->getTable('bms_supplier'),
                'sup_file_import_separator',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => null,
                    'nullable' => true,
                    'comment' => 'File import separator'
                ]
            );

            $setup->getConnection()->addColumn(
                $setup->getTable('bms_supplier'),
                'sup_file_import_enclosure',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => null,
                    'nullable' => true,
                    'comment' => 'File import enclosure'
                ]
            );

            $setup->getConnection()->addColumn(
                $setup->getTable('bms_supplier'),
                'sup_target_warehouse',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'length' => null,
                    'nullable' => true,
                    'unsigned' => true,
                    'comment' => 'Target warehouse'
                ]
            );

            $setup->getConnection()->addColumn(
                $setup->getTable('bms_supplier'),
                'sup_file_import_index_sku',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => null,
                    'nullable' => true,
                    'comment' => 'File import index sku'
                ]
            );

            $setup->getConnection()->addColumn(
                $setup->getTable('bms_supplier'),
                'sup_file_import_index_supplier_sku',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => null,
                    'nullable' => true,
                    'comment' => 'File import index supplier sku'
                ]
            );

            $setup->getConnection()->addColumn(
                $setup->getTable('bms_supplier'),
                'sup_file_import_index_qty',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => null,
                    'nullable' => true,
                    'comment' => 'File import index qty'
                ]
            );

            $setup->getConnection()->addColumn(
                $setup->getTable('bms_supplier'),
                'sup_file_import_index_buying_price',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => null,
                    'nullable' => true,
                    'comment' => 'File import index buying price'
                ]
            );

        }


        if (version_compare($context->getVersion(), '0.0.8', '<'))  {

            $table = $setup->getConnection()
                ->newTable($setup->getTable('bms_dropship_log'))
                ->addColumn(
                    'dsl_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'ID'
                )
                ->addColumn('dsl_created_at', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, ['unsigned' => true, 'nullable' => false], 'Log date')
                ->addColumn('dsl_message', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, null, [], 'Message')
                ->setComment('Autodrop logs');
            $setup->getConnection()->createTable($table);

        }

        $setup->endSetup();
    }

}
