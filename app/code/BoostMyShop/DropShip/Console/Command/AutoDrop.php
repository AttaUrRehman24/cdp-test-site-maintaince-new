<?php namespace BoostMyShop\DropShip\Console\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;


class AutoDrop extends \Symfony\Component\Console\Command\Command {


    protected $_state;

    protected $_autoDrop;


    public function __construct(
        \BoostMyShop\DropShip\Model\AutoDrop $autoDrop,
        \Magento\Framework\App\State $state,
        $name = null
    ){
        parent::__construct($name);
        $this->_state = $state;
        $this->_autoDrop = $autoDrop;
    }

    protected function configure(){
        $this->setName('bms_dropshipping:auto_drop')->setDescription('Automatic drop ship');
    }

    protected function execute(InputInterface $input, OutputInterface $output){


        $output->writeln('START auto drop');

        $this->_state->setAreaCode('adminhtml');

        $this->_autoDrop->process();
        foreach($this->_autoDrop->getDebug() as $debugLine)
        {
            $output->writeln('--> '.$debugLine);
        }


        $output->writeln('END auto drop');

        return $this;

    }

}