<?php namespace BoostMyShop\DropShip\Console\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;

/**
 * Class SupplierStockImport
 *
 * @package   BoostMyShop\DropShip\Console\Command
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class SupplierStockImport extends \Symfony\Component\Console\Command\Command {

    /**
     * @var \Magento\Framework\App\State
     */
    protected $_state;

    /**
     * @var \BoostMyShop\DropShip\Model\SupplierStockImport
     */
    protected $_supplierStockImport;

    /**
     * SupplierStockImport constructor.
     * @param \BoostMyShop\DropShip\Model\SupplierStockImport $supplierStockImport
     * @param \Magento\Framework\App\State $state
     * @param null $name
     */
    public function __construct(
        \BoostMyShop\DropShip\Model\SupplierStockImport $supplierStockImport,
        \Magento\Framework\App\State $state,
        $name = null
    ){
        parent::__construct($name);
        $this->_state = $state;
        $this->_supplierStockImport = $supplierStockImport;
    }

    protected function configure(){

        $options = [
            new InputOption(
                'supplierId',
                null,
                InputOption::VALUE_REQUIRED,
                'Supplier ID'
            )
        ];

        $this->setName('bms_dropshipping:supplier_stock_import')
            ->setDescription('Import supplier catalog')
            ->setDefinition($options);

    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     * @return int
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function execute(InputInterface $input, OutputInterface $output){

        $output->writeln('START Supplier stock import');

        $this->_state->setAreaCode('adminhtml');

        $supplierId = filter_var($input->getOption('supplierId'), FILTER_VALIDATE_INT);

        if(!empty($supplierId)) {

            $output->writeln('process supplier '.$supplierId);

            try {

                $this->_supplierStockImport->run($supplierId);

            } catch(\Exception $e){

                $output->writeln('<error>'.$e->getMessage().'</error>');

            }

        }else{

            $output->writeln('<error>Supplier ID is empty</error>');

        }

        $output->writeln('END supplier import');

        return 0;

    }

}