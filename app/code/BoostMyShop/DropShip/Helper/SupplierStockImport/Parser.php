<?php namespace BoostMyShop\DropShip\Helper\SupplierStockImport;

/**
 * Class Parser
 *
 * @package   BoostMyShop\DropShip\Helper\SupplierStockImport
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Parser {

    /**
     * @param array $line
     * @return array
     */
    public function parseLine($line) {

        return $line;

    }

}