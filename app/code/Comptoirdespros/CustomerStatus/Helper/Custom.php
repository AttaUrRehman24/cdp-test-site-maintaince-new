<?php

namespace Comptoirdespros\CustomerStatus\Helper;

use Magento\Framework\App\Helper\AbstractHelper;


class Custom extends AbstractHelper
{
    /**
     * @var \Magento\Framework\App\Http\Context
     */

    const START_RANGE = 'comptoirdespros_instalment_range/general/instalment_start_range';
    const END_RANGE = 'comptoirdespros_instalment_range/general/instalment_end_range';

    //  private $_objectManager;
    protected $_registry;
    protected $_productFactory;
    protected $_storeManagerInterface;
    protected $_categoryRepository;
    protected $_categoryFactory;
    protected $_bundleProdcutType;
    protected $_budndleOption;
    protected $_scopeConfig;
    protected $_pricehelper;
    protected $_filterProvider;
    protected $_helperData;
    protected $_resourceConnection;
    protected $_stockRegistryInterface;


    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManagerInterface,
        \Magento\Catalog\Model\CategoryRepository $categoryRepository,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Bundle\Model\Product\Type $bundleProdcutType,
        \Magento\Bundle\Model\Option $budndleOption,
        \Magento\Framework\Pricing\Helper\Data $price,
        \Magento\Cms\Model\Template\FilterProvider $filterProvider,
        \Magento\Catalog\Helper\Output $helperData,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistryInterface

    )
    {
        parent::__construct($context);
        $this->_registry = $registry;
        $this->_productFactory = $productFactory;
        $this->_storeManagerInterface = $storeManagerInterface;
        $this->_categoryRepository = $categoryRepository;
        $this->_categoryFactory = $categoryFactory;
        $this->_bundleProdcutType = $bundleProdcutType;
        $this->_budndleOption = $budndleOption;
        $this->_pricehelper = $price;
        $this->_filterProvider = $filterProvider;
        $this->_helperData = $helperData;
        $this->_resourceConnection = $resourceConnection;
        $this->_stockRegistryInterface = $stockRegistryInterface;
    }

    public function getChildCategories($parentCatId = null, $categoryTitle = null)
    {

        if ($categoryTitle) {
            $collection = $this->_categoryFactory->create()->getCollection()->addAttributeToFilter('name', $categoryTitle)->setPageSize(1);
            if ($collection->getSize()) {
                $parentCatId = $collection->getFirstItem()->getId();
            }
        }

        $childrenCategory = [];

        $parentcategories = $this->_categoryRepository->get($parentCatId);
        $childrenCategories = $parentcategories->getChildrenCategories();
        foreach ($childrenCategories as $childrenCategorie) {
            $childrenCategory[] = $childrenCategorie->getId();
        }

        return $childrenCategory;

    }

    public function getMediaUrl()
    {
        return $this->getStoreData()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }

    public function getStoreData()
    {
        return $this->_storeManagerInterface->getStore();
    }

    public function getUpSellProducts()
    {

        $upSellProductsData = [];
        $product = $this->getCurrentProduct();
        $upSellProducts = $product->getUpSellProducts();
        if (!empty($upSellProducts)) {

            foreach ($upSellProducts as $upSellProduct) {
                if ($upSellProduct->getTypeId() == 'bundle') {
                    $data = [];
                    $attributeSetData = [];
                    $data['id'] = $upSellProduct->getId();
                    $data['is_salable'] = $upSellProduct->getIsSalable();
                    $data['featured'] = $this->getProductDetail($upSellProduct->getId())->getData('featured_product_bundles');
                    $selections = $this->_bundleProdcutType->getSelectionsCollection($this->_bundleProdcutType->getOptionsIds($upSellProduct), $upSellProduct);
                    foreach ($selections as $selection) {
                        $attributeSetData[] = $selection->getData();
                    }
                    $data['attributeSetData'] = $attributeSetData;
                    $upSellProductsData[$upSellProduct->getId()] = $data;

                }
            }

        }


        usort($upSellProductsData, [$this, 'sortByFeatured']);
        return $upSellProductsData;
    }

    public function getCurrentProduct()
    {
        return $this->_registry->registry('current_product');
    }

    public function getProductDetail($product_id)
    {
        return $this->_productFactory->create()->load($product_id);
    }

    public function getOptionsItem($selectionsId)
    {

        $_item = $this->getProductDetail($selectionsId);
        $store_id = $this->getStoreData()->getId();
        $options = $this->_budndleOption->getResourceCollection()->setProductIdFilter($_item->getId())->setPositionOrder();
        $options->joinValues($store_id);
        $_selections = $_item->getTypeInstance(true)->getSelectionsCollection($_item->getTypeInstance(true)->getOptionsIds($_item), $_item);
        return ['options' => $options, 'selections' => $_selections, 'product_item' => $_item];

    }

    public function checkInstalmentRange($value)
    {
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $start_range = $this->scopeConfig->getValue(self::START_RANGE, $storeScope);
        $end_range = $this->scopeConfig->getValue(self::END_RANGE, $storeScope);
        if ($start_range < $value && $value < $end_range) {
            return true;
        } else {
            return false;
        }
    }

    public function currencyFormate($number, $decimal = 2)
    {
        return $this->_pricehelper->currency($number, true, false);
    }

    public function sortByFeatured($a, $b)
    {
        return $b['featured'] - $a['featured'];
    }

    public function categoryDescription($block, $_description)
    {
        $store_id = $this->getStoreData()->getId();

        return $this->_filterProvider->getBlockFilter()
            ->setStoreId($store_id)
            ->filter(
                $this->_helperData->categoryAttribute($block->getCurrentCategory(), $_description, 'description')
            );
    }

    public function subDepartmentCategories()
    {
        return $this->departmentCategories();
    }

    public function departmentCategories()
    {
        $_category = $this->getCurrentCategory();
        $collection = $this->_categoryFactory->create()->getCollection()->addAttributeToSelect('*')
            ->addAttributeToFilter('is_active', 1)
            ->setOrder('position', 'ASC')
            ->addIdFilter($_category->getChildren());
        return $collection;

    }

    public function getCurrentCategory()
    {
        return $this->_registry->registry('current_category');
    }

    public function departmentBestSellingProducts()
    {
        $categoryId = $this->getCurrentCategory()->getId();
        $connection = $this->_resourceConnection->getConnection();
        $tableName = $this->_resourceConnection->getTableName('catalog_department_relation');
        $query = $connection->select()
            ->from($tableName)
            ->where('department_category_id = ?', $categoryId)
            ->where('created_at > ?', new \Zend_Db_Expr("DATE_SUB(NOW(), INTERVAL 24 HOUR)"))
            ->where('created_at <= ?', new \Zend_Db_Expr('NOW()'))
            ->order('id ASC')
            ->limit(8);

        return $connection->fetchAll($query);
    }

    public function subDepartmentBestSellingProducts()
    {
        $categoryId = $this->getCurrentCategory()->getId();
        $connection = $this->_resourceConnection->getConnection();
        $tableName = $this->_resourceConnection->getTableName('catalog_subdepartment_relation');
        $query = $connection->select()
            ->from($tableName)
            ->where('subdepartment_category_id = ?', $categoryId)
            ->where('created_at > ?', new \Zend_Db_Expr("DATE_SUB(NOW(), INTERVAL 24 HOUR)"))
            ->where('created_at <= ?', new \Zend_Db_Expr('NOW()'))
            ->order('id ASC')
            ->limit(8);

        return $connection->fetchAll($query);
    }

    public function getProductQty($product)
    {
        $stockitem = $this->_stockRegistryInterface->getStockItem(
            $product->getId(),
            $product->getStore()->getWebsiteId()
        );
        return $stockitem->getQty();
    }
}