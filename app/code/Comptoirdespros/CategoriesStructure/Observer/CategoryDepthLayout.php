<?php

namespace Comptoirdespros\CategoriesStructure\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Registry;

class CategoryDepthLayout implements ObserverInterface
{
    const ACTION_NAME = 'catalog_category_view';

    public $handlerName;

    /** @var Registry */
    private $registry;

    public function __construct(
        Registry $registry
    )
    {
        $this->registry = $registry;
    }

    public function execute(Observer $observer)
    {
        if ($observer->getFullActionName() !== self::ACTION_NAME) {
            return;
        }

        $category = $this->registry->registry('current_category');
        $catLevel = $category->getLevel();
        $catParentId = $category->getParentId();


        if ($catParentId == 122) {
            $handlerName = 'brand_categories';
        } elseif ($catLevel == 2 || $catLevel == 3) {
            $handlerName = 'department_categories';
        } /*elseif ($catLevel == 4) {
            $handlerName = 'sub_department_categories';
        }*/

        /** @var \Magento\Framework\View\Layout $layout */

        if (!empty($handlerName)) {
            $layout = $observer->getLayout();
            $layout->getUpdate()->addHandle($handlerName);
        }
    }
}