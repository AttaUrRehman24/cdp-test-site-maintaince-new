<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Flags
 */


namespace Amasty\Flags\Block\Adminhtml\Column\Edit\Tab;

use Amasty\Flags\Model\Column;
use Amasty\Flags\Model\Flag;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;

class Apply extends Generic implements TabInterface
{
    /**
     * @var \Amasty\Flags\Model\ResourceModel\Flag\CollectionFactory
     */
    private $flagCollectionFactory;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Sales\Model\Order\Config $orderConfig,
        \Magento\Shipping\Model\Config\Source\Allmethods $shippingConfig,
        \Amasty\Flags\Model\ResourceModel\Flag\CollectionFactory $flagCollectionFactory,
        array $data = []
    ) {
        parent::__construct($context, $registry, $formFactory, $data);
        $this->flagCollectionFactory = $flagCollectionFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function getTabLabel()
    {
        return __('Apply Flags');
    }

    /**
     * {@inheritdoc}
     */
    public function getTabTitle()
    {
        return __('Apply Flags');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    protected function _prepareForm()
    {
        /** @var Column $model */
        $model = $this->_coreRegistry->registry('amflags_column');

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('column_');

        $fieldset = $form->addFieldset('apply_fieldset', [
            'legend' => __('Apply Flags To Column')
        ]);

        /** @var \Amasty\Flags\Model\ResourceModel\Flag\Collection $flags */
        $flags = $this->flagCollectionFactory->create();

        $values = [];
        /** @var Flag $flag */
        foreach ($flags as $flag) {
            $values[] = [
                'value' => $flag->getId(),
                'label' => $flag->getName(),
                'style' => "background-image:url({$flag->getImageUrl()})"
            ];
        }

        $fieldset->addField('apply_flag', 'multiselect', [
            'name'      => 'apply_flag',
            'label'     => __('Flags'),
            'title'     => __('Flags'),
            'values'    => $values,
            'note'      => __('Set flags to column'),
        ]);

        $model->setData('apply_flag', $model->getAppliedFlagIds());

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
