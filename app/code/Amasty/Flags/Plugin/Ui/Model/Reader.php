<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Flags
 */


namespace Amasty\Flags\Plugin\Ui\Model;

class Reader extends AbstractReader
{
    /**
     * Added mass actions on Magento 2.2.x
     *
     * @param \Magento\Ui\Config\Reader $subject
     * @param array $result
     * @return array
     */
    public function afterRead(
        \Magento\Ui\Config\Reader $subject,
        $result
    ) {
        if (!isset($result['children']['listing_top']['children']['listing_massaction']['children'])) {
            return $result;
        }

        /** @var \Amasty\Flags\Model\ResourceModel\Column\Collection $columns */
        $columns = $this->columnCollectionFactory->create();

        /** @var \Amasty\Flags\Model\ResourceModel\Flag\Collection $columns */
        $flags = $this->flagCollectionFactory->create();

        $children = &$result['children']['listing_top']['children']['listing_massaction']['children'];

        /** @var Column $column */
        foreach ($columns as $column) {
            $actionKey = 'amflags_assign_' . $column->getId();

            if (!array_key_exists($actionKey, $children)) {
                $applicableFlags = [];

                foreach ($column->getAppliedFlagIds() as $flagId) {
                    $flag = $flags->getItemById($flagId);
                    if ($flag) {
                        $applicableFlags[] = $flag;
                    }
                }

                if (!empty($applicableFlags)) {
                    $children[$actionKey] = $this->addAssignMenu($column, $applicableFlags);
                }
            }
        }

        if (!isset($children['amflags_unset'])) {
            $children['amflags_unset'] = $this->addUnassignMenu($columns);
        }

        return $result;
    }
}