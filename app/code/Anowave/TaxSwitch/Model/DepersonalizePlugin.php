<?php
/**
 * Anowave Magento 2 Price Per Customer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Anowave license that is
 * available through the world-wide-web at this URL:
 * http://www.anowave.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category 	Anowave
 * @package 	Anowave_Price
 * @copyright 	Copyright (c) 2016 Anowave (http://www.anowave.com/)
 * @license  	http://www.anowave.com/license-agreement/
 */

namespace Anowave\TaxSwitch\Model;

use Magento\PageCache\Model\DepersonalizeChecker;
use Magento\Framework\Registry;

/**
 * Google Analytics module observer
 *
 */
class DepersonalizePlugin
{
	/**
	 * @var DepersonalizeChecker
	 */
	protected $depersonalizeChecker;
	
	/**
	 * @var \Magento\Framework\Session\SessionManagerInterface
	 */
	protected $session;
	
	/**
	 * @var \Magento\Framework\Registry
	 */
	protected $registry;

	/**
	 * @param DepersonalizeChecker $depersonalizeChecker
	 * @param \Magento\Framework\Session\SessionManagerInterface $session
	 * @param \Magento\Customer\Model\Session $customerSession
	 * @param \Magento\Customer\Model\CustomerFactory $customerFactory
	 * @param \Magento\Customer\Model\Visitor $visitor
	 */
	public function __construct
	(
		DepersonalizeChecker $depersonalizeChecker,
		\Magento\Framework\Session\SessionManagerInterface $session,
		\Magento\Framework\Registry $registry
	) 
	{
		$this->depersonalizeChecker = $depersonalizeChecker;
		
		$this->session 	= $session;
		$this->registry	= $registry;
	}
	
	/**
	 * After generate Xml
	 *
	 * @param \Magento\Framework\View\LayoutInterface $subject
	 * @param \Magento\Framework\View\LayoutInterface $result
	 * @return \Magento\Framework\View\LayoutInterface
	 */
	public function afterGenerateXml(\Magento\Framework\View\LayoutInterface $subject, $result)
	{
		if (is_null($this->registry->registry('tax_display')) && 0 < (int) $this->session->getTaxDisplay())
		{
			$this->registry->register('tax_display', (int) $this->session->getTaxDisplay());
		}
		
		return $result;
	}
}