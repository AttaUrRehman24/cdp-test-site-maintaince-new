<?php
/**
 * Anowave Magento 2 Tax Switcher
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Anowave license that is
 * available through the world-wide-web at this URL:
 * http://www.anowave.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category 	Anowave
 * @package 	Anowave_TaxSwitch
 * @copyright 	Copyright (c) 2016 Anowave (http://www.anowave.com/)
 * @license  	http://www.anowave.com/license-agreement/
 */
 
namespace Anowave\TaxSwitch\Plugin;

use Magento\Framework\Registry;

class Tax
{
	const CLEAR_CACHE_ON_SWITCH = false;
	
	/**
	 * @var \Anowave\TaxSwitch\Model\Api
	 */
	protected $api = null;
	
	/**
	 * @var \Magento\Framework\Registry
	 */
	protected $registry = null;
	
	/** 
	 * @var \Magento\Framework\App\RequestInterface
	 */
	protected $request = null;
	
	/**
	 * @var \Magento\Catalog\Model\Session
	 */
	protected $session = null;
	
	/**
	 * @var \Anowave\TaxSwitch\Helper\Data
	 */
	protected $helper = null;
	
	/**
	 * @var \Magento\Tax\Model\Calculation\RateFactory
	 */
	protected $rates = null;
	
	/**
	 * @var \Magento\Framework\App\State
	 */
	protected $state = null;
	
	/**
	 * @var \Magento\Framework\App\Cache\TypeListInterface
	 */
	protected $cacheTypeList = null;
	
	/**
	 * @var \Magento\Framework\App\Cache\Frontend\Pool
	 */
	protected $cacheFrontendPool = null;
	
	/**
	 * Constructor 
	 * 
	 * @param \Magento\Framework\Registry $registry
	 * @param \Magento\Framework\View\Element\Context $context
	 * @param \Anowave\TaxSwitch\Helper\Data $helper
	 * @param \Magento\Tax\Model\Calculation\RateFactory $rates
	 * @param \Magento\Framework\App\State $state
	 * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
	 * @param \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
	 * @param array $data
	 */
	public function __construct
	(
		\Magento\Framework\Registry $registry,
		\Magento\Framework\View\Element\Context $context,
		\Anowave\TaxSwitch\Helper\Data $helper,
		\Magento\Tax\Model\Calculation\RateFactory $rates,
		\Magento\Framework\App\State $state,
		\Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
		\Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
		array $data = []
	)
	{
		$this->registry 			= $registry;
		$this->request 				= $context->getRequest();
		$this->session  			= $context->getSession();
		$this->helper				= $helper;
		$this->rates 				= $rates;
		$this->state				= $state;
		$this->cacheTypeList 		= $cacheTypeList;
		$this->cacheFrontendPool 	= $cacheFrontendPool;
	}
	
	/**
	 * Dynamic tax display
	 *
	 * @param \Magento\Tax\Model\Config $config
	 * @param unknown $type
	 */
	public function afterGetPriceDisplayType(\Magento\Tax\Model\Config $config, $type)
	{
		if ($this->state->getAreaCode() === \Magento\Framework\App\Area::AREA_ADMINHTML)
		{
			return $type;
		}
		
		try
		{
			$this->registry->unregister('tax_display');
		}
		catch (\Exception $e){}
	
		/**
		 * Get tax display
		 */
		if ($this->request->getParam('tax_display'))
		{
			/**
			 * Clear cache
			 */
			if (self::CLEAR_CACHE_ON_SWITCH)
			{
				$this->clearCache();
			}

			/**
			 * Get display type
			 *
			 * @var int
			*/
			$display = (int) $this->request->getParam('tax_display');

			switch($display)
			{
				case \Magento\Tax\Model\Config::DISPLAY_TYPE_INCLUDING_TAX:
				case \Magento\Tax\Model\Config::DISPLAY_TYPE_EXCLUDING_TAX:
				case \Magento\Tax\Model\Config::DISPLAY_TYPE_BOTH:
					$type = $display;
					break;
			}

			$this->session->setTaxDisplay($type);
			
			return $type;
		}
		else
		{
			$tax_display = (int) $this->session->getTaxDisplay();
			
			if ($tax_display)
			{
				return $tax_display;
			}
			else
			{
				if ($this->helper->usePrecisionServices())
				{
					if (!($insights = $this->session->getInsights()))
					{
						$insights = $this->getApi()->getInsights();

						/**
						 * Set insights
						*/
						$this->session->setInsights($insights);
					}

					if ($insights)
					{
						if (property_exists($insights, 'registered_country'))
						{
							/**
							 * Get country name
							 *
							 * @var string
							 */
							$country = $insights->registered_country->names->en;

							/**
							 * Get country ISO code
							 *
							 * @var string
							 */
							$iso = $insights->registered_country->iso_code;

							$rates = [];

							foreach ($this->rates->create()->getCollection()->addFieldToFilter('tax_country_id', $iso) as $rate)
							{
								if ((float) $rate->getRate() > 0)
								{
									$this->session->setTaxDisplay(\Magento\Tax\Model\Config::DISPLAY_TYPE_BOTH);
									
									return \Magento\Tax\Model\Config::DISPLAY_TYPE_BOTH;
								}
							}

							$this->session->setTaxDisplay(\Magento\Tax\Model\Config::DISPLAY_TYPE_INCLUDING_TAX);
							
							/**
							 * Display including tax for all countries without tax
							 */
							return \Magento\Tax\Model\Config::DISPLAY_TYPE_INCLUDING_TAX;
						}
					}
				}
			}
		}
	
		return $type;
	}
	
	/**
	 * Clear ALL cache
	 * 
	 * @deprecated
	 */
	protected function clearCache()
	{
		$types = array
		(
			'config',
			'layout',
			'block_html',
			'collections',
			'reflection',
			'db_ddl',
			'eav',
			'config_integration',
			'config_integration_api',
			'full_page',
			'translate',
			'config_webservice'
		);
	
		foreach ($types as $type)
		{
			$this->cacheTypeList->cleanType($type);
		}
	
		foreach ($this->cacheFrontendPool as $cacheFrontend)
		{
			$cacheFrontend->getBackend()->clean();
		}
	
		return $this;
	}
	
	/**
	 * Get API
	 */
	protected function getApi()
	{
		if (!$this->api)
		{
			$this->api = new \Anowave\TaxSwitch\Model\Api($this->helper);
		}

		return $this->api;
	}
}