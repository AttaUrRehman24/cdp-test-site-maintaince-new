# Changelog

All notable changes to this project will be documented in this file.

##[3.0.7]

### Added

- Added session state for pages without products to keep tax switcher selected

### Removed

- Removed hard cache flush on ?tax_display parameter

##[2.0.2]

### Added

- Added cacheable="false" in app/code/Anowave/TaxSwitch/view/frontend/layout/default.xml to mitigate cached display and switcher not changing it's state after switch.

## [2.0.1]

### Added

- First release