<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magento\Sales\Controller\Adminhtml\Order;

class Hold extends \Magento\Sales\Controller\Adminhtml\Order
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Magento_Sales::hold';

    /**
     * Hold order
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {

       
        $resultRedirect = $this->resultRedirectFactory->create();
        if (!$this->isValidPostRequest()) {
            $this->messageManager->addError(__('You have not put the order on hold.'));
            return $resultRedirect->setPath('sales/*/');
        }
        $order = $this->_initOrder();

       
        if ($order) {
            try {
                $this->orderManagement->hold($order->getEntityId());
                $this->failed($order->getEntityId());
                $this->messageManager->addSuccess(__('You put the order on hold.'));
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addError(__('You have not put the order on hold.'));
            }
            $resultRedirect->setPath('sales/order/view', ['order_id' => $order->getId()]);
           
            return $resultRedirect;
        }
        $resultRedirect->setPath('sales/*/');
        return $resultRedirect;
    }


   private function failed($Id){
        
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();       
            $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();
            $tableName = $resource->getTableName('sales_order'); //gives table name with 
            $sales_order_status_history = $resource->getTableName('sales_order_status_history'); //gives table name with 

            $sql = "Update `" . $tableName . "` Set payment_failed=1  where entity_id =".$Id;
            $connection->query($sql);    
            
            // $sales_order_status_history_sql ="Insert into `".$sales_order_status_history."` (parent_id,is_customer_notified,comment,status,entity_name) values('".$Id."','1','Holded cron updated','holded','order')"; 
            //  $connection->query($sales_order_status_history_sql);
    }

}
