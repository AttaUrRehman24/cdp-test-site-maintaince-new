<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Chronopost\Chronorelais\Ui\Component\Listing\Column;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

/**
 * Class ViewAction
 */
class PrintAction extends Column
{
    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * Constructor
     *
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {

                if (isset($item["shipment_id"])) {
                    $viewUrlPath = $this->getData('config/viewUrlPath') ?: '#';

                    $urlEntityParamName = $item["shipment_id"] == '--' ? 'order_id' : 'shipment_increment_id';

                    if($item["shipment_id"] == '--') {
                        $indexFieldValues = array($item['entity_id']);
                    } else {
                        $indexFieldValues = explode(',',$item['shipment_id']);
                    }

                    $item[$this->getData('name')] = '';
                    foreach($indexFieldValues as $indexFieldValue) {

                        $url = $this->urlBuilder->getUrl(
                            $viewUrlPath,
                            [
                                $urlEntityParamName => trim($indexFieldValue)
                            ]
                        );

                        if(count($indexFieldValues) === 1) {
                            $item[$this->getData('name')] = '<a href="'.$url.'">'.__('Shipment').'</a>';
                        } else {
                            $item[$this->getData('name')] .= '<a href="'.$url.'">'.__('Shipment').' '.trim($indexFieldValue).'</a><br />';
                        }

                    }
                }
            }
        }

        return $dataSource;
    }
}
