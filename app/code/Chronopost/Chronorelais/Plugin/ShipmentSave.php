<?php

namespace Chronopost\Chronorelais\Plugin;

use Chronopost\Chronorelais\Helper\Data as HelperData;
class ShipmentSave
{
    protected $_helperShipment;

    protected $_scopeConfig;

    protected $_orderFactory;

    /**
     * @var HelperData
     */
    protected $_helperData;

    public function __construct(
        \Chronopost\Chronorelais\Helper\Shipment $helperShipment,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        HelperData $helperData
    )
    {
        $this->_helperShipment = $helperShipment;
        $this->_scopeConfig = $scopeConfig;
        $this->_orderFactory = $orderFactory;
        $this->_helperData = $helperData;
    }

    /**
     * @param \Magento\Sales\Model\Order\Shipment $subject
     * @return \Magento\Sales\Model\Order\Shipment
     */
    public function afterSave(\Magento\Sales\Model\Order\Shipment $subject) {
        if(!$subject->getData('create_track_to_shipment')) { /* pour eviter multiple creation etiquette */
            return $subject;
        }
        $order = $subject->getOrder();
        $trackData = $subject->getTrackData() ? $subject->getTrackData() : array();
        $subject = $subject->loadByIncrementId($subject->getIncrementId()); /* reload pour etre sur de récup les tracks */
        $shippingMethod = $order->getData('shipping_method');
        $shippingMethodCode = explode("_", $shippingMethod);
        $shippingMethodCode = isset($shippingMethodCode[1]) ? $shippingMethodCode[1] : $shippingMethodCode[0];
        if($this->_helperData->isChronoMethod($shippingMethodCode)) { /* methode chronopost */
            $trackExist = false;
            $tracks = $subject->getAllTracks();
            if(count($tracks)) {
                foreach($tracks as $track) {
                    if($track->getData('chrono_reservation_number')) {
                        $trackExist = true;
                        break;
                    }
                }
            }
            /* commande chronopost sans track : on le crée */
            if(!$trackExist) {
                $this->_helperShipment->createTrackToShipment($subject,$trackData);
            }
        }
        $subject->setData('create_track_to_shipment',false);
        return $subject;
    }

    /*
     * à la sauvegarde d'une expédition : verif si creation de plusieurs expédition au lieu d'une
     */
    public function aroundSave(\Magento\Sales\Model\Order\Shipment $subject, callable $proceed) {

        $canSaveObject = true;

        $order = $subject->getOrder();
        $shippingMethod = $order->getData('shipping_method');
        $shippingMethodCode = explode("_", $shippingMethod);
        $shippingMethodCode = isset($shippingMethodCode[1]) ? $shippingMethodCode[1] : $shippingMethodCode[0];
        if($this->_helperData->isChronoMethod($shippingMethodCode)) { /* methode chronopost */
            $items = $subject->getItems();

            $shippingMethod = explode("_", $shippingMethod);
            $shippingMethod = $shippingMethod[1];
            $weight_limit = $this->_scopeConfig->getValue('carriers/'.$shippingMethod.'/weight_limit');
            $weightShipping = 0;
            foreach($items as $item) {
                $weightShipping += $item->getWeight()*$item->getQty();
            }
            if($this->_scopeConfig->getValue('chronorelais/weightunit/unit') == 'g')
            {
                $weightShipping = $weightShipping / 1000; // conversion g => kg
            }
            if($weightShipping > $weight_limit) {

                /* Create one shipment by product ordered */
                $canSaveObject = false;
                $order = $this->_orderFactory->create()->loadByIncrementId($order->getIncrementId());
                foreach($items as $item) {
                    for($i = 1; $i <= $item->getQty(); $i++) {
                        try {
                            $this->_helperShipment->createNewShipment($order,array($item->getOrderItemId() => '1'));
                        } catch (\Exception $e) {
                            //echo "erreur createNewShipment : ".$e->getMessage()."<br>";
                        }

                    }
                }
                /* return first shipment */
                $shipments = $order->getShipmentsCollection();
                if($shipments) {
                    $shipment = $shipments->getLastItem();
                    return $shipment;
                }

            }
        }

        if($canSaveObject) {
            $result = $proceed();
            return $result;
        }
    }
}