<?php

namespace Chronopost\Chronorelais\Block\Adminhtml\Sales\Import\Edit;

class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    /**
     * Form constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        array $data = []
    ) {

        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form before rendering HTML
     *
     * @return $this
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post']]
        );

        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __(''), 'class' => 'fieldset-wide']
        );

        $fieldset->addField(
            'import_chronorelais_tracking_title',
            'text',
            [
                'name' => 'import_chronorelais_tracking_title',
                'label' => __('Tracking title'),
                'title' => __('Tracking title'),
                'required' => true,
                'value' => __("Chronopost - Chronopost express home deliveries")
            ]
        );

        $fieldset->addField(
            'import_chronorelais_file',
            'file',
            [
                'name' => 'import_chronorelais_file',
                'label' => __('Import file'),
                'title' => __('Import file'),
                'required' => true,
                'note' => __('Line format: Order_id,tracking number')
            ]
        );

        $form->setAction($this->getUrl('*/*/import_save'));
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}