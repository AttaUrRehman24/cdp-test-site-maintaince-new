<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Chronopost\Chronorelais\Model\ResourceModel\Order\Grid\Bordereau;

/**
 * Order grid collection
 */
class Collection extends \Chronopost\Chronorelais\Model\ResourceModel\Order\Grid\Collection
{
    /**
     * {@inheritdoc}
     */
    protected function _initSelect()
    {
        parent::_initSelect();
        $this->getSelect()->where($this->getTable('sales_order') . ".status = 'processing'");
        return $this;
    }
}
