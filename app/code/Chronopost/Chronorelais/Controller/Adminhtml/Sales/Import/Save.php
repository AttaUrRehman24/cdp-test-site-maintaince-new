<?php
namespace Chronopost\Chronorelais\Controller\Adminhtml\Sales\Import;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Chronopost\Chronorelais\Helper\Data as HelperData;
use Chronopost\Chronorelais\Helper\Shipment as HelperShipment;

use Magento\Framework\File\Csv;
use Magento\Sales\Model\OrderFactory;
use Magento\Framework\HTTP\PhpEnvironment\Request as RequestPhp;

class Save extends \Magento\Backend\App\Action
{

    /**
     * @var HelperData
     */
    protected $_helperData;

    /**
     * @var HelperShipment
     */
    protected $_helperShipment;

    /**
     * @var Csv
     */
    protected $_fileCsv;

    /**
     * @var OrderFactory
     */
    protected $_orderFactory;

    /**
     * @var RequestPhp
     */
    protected $_requestPhp;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        HelperShipment $helperShipment,
        HelperData $helperData,
        Csv $csv,
        OrderFactory $orderFactory,
        RequestPhp $requestPhp
    ) {
        parent::__construct($context);
        $this->_helperData = $helperData;
        $this->_helperShipment = $helperShipment;
        $this->_fileCsv = $csv;
        $this->_orderFactory = $orderFactory;
        $this->_requestPhp = $requestPhp;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Chronopost_Chronorelais::sales');
    }

    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();

        try {
            $chronoFile = $this->_requestPhp->getFiles('import_chronorelais_file');
            if ($this->getRequest()->getParams() && $chronoFile && !empty($chronoFile['tmp_name'])) {
                $trackingTitle = $this->_requestPhp->getPost('import_chronorelais_tracking_title');
                if(!$trackingTitle) {
                    Throw new \Exception(__("Please enter a title for the tracking"));
                }
                $this->_importChronorelaisFile($chronoFile['tmp_name'], $trackingTitle);
                $this->messageManager->addSuccessMessage(__("The parcels have been imported"));
            } else {
                Throw new \Exception(__("Please select a file"));
            }

        } catch(\Exception $e) {
            $this->messageManager->addErrorMessage(__($e->getMessage()));
        }

        $resultRedirect->setPath("chronopost_chronorelais/sales/import");
        return $resultRedirect;
    }

    /**
     * @param $fileName
     * @param $trackingTitle
     */
    protected function _importChronorelaisFile($fileName, $trackingTitle)
    {
        /**
         * File handling
         **/
        ini_set('auto_detect_line_endings', true);
        $csvData = $this->_fileCsv->getData($fileName);

        /**
         * File expected fields
         */
        $expectedCsvFields  = array(
            0   => __('Order Id'),
            1   => __('Tracking Number')
        );

        /**
         * Get configuration
         */
        $sendEmail = $this->_helperData->getConfig("chronorelais/import/send_mail");
        $comment = $this->_helperData->getConfig("chronorelais/import/shipping_comment");
        $includeComment = $this->_helperData->getConfig("chronorelais/import/include_comment");

        /**
         * $k is line number
         * $v is line content array
         */
        foreach ($csvData as $k => $v) {

            /**
             * End of file has more than one empty lines
             */
            if (count($v) <= 1 && !strlen($v[0])) {
                continue;
            }

            /**
             * Check that the number of fields is not lower than expected
             */
            if (count($v) < count($expectedCsvFields)) {
                $this->messageManager->addErrorMessage(__('The line format %1 is invalid', $k));
                continue;
            }

            /**
             * Get fields content
             */
            $orderIncrementId = $v[0];
            $trackingNumber = $v[1];

            /**
             * Try to load the order
             */
            $order = $this->_orderFactory->create()->loadByIncrementId($orderIncrementId);
            if (!$order->getId()) {
                $this->messageManager->addErrorMessage(__("The order %1 does not exist", $orderIncrementId));
                continue;
            }

            /**
             * Try to create a shipment
             */
            try {

                $_shippingMethod = explode("_",$order->getShippingMethod());
                if(!$this->_helperData->isChronoMethod($_shippingMethod[1])) { /* methode chronopost */
                    $carrier_code = $_shippingMethod[1];
                    $popup = 1;
                } else {
                    $carrier_code = 'custom';
                    $popup = 0;
                }

                $trackData = array(
                    'track_number' => $trackingNumber,
                    'carrier' => ucwords($carrier_code),
                    'carrier_code' => $carrier_code,
                    'title' => $trackingTitle,
                    'popup' => $popup,
                    'send_mail' => $sendEmail,
                    'comment' => $comment,
                    'include_comment' => $includeComment
                );

                $this->_helperShipment->createNewShipment($order,array(),$trackData);
                $this->messageManager->addSuccessMessage(__("The shipment with the number %1 has been created for order %2", $trackingNumber, $orderIncrementId));
            } catch(\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            }

        }//foreach
    }

}