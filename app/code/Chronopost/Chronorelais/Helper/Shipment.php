<?php
namespace Chronopost\Chronorelais\Helper;

use Magento\Sales\Model\Order\Shipment\TrackFactory;

use \Magento\Sales\Model\Convert\Order as ConvertOrder;
use \Magento\Shipping\Model\ShipmentNotifier;
use \Magento\Sales\Model\Order\Shipment as OrderShipment;
/**
 * gestion des expeditions
 * Class Shipment
 * @package Chronopost\Chronorelais\Helper
 */
class Shipment extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var TrackFactory
     */
    protected $_trackFactory;

    /**
     * @var ConvertOrder
     */
    protected $_convertOrder;

    /**
     * @var ShipmentNotifier
     */
    protected $_shipmentNotifier;

    /**
     * @var Webservice
     */
    protected $_helperWebservice;

    /**
     * @var OrderShipment
     */
    protected $_shipment;

    /**
     * Shipment constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param TrackFactory $trackFactory
     * @param ConvertOrder $convertOrder
     * @param ShipmentNotifier $shipmentNotifier
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        TrackFactory $trackFactory,
        ConvertOrder $convertOrder,
        ShipmentNotifier $shipmentNotifier,
        Webservice $webservice,
        OrderShipment $shipment
    ) {
        parent::__construct($context);
        $this->_trackFactory = $trackFactory;

        $this->_convertOrder = $convertOrder;
        $this->_shipmentNotifier = $shipmentNotifier;
        $this->_helperWebservice = $webservice;
        $this->_shipment = $shipment;
    }

    /**
     * Creation expedition + etiquette
     * @param \Magento\Sales\Model\Order $order
     * @param array $savedQtys
     * @param array $trackData
     * @return bool|mixed|string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function createNewShipment(\Magento\Sales\Model\Order $order,$savedQtys = array(), $trackData = array()) {

        if (!$order->canShip()) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __("You can't create a shipment.")
            );
        }
        $shipment = $this->_convertOrder->toShipment($order);
        foreach ($order->getAllItems() AS $orderItem) {

            if (! $orderItem->getQtyToShip() || $orderItem->getIsVirtual()) {
                continue;
            }

            if (isset($savedQtys[$orderItem->getId()])) {
                $qtyShipped = min($savedQtys[$orderItem->getId()], $orderItem->getQtyToShip());
            } elseif (!count($savedQtys)) {
                $qtyShipped = $orderItem->getQtyToShip();
            } else {
                continue;
            }

            $shipmentItem = $this->_convertOrder->itemToShipmentItem($orderItem)->setQty($qtyShipped);
            $shipment->addItem($shipmentItem);
        }

        /* cas d'import de tracking via le BO */
        $shipment->setTrackData($trackData);

        $shipment->register();
        $shipment->getOrder()->setIsInProcess(true);

        $shipment->setData('create_track_to_shipment',true)->save();
        $shipment->getOrder()->save();

        if(!count($trackData)) {
            //$etiquetteUrl = $this->createTrackToShipment($shipment);
            // etiquette créé avec le plugin sur le aftersave de shipment
            $etiquetteUrl = $this->getEtiquetteUrl($shipment->getId());
        } else {
            $etiquetteUrl = false;
        }

        if(!isset($trackData['send_mail']) || (isset($trackData['send_mail']) && $trackData['send_mail'])) {
            if(isset($trackData['comment'])) {
                $shipment->addComment($trackData['comment'],true, $trackData['include_comment']);
            }
            $this->_shipmentNotifier->notify($shipment);
        }


        $shipment->setData('create_track_to_shipment',false)->save();

        return $etiquetteUrl;
    }

    /**
     * @param OrderShipment $shipment
     * @param array $trackData
     * @return bool
     */
    public function createTrackToShipment(\Magento\Sales\Model\Order\Shipment $shipment, $trackData = array()) {
        $order = $shipment->getOrder();
        $_shippingMethod = explode("_", $order->getShippingMethod());
        try {
            $expedition = false;
            if(count($trackData) > 0) {
                $trackData = array_merge($trackData,array(
                    'parent_id' => $shipment->getId(),
                    'order_id' => $order->getId()
                ));
            } else {
                $expedition = $this->_helperWebservice->createEtiquette($shipment);
                $trackData = array(
                    'track_number' => $expedition->return->skybillNumber,
                    'parent_id' => $shipment->getId(),
                    'order_id' => $order->getId(),
                    'chrono_reservation_number' => base64_encode($expedition->return->skybill),
                    'carrier' => ucwords($_shippingMethod[1]),
                    'carrier_code' => $_shippingMethod[1],
                    'title' => ucwords($_shippingMethod[1]),
                    'popup' => '1',
                );
            }

            $track = $this->_trackFactory->create();
            $track->setData($trackData)->setShipment($shipment)->save();

            return $expedition !== false ? $expedition->return->skybill : false;
        } catch(\Exception $e) {

        }
        return false;
    }

    /**
     * @param $incrementId
     * @return OrderShipment
     */
    public function getShipmentByIncrementId($incrementId) {
        $shipment = $this->_shipment->setId(null)->loadByIncrementId($incrementId);
        return $shipment;
    }

    public function getEtiquetteUrl($shipment)
    {
        if(!$shipment instanceof \Magento\Sales\Model\Order\Shipment) {
            $shipment = $this->_shipment->setData(null)->load($shipment);
        }
        $etiquetteUrl = false;

        if ($_shipTracks = $shipment->getAllTracks()) {
            foreach ($_shipTracks as $_shipTrack) {
                if ($_shipTrack->getNumber() && $_shipTrack->getChronoReservationNumber()) {
                    $etiquetteUrl = base64_decode($_shipTrack->getChronoReservationNumber());
                    break;
                }
            }
            if ($etiquetteUrl) {
                return $etiquetteUrl;
            }
        }

        /* pas de tracking chronopost */
        return $this->createTrackToShipment($shipment);
    }
}
