define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'comnpay',
                component: 'Afone_ComNpay/js/view/payment/method-renderer/comnpay-method'
            }
        );
        return Component.extend({});
    }
);