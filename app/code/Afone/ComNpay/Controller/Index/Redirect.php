<?php
namespace Afone\ComNpay\Controller\Index;

use \Magento\Framework\App\Action\Context;
use \Magento\Framework\View\Result\PageFactory;
use \Magento\Framework\App\Action\Action;
use \Magento\Framework\Registry;

class Redirect extends Action
{
  
  protected $_coreRegistry;
  protected $_pageFactory;
  
  public function __construct(
    Context $context,
    PageFactory $resultPageFactory,
    Registry $resultRegistry) 
  {
    $this->_pageFactory = $resultPageFactory;
    $this->_coreRegistry = $resultRegistry;
    parent::__construct(
        $context
    );
  }
    
  
  public function execute()
  {


    
    // Get Order by id
    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    $order = $objectManager->create('\Magento\Sales\Model\Order')->load(intval($_GET['id']));
    $order_data = $order->getData(); 
    if(!empty($order_data['customer_id'])){
      
      // Get Customer by id
      $customer = $objectManager->create('\Magento\Customer\Model\Customer')->load(intval($order_data['customer_id']));
      $customer_data = $customer->getData();


      // Get Address by id
      $address = $objectManager->create('\Magento\Customer\Model\Address')->load(intval($customer_data['default_shipping']));
      $address_data = $address->getData();
    }


    // Get helper config (Data from admin ComNpay)
    $helper = $objectManager->create('Afone\ComNpay\Helper\Data');

    // Get store config
    $store = $objectManager->get('\Magento\Framework\Locale\Resolver');
    $lang = explode("_", $store->getLocale());

    // Get BaseUrl
    $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
    $baseUrl = $storeManager->getStore()->getBaseUrl();


    // Create ComNpay inputs
    $comnpay['montant']         = number_format($order_data['grand_total'], 2 , '.', '');

    $comnpay['idTPE']           = $helper->getGeneralConfig('tpe_nb');

    $comnpay['idTransaction']   = time() . '-' . intval($_GET['id']);
    $comnpay['idCommande']      = intval($_GET['id']);
    $comnpay['devise']          = 'EUR';
    $comnpay['lang']            = $lang[0];
    $comnpay['urlRetourNOK']    = $baseUrl;
    $comnpay['nom_produit']     = $order_data['increment_id'];
    $comnpay['source']          = $baseUrl;
    $comnpay['urlIPN']          = $baseUrl . "afone_comnpay/index/ipn/";
    $comnpay['extension']       = "magento2-comnpay";
    if($_GET['pnf'] == 1){
      $comnpay['typeTr']          = "P3F";
      $comnpay['data']            = "P3F";
    }else{
      $comnpay['typeTr']          = "D";
      $comnpay['data']            = "D";
    }

    
    // Check if customer has id (to recover address) and redirect after payment in order view 
    if(!empty($order_data['customer_id'])){
      $comnpay['urlRetourOK']     = $baseUrl . "sales/order/view/order_id/" . $_GET['id'];
      $porteur = array(
        "nom"         => $order_data['customer_lastname'],
        "prenom"      => $order_data['customer_firstname'],
        "email"       => $order_data['customer_email'],
        "adresse"     => $address_data['street'],
        "codePostal"  => $address_data['postcode'],
        "ville"       => $address_data['city'],
        "telephone"   => $address_data['telephone'],
        "pays"        => $address_data['country_id'],
        "refPorteur"  => $order_data['customer_id']
      );
    }else{
      $comnpay['urlRetourOK']     = $baseUrl;
      $porteur = array(
        "nom"         => $order_data['customer_lastname'],
        "prenom"      => $order_data['customer_firstname'],
        "email"       => $order_data['customer_email']
      );
    }


    
    $comnpay['porteur']         = base64_encode(json_encode($porteur));

    $comnpay['key']           = $helper->getGeneralConfig('secret_key');


    // Encoding
    $comnpayWithKey = base64_encode(implode("|", $comnpay));
    unset($comnpay['key']);
    $comnpay['sec'] = hash("sha512",$comnpayWithKey);
    
    
    // Save data in register
    $this->_coreRegistry->register('data_comnpay', json_encode($comnpay));
    
    // Call layout.xml
    $resultPage = $this->_pageFactory->create();
    $resultPage->addHandle('comnpay_index_redirect');
    $resultPage->getConfig()->getTitle()->prepend(__('Creating payment'));
    return $resultPage;
    
  }
}