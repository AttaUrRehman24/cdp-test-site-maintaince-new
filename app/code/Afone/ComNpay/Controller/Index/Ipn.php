<?php
namespace Afone\ComNpay\Controller\Index;

use \Magento\Framework\App\Action\Context;
use \Magento\Framework\App\Action\Action;
use \Magento\Sales\Model\Order;

class Ipn extends Action
{
  
  
  public function execute()
  {
    // Get the data

 //print_r($_POST);
 //exit();
    $transactionId = $_POST["idTransaction"];
    $result = $_POST["result"];
    $amountGateway = (float)$_POST["montant"];
    $explodeTransac = explode("-", $transactionId);

    // Get order
    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

    // Get helper config (Data from admin ComNpay)
    $helper = $objectManager->create('Afone\ComNpay\Helper\Data');
    // Get secret key to verify IPN call
    $ipn_secret_key = $helper->getGeneralConfig('secret_key');



    $sec = $_POST['sec'];
    unset($_POST['sec']);
    if(strtoupper(hash("sha512", base64_encode(implode("|",$_POST)."|".$ipn_secret_key))) != strtoupper($sec))  {
      header("Status: 400 Bad Request", false, 400);
      exit();
    }



    $order = $objectManager->create('\Magento\Sales\Model\Order')->load($explodeTransac[1]);
    $order_data = $order->getData(); 
    
    if($result == 'OK'){
      $order->setState(Order::STATE_COMPLETE)->setStatus(Order::STATE_COMPLETE);
      $order->setCanSendNewEmailFlag(true);
      if($_POST['data'] == 'P3F'){
        $order->addStatusHistoryComment(__('IMPORTANT, this transaction is made with a payment in 3 installments'));
      }
      $order->save();
      $emailSender = $objectManager->create('\Magento\Sales\Model\Order\Email\Sender\OrderSender');
      $emailSender->send($order, true);
    }else{
      $order->setState(Order::STATE_CANCELED)->setStatus(Order::STATE_CANCELED);
      $order->save();
    }
    
    
    
  }

}