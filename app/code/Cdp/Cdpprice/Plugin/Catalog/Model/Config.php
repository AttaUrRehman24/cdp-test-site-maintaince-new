<?php

namespace Cdp\Cdpprice\Plugin\Catalog\Model;

class Config
{
    public function afterGetAttributeUsedForSortByArray(\Magento\Catalog\Model\Config $catalogConfig, $options ) {

    	unset($options['price']);
        $options['low_to_high'] = __('Prix croissant');
        $options['high_to_low'] = __('Prix décroissant');
        return $options;

    }

}